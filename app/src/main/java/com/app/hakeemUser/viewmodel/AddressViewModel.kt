package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.AddAddressResponse
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.ListAddressResponse
import com.app.hakeemUser.models.ProfileDetailsResponse
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.AddressRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class AddressViewModel(application: Application) : AndroidViewModel(application) {

    var repository: AddressRepository = AddressRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun getlistaddress(context : Context): LiveData<ListAddressResponse>? {
        return repository.getlistaddress(getApiParams(context,null, UrlHelper.LISTADDRESS))
    }

    fun addaddresss(
            context: Context,
            address: String,
            lat: Double?,
            lng: Double?
    ): LiveData<CommonResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.ADDRESSS, address)
        jsonObject.put(Constants.ApiKeys.LATITUDE, lat)
        jsonObject.put(Constants.ApiKeys.LONGITUDE, lng)

        return repository.addaddress(getApiParams(context,jsonObject, UrlHelper.ADDADDRESS))
    }



    fun deleteaddresss(
            context: Context,
            addressid: Int,
    ): LiveData<CommonResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.ID, addressid)

        return repository.deleteaddress(getApiParams(context,jsonObject, UrlHelper.DELETEADDRESS))
    }




}