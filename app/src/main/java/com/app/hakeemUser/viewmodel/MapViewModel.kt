package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.R
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.MapRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder

class MapViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MapRepository = MapRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


//    fun createAccount(
//        name: String,
//        emailId: String,
//        countryCode: String,
//        mobileNumber: String,
//        password: String
//    ): LiveData<RegisterResponse>? {
//
//        val jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.NAME, name)
//        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
//        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
//        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//
//        return repository.createAccount(
//            getApiParams(
//                jsonObject,
//                UrlHelper.CREATE_ACCOUNT
//            )
//        )
//    }

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        Log.d("wsygsywhagswa",""+apiInputs.headers)

        return apiInputs
    }

    fun getAddress(context: Context, input: ApiInput): LiveData<AutoCompleteResponse>? {
        return repository.getAddress(input)
    }

    fun listdoctorlive(context : Context, lat: String,
                         lng: String,
                         page: String,
    ): LiveData<LivedoctorResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("latitude", lat)
        jsonObject.put("longitude", lng)
        jsonObject.put("page", page)

        Log.d("page",""+jsonObject);

        return repository.getdoctorlive(getApiParams(context,jsonObject, UrlHelper.GETLIVEDOCTOR))
    }

    fun searchAutoComplete(
        place: String,
        lat: String,
        lng: String
    ): LiveData<AutoCompleteAddressResponseModel> {

        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.url = getAutoCompleteUrl(place, lat, lng)

        return repository.getAutoCompleteAddress(apiInputs)
    }

    fun getAutoCompleteUrl(input: String, currentLat: String?, currentLong: String?): String {
        val urlString = StringBuilder()
        urlString.append(UrlHelper.GOOGLE_API_AUTOCOMPLETE_BASE_URL)
        urlString.append("input=")
        try {
            urlString.append(URLEncoder.encode(input, "utf8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        urlString.append("&language=en")
        urlString.append("&location=$currentLat,$currentLong&radius=50")
        urlString.append("&key=").append(applicationIns.getString(R.string.map_api_key))
        return urlString.toString()
    }

    fun getPlaceDetails(placeID: String): String {
        return UrlHelper.GOOGLE_API_PLACE_DETAILS_BASE_URL + "placeid=" + placeID + "&key=" + applicationIns.getString(
            R.string.map_api_key
        )
    }

    fun getLatLngDetails(placeID: String): LiveData<GetLatLongFromIdResponseModel> {

        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.url = getPlaceDetails(placeID)

        return repository.getLatLngDetails(apiInputs)
    }

}