package com.app.hakeemUser.viewmodel

import android.app.Activity
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.paging.AllDoctorsDataSource
import com.app.hakeemUser.paging.AllDoctorsDataSourceFactory
import com.app.hakeemUser.paging.AvailableDoctorsDataSource
import com.app.hakeemUser.paging.AvailableDoctorsDataSourceFactory
import com.app.hakeemUser.repository.AvailableDoctorRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class AvailableDoctorViewModel(application: Application) : AndroidViewModel(application) {

    var repository: AvailableDoctorRepository = AvailableDoctorRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)

    var itemPagedList: LiveData<PagedList<DoctorData>> = MutableLiveData()
    var liveDataSource: LiveData<PageKeyedDataSource<Int, DoctorData>> = MutableLiveData()

    var allItemPagedList: LiveData<PagedList<DoctorData>> = MutableLiveData()
    var allLiveDataSource: LiveData<PageKeyedDataSource<Int, DoctorData>> = MutableLiveData()

    private fun getApiParams(jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

//    fun getAvailableDoctors(): LiveData<ConsultationResponse>? {
//         return   repository.getAvailableDoctors(getApiParams(null, UrlHelper.CONSULTATION_HISTORY))
//    }


    fun configDataSource(activity: Activity?, methodName: String?) {

        val tripEatooDataSourceFactory = AvailableDoctorsDataSourceFactory()
        tripEatooDataSourceFactory.setInputs(activity, methodName)
        liveDataSource = tripEatooDataSourceFactory.getItemLiveDataSource()
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(AvailableDoctorsDataSource.TOTAL_PAGENUMBER).build()
        itemPagedList =
            LivePagedListBuilder<Int, DoctorData>(
                tripEatooDataSourceFactory,
                pagedListConfig
            ).build()
    }

    fun configDataSourceAllDoctor(activity: Activity?) {

        AllDoctorsDataSource.PAGENUMBER = 1

        val dataSourceFactory = AllDoctorsDataSourceFactory()
        dataSourceFactory.setInputs(activity)
        allLiveDataSource = dataSourceFactory.getItemLiveDataSource()
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(AllDoctorsDataSource.TOTAL_PAGENUMBER).build()
        allItemPagedList =
            LivePagedListBuilder<Int, DoctorData>(
                dataSourceFactory,
                pagedListConfig
            ).build()
    }


}