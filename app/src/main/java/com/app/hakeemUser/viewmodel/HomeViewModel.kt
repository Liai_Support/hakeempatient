package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.AppSettingsResponse
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.HomeDashBoardResponse
import com.app.hakeemUser.models.PaymentGatewayResponse
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.HomeRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    var repository: HomeRepository = HomeRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


//    fun createAccount(
//        name: String,
//        emailId: String,
//        countryCode: String,
//        mobileNumber: String,
//        password: String
//    ): LiveData<RegisterResponse>? {
//
//        val jsonObject = JSONObject()
//        jsonObject.put(Constants.ApiKeys.NAME, name)
//        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
//        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
//        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
//        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
//
//        return repository.createAccount(
//            getApiParams(
//                jsonObject,
//                UrlHelper.CREATE_ACCOUNT
//            )
//        )
//    }

    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }



    fun updateDeviceToken(context: Context) {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.OS, Constants.ApiKeys.ANDROID)
        jsonObject.put(Constants.ApiKeys.FCMTOKEN, sharedHelper?.fcmToken)

        repository.updateDeviceToken(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATE_DEVICE_TOKEN
            )
        )

    }



    fun getHomeBoardDetails(context: Context): LiveData<HomeDashBoardResponse>? {
        return repository.getHomeDetails(getApiParams(context, null, UrlHelper.HOMEDASHBOARD))

    }

    fun getAppSettings(context: Context): LiveData<AppSettingsResponse>? {

        return repository.getAppSettings(
            getApiParams(
                context,
                null,
                UrlHelper.APPSETTINGS
            )
        )
    }

    fun emergencybooking(
        context: Context,
        providerId: Int,
        specialityId: Int,
        date: String,
        time: String,
        period: String,
        location: String): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PROVIDERID, providerId)
        jsonObject.put(Constants.ApiKeys.SPECIALITYID, specialityId)
        jsonObject.put(Constants.ApiKeys.BOOKINGDATE, date)
        jsonObject.put(Constants.ApiKeys.BOOKINGTIME, time)
        jsonObject.put(Constants.ApiKeys.BOOKINGPERIOD, period)
        jsonObject.put(Constants.ApiKeys.LOCATION, location)

       return repository.emergencybooking(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.NEWBOOKING
            )
        )

    }


}