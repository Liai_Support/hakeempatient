package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.repository.AmazonRepository
import com.app.hakeemUser.utils.SharedHelper
import java.io.File

class AmazonViewModel(application: Application) : AndroidViewModel(application) {

    var repository: AmazonRepository = AmazonRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    fun uploadImage(
        context: Context,
        file: File
    ): LiveData<CommonResponse>? {
        return repository.uploadImageToAWS(context, file)
    }


}