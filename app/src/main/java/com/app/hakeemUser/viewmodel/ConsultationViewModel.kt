package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.ConsultationRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class ConsultationViewModel(application: Application) : AndroidViewModel(application) {

    var repository: ConsultationRepository = ConsultationRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language}
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun getConsultation(context: Context, type: Int): LiveData<ConsultationResponse>? {
        return if (type == 1)
            repository.getConsultation(getApiParams(context, null, UrlHelper.RECENT_CONSULTATION))
        else
            repository.getConsultation(getApiParams(context, null, UrlHelper.CONSULTATION_HISTORY))
    }

    fun getInsuranceStatus(context: Context,id:Int): LiveData<GetInsuranceStatusResponse> {
        val jsonObject = JSONObject()
        jsonObject.put("bookingId", id)

        return repository.getInsuranceStatus(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.GETINSURANCESATUS
            )
        )
    }

    fun viewlabreport(context: Context,id:Int): LiveData<ViewLabReportResponse> {
        val jsonObject = JSONObject()
        jsonObject.put("bookingId", id)

        return repository.viewlabreport(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VIEWLABREPORT
            )
        )
    }

    fun getNotificationData(context: Context): LiveData<NotificationResponse> {
        return repository.getNotificationData(
            getApiParams(
                context,
                null,
                UrlHelper.GETMYNOTIFICATION
            )
        )
    }


    fun getcheckversion(context: Context): LiveData<VersionCheckResponse> {
        return repository.getcheckversion(
            getApiParams(
                context,
                null,
                UrlHelper.VERSION_CHECK
            )
        )
    }


    fun clearNotification(context: Context): LiveData<CommonResponse>? {
        return repository.clearNotification(
            getApiParams(
                context,
                null,
                UrlHelper.DELETEMYNOTIFICATION
            )
        )
    }


}