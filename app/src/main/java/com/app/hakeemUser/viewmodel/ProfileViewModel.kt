package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.ProfileDetailsResponse
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.ProfileRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    var repository: ProfileRepository = ProfileRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun getProfileDetails(context : Context): LiveData<ProfileDetailsResponse>? {
        return repository.getProfileDetails(getApiParams(context,null, UrlHelper.GET_PROFILE))
    }

    fun updateProfile(
        context: Context,
        name: String,
        email: String,
        cCode: String,
        mobile: String,
        gender: String,
        dob: String,
        bloodGroup: String,
        height: String,
        weight: String,
        classid: String?,
        insuranceid: String,
        icnameid: String?,
        status: String?,
        sid: String?,
        profilePic: String?,
    age: Int?
    ): LiveData<CommonResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, cCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobile)
        jsonObject.put(Constants.ApiKeys.GENDER, gender)
        jsonObject.put(Constants.ApiKeys.DOB, dob)
        jsonObject.put(Constants.ApiKeys.BLOOD_GROUP, bloodGroup)
        jsonObject.put(Constants.ApiKeys.HEIGHT, height)
        jsonObject.put(Constants.ApiKeys.WEIGHT, weight)
        jsonObject.put(Constants.ApiKeys.SAUDIID,sid)
        jsonObject.put(Constants.ApiKeys.ADMINAPPROVE,status)
        jsonObject.put("age",age)
        profilePic?.let { jsonObject.put(Constants.ApiKeys.PROFILEPIC, it) }
        jsonObject.put(Constants.ApiKeys.INSURENCEID,insuranceid)
        if(classid.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, classid)
        }
        if(icnameid.equals("0")){
            jsonObject.put("ins_name", "")
        }
        else{
            jsonObject.put("ins_name", icnameid)
        }
        Log.d("cdsxscsdxvs",""+jsonObject);


        return repository.updateProfile(getApiParams(context,jsonObject, UrlHelper.UPDATE_PROFILE))
    }


}