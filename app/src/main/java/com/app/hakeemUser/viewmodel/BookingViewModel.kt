package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.RouteGenerate.Route
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.BookingRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.ScheduledBookingSingleton
import com.app.hakeemUser.utils.SharedHelper
import com.google.android.gms.maps.model.LatLng
import org.json.JSONException
import org.json.JSONObject

class BookingViewModel(application: Application) : AndroidViewModel(application) {

    var repository: BookingRepository = BookingRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language}
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun newvirtualfee(context: Context): LiveData<NewVirutualFeeResponse>? {
        var jsonObject = JSONObject()
        jsonObject.put(
            "memberId",
            ScheduledBookingSingleton.getInstance().memberid
        )
        jsonObject.put(
            "doctor_id",
            ScheduledBookingSingleton.getInstance().providerId
        )

        Log.d("cxbvh", "" + jsonObject);

        return repository.Newvirtualfee(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.NEWVIRTUALFEE
            )
        )
    }

    private fun getApiParamsgate(
        context: Context,
        jsonObject: JSONObject?,
        methodName: String
    ): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        // sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
       // header.put("Authorization", BuildConfig.PAYMENT_KEY);
        header["Authorization"] = UrlHelper.PAYMENT_KEY
        //header["Content-Type"] = "application/json; charset=utf-8"
        //header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName
        Log.d("paymentgatewayHeaders:", "" + header)

        Log.d("returnstaement","apiInputs"+apiInputs)
        return apiInputs

        Log.d("paymentgatewayHeaders:", "" + header)

        Log.d("returnstaement","apiInputs"+apiInputs)
    }


    fun   bookNewConsultation(context: Context): LiveData<BookConsultationResponse>? {


        var jsonObject = JSONObject()
        jsonObject.put(
            Constants.ApiKeys.PROVIDERID,
            ScheduledBookingSingleton.getInstance().providerId
        )
        jsonObject.put(
            Constants.ApiKeys.SPECIALITYID,
            ScheduledBookingSingleton.getInstance().specialityId
        )

        jsonObject.put(Constants.ApiKeys.LOCATION, ScheduledBookingSingleton.getInstance().location)
        jsonObject.put(
            Constants.ApiKeys.PATIENTTYPE,
            ScheduledBookingSingleton.getInstance().patientType
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTNAME,
            ScheduledBookingSingleton.getInstance().patientName
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTAGE,
            ScheduledBookingSingleton.getInstance().patientAge
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTGENDER,
            ScheduledBookingSingleton.getInstance().patientGender
        )
        jsonObject.put(
            Constants.ApiKeys.PREVIOUSISSUE,
            ScheduledBookingSingleton.getInstance().previousIssue
        )
        jsonObject.put(
            "description",
            ScheduledBookingSingleton.getInstance().previousIssue
        )

        jsonObject.put(
            Constants.ApiKeys.BOOKING_LATITUDE,
            ScheduledBookingSingleton.getInstance().latitude
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKING_LONGITUDE,
            ScheduledBookingSingleton.getInstance().longitude
        )

        jsonObject.put(
            Constants.ApiKeys.ISVIRTUALBOOKING,
            ScheduledBookingSingleton.getInstance().isVirtualBooking
        )

        jsonObject.put(
            Constants.ApiKeys.BOOKINGDATE,
            ScheduledBookingSingleton.getInstance().bookingDate
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKINGTIME,
            ScheduledBookingSingleton.getInstance().bookingTime
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKINGPERIOD,
            ScheduledBookingSingleton.getInstance().bookingPeriod
        )
        jsonObject.put(
            "bookingAddress",
            ScheduledBookingSingleton.getInstance().location
        )
        jsonObject.put(
            "memberId",
            ScheduledBookingSingleton.getInstance().memberid
        )
        jsonObject.put(
            "transactionId",
            ScheduledBookingSingleton.getInstance().transactionId
        )
        jsonObject.put(
            "order_id",
            ScheduledBookingSingleton.getInstance().torderId
        )
        jsonObject.put(
                "paymentMethod",
        ScheduledBookingSingleton.getInstance().paymentMethod
        )
        jsonObject.put(
            "fee",
            ScheduledBookingSingleton.getInstance().dfee
        )

        jsonObject.put("is_ins",ScheduledBookingSingleton.getInstance().chooseoption)



        if(ScheduledBookingSingleton.getInstance().isElderlyFlow == true){
            jsonObject.put(
                "type",
                "home_health_care"
            )
        }
        else{
            jsonObject.put(
                "type",
                "home_visit"
            )
        }




        Log.d("cxbvh", "" + jsonObject);

        return repository.bookNewConsultation(
            getApiParams(
                context,
                jsonObject,
                if (ScheduledBookingSingleton.getInstance().isElderlyFlow == true) {
                    UrlHelper.NEWBOOKING
                } else {
                    UrlHelper.FEATUREBOOKING
                }
            )
        )
    }

    fun bookVirtualConsultation(
        context: Context,
        bookingType: Int
    ): LiveData<BookConsultationResponse>? {


        var jsonObject = JSONObject()
        jsonObject.put(
            Constants.ApiKeys.PROVIDERID,
            ScheduledBookingSingleton.getInstance().providerId
        )
        jsonObject.put(
            Constants.ApiKeys.SPECIALITYID,
            ScheduledBookingSingleton.getInstance().specialityId
        )

        jsonObject.put(Constants.ApiKeys.LOCATION, ScheduledBookingSingleton.getInstance().location)
        jsonObject.put(
            Constants.ApiKeys.PATIENTTYPE,
            ScheduledBookingSingleton.getInstance().patientType
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTNAME,
            ScheduledBookingSingleton.getInstance().patientName
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTAGE,
            ScheduledBookingSingleton.getInstance().patientAge
        )
        jsonObject.put(
            Constants.ApiKeys.PATIENTGENDER,
            ScheduledBookingSingleton.getInstance().patientGender
        )
        jsonObject.put(
            Constants.ApiKeys.PREVIOUSISSUE,
            ScheduledBookingSingleton.getInstance().previousIssue
        )
        jsonObject.put(
            "description",
            ScheduledBookingSingleton.getInstance().previousIssue
        )

        jsonObject.put(
            Constants.ApiKeys.BOOKING_LATITUDE,
            ScheduledBookingSingleton.getInstance().latitude
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKING_LONGITUDE,
            ScheduledBookingSingleton.getInstance().longitude
        )

        jsonObject.put(
            Constants.ApiKeys.ISVIRTUALBOOKING,
            "1"
        )

        jsonObject.put(
            Constants.ApiKeys.BOOKINGDATE,
            // "2021/02/09 05:00"
            ScheduledBookingSingleton.getInstance().bookingDate
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKINGTIME,
            // "05:00"
            ScheduledBookingSingleton.getInstance().bookingTime
        )
        jsonObject.put(
            Constants.ApiKeys.BOOKINGPERIOD,
            //"PM"
            ScheduledBookingSingleton.getInstance().bookingPeriod
        )

        jsonObject.put(
            Constants.ApiKeys.TRANSACTIONID,
            ScheduledBookingSingleton.getInstance().transactionId
        )
        jsonObject.put(
            "order_id",
            ScheduledBookingSingleton.getInstance().torderId
        )
        jsonObject.put(
            "fee",
            ScheduledBookingSingleton.getInstance().vfee
        )
        jsonObject.put(
            "memberId",
            ScheduledBookingSingleton.getInstance().memberid
        )
        jsonObject.put(
            "paymentMethod",
            ScheduledBookingSingleton.getInstance().paymentMethod
        )

        jsonObject.put("is_ins",ScheduledBookingSingleton.getInstance().chooseoption)

        if(ScheduledBookingSingleton.getInstance().isVirtualBooking == 1){
            jsonObject.put(
                "type",
                "virtual"
            )
        }
        else{
            jsonObject.put(
                "type",
                "global"
            )
        }


        Log.d("cxbvh", "" + jsonObject);


        when (bookingType) {
            1 -> {
                jsonObject.put(
                    Constants.ApiKeys.STREAM,
                    0
                )
                jsonObject.put(
                    Constants.ApiKeys.CHAT,
                    1
                )
            }
            2 -> {
                jsonObject.put(
                    Constants.ApiKeys.STREAM,
                    0
                )
                jsonObject.put(
                    Constants.ApiKeys.CHAT,
                    0
                )
            }
            3 -> {
                jsonObject.put(
                    Constants.ApiKeys.STREAM,
                    1
                )
                jsonObject.put(
                    Constants.ApiKeys.CHAT,
                    0
                )
            }
        }

        return repository.bookVirtualConsultation(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.VIRTUALBOOKING
            )
        )
    }


    fun updateInsBooking(
        context: Context,
        bookingId: Int
    ): LiveData<CommonResponse>? {


        var jsonObject = JSONObject()
        jsonObject.put(
            "transactionId",
            ScheduledBookingSingleton.getInstance().transactionId
        )
        jsonObject.put(
            "order_id",
            ScheduledBookingSingleton.getInstance().torderId
        )
        jsonObject.put(
            "bookingId",
            bookingId
        )

        jsonObject.put(
            "paymentMethod",
            ScheduledBookingSingleton.getInstance().paymentMethod
        )

        return repository.updateInsBooking(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.UPDATEINSBOOKING
            )
        )
    }

    fun getDetails(
        context: Context,
        bookingID: Int,
        providerId: Int
    ): LiveData<BookingDetailResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PROVIDERID, providerId)
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingID)

        return repository.getBookingDetails(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.TRACKPROVIDER
            )
        )
    }

    fun getRoute(context: Context, source: LatLng, destination: LatLng): LiveData<Route>? {
        return repository.getRouteDetails(context, source, destination)
    }

    fun cancelBooking(context: Context, bookingId: Int): LiveData<CommonResponse>? {
        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        return repository.cancelBooking(getApiParams(context, jsonObject, UrlHelper.CANCELBOOKING))
    }

    fun payAmount(
        context: Context,
        paymentMode: String,
        bookingId: Int?
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.METHOD, paymentMode)
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)
        return repository.payAmount(getApiParams(context, jsonObject, UrlHelper.PAY))

    }


    fun payAmount(
        context: Context,
        paymentMode: String,
        bookingId: Int?,
        tnxId: String?
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.METHOD, paymentMode)
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)
        jsonObject.put(Constants.ApiKeys.TRANSACTIONID, tnxId)
        return repository.payAmount(getApiParams(context, jsonObject, UrlHelper.PAY))

    }


    fun rateConsultation(
        context: Context,
        rating: Float?,
        comment: String,
        bookingId: Int?
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.REVIEW, comment)
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)
        jsonObject.put(Constants.ApiKeys.RATING, rating)
        return repository.rateConsultation(getApiParams(context, jsonObject, UrlHelper.POSTREVIEW))
    }

    fun getActiveBooking(context: Context, date: String): LiveData<ActivityBookingResponse> {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.DATE, date)

        return repository.getActiveBooking(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.GETACTIVEBOOKINGS
            )
        )

    }

    fun addAmountToWallet(context: Context, id: Any, amount: Double): LiveData<CommonResponse> {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.AMOUNT, amount)
        jsonObject.put(Constants.ApiKeys.TRANSACTIONID, id)

        return repository.addAmountToWallet(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.ADDMONEYWALLET
            )
        )
    }

    fun getWalletDetails(context: Context): LiveData<WalletTransactionResponse> {
        return repository.getWalletDetails(
            getApiParams(
                context,
                null,
                UrlHelper.WALLETHISTORY
            )
        )

    }



    fun payUsingWallet(context: Context, amount: String): LiveData<PayUsingWalletResponse> {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.AMOUNT, amount)

        return repository.payUsingWallet(
            getApiParams(
                context,
                jsonObject,
                UrlHelper.PAYFROMWALLET
            )
        )
    }

    fun payment(context: Context, amount: Double): LiveData<PaymentGatewayResponse>?  {

        var jsonObject = JSONObject()

        jsonObject.put("apiOperation", "INITIATE")

        var jsonObject1 = JSONObject()
        jsonObject1.put("reference", "Ref#1234")
        jsonObject1.put("amount", amount)
        jsonObject1.put("currency", "SAR")
        jsonObject1.put("name", "Sample order name")
        jsonObject1.put("channel", "Mobile")
        jsonObject1.put("category", "PAY")

        jsonObject.put("order", jsonObject1)

        var jsonObject2 = JSONObject()
        jsonObject2.put("tokenizeCc", "true")
        jsonObject2.put("returnUrl", "https://hakeem.com.sa/")
        jsonObject2.put("locale",  sharedHelper?.language)

        jsonObject.put("configuration", jsonObject2)

        return repository.payment(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }

    fun payment1(context: Context, orderid: String): LiveData<PaymentGatewayResponse>?  {

        return repository.payment1(
            getApiParamsgate(
                context,
                null,
                UrlHelper.PAYMENT_URL+"/"+orderid
            )
        )

    }

    fun payment2(context: Context, orderid: String): LiveData<PaymentGatewayResponse1>?  {

        val jsonObject = JSONObject()
        val order = JSONObject()
        try {
            jsonObject.put("apiOperation", "SALE")
            order.put("id", orderid)
            jsonObject.put("order", order)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return repository.payment2(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }

    fun refundpayment(context: Context, amount: Double,orderid: String): LiveData<PaymentRefundResponse>?  {

        var jsonObject = JSONObject()
        jsonObject.put("apiOperation", "REFUND")

        var jsonObject1 = JSONObject()
        jsonObject1.put("Id", orderid)

        jsonObject.put("order", jsonObject1)

        var jsonObject2 = JSONObject()
        jsonObject2.put("amount", amount.toString())
        jsonObject2.put("currency", "SAR")

        jsonObject.put("transaction", jsonObject2)

        return repository.refundpayment(
            getApiParamsgate(
                context,
                jsonObject,
                UrlHelper.PAYMENT_URL
            )
        )

    }



//    fun payConsultation(context : Context, paymentType: String): LiveData<CommonResponse>? {
//
//
//
//        return repository.cancelBooking(getApiParams(context,jsonObject, UrlHelper.CANCELBOOKING))
//    }


}