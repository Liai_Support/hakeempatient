package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.RegisterRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    var repository: RegisterRepository = RegisterRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    fun getclassdetails(context : Context,id : String): LiveData<ClassdataResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("ins_com_id", id)
        return repository.getClassdata(getApiParams1(context,jsonObject, UrlHelper.GET_CLASS))
    }

    fun geticnamedetails(context : Context): LiveData<IcnamedataResponse>? {
        return repository.geticnamedata(getApiParams1(context,null, UrlHelper.GET_ICNAME))
    }



    private fun getApiParams1(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }


    fun createAccount(
        name: String,
        emailId: String,
        countryCode: String,
        mobileNumber: String,
        password: String,
        insurenceid: String,
        classtype: String,
        icnameid: String,
        sid: String,
        pic: String
    ): LiveData<RegisterResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.EMAIL, emailId)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, countryCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, mobileNumber)
        jsonObject.put(Constants.ApiKeys.SAUDIID, sid)
        jsonObject.put(Constants.ApiKeys.INSURENCEID, insurenceid)
        if(classtype.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, classtype)
        }
        if(icnameid.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, icnameid)
        }
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)
        jsonObject.put(Constants.ApiKeys.PROFILEPIC, pic)

        return repository.createAccount(
            getApiParams(
                jsonObject,
                UrlHelper.CREATE_ACCOUNT
            )
        )
    }

    private fun getApiParams(jsonObject: JSONObject, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT


        val apiInputs = ApiInput()
        apiInputs.context = applicationIns.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun signin(email: String, password: String): LiveData<LoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.signIn(
            getApiParams(
                jsonObject,
                UrlHelper.LOGIN
            )
        )
    }

    fun getOtp(email: String): LiveData<GetOtpResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)

        return repository.getOtp(
            getApiParams(
                jsonObject,
                UrlHelper.GET_OTP
            )
        )
    }

    fun verifyOtp(email: String, otp: String): LiveData<CheckOtpResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.OTP, otp)

        return repository.checkOtp(
            getApiParams(
                jsonObject,
                UrlHelper.CHECK_OTP
            )
        )

    }

    fun resetPassword(password: String): LiveData<CommonResponse> {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PASSWORD, password)

        return repository.resetPassword(
            getApiParams(
                jsonObject,
                UrlHelper.ENTER_NEW_PASSWORD
            )
        )

    }

    fun socialLogin(id: String?, type: String): LiveData<SocialLoginResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.SOCIALTOKEN, id)
        jsonObject.put(Constants.ApiKeys.TYPE, type)

        return repository.socialLogin(
            getApiParams(
                jsonObject,
                UrlHelper.CHECK_SOCIAL_LOGIN
            )
        )
    }

    fun socialRegister(
        email: String,
        imageurl: String,
        name: String,
        socialToken: String,
        selectedCountryCode: String?,
        phoneNumber: String
        , socialType: String,
    insurence: String,
    insurence_clase: String,
    icname_id: String,
    sid: String
    ): LiveData<SocialRegisterResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.EMAIL, email)
        jsonObject.put(Constants.ApiKeys.PROFILEPIC, imageurl)
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put(Constants.ApiKeys.SOCIALTOKEN, socialToken)
        jsonObject.put(Constants.ApiKeys.COUNTRYCODE, selectedCountryCode)
        jsonObject.put(Constants.ApiKeys.MOBILENUMBER, phoneNumber)
        jsonObject.put(Constants.ApiKeys.SAUDIID, sid)
        jsonObject.put(Constants.ApiKeys.INSURENCEID, insurence)
        if(insurence_clase.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, insurence_clase)
        }
        if(icname_id.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, icname_id)
        }



        return if (socialType == Constants.SocialLogin.GOOGLE)
            repository.socilaRegisterDetails(
                getApiParams(
                    jsonObject,
                    UrlHelper.GOOGLE_LOGIN
                )
            )
        else
            repository.socilaRegisterDetails(
                getApiParams(
                    jsonObject,
                    UrlHelper.FACEBOOKLOGIN
                )
            )

    }


}