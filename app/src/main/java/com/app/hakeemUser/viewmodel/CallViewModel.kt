package com.app.hakeemUser.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemUser.models.ChatResponse
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.CreateCallResponseModel
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.CallRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONObject

class CallViewModel(application: Application) : AndroidViewModel(application) {

    var sharedHelper: SharedHelper? = null
    var callRepository: CallRepository? = null
    var applicationInstance: Application? = null

    init {
        sharedHelper = SharedHelper(application)
        callRepository = CallRepository.getInstance()
        applicationInstance = application
    }


    private fun getApiParams(jsonObject: JSONObject?, url: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT

        val apiInputs = ApiInput()
        apiInputs.context = applicationInstance?.applicationContext
        apiInputs.jsonObject = jsonObject
        apiInputs.url = url
        apiInputs.headers = header

        return apiInputs
    }

    fun createCall(
        channelName: String,
        to: String,
        callType: String,
        from: String
    ): LiveData<CreateCallResponseModel>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.TO, to)
        jsonObject.put(Constants.ApiKeys.CALLTYPE, callType)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        return callRepository?.createCall(getApiParams(jsonObject, UrlHelper.CREATECALL))
    }

    fun acceptCall(channelName: String, from: String, id: String): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        jsonObject.put(Constants.ApiKeys.TO, id)
        return callRepository?.acceptCall(getApiParams(jsonObject, UrlHelper.ACCEPTCALL))
    }

    fun endCall(
        channelName: String,
        from: String,
        to: String,
        callType: String
    ): LiveData<CommonResponse>? {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.CHANNELNAME, channelName)
        jsonObject.put(Constants.ApiKeys.FROM, from)
        jsonObject.put(Constants.ApiKeys.CALLTYPE, callType)
        jsonObject.put(Constants.ApiKeys.TO, to)
        return callRepository?.endCall(getApiParams(jsonObject, UrlHelper.ENDCALL))
    }


    fun completeBooking(bookingId: String) {

        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.BOOKINGID, bookingId)

        callRepository?.completeBooking(getApiParams(jsonObject, UrlHelper.COMPLETEVIRTUALBOOKIG))
    }

    fun getChatDetails(reciverID: Int,bookingId: String): LiveData<ChatResponse>? {

        var jsonObject = JSONObject()

        jsonObject.put(Constants.ApiKeys.RECIVERID, reciverID)
        jsonObject.put("page", 1)
        jsonObject.put("bookingID",bookingId )

        Log.d("swertyui",""+jsonObject)

        return callRepository?.getChatDetails(getApiParams(jsonObject, UrlHelper.CHATDETAILS))
    }

}