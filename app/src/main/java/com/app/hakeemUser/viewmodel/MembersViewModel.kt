package com.app.hakeemUser.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.app.hakeemDoctor.models.GetLabListResponseModel
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.MembersRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

class MembersViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MembersRepository = MembersRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)


    private fun getApiParams(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT
        //header["Content-Type"] = "application/x-www-form-urlencoded"


        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        return apiInputs
    }

    fun addmemberDetails(context : Context, ptype: String,
                          name: String,
                          dob: String,
                          bloodgroup: String,
                          insurance: String,
                          gender: String,
                          age: Int,
                         insurence_clase: String,
                         icname_id: String,
                         mail: String,
                         mobile: String,
                         sauthiid: String
    ): LiveData<AddMembersDetailsResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put("type", ptype)
        jsonObject.put("gender", gender)
        jsonObject.put("DOB", dob)
        jsonObject.put("bloodGroup", bloodgroup)
        jsonObject.put("mobile", mobile)
        jsonObject.put("insurence_id", insurance)
        jsonObject.put("age",age)
        jsonObject.put("email",mail)
        jsonObject.put(Constants.ApiKeys.SAUDIID,sauthiid)
        if(insurence_clase.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, insurence_clase)
        }
        if(icname_id.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, icname_id)
        }

        return repository.addmemberDetails(getApiParams(context,jsonObject, UrlHelper.ADDMEMBER))
    }

    fun updatememberDetails(context : Context,
                            memberId: String,
                            ptype: String,
                         name: String,
                         dob: String,
                         bloodgroup: String,
                         insurance: String,
                         gender: String,
                         age: Int,
                         insurence_clase: String,
                         icname_id: String,
                         mail: String,
                         mobile: String,
                            sauthiid: String
    ): LiveData<CommonResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("memberId", memberId)
        jsonObject.put(Constants.ApiKeys.NAME, name)
        jsonObject.put("type", ptype)
        jsonObject.put("gender", gender)
        jsonObject.put("DOB", dob)
        jsonObject.put("bloodGroup", bloodgroup)
        jsonObject.put("mobile", mobile)
        jsonObject.put("insurence_id", insurance)
        jsonObject.put("age",age)
        jsonObject.put("email",mail)
        jsonObject.put(Constants.ApiKeys.SAUDIID,sauthiid)
        if(insurence_clase.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECLASSID, insurence_clase)
        }
        if(icname_id.equals("0")){
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, "")
        }
        else{
            jsonObject.put(Constants.ApiKeys.INSURENCECOMPANYID, icname_id)
        }
        return repository.updatememberDetails(getApiParams(context,jsonObject, UrlHelper.UPDATEMEMBER))
    }

    fun getmemberDetails(context : Context): LiveData<GetMemberDetailsResponse>? {

        return repository.getmemberDetails(getApiParams(context,null, UrlHelper.GETMEMBER))
    }

    fun deletemember(context : Context,id : Int?): LiveData<CommonResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("memberId", id)
        return repository.deletemember(getApiParams(context,jsonObject, UrlHelper.DELETEMEMBER))
    }



    fun gettestlistDetails(context : Context): LiveData<GetTestListResponse>? {

        return repository.gettestlistDetails(getApiParams(context,null, UrlHelper.GETTESTLIST))
    }


    fun getlablist(context: Context, idarray: JSONArray,
    ): LiveData<GetLabListResponseModel>? {

        var ha:String= ""
        var array = JSONArray()
        for(i in 0 until idarray.length()){
            array.put(idarray[i])
            var h = i+1
            if(h == idarray.length()){
                ha = ha+idarray[i]
            }
            else{
                ha = ha+idarray[i]+","
            }
        }

        Log.d("gyvgvgg",""+ha)

        val jsonObject = JSONObject()
        jsonObject.put("lat", sharedHelper?.currentLat)
        jsonObject.put("lan", sharedHelper?.currentLng)
        jsonObject.put("test_ids", idarray)
        Log.d("khjkjkjk",""+jsonObject);


        return repository.getlablistrepo(getApiParams(context,jsonObject, UrlHelper.GETLABLIST))
    }


    fun getpharmachylistDetails(context : Context): LiveData<GetPharmachyListResponse>? {
        val jsonObject = JSONObject()
        jsonObject.put("lat", sharedHelper?.currentLat)
        jsonObject.put("lan", sharedHelper?.currentLng)
      //  jsonObject.put("lat", "13.050000")
       // jsonObject.put("lan", "80.282402")
        Log.d("dshcsd",""+jsonObject)
        return repository.getpharmacylistDetails(getApiParams(context,jsonObject, UrlHelper.GETPHARMACYLIST))
    }




}