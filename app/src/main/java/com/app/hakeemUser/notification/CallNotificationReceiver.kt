package com.app.hakeemUser.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.app.hakeemUser.rxbus.RxBusNotification
import com.app.hakeemUser.utils.Constants

class CallNotificationReceiver : BroadcastReceiver() {

    override fun onReceive(p0: Context?, p1: Intent?) {
        getIntentAction(p1)
    }

    private fun getIntentAction(p1: Intent?) {
        p1?.action?.let {
            when (it) {

                Constants.NotificationActions.ACCEPT_CALL -> {
                    RxBusNotification.send(Constants.EventBusKeys.ACCEPT_CALL)
                }
                Constants.NotificationActions.REJECT_CALL -> {
                    RxBusNotification.send(Constants.EventBusKeys.REJECT_CALL)
                }
                Constants.NotificationActions.END_CALL -> {
                    RxBusNotification.send(Constants.EventBusKeys.END_CALL)
                }
                Constants.NotificationActions.HANGUP_CALL -> {
                    RxBusNotification.send(Constants.EventBusKeys.HANGUP_CALL)
                }

            }
        }
    }
}