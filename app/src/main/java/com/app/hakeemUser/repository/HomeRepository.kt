package com.app.hakeemUser.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.AppSettingsResponse
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.HomeDashBoardResponse
import com.app.hakeemUser.models.PaymentGatewayResponse
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class HomeRepository private constructor() {

    companion object {
        private var repository: HomeRepository? = null

        fun getInstance(): HomeRepository {
            if (repository == null) {
                repository = HomeRepository()
            }
            return repository as HomeRepository
        }
    }


    fun getHomeDetails(input: ApiInput): LiveData<HomeDashBoardResponse>? {

        val apiResponse: MutableLiveData<HomeDashBoardResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: HomeDashBoardResponse =
                    gson.fromJson(jsonObject.toString(), HomeDashBoardResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = HomeDashBoardResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateDeviceToken(apiParams: ApiInput) {

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
            }
            override fun setErrorResponse(error: String) {
            }
        })

    }





    fun getAppSettings(input: ApiInput): LiveData<AppSettingsResponse>? {


        val apiResponse: MutableLiveData<AppSettingsResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AppSettingsResponse =
                    gson.fromJson(jsonObject.toString(), AppSettingsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AppSettingsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun emergencybooking(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}