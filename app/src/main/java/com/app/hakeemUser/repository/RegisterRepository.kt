package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class RegisterRepository private constructor() {

    companion object {
        private var registerRepository: RegisterRepository? = null

        fun getInstance(): RegisterRepository {
            if (registerRepository == null) {
                registerRepository = RegisterRepository()
            }
            return registerRepository as RegisterRepository
        }
    }

    fun getClassdata(input: ApiInput): LiveData<ClassdataResponse>? {

        val apiResponse: MutableLiveData<ClassdataResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ClassdataResponse =
                    gson.fromJson(jsonObject.toString(), ClassdataResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ClassdataResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun geticnamedata(input: ApiInput): LiveData<IcnamedataResponse>? {

        val apiResponse: MutableLiveData<IcnamedataResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: IcnamedataResponse =
                    gson.fromJson(jsonObject.toString(), IcnamedataResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = IcnamedataResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun createAccount(input: ApiInput): LiveData<RegisterResponse>? {

        val apiResponse: MutableLiveData<RegisterResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: RegisterResponse =
                    gson.fromJson(jsonObject.toString(), RegisterResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = RegisterResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun signIn(input: ApiInput): LiveData<LoginResponse>? {

        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: LoginResponse =
                    gson.fromJson(jsonObject.toString(), LoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = LoginResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getOtp(apiParams: ApiInput): LiveData<GetOtpResponse>? {

        val apiResponse: MutableLiveData<GetOtpResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetOtpResponse =
                    gson.fromJson(jsonObject.toString(), GetOtpResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetOtpResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun checkOtp(apiParams: ApiInput): LiveData<CheckOtpResponse>? {
        val apiResponse: MutableLiveData<CheckOtpResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CheckOtpResponse =
                    gson.fromJson(jsonObject.toString(), CheckOtpResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CheckOtpResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun resetPassword(apiParams: ApiInput): LiveData<CommonResponse> {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun socialLogin(apiParams: ApiInput): LiveData<SocialLoginResponse>? {

        val apiResponse: MutableLiveData<SocialLoginResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: SocialLoginResponse =
                    gson.fromJson(jsonObject.toString(), SocialLoginResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = SocialLoginResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun socilaRegisterDetails(apiParams: ApiInput): LiveData<SocialRegisterResponse>? {
        val apiResponse: MutableLiveData<SocialRegisterResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: SocialRegisterResponse =
                    gson.fromJson(jsonObject.toString(), SocialRegisterResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = SocialRegisterResponse()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}