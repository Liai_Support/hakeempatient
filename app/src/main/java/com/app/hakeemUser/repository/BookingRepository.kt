package com.app.hakeemUser.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.R
import com.app.hakeemUser.RouteGenerate.*
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.utils.SharedHelper
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import org.json.JSONObject

class BookingRepository private constructor() {

    companion object {
        private var repository: BookingRepository? = null

        fun getInstance(): BookingRepository {
            if (repository == null) {
                repository = BookingRepository()
            }
            return repository as BookingRepository
        }
    }


    fun bookNewConsultation(input: ApiInput): LiveData<BookConsultationResponse>? {

        val apiResponse: MutableLiveData<BookConsultationResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: BookConsultationResponse =
                    gson.fromJson(jsonObject.toString(), BookConsultationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = BookConsultationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun Newvirtualfee(input: ApiInput): LiveData<NewVirutualFeeResponse>? {

        val apiResponse: MutableLiveData<NewVirutualFeeResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: NewVirutualFeeResponse =
                    gson.fromJson(jsonObject.toString(), NewVirutualFeeResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = NewVirutualFeeResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun updateInsBooking(input: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getBookingDetails(apiParams: ApiInput): LiveData<BookingDetailResponse>? {
        val apiResponse: MutableLiveData<BookingDetailResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: BookingDetailResponse =
                    gson.fromJson(jsonObject.toString(), BookingDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = BookingDetailResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getRouteDetails(
        context: Context,
        srcLatLng: LatLng?,
        desLatLng: LatLng?
    ): LiveData<Route>? {
        val liveData: MutableLiveData<Route> =
            MutableLiveData()
        val routing: Routing = Routing.Builder()
            .travelMode(AbstractRouting.TravelMode.TRANSIT)
            .language(SharedHelper(context).language)
            .key("AIzaSyAv1vK2PSA9AYlDp3-gZuU1wfoMptiapjA")

            .withListener(object : RoutingListener {
                override fun onRoutingFailure(e: RouteException?) {
                    Log.d("kaniakshannaanan", e.toString())
                   // Toast.makeText(context, "EXCEPTION: Cannot parse routing response", Toast.LENGTH_SHORT).show();

                }

                override fun onRoutingStart() {}
                override fun onRoutingSuccess(
                    route: List<Route>,
                    shortestRouteIndex: Int
                ) {
                    liveData.value = route[shortestRouteIndex]
                }

                override fun onRoutingCancelled() {
                    Log.d("error", "canceled")
                }
            })
            .waypoints(srcLatLng, desLatLng).
            alternativeRoutes(false)
            .build()
        routing.execute()
        return liveData
    }

    fun cancelBooking(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun payAmount(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun rateConsultation(apiParams: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getActiveBooking(apiParams: ApiInput): LiveData<ActivityBookingResponse> {

        val apiResponse: MutableLiveData<ActivityBookingResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ActivityBookingResponse =
                    gson.fromJson(jsonObject.toString(), ActivityBookingResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ActivityBookingResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun addAmountToWallet(apiParams: ApiInput): LiveData<CommonResponse> {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getWalletDetails(apiParams: ApiInput): LiveData<WalletTransactionResponse> {


        val apiResponse: MutableLiveData<WalletTransactionResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: WalletTransactionResponse =
                    gson.fromJson(jsonObject.toString(), WalletTransactionResponse::class.java)

                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = WalletTransactionResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun bookVirtualConsultation(input: ApiInput): LiveData<BookConsultationResponse>? {

        val apiResponse: MutableLiveData<BookConsultationResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: BookConsultationResponse =
                    gson.fromJson(jsonObject.toString(), BookConsultationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = BookConsultationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun payUsingWallet(input: ApiInput): LiveData<PayUsingWalletResponse> {
        val apiResponse: MutableLiveData<PayUsingWalletResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PayUsingWalletResponse =
                    gson.fromJson(jsonObject.toString(), PayUsingWalletResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PayUsingWalletResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun payment(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


    fun payment1(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse> = MutableLiveData()

        Api.getMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse()
                response.rcode = 0
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun payment2(apiParams: ApiInput): MutableLiveData<PaymentGatewayResponse1> {
        val apiResponse: MutableLiveData<PaymentGatewayResponse1> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentGatewayResponse1 =
                    gson.fromJson(jsonObject.toString(), PaymentGatewayResponse1::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentGatewayResponse1()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


    fun refundpayment(apiParams: ApiInput): MutableLiveData<PaymentRefundResponse> {
        val apiResponse: MutableLiveData<PaymentRefundResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: PaymentRefundResponse =
                    gson.fromJson(jsonObject.toString(), PaymentRefundResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = PaymentRefundResponse()
                //response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }



}