package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class ConsultationRepository private constructor() {

    companion object {
        private var repository: ConsultationRepository? = null

        fun getInstance(): ConsultationRepository {
            if (repository == null) {
                repository = ConsultationRepository()
            }
            return repository as ConsultationRepository
        }
    }


    fun getConsultation(input: ApiInput): LiveData<ConsultationResponse>? {

        val apiResponse: MutableLiveData<ConsultationResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ConsultationResponse =
                    gson.fromJson(jsonObject.toString(), ConsultationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ConsultationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getNotificationData(input: ApiInput): LiveData<NotificationResponse> {

        val apiResponse: MutableLiveData<NotificationResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: NotificationResponse =
                    gson.fromJson(jsonObject.toString(), NotificationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = NotificationResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getInsuranceStatus(input: ApiInput): LiveData<GetInsuranceStatusResponse> {

        val apiResponse: MutableLiveData<GetInsuranceStatusResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: GetInsuranceStatusResponse =
                    gson.fromJson(jsonObject.toString(), GetInsuranceStatusResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = GetInsuranceStatusResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun viewlabreport(input: ApiInput): LiveData<ViewLabReportResponse> {

        val apiResponse: MutableLiveData<ViewLabReportResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: ViewLabReportResponse =
                    gson.fromJson(jsonObject.toString(), ViewLabReportResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = ViewLabReportResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun clearNotification(apiParams: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(apiParams, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getcheckversion(input: ApiInput): LiveData<VersionCheckResponse> {

        val apiResponse: MutableLiveData<VersionCheckResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: VersionCheckResponse =
                    gson.fromJson(jsonObject.toString(), VersionCheckResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                val response = VersionCheckResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

}