package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemDoctor.models.GetLabListResponseModel
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class MembersRepository private constructor() {

    companion object {
        private var repository: MembersRepository? = null

        fun getInstance(): MembersRepository {
            if (repository == null) {
                repository = MembersRepository()
            }
            return repository as MembersRepository
        }
    }



    fun addmemberDetails(input: ApiInput): LiveData<AddMembersDetailsResponse>? {

        val apiResponse: MutableLiveData<AddMembersDetailsResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddMembersDetailsResponse =
                    gson.fromJson(jsonObject.toString(), AddMembersDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddMembersDetailsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun updatememberDetails(input: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


   fun getmemberDetails(input: ApiInput): LiveData<GetMemberDetailsResponse>? {

        val apiResponse: MutableLiveData<GetMemberDetailsResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetMemberDetailsResponse =
                        gson.fromJson(jsonObject.toString(), GetMemberDetailsResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetMemberDetailsResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun deletemember(input: ApiInput): LiveData<CommonResponse>? {

        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun gettestlistDetails(input: ApiInput): LiveData<GetTestListResponse>? {

        val apiResponse: MutableLiveData<GetTestListResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetTestListResponse =
                    gson.fromJson(jsonObject.toString(), GetTestListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetTestListResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getpharmacylistDetails(input: ApiInput): LiveData<GetPharmachyListResponse>? {

        val apiResponse: MutableLiveData<GetPharmachyListResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetPharmachyListResponse =
                        gson.fromJson(jsonObject.toString(), GetPharmachyListResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetPharmachyListResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun updateProfile(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getlablistrepo(input: ApiInput): LiveData<GetLabListResponseModel>? {

        val apiResponse: MutableLiveData<GetLabListResponseModel> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetLabListResponseModel =
                    gson.fromJson(jsonObject.toString(), GetLabListResponseModel::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetLabListResponseModel()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}