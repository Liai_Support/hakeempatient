package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        private var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository = MapRepository()
            }
            return repository as MapRepository
        }
    }


    fun getAddress(input: ApiInput): LiveData<AutoCompleteResponse>? {

        val apiResponse: MutableLiveData<AutoCompleteResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AutoCompleteResponse =
                    gson.fromJson(jsonObject.toString(), AutoCompleteResponse::class.java)
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AutoCompleteResponse()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getdoctorlive(input: ApiInput): LiveData<LivedoctorResponse>? {

        val apiResponse: MutableLiveData<LivedoctorResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: LivedoctorResponse =
                    gson.fromJson(jsonObject.toString(), LivedoctorResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = LivedoctorResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun getAutoCompleteAddress(apiInputs: ApiInput): LiveData<AutoCompleteAddressResponseModel> {
        val apiResponse: MutableLiveData<AutoCompleteAddressResponseModel> = MutableLiveData()

        Api.getMethod(apiInputs, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AutoCompleteAddressResponseModel =
                    gson.fromJson(
                        jsonObject.toString(),
                        AutoCompleteAddressResponseModel::class.java
                    )
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AutoCompleteAddressResponseModel()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }

    fun getLatLngDetails(apiInputs: ApiInput): LiveData<GetLatLongFromIdResponseModel> {

        val apiResponse: MutableLiveData<GetLatLongFromIdResponseModel> = MutableLiveData()

        Api.getMethod(apiInputs, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetLatLongFromIdResponseModel =
                    gson.fromJson(
                        jsonObject.toString(),
                        GetLatLongFromIdResponseModel::class.java
                    )
                response.error = false
                response.errorMessage = ""
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetLatLongFromIdResponseModel()
                response.error = true
                response.errorMessage = error
                apiResponse.value = response
            }
        })

        return apiResponse

    }


}