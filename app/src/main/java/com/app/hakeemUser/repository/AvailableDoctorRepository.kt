package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.AvailableDoctorResponse
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import org.json.JSONObject

class AvailableDoctorRepository private constructor() {

    companion object {
        private var repository: AvailableDoctorRepository? = null

        fun getInstance(): AvailableDoctorRepository {
            if (repository == null) {
                repository = AvailableDoctorRepository()
            }
            return repository as AvailableDoctorRepository
        }
    }


    fun getAvailableDoctors(input: ApiInput, apiResponseCallback: ApiResponseCallback): LiveData<AvailableDoctorResponse>? {

        val apiResponse: MutableLiveData<AvailableDoctorResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                apiResponseCallback.setResponseSuccess(jsonObject)
            }

            override fun setErrorResponse(error: String) {
                apiResponseCallback.setErrorResponse(error)
            }
        })

        return apiResponse
    }


}