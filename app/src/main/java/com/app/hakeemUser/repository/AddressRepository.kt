package com.app.hakeemUser.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.AddAddressResponse
import com.app.hakeemUser.models.CommonResponse
import com.app.hakeemUser.models.ListAddressResponse
import com.app.hakeemUser.models.ProfileDetailsResponse
import com.app.hakeemUser.network.Api
import com.app.hakeemUser.network.ApiInput
import com.google.gson.Gson
import org.json.JSONObject

class AddressRepository private constructor() {

    companion object {
        private var repository: AddressRepository? = null

        fun getInstance(): AddressRepository {
            if (repository == null) {
                repository = AddressRepository()
            }
            return repository as AddressRepository
        }
    }


    fun getlistaddress(input: ApiInput): LiveData<ListAddressResponse>? {

        val apiResponse: MutableLiveData<ListAddressResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ListAddressResponse =
                    gson.fromJson(jsonObject.toString(), ListAddressResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ListAddressResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun addaddress(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun deleteaddress(input: ApiInput): LiveData<CommonResponse>? {


        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                        gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }


}