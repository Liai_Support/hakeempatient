package com.app.hakeemUser.network

import com.app.hakeemUser.BuildConfig
import com.app.hakeemUser.R
import com.app.hakeemUser.app.AppController


object UrlHelper {
    private const val BASE = BuildConfig.Base
    private const val BASE_URL = BASE + "patient/"
    const val SOCKETURL = BuildConfig.SocketURL

   const val PAYMENT_URL = "https://api.noonpayments.com/payment/v1/order"

    const val PAYMENT_KEY = "Key_Live aGFrZWVtLmhha2VlbTo0OTlkOWE2ZDY0ZmM0ODg5YjgwZTY0NGRmNzQyMjdmMQ=="
   /*const val PAYMENT_URL = "https://api-test.noonpayments.com/payment/v1/order"
    const val PAYMENT_KEY = "Key_Test aGFrZWVtLkhha2VlbTo1MDBhODVhMGIwYjE0Yzg0OGRmZGZmOTAwNjFiODhjNw=="
*/
    const val CREATE_ACCOUNT = BASE_URL + "createAccount"
    const val LOGIN = BASE_URL + "login"
    const val GET_OTP = BASE_URL + "getOTP"
    const val CHECK_OTP = BASE_URL + "checkOTP"
    const val ENTER_NEW_PASSWORD = BASE_URL + "enterNewPassword"

    const val CHECK_SOCIAL_LOGIN = BASE_URL + "checkSocialLogin"

    const val UPDATE_DEVICE_TOKEN = BASE_URL + "updateDeviceToken"
    const val HOMEDASHBOARD = BASE_URL + "homeDashboard"
    const val GOOGLE_LOGIN = BASE_URL + "googleLogin"
    const val FACEBOOKLOGIN = BASE_URL + "facebookLogin"
    const val GET_PROFILE = BASE_URL + "getMyProfile"
    const val UPDATE_PROFILE = BASE_URL + "updateProfile"

    const val CONSULTATION_HISTORY = BASE_URL + "getMyConsultationHistory"
    const val RECENT_CONSULTATION = BASE_URL + "getMyRecentConsultant"
    const val LISTAVAILABLEDOCTORS = BASE_URL + "listAvailableDoctors"
    const val LISTAVAILABLEDOCTORSGLOBAL = BASE_URL + "listGlobleDoctors"

    const val GETALLDOCTORS = BASE_URL + "getAllDoctors"
    const val GETLABLIST = BASE_URL + "get_lab_list"
    const val GETPHARMACYLIST = BASE_URL + "get_pharm_list"
    const val VIEWLABREPORT = BASE_URL + "getLabTestReports"

    const val NEWVIRTUALFEE = BASE_URL + "check_ins_com"
    const val FEATUREBOOKING = BASE_URL + "featureBooking"
    const val VIRTUALBOOKING = BASE_URL + "virtualBooking"
    const val NEWBOOKING = BASE_URL + "newBooking"
    const val TRACKPROVIDER = BASE_URL + "trackProvider"
    const val CANCELBOOKING = BASE_URL + "cancelBooking"
    const val UPDATEINSBOOKING = BASE_URL + "updateInsBooking"
    const val APPSETTINGS = BASE_URL + "appSetting"

    const val PAY = BASE_URL + "pay"
    const val POSTREVIEW = BASE_URL + "postReview"

    const val CHATDETAILS = BASE_URL + "chatDetailList"
    const val GETACTIVEBOOKINGS = BASE_URL + "getActiveBookings"
    const val ADDMONEYWALLET = BASE_URL + "addMoneyWallet"
    const val WALLETHISTORY = BASE_URL + "walletHistory"

    const val PAYFROMWALLET = BASE_URL + "payFromWallet"
    const val ADDMEMBER = BASE_URL + "addMembers"
    const val GETLIVEDOCTOR = BASE_URL + "listAvailableDoctorsOnMap"

    const val GETMEMBER = BASE_URL + "getMyMembers"
    const val UPDATEMEMBER = BASE_URL + "updateMembers"
    const val DELETEMEMBER = BASE_URL + "deleteMembers"
    const val GETTESTLIST = BASE_URL + "get_test_names"

    const val GET_CLASS = BASE_URL + "get_class"
    const val GET_ICNAME = BASE_URL + "get_ins_com"


    const val ADDADDRESS = BASE_URL +"addAddress"
    const val LISTADDRESS = BASE_URL +"list_address"
    const val DELETEADDRESS = BASE_URL +"deleteAddress"
    const val VERSION_CHECK = BASE_URL +"versionCheck"



    const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
    const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
    const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
    const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
    const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
    const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"


    const val CREATECALL = BASE_URL + "createCall"
    const val ACCEPTCALL = BASE_URL + "acceptCall"
    const val ENDCALL = BASE_URL + "endCall"
    const val COMPLETEVIRTUALBOOKIG = BASE_URL + "completeVirtualBooking"
    const val GETMYNOTIFICATION = BASE_URL + "getMyNotifications"
    const val GETINSURANCESATUS = BASE_URL + "getInsuranceStatus"
    const val DELETEMYNOTIFICATION = BASE_URL + "deleteMyNotifications"


    //socket
    const val SENDRANDOMREQUEST = "sendRandomRequest"
    const val SENDMESSAGE = "sendmessage"
    const val GET_ONLINE = "get_online"
    const val RECIVE_MESSAGE = "recievemessage"

    fun getAddress(
        latitude: Double,
        longitude: Double
    ): String? {
        val lat = latitude.toString()
        val lngg = longitude.toString()
        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + AppController.getInstance().resources.getString(
            R.string.map_api_key
        ))
    }
}