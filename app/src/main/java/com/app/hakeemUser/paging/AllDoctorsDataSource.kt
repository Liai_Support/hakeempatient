package com.app.hakeemUser.paging

import android.app.Activity
import android.content.Context
import androidx.paging.PageKeyedDataSource
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.models.AllDoctorResponse
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.AvailableDoctorRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import com.google.gson.Gson
import org.json.JSONObject

class AllDoctorsDataSource : PageKeyedDataSource<Int, DoctorData>() {

    private var context: Context? = null
    var repository: AvailableDoctorRepository = AvailableDoctorRepository.getInstance()


    override fun loadInitial(
        params: LoadInitialParams<Int?>,
        callback: LoadInitialCallback<Int?, DoctorData?>
    ) {
        PAGENUMBER = 1
        repository.getAvailableDoctors(inputForTripsLists, object : ApiResponseCallback {

            override fun setResponseSuccess(jsonObject: JSONObject) {

                val gson = Gson()
                val response: AllDoctorResponse =
                    gson.fromJson(jsonObject.toString(), AllDoctorResponse::class.java)

                response.data?.doctors?.let {
                    if (it.isEmpty()) {
                        callback.onResult(it, null, null)
                    } else {
                        callback.onResult(
                            it,
                            null,
                            PAGENUMBER + 1
                        )
                    }
                }

                PAGENUMBER += 1
            }

            override fun setErrorResponse(error: String) {


            }
        })
    }

    override fun loadBefore(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, DoctorData?>
    ) {
    }

    override fun loadAfter(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, DoctorData?>
    ) {
        if (params.key != null) {
            repository.getAvailableDoctors(inputForTripsLists, object : ApiResponseCallback {

                override fun setResponseSuccess(jsonObject: JSONObject) {
                    val gson = Gson()
                    val response: AllDoctorResponse =
                        gson.fromJson(jsonObject.toString(), AllDoctorResponse::class.java)

                    response.data?.doctors?.let {
                        if (it.isEmpty()) {
                            callback.onResult(it, null)
                        } else {
                            callback.onResult(
                                it,
                                PAGENUMBER + 1
                            )
                        }
                    }

                    PAGENUMBER += 1
                }

                override fun setErrorResponse(error: String) {


                }
            })
        }
    }


    private val inputForTripsLists: ApiInput
        get() {

            val sharedHelper: SharedHelper? = SharedHelper(context!!)
            val header: MutableMap<String, String> = HashMap()
            sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
            sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
            header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT

            val jsonObject = JSONObject()

            jsonObject.put(Constants.ApiKeys.PAGE, PAGENUMBER)

            val inputForAPI = ApiInput()
            inputForAPI.context = context
            inputForAPI.url = UrlHelper.GETALLDOCTORS
            inputForAPI.headers = header
            inputForAPI.jsonObject = jsonObject
            return inputForAPI
        }

    fun setInputs(activity: Activity?) {
        context = activity
    }

    companion object {
        var PAGENUMBER = 1
        var TOTAL_PAGENUMBER = 500
    }


}