package com.app.hakeemUser.paging

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.app.hakeemUser.models.DoctorData

class AllDoctorsDataSourceFactory : DataSource.Factory<Int, DoctorData>() {
    private val itemLiveDataSource: MutableLiveData<PageKeyedDataSource<Int, DoctorData>> =
        MutableLiveData()
    var dataSource: AllDoctorsDataSource? = null
    var activity: Activity? = null

    override fun create(): DataSource<Int, DoctorData> {
        dataSource = AllDoctorsDataSource()
        dataSource!!.setInputs(activity)
        itemLiveDataSource.postValue(dataSource)
        return dataSource!!
    }

    fun getItemLiveDataSource(): MutableLiveData<PageKeyedDataSource<Int, DoctorData>> {
        return itemLiveDataSource
    }

    fun setInputs(activity: Activity?) {
        this.activity = activity
    }
}