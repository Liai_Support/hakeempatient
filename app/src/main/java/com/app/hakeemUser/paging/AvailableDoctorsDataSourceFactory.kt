package com.app.hakeemUser.paging

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.app.hakeemUser.models.DoctorData

class AvailableDoctorsDataSourceFactory : DataSource.Factory<Int, DoctorData>() {
    private val itemLiveDataSource: MutableLiveData<PageKeyedDataSource<Int, DoctorData>> =
        MutableLiveData()
    var dataSource: AvailableDoctorsDataSource? = null
    var activity: Activity? = null
    var specialityId: String? = null

    override fun create(): DataSource<Int, DoctorData> {
        dataSource = AvailableDoctorsDataSource()
        dataSource!!.setInputs(activity, specialityId)
        itemLiveDataSource.postValue(dataSource)
        return dataSource!!
    }

    fun getItemLiveDataSource(): MutableLiveData<PageKeyedDataSource<Int, DoctorData>> {
        return itemLiveDataSource
    }

    fun setInputs(activity: Activity?, specialityId: String?) {
        this.activity = activity
        this.specialityId = specialityId
    }
}