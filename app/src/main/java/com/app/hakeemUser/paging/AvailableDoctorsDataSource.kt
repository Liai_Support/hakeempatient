package com.app.hakeemUser.paging

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.app.hakeemUser.interfaces.ApiResponseCallback
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.AvailableDoctorResponse
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.repository.AvailableDoctorRepository
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.ScheduledBookingSingleton
import com.app.hakeemUser.utils.SharedHelper
import com.google.gson.Gson
import org.json.JSONObject

class AvailableDoctorsDataSource : PageKeyedDataSource<Int, DoctorData>() {

    private var specialityId: String? = null
    private var context: Context? = null
    var repository: AvailableDoctorRepository = AvailableDoctorRepository.getInstance()


    override fun loadInitial(
        params: LoadInitialParams<Int?>,
        callback: LoadInitialCallback<Int?, DoctorData?>
    ) {
        PAGENUMBER = 1
        repository.getAvailableDoctors(inputForTripsLists, object : ApiResponseCallback {

            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                val response: AvailableDoctorResponse =
                    gson.fromJson(jsonObject.toString(), AvailableDoctorResponse::class.java)

                if(response.error == true){
                    context?.let {
                        response.message?.let { it1 ->
                            DialogUtils.showAlert(it, object : SingleTapListener {
                                override fun singleTap() {
                                  //  var activity: Activity = AvailableDoctorActivity()
                                  //  activity.onBackPressed()
                                }

                            }, it1)
                        }
                    }
                }

                response.data?.doctors?.let {
                    Log.d("xvxjugjyc",""+it.size+"vgb"+it.isEmpty())
                    if (it.isEmpty()) {
                        callback.onResult(it, null, null)
                        context?.let { it1 ->
                            DialogUtils.showAlert(it1, object : SingleTapListener {
                                override fun singleTap() {
                                  //  var homefragment: AvailableDoctorActivity = AvailableDoctorActivity()
                                  //  homefragment.onBackPressed()
                                }

                            }, "No Doctors Available")
                        }
                    } else {
                        callback.onResult(
                            it,
                            null,
                            PAGENUMBER + 1
                        )
                    }
                }

                PAGENUMBER += 1
            }

            override fun setErrorResponse(error: String) {


            }
        })
    }

    override fun loadBefore(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, DoctorData?>
    ) {
    }

    override fun loadAfter(
        params: LoadParams<Int?>,
        callback: LoadCallback<Int?, DoctorData?>
    ) {
        if (params.key != null) {
            repository.getAvailableDoctors(inputForTripsLists, object : ApiResponseCallback {

                override fun setResponseSuccess(jsonObject: JSONObject) {
                    val gson = Gson()
                    val response: AvailableDoctorResponse =
                        gson.fromJson(jsonObject.toString(), AvailableDoctorResponse::class.java)

                    response.data?.doctors?.let {
                        if (it.isEmpty()) {
                            callback.onResult(it, null)
                        } else {
                            callback.onResult(
                                it,
                                PAGENUMBER + 1
                            )
                        }
                    }

                    PAGENUMBER += 1
                }

                override fun setErrorResponse(error: String) {


                }
            })
        }
    }


    private val inputForTripsLists: ApiInput
        get() {

            val sharedHelper: SharedHelper? = SharedHelper(context!!)
            val header: MutableMap<String, String> = HashMap()
            sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
            sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
            header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT

            val jsonObject = JSONObject()

            if(!ScheduledBookingSingleton.getInstance().specialityGender.equals("")){
                jsonObject.put(Constants.ApiKeys.LATITUDE, ScheduledBookingSingleton.getInstance().latitude)
                jsonObject.put(Constants.ApiKeys.LONGITUDE,ScheduledBookingSingleton.getInstance().longitude)
                jsonObject.put(Constants.ApiKeys.PAGE, PAGENUMBER)
                jsonObject.put(Constants.ApiKeys.SPECIALITYID, specialityId)
                jsonObject.put("gender", ScheduledBookingSingleton.getInstance().specialityGender)
                jsonObject.put("ins", ScheduledBookingSingleton.getInstance().chooseoption)
            }
            else {
                Log.d("sdfghjk",""+ScheduledBookingSingleton.getInstance().latitude)
                Log.d("qwertyui",""+ScheduledBookingSingleton.getInstance().longitude)
                if(ScheduledBookingSingleton.getInstance().latitude == null || ScheduledBookingSingleton.getInstance().longitude ==null){
                    jsonObject.put(Constants.ApiKeys.LATITUDE,sharedHelper!!.selectedLat)
                    jsonObject.put(Constants.ApiKeys.LONGITUDE, sharedHelper!!.selectedLng)
                }
                else{
                    jsonObject.put(Constants.ApiKeys.LATITUDE, ScheduledBookingSingleton.getInstance().latitude)
                    jsonObject.put(Constants.ApiKeys.LONGITUDE, ScheduledBookingSingleton.getInstance().longitude)
                }
                jsonObject.put(Constants.ApiKeys.PAGE, PAGENUMBER)
                jsonObject.put(Constants.ApiKeys.SPECIALITYID, specialityId)
                jsonObject.put("ins", ScheduledBookingSingleton.getInstance().chooseoption)

            }


            if(ScheduledBookingSingleton.getInstance().isGlobalBooking == 1){
                val inputForAPI = ApiInput()
                inputForAPI.context = context
                inputForAPI.url = UrlHelper.LISTAVAILABLEDOCTORSGLOBAL
                inputForAPI.headers = header
                inputForAPI.jsonObject = jsonObject
                return inputForAPI
            }
            else{
                val inputForAPI = ApiInput()
                inputForAPI.context = context
                inputForAPI.url = UrlHelper.LISTAVAILABLEDOCTORS
                inputForAPI.headers = header
                inputForAPI.jsonObject = jsonObject
                return inputForAPI
            }
        }

    fun setInputs(activity: Activity?, specialityId: String?) {
        this.specialityId = specialityId
        context = activity
    }

    companion object {
        var PAGENUMBER = 1
        var TOTAL_PAGENUMBER = 50
    }


}