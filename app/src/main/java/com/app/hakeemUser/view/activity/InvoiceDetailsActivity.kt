package com.app.hakeemUser.view.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivitySummaryBinding
import com.app.hakeemUser.models.*
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.ConsultationViewModel

class InvoiceDetailsActivity : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var viewmodel: ConsultationViewModel? = null
    var bookingid = 0
    var sharedHelper: SharedHelper? = null
    var ratingDialog: Dialog? = null
    var viewmodel1: BookingViewModel? = null
    var consultationdata: ConsultationData? = null
    var descrip:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_summary)
        viewmodel = ViewModelProvider(this).get(ConsultationViewModel::class.java)
        viewmodel1 = ViewModelProvider(this).get(BookingViewModel::class.java)


       // binding!!.textView.text = getString(R.string.invoice)

        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
        if (sharedHelper!!.isreview == "true"){
            Log.d("ertyui",""+sharedHelper!!.isreview)
            binding!!.getStarted.visibility=View.GONE
            binding!!.button.visibility=View.GONE
        }else{
            binding!!.getStarted.visibility = View.VISIBLE
            binding!!.button.text = getString(R.string.givereviewandratings)
        }

        binding!!.textView33.text = getString(R.string.consultationfee)
        binding!!.textView37.text = getString(R.string.amount_paid)
        getIntentValues()
    }

    private fun getIntentValues() {

        intent.extras?.let {
            it.getParcelable<ConsultationData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { data ->
                consultationdata = data
                bookingid = data.bookingId!!.toInt()
                UiUtils.setRating(this, data.averageRating?.toFloat(), binding!!.rating, 35)
                data.status?.let {

                    if (it.equals("online", true))
                        View.VISIBLE
                    else View.GONE
                }

                Log.d("xcdfgvhjk",""+data.noteContent)

                data.noteContent?.let { binding!!.textViewnote.text = it }


                data.providerName?.let { binding!!.doctorName.text = it }
                data.education?.let { binding!!.qualification.text = it+"-"+data.specialityName }
                data.expierence?.let { binding!!.experience.text = it+" "+getString(R.string.year)+" "+getString(R.string.experience) }
//                data.averageRating?.let { ratingValue.text = "$it/5" }
                binding!!.ratingValue.text = "0/5"
                data.status?.let {
                    if (it.equals("online", true))
                        binding!!.isOnline.visibility =
                            View.VISIBLE else binding!!.isOnline.visibility = View.GONE
                }

                data.profilePic?.let {
                    UiUtils.loadImage(
                        binding!!.profileImage,
                        it,
                        ContextCompat.getDrawable(this, R.drawable.splash_image)!!
                    )
                }

                binding!!.extraChargeText.text = getString(R.string.extra_charges_by_doc)

                data.fee?.let {
                    if (data.extraFee != null) {

                        binding!!.singleConsultation.text = "${it.toInt()} SAR"
                        binding!!.consultationAmount.text = "${data.extraFee} SAR"
                        binding!!.totalAmount.text = "${it.toInt() + data.extraFee!!.toInt()} SAR"

                    } else {
                        binding!!.singleConsultation.text = "$it SAR"
                        binding!!.consultationAmount.text = "0 SAR"
                        binding!!.totalAmount.text = "$it SAR"
                    }

                }

                if(data.status.equals("pending") || data.status.equals("cancelledbyprovider") || data.status.equals("cancelledbypatient")){
                    binding!!.cardView10.visibility = View.GONE
                    binding!!.getStarted.visibility = View.GONE
                }
                /*else if ((data.status.equals("completed")&&data.isreviewed.equals("true"))){
                    binding!!.getStarted.visibility = View.GONE
                    Log.d("ijijijij",""+sharedHelper!!.isreview)

                }else if((data.status.equals("completed")&&data.isreviewed.equals("false"))){
                    Log.d("kakakak", "kakakakakak")

                    binding!!.cardView10.visibility = View.VISIBLE
                    binding!!.getStarted.visibility = View.VISIBLE

                }*/

            }
        }

        val lab: ArrayList<SlabData> = intent.getSerializableExtra("lab") as ArrayList<SlabData>
        val medicine: ArrayList<MedData> = intent.getSerializableExtra("med") as ArrayList<MedData>
        val test: ArrayList<TestData> = intent.getSerializableExtra("test") as ArrayList<TestData>




 /*       if(medicine.isEmpty()==false){
            val str1 = medicine[0].medicine
            val str2 = medicine[0].medicine_desc
            val separate1 = str1!!.split(",").map { it.trim() }
            val separate2 = str2!!.split(",").map { it.trim() }
            var finaltext = ""
            for ((index,value) in separate1.withIndex()){
                if(index == 0){
                    finaltext = finaltext+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
                else{
                    finaltext = finaltext+"\n"+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
            }
            binding!!.medicinelist.text = finaltext
        }*/


        if(medicine.isEmpty()==false){

            Log.d("vdvbdcbcdb", medicine[0].medicine.toString())
            Log.d("vdvbdcbcdb", medicine[0].medicine_desc.toString())
            var finaltext = ""

            for (i in 0 until  medicine[0].medicine!!.size ){
                Log.d("medicinee", medicine[0].medicine!![i])
                for (j in 0 until  medicine[0].medicine_desc!!.size ){

                    var sizee = medicine[0].medicine_desc!!.size
                   /* Log.d("kanikashanmugam", medicine[0].medicine_desc!![0])
                    Log.d("kanikashanmugam", medicine[0].medicine_desc!![1])*/
                    Log.d("kanikashanmugam", medicine[0].medicine_desc!!.size.toString())
                    Log.d("vcvcvcvvcvccv", medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![j])

                    if (i ==0){
                        finaltext = finaltext+(i+1)+" . "+medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![0]

                    }else{
                        finaltext = finaltext+"\n"+(i+1)+" . "+medicine[0].medicine!![i]+" - "+medicine[0].medicine_desc!![sizee-1]

                    }

                    break
                }
            }
            val str1 = medicine[0].medicine
            val str2 = medicine[0].medicine_desc
            /*  val separate1 = str1!!.split(",").map { it.trim() }
               val separate2 = str2!!.split(",").map { it.trim() }*/
/*
            for ((index,value) in separate1.withIndex()){
                if(index == 0){
                    finaltext = finaltext+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
                else{
                    finaltext = finaltext+"\n"+(index+1)+"."+separate1[index]+" - "+separate2[index]
                }
            }
*/
            binding!!.medicinelist.text = finaltext
        }


        if(lab.isEmpty()==false){
            var labnames: String? = ""
            for (i in 0 until lab.size){
                if(i == 0){
                    labnames = ""+(i+1)+"."+lab[i].lab_name
                }
                else{
                    labnames = labnames+"\n"+(i+1)+"."+lab[i].lab_name
                }
            }
            binding!!.lablist.text = labnames
        }

        if(test.isEmpty()==false){
            var testnames: String? = ""
            for (i in 0 until test.size){
                if(i == 0){
                    testnames = ""+(i+1)+"."+test[i].Test_name
                }
                else{
                    testnames = testnames+"\n"+(i+1)+"."+test[i].Test_name
                }
            }
            binding!!.testlist.text = testnames
        }

        binding!!.labreport.setOnClickListener {
            DialogUtils.showLoader(this@InvoiceDetailsActivity)
            viewmodel?.viewlabreport(this@InvoiceDetailsActivity, bookingid)?.observe(this@InvoiceDetailsActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        }
                        else {
                            it.data?.let { data ->
                                if(data.size!=0){
                                    startActivity(
                                        Intent(
                                            this@InvoiceDetailsActivity,
                                            ViewLabReportActivity::class.java
                                        )
                                            .putExtra("labreports",data as ArrayList<ReportData>)
                                    )
                                }
                                else{
                                    UiUtils.showSnack(binding!!.root, getString(R.string.labreportnotuploaded))
                                }
                            }
                        }
                    }
                }
            })
        }

    }

    fun onConfirmClicked(view: View){
        showRatingDialog()
    }

    private fun showRatingDialog() {

        ratingDialog = DialogUtils.getBookingRatingDialog(this)

        var doctorName = ratingDialog?.findViewById<TextView>(R.id.doctorName)
        var ratingBar = ratingDialog?.findViewById<RatingBar>(R.id.ratingBar)
        var docImage = ratingDialog?.findViewById<ImageView>(R.id.docImage)
        var comment = ratingDialog?.findViewById<EditText>(R.id.comment)
        var submit = ratingDialog?.findViewById<CardView>(R.id.submit)
        var dialogRoot = ratingDialog?.findViewById<ConstraintLayout>(R.id.dialogRoot)


        comment!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {

                descrip  = comment?.text.toString()
                Log.d("descripwertyuio",""+descrip)

                Log.d("wertyuio",""+descrip.length)





            }
        })


        doctorName?.text =
            "${getString(R.string.rate_you_consultation_with)} ${consultationdata?.providerName}"

        consultationdata?.profilePic.let {
            UiUtils.loadImage(docImage, it, getDrawable(R.drawable.place_holder_doctor)!!)
        }

        submit?.setOnClickListener {
            if (ratingBar?.rating != 0.0f) {
                if (descrip!= "") {
                    if (descrip.length>250){
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle(R.string.alert)
                        builder.setMessage(R.string.minimumcharacter)
                        builder.setCancelable(true)
                        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                            builder.setCancelable(true)

                        }
                        /*builder.setNegativeButton(android.R.string.no) { dialog, which ->
                            builder.setCancelable(true)
                        }*/
                        builder.show()
                    }else{
                        postReview(ratingBar?.rating,descrip)

                    }
                   // postReview(ratingBar?.rating, comment?.text.toString().trim())

                } else {
                    dialogRoot?.let { UiUtils.showSnack(it, getString(R.string.rate_consultation)) }
                }
            } else {
                dialogRoot?.let {
                    UiUtils.showSnack(
                        it,
                        getString(R.string.provider_rating)
                    )
                }
            }
        }

        ratingDialog?.show()
    }

    private fun postReview(rating: Float?, comment: String) {

        DialogUtils.showLoader(this@InvoiceDetailsActivity)
        viewmodel1?.rateConsultation(
            this@InvoiceDetailsActivity,
            rating,
            comment,
            consultationdata?.bookingId
        )
            ?.observe(this@InvoiceDetailsActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                                ratingDialog?.dismiss()
                            }
                        } else {
                            //showSuccessDialog()
                            binding!!.getStarted.visibility=View.GONE

                            ratingDialog?.dismiss()
                        }
                    }

                }
            })
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

}