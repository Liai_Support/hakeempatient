package com.app.hakeemUser.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.*
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.rxbus.RxBusNotification
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.activity.*
import com.app.hakeemUser.view.adapter.HomeArticleAdapter
import com.app.hakeemUser.view.adapter.HomeHeaderAdapter
import com.app.hakeemUser.view.adapter.ListAddressAdapter
import com.app.hakeemUser.viewmodel.AddressViewModel
import com.app.hakeemUser.viewmodel.HomeViewModel
import com.app.hakeemUser.viewmodel.MapViewModel
import com.app.hakeemUser.viewmodel.ProfileViewModel
import com.google.android.gms.location.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager
import io.reactivex.disposables.Disposable
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException
import java.util.*
import kotlin.system.exitProcess


class HomeFragment : Fragment(R.layout.fragment_home1) {

    var binding: FragmentHome1Binding? = null;
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var profileViewModel: ProfileViewModel? = null
    public var viewmodel: AddressViewModel? = null

    private var mapViewModel: MapViewModel? = null
    private var homeViewModel: HomeViewModel? = null
    var doubleBackToExitPressedOnce = false

    private var homeData: HomeBoardData? = null

    public var myLat = 0.0
    public var myLng = 0.0
    public var sharedHelper: SharedHelper? = null
    private lateinit var notificationEventListner: Disposable

    private var isAttached = false

    private var socket: Socket? = null
    private var automaticHandler = Handler()
    lateinit var runnable: Runnable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHome1Binding.inflate(inflater, container, false)
        val view = binding!!.root
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        viewmodel = ViewModelProvider(this).get(AddressViewModel::class.java)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
             //   isEnabled = false
                // activity?.onBackPressed()

                 if(activity==null||(activity as DashBoardActivity?)==null||(activity as DashBoardActivity?)!!.binding==null ||(activity as DashBoardActivity?)!!.binding!!.drawer==null){
                    Log.d("DrawerNotWorking","variables are null")}
                 else{
                     Log.d("DrawerNotWorking","Is open"+(activity as DashBoardActivity?)!!.binding!!.drawer.isDrawerOpen(GravityCompat.START))
                 }
                  if ((activity as DashBoardActivity?)!!.binding!!.drawer.isDrawerOpen(GravityCompat.START) ) {
                    (activity as DashBoardActivity?)!!.binding!!.drawer.closeDrawer(GravityCompat.START)

                }
                else {
                    (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(
                        BottomSheetBehavior.STATE_HIDDEN
                    )
                    //moveTaskToBack(true);
                    //exitProcess(-1)
                    if (doubleBackToExitPressedOnce) {
                        requireActivity().moveTaskToBack(true);
                        exitProcess(-1)
                        /* val intent = Intent(Intent.ACTION_MAIN)
                        intent.addCategory(Intent.CATEGORY_HOME)
                        startActivity(intent)*/
                        return
                    }

                    doubleBackToExitPressedOnce = true
                    Toast.makeText(requireContext(), getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

                    Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
                }

                /* if(shouldInterceptBackPress()){
                     // in here you can do logic when backPress is clicked
                 }else{
                     isEnabled = false
                 }*/
            }
        })
        sharedHelper = SharedHelper(requireContext())

        Log.d("vbvbvbvbv1;",""+ sharedHelper!!.language)
        sharedHelper?.language?.let {
            if (it == "en") {
                binding!!.lang.text = "عربي"
            }
            else{
                binding!!.lang.text = "English"
            }
        }
        setListeners()
        locationListner()
        askLocationPermission()
        getHomeboardDetails()
        listernNotification()
        initSockets()

        (activity as DashBoardActivity?)!!.binding!!.popupLoc.closePop.setOnClickListener(View.OnClickListener {
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        })
        (activity as DashBoardActivity?)!!.binding!!.popupLoc.addLocation.setOnClickListener(View.OnClickListener {
            if (sharedHelper?.loggedIn!!){
                startActivityForResult(Intent(requireContext(), SearchLocationActivity::class.java), Constants.RequestCode.LOCATION_REQUEST)
            }
            else{
                pleaseLogin()
            }
        })
        (activity as DashBoardActivity?)!!.binding!!.popupLoc.currentLocation.setOnClickListener(View.OnClickListener {
            if(sharedHelper!!.selectedaddressid!=0){
                sharedHelper!!.Iscurrentlocationseleted = true
                sharedHelper!!.Islocationseleted = false
                sharedHelper!!.selectedaddressid = 0
                getUserLocation()
                (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
            else{
                (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        })
        if (sharedHelper?.loggedIn!!){
            listaddress()
        }

        return view
    }

    public fun botomclose(){
        (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
    }

    public fun listaddress(){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getlistaddress(requireContext())
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.addressDetails?.let { list ->
                                    (activity as DashBoardActivity?)!!.binding!!.popupLoc.addressRecycler.layoutManager = LinearLayoutManager(requireContext())
                                    (activity as DashBoardActivity?)!!.binding!!.popupLoc.addressRecycler.adapter = ListAddressAdapter(requireContext(), list, this)
                                }
                            }
                        }
                    }

                }
            })

    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
//        opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }

    }

    override fun onResume() {
        super.onResume()
        automaticSilder(true)
        getLastKnownLocation()
        isAttached = true
        if (sharedHelper?.loggedIn!!){
            getAppSettings()
            setListeners()
        }
    }

    public fun automaticSilder(resume: Boolean) {

        Log.d("zxcfvgbnm",""+resume)
        homeData?.bannerImages?.let { bannerList ->

            runnable = Runnable {


                binding!!.viewPager?.let {

                    if (binding!!.viewPager.layoutManager!=null){
                        Log.d("sdfvg",""+bannerList)
                        if (bannerList.size - 1 > binding!!.viewPager.currentPosition) {
                            binding!!.viewPager.smoothScrollToPosition(binding!!.viewPager.currentPosition + 1)
                        } else {
                            binding!!.viewPager.smoothScrollToPosition(0)
                        }

                    }
                    automaticHandler.removeCallbacks(runnable)
                    automaticHandler.postDelayed(runnable, 6000)

                }

            }

            if (resume) {
                automaticHandler.removeCallbacks(runnable)
                automaticHandler.postDelayed(runnable, 6000)

            } else {

                automaticHandler.removeCallbacks(runnable)

            }

        }
    }

    override fun onPause() {
        super.onPause()
        isAttached = false
        automaticSilder(false)
    }

    private fun listernNotification() {
        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                ThreadUtils.runOnUiThread {
                    if (isAttached)
                        getAppSettings()
                }
            }
    }

    private fun getAppSettings() {

        homeViewModel?.getAppSettings(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                            }
                        } else {
                            it.data?.let { data ->
                                handleAppResponse(data)

                            }
                        }
                    }

                }
            })

    }

    private fun handleAppResponse(data: AppData) {
        data.completed?.let {
            if (it.size != 0) {
              /*  DialogUtils.showAlertDialogPayment(requireContext(),
                        requireContext().resources.getString(R.string.you_have_some_pending_invoice_would_n_you_like_to_pay),
                        requireContext().resources.getString(R.string.invoice),
                        requireContext().resources.getString(R.string.ok),
                        requireContext().resources.getString(R.string.cancel), object : DialogCallBack {
                    override fun onPositiveClick() {
                        startActivity(
                                Intent(requireContext(), SummaryDetailsActivity::class.java)
                                        .putExtra(
                                                Constants.IntentKeys.PAYMENTDETAIL,
                                                it[0] as Parcelable
                                        )
                                        .putExtra(Constants.IntentKeys.BACKPRESSALLOWED, true)
                        )
                    }

                    override fun onNegativeClick() {

                    }
                })*/
            }
        }
        data.configurations?.let {
            if (it.size != 0) {
                data.configurations?.forEachIndexed { _, configurations ->
                   /* if (configurations.objkey.equals("cancel_booking_share_percentage", true)) {

                        sharedHelper?.cancelInPercetage = configurations.objvalue?.toInt() == 1

                    } else if (configurations.objkey.equals("cancel_booking_share", true)) {

                        configurations.objvalue?.let { sharedHelper?.cancelPercORAmt = it }

                    }*/

                    if(configurations.objkey.equals("patient_cancellation_charge", true)){
                        configurations.objvalue?.let { sharedHelper?.cancelPercORAmt = it }
                        sharedHelper?.cancelInPercetage = 1 == 1

                    }
                }
            }
        }
    }

    private fun getHomeboardDetails() {
        DialogUtils.showLoader(requireContext())
        homeViewModel?.getHomeBoardDetails(requireContext())
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                homeData = data
                                handleResponse(data)
                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: HomeBoardData) {

        data.bannerImages?.let { listBanner ->
            setHomeAdapter(listBanner)
        }
        data.articles?.let { article ->
            setArticleAdapter(article)
        }

        data.configurations?.let { config ->
            if (config.size >= 2) {
                config[0].objvalue?.let {
                    sharedHelper?.calendarTiming = config[0].objvalue?.toInt()!!
                }

                config[1].objvalue?.let {
                    sharedHelper?.chatTiming = config[1].objvalue?.toInt()!!
                }

            }

            config.forEachIndexed { index, configurations ->
                if (configurations.objkey.equals("cancel_booking_share_percentage", true)) {

                    sharedHelper?.cancelInPercetage = configurations.objvalue?.toInt() == 1

                } else if (configurations.objkey.equals("cancel_booking_share", true)) {

                    configurations.objvalue?.let { sharedHelper?.cancelPercORAmt = it }

                }
            }
        }


    }


    private fun setListeners() {

        binding!!.lang.setOnClickListener{
            sharedHelper?.language?.let {
                if (it == "ar") {
                    sharedHelper?.language = "en"
                    reloadApp()
                }
                else{

                    sharedHelper?.language = "ar"
                    reloadApp()
                }
            }
        }
        binding!!.lab.setOnClickListener {
                restrictDoubleTap(it)
                startActivity(
                        Intent(requireContext(), LablistActivity::class.java)
                )
            }
        binding!!.parmachy.setOnClickListener {
                restrictDoubleTap(it)
                startActivity(
                        Intent(requireContext(), PharmachylistActivity::class.java)
                )
            }
        binding!!.GlobalDoctor.setOnClickListener {
            Log.d("vbvbvbvbv2.0;",""+ sharedHelper!!.language)

             restrictDoubleTap(it)
            Log.d("vbvbvbvbv2.1;",""+ sharedHelper!!.language)

            ScheduledBookingSingleton.clearAllValues()
            Log.d("vbvbvbvbv2.2;",""+ sharedHelper!!.language)

             datePicker()
                ScheduledBookingSingleton.getInstance().isElderlyFlow = false
                ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
                ScheduledBookingSingleton.getInstance().longitude = myLng.toString()
                ScheduledBookingSingleton.getInstance().location = binding!!.location.text.toString().trim()
                ScheduledBookingSingleton.getInstance().isVirtualBooking = 0
                ScheduledBookingSingleton.getInstance().isGlobalBooking = 1
            }
        binding!!.emergency.setOnClickListener {
                restrictDoubleTap(it)
                showEmergencyDialog()

            }
        binding!!.scheduleAppoinment.setOnClickListener {
                restrictDoubleTap(it)
                ScheduledBookingSingleton.clearAllValues()
                datePicker()
                ScheduledBookingSingleton.getInstance().isElderlyFlow = false
                ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
                ScheduledBookingSingleton.getInstance().longitude = myLng.toString()
                ScheduledBookingSingleton.getInstance().location = binding!!.location.text.toString().trim()
                ScheduledBookingSingleton.getInstance().isVirtualBooking = 0
                ScheduledBookingSingleton.getInstance().isGlobalBooking = 0

            }
        binding!!.virtualConsultation.setOnClickListener {
                restrictDoubleTap(it)
                ScheduledBookingSingleton.clearAllValues()
                datePicker()
                ScheduledBookingSingleton.getInstance().isElderlyFlow = false
                ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
                ScheduledBookingSingleton.getInstance().longitude = myLng.toString()
                ScheduledBookingSingleton.getInstance().location = binding!!.location.text.toString().trim()
                ScheduledBookingSingleton.getInstance().isVirtualBooking = 1
                ScheduledBookingSingleton.getInstance().isGlobalBooking = 0
            }
        binding!!.ElderCare.setOnClickListener {
                restrictDoubleTap(it)
                ScheduledBookingSingleton.clearAllValues()
                datePicker()
                ScheduledBookingSingleton.getInstance().isElderlyFlow = true
                ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
                ScheduledBookingSingleton.getInstance().longitude = myLng.toString()
                ScheduledBookingSingleton.getInstance().isVirtualBooking = 0
                ScheduledBookingSingleton.getInstance().isGlobalBooking = 0
            }
        binding!!.location.setOnClickListener{
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_COLLAPSED)
            /*startActivityForResult(
                    Intent(requireContext(), SearchLocationActivity::class.java),
                    Constants.RequestCode.LOCATION_REQUEST
            )*/
        }
        binding!!.promotions.setOnClickListener{
            showComingSoon()
        }
        /* consultDoctor.setOnClickListener {
        restrictDoubleTap(consultDoctor)
        ScheduledBookingSingleton.clearAllValues()
        if (location.text.toString() != getString(R.string.fetching)) {
            ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
            ScheduledBookingSingleton.getInstance().longitude = myLng.toString()
            ScheduledBookingSingleton.getInstance().location = location.text.toString().trim()

            homeData?.speciality?.let {
                startActivity(
                        Intent(requireContext(), ListSpecialityActivity::class.java)
                                .putExtra(Constants.IntentKeys.SPECIALITYLIST, it)
                )
            }

        }

    }*/
        /*pickCurrentLocation.setOnClickListener {
                startActivityForResult(
                    Intent(requireContext(), SearchLocationActivity::class.java),
                    Constants.RequestCode.LOCATION_REQUEST
                )
            }*/
        /* myRecentConsultation.setOnClickListener {
                restrictDoubleTap(it)
                if (sharedHelper?.loggedIn!!)
                    startActivity(
                            Intent(requireContext(), ConsultationActivity::class.java)
                                    .putExtra(Constants.IntentKeys.CONSULTATIONTYPE, 1)
                                    .putExtra(
                                            Constants.IntentKeys.HEADER,
                                            getString(R.string.recent_consultation)
                                    )
                    )
                else
                    pleaseLogin()
            }*/
        /*   requireActivity().current_location.setOnClickListener {
            Log.d("second","sdsdvsd");
            getUserLocation()
        }*/

    }

    private fun reloadApp() {
        val intent = Intent(requireContext(), DashBoardActivity::class.java)
        startActivity(intent)

    }

    private fun setArticleAdapter(listSpeciality: ArrayList<ArticlesData>) {

        var articleAdapter =
            HomeArticleAdapter(requireContext(), listSpeciality, object : OnClickListener {
                override fun onClickItem(position: Int) {
                    requireContext().startActivity(
                            Intent(requireContext(), ArticleActivity::class.java)
                                    .putExtra(Constants.IntentKeys.ARTICLEID, listSpeciality[position].id)
                                    .putExtra(Constants.IntentKeys.ARTICLELIST, listSpeciality)
                    )
                }

            })
        binding!!.articleView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding!!.articleView.adapter = articleAdapter

    }

    private fun setHomeAdapter(listBanner: ArrayList<BannerImageData>) {

        var homeAdapter = HomeHeaderAdapter(requireContext(), listBanner)
        binding!!.viewPager.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        binding!!.viewPager.adapter = homeAdapter

        binding!!.viewPager.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, scrollState: Int) {
            }

            override fun onScrolled(recyclerView: RecyclerView, i: Int, i2: Int) {
              //  Log.d("Value", "SCroll Called")

                val childCount = binding!!.viewPager.childCount
                val width = binding!!.viewPager.getChildAt(0).width
                val padding = (binding!!.viewPager.width - width) / 2

                for (j in 0 until childCount) {

                    if (binding!!.viewPager.currentPosition == j) {

//                        recyclerView.getChildAt(j).alpha = 0.7f
                        recyclerView.getChildAt(j).animate()
                                .alpha(1f)
                                .setDuration(1000)
                                .setListener(null)

                    } else {
//                        recyclerView.getChildAt(j).alpha = 1f
                        recyclerView.getChildAt(j).animate()
                                .alpha(0.7f)
                                .setDuration(1000)
                                .setListener(null)
                    }

                    val v = recyclerView.getChildAt(j)
                    var rate = 0f
                    if (v.left <= padding) {
                        rate = if (v.left >= padding - v.width) {
                            (padding - v.left) * 1f / v.width
                        } else {
                            1f
                        }
                        v.scaleY = 1 - rate * 0.1f
                        v.scaleX = 1 - rate * 0.1f

                    } else {
                        if (v.left <= recyclerView.width - padding) {
                            rate = (recyclerView.width - padding - v.left) * 1f / v.width
                        }
                        v.scaleY = 0.9f + rate * 0.1f
                        v.scaleX = 0.9f + rate * 0.1f
                    }
                }
            }
        })
        binding!!.viewPager.addOnPageChangedListener(
                RecyclerViewPager.OnPageChangedListener
                { oldPosition, newPosition ->
                    //Log.d("test", "oldPosition:$oldPosition newPosition:$newPosition")
                })

        binding!!.viewPager.addOnLayoutChangeListener(View.OnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
           // Log.d("Value", "Called")
            if (binding!!.viewPager.childCount < 3) {
                if (binding!!.viewPager.getChildAt(1) != null) {
                    if (binding!!.viewPager.currentPosition == 0) {
                        val v1 = binding!!.viewPager.getChildAt(1)
                        v1.scaleY = 0.8f
                        v1.scaleX = 0.8f
                    } else {
                        val v1 = binding!!.viewPager.getChildAt(0)
                        v1.scaleY = 0.8f
                        v1.scaleX = 0.8f
                    }
                }
            } else {
                if (binding!!.viewPager.getChildAt(0) != null) {
                    val v0 = binding!!.viewPager.getChildAt(0)
                    v0.scaleY = 0.8f
                    v0.scaleX = 0.8f
                }
                if (binding!!.viewPager.getChildAt(2) != null) {
                    val v2 = binding!!.viewPager.getChildAt(2)
                    v2.scaleY = 0.8f
                    v2.scaleX = 0.8f
                }
            }

        })

        automaticSilder(true)
    }


    private fun datePicker() {

        Log.d("vbvbvbvbv2;",""+ sharedHelper!!.language)

        // Get Current Date
        var c = Calendar.getInstance()
        var mYear = c.get(Calendar.YEAR)
        var mMonth = c.get(Calendar.MONTH)
        var mDay = c.get(Calendar.DAY_OF_MONTH)


        var datePickerDialog: DatePickerDialog = DatePickerDialog(
                requireContext(), R.style.CalenderPickerTheme,
                DatePickerDialog.OnDateSetListener { p0, p1, p2, p3 ->
                    timePicker("$p1/${BaseUtils.numberFormat(p2 + 1)}/${BaseUtils.numberFormat(p3)}")
                }, mYear, mMonth, mDay
        )

        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.show()


    }

    private fun timePicker(date: String) {

        // Get Current Time
        var c = Calendar.getInstance();
        var mHour = c.get(Calendar.HOUR_OF_DAY);
        var mMinute = c.get(Calendar.MINUTE);

        Log.d("cncvdfx ", "" + date);
        Log.d("cncx ", "" + mHour);
        Log.d("cncxdfv ", "" + mMinute);

        Log.d("vbvbvbvbv3;",""+ sharedHelper!!.language)

        // Launch Time Picker Dialog
        var timePickerDialog: TimePickerDialog = TimePickerDialog(
                requireContext(), R.style.CalenderPickerTheme,
                TimePickerDialog.OnTimeSetListener { p0, p1, p2 ->
                    homeData?.speciality?.let {

                        var utctime = BaseUtils.getUtcTime(
                                "$date $p1:$p2",
                                "yyyy/MM/dd HH:mm"
                        )

                        Log.d("xmvbxdc", "" + utctime);
                        Log.d("xmvbxdc", "" + myLat);
                        Log.d("xmvbxdc", "" + myLng);


                        ScheduledBookingSingleton.getInstance().location = binding!!.location.text.toString()
                        ScheduledBookingSingleton.getInstance().latitude = myLat.toString()
                        ScheduledBookingSingleton.getInstance().longitude = myLng.toString()

                        var time = "$date $p1:$p2"

                        Log.d("cncxjhb", "" + time);


                        if (sharedHelper?.loggedIn!!){
                            utctime.let { date ->
                                Log.d("jcbdf hjbjvc", "" + date);

                                if (ScheduledBookingSingleton.getInstance().isVirtualBooking == 1) {

                                    if (BaseUtils.isValidTime(
                                            time,
                                            "yyyy/MM/dd HH:mm",
                                            sharedHelper?.calendarTiming!!
                                        )
                                    ) {
                                        Log.d("jcb hjbjvc", "" + date);
                                        ScheduledBookingSingleton.getInstance().bookingPeriod =
                                            BaseUtils.getFormatedDate(date, "yyyy/MM/dd HH:mm", "a")
                                        Log.d(
                                            "gghn1",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "a"
                                            )
                                        )
                                        ScheduledBookingSingleton.getInstance().bookingTime =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        Log.d(
                                            "gghn2",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        )

                                        ScheduledBookingSingleton.getInstance().bookingDate =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "yyyy/MM/dd HH:mm"
                                            )

                                        Log.d(
                                            "gghn3", "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "yyyy/MM/dd HH:mm"
                                            )
                                        )

                                        Log.d("yhfyhyfy",""+it)
                                        Log.d("vbvbvbvbv;4",""+ sharedHelper!!.language)

                                        startActivity(
                                            Intent(
                                                requireContext(),
                                                ChooseDetailsActivity::class.java
                                            )
                                                .putExtra(Constants.IntentKeys.SPECIALITYLIST, it)
                                        )
                                    } else {
                                        DialogUtils.showAlert(
                                            requireContext(),
                                            object : SingleTapListener {
                                                override fun singleTap() {

                                                }

                                            },
                                            getString(R.string.virtual_alert1) + " " + sharedHelper?.calendarTiming + " " + getString(
                                                R.string.virtual_alert2
                                            )
                                        )
                                    }

                                }
                                else if (ScheduledBookingSingleton.getInstance().isGlobalBooking == 1) {

                                    if (BaseUtils.isValidTime(
                                            time,
                                            "yyyy/MM/dd HH:mm",
                                            sharedHelper?.calendarTiming!!
                                        )
                                    ) {
                                        Log.d("jcb hjbjvc", "" + date);
                                        ScheduledBookingSingleton.getInstance().bookingPeriod =
                                            BaseUtils.getFormatedDate(date, "yyyy/MM/dd HH:mm", "a")
                                        Log.d(
                                            "gghn1",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "a"
                                            )
                                        );
                                        ScheduledBookingSingleton.getInstance().bookingTime =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        Log.d(
                                            "gghn2",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        );

                                        ScheduledBookingSingleton.getInstance().bookingDate =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "yyyy/MM/dd HH:mm"
                                            )

                                        Log.d(
                                            "gghn3", "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "yyyy/MM/dd HH:mm"
                                            )
                                        );

                                        Log.d("fffff",""+it)

                                        startActivity(
                                            Intent(
                                                requireContext(),
                                                ChooseDetailsActivity::class.java
                                            )
                                                .putExtra(Constants.IntentKeys.SPECIALITYLIST, it)
                                        )
                                    } else {
                                        DialogUtils.showAlert(
                                            requireContext(),
                                            object : SingleTapListener {
                                                override fun singleTap() {

                                                }

                                            },
                                            getString(R.string.virtual_alert1) + " " + sharedHelper?.calendarTiming + " " + getString(
                                                R.string.virtual_alert2
                                            )
                                        )
                                    }

                                }
                                else {

                                    if (BaseUtils.isValidTime(
                                            time,
                                            "yyyy/MM/dd HH:mm",
                                            0
                                        )
                                    ) {
                                        Log.d(
                                            "xcxcxc",
                                            "" + ScheduledBookingSingleton.getInstance().latitude + "dcdc" + ScheduledBookingSingleton.getInstance().longitude
                                        );
                                        ScheduledBookingSingleton.getInstance().bookingPeriod =
                                            BaseUtils.getFormatedDate(date, "yyyy/MM/dd HH:mm", "a")
                                        Log.d(
                                            "gghn1",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "a"
                                            )
                                        );
                                        ScheduledBookingSingleton.getInstance().bookingTime =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        Log.d(
                                            "gghn2",
                                            "" + BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "HH:mm"
                                            )
                                        );

                                        ScheduledBookingSingleton.getInstance().bookingDate =
                                            BaseUtils.getFormatedDate(
                                                date,
                                                "yyyy/MM/dd HH:mm",
                                                "yyyy/MM/dd HH:mm"
                                            )

                                        Log.d("vcvcvc",""+it)

                                        Log.d("vbvbvbvbv;5",""+ sharedHelper!!.language)


                                        startActivity(
                                            Intent(
                                                requireContext(),
                                                ChooseDetailsActivity::class.java
                                            )
                                                .putExtra(Constants.IntentKeys.SPECIALITYLIST, it)
                                        )
                                    } else {
                                        DialogUtils.showAlert(
                                            requireContext(),
                                            object : SingleTapListener {
                                                override fun singleTap() {

                                                }

                                            },
                                            getString(R.string.booking_alert_past)
                                        )
                                    }

                                }
                            }
                        }
                        else {
                            pleaseLogin()
                        }

//                    if (p1 <= 11) {
//                        if (p1 == 0) {
//                            ScheduledBookingSingleton.getInstance().bookingPeriod = "AM"
//                            ScheduledBookingSingleton.getInstance().bookingTime =
//                                "12:${BaseUtils.numberFormat(p2)}"
//                        } else {
//                            ScheduledBookingSingleton.getInstance().bookingPeriod = "AM"
//                            ScheduledBookingSingleton.getInstance().bookingTime =
//                                "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
//                        }
//
//                    } else {
//                        ScheduledBookingSingleton.getInstance().bookingPeriod = "PM"
//                        if (p1 < 13) {
//                            ScheduledBookingSingleton.getInstance().bookingTime =
//                                "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
//                        } else {
//                            ScheduledBookingSingleton.getInstance().bookingTime =
//                                "${BaseUtils.numberFormat(p1 - 12)}:${BaseUtils.numberFormat(p2)}"
//                        }
//
//                    }


                    }

                }, mHour, mMinute, false

        )
        timePickerDialog.show();
    }


    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                        Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {
        GpsUtils(requireActivity()).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                    Log.d("test2","sdfvsdv")
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {


            fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                // Got last known location. In some rare situations this can be null.

                if (isAdded) {
                if (location != null) {
                    Log.d("test3", "ewfwe")
                    sharedHelper?.currentLat = location.latitude.toString()
                    sharedHelper?.currentLng = location.longitude.toString()
                    Log.d("zzsdfghjkl",""+sharedHelper?.currentLat)
                    Log.d("zzsdfghjkl",""+sharedHelper?.currentLng)

                    searchPlaces()
                    /* if (sharedHelper?.location.toString().isNotEmpty()) {
                    // this is for add address seleted comment
                    myLat = location.latitude
                    myLng = location.longitude
                    searchPlaces()
                }
                else {
                    Log.d("test5","fffvdsd")
                       // myLat = location.latitude
                       // myLng = location.longitude

                    sharedHelper?.currentLat = location.latitude.toString()
                    sharedHelper?.currentLng = location.longitude.toString()
                    searchPlaces()
                }*/

                } else {
                    Log.d("test4", "efew")
                    fusedLocationClient?.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        null
                    )

                }
            }

        }
    }

    private fun locationListner() {
        Log.d("test10","wefwe")

        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        Log.d("test11","dssd")
                        if (myLat == 0.0 && myLng == 0.0)
                            Log.d("test12","fvdfv")
                        getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    fun onFragmentResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                if (!isDetached)
                    getLastKnownLocation()
        }
    }


    private fun searchPlaces() {
        Log.d("test6","vxcv")
        val url = UrlHelper.getAddress(sharedHelper?.currentLat!!.toDouble(),  sharedHelper?.currentLng!!.toDouble())
        val inputForAPI = ApiInput()
        Log.d("wertyuio",""+inputForAPI)
        inputForAPI.context = requireContext()
        Log.d("ddfghjkl",""+inputForAPI.context)

        inputForAPI.url = url
        mapViewModel?.getAddress(requireContext(), inputForAPI)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                it.let { responses ->
                    responses.error?.let { errorValue ->
                        if (!errorValue) {
                            responses.predictions?.let { value ->
                                getAddressFromResponse(value)
                            }
                        }
                    }

                }

            })
    }

    private fun getAddressFromResponse(value: List<Prediction>) {
        if (value.isNotEmpty()) {

            value[0].formatted_address?.let {
                Log.d("test7","dvgdf")
                //location.text = it
                Log.d("ygggb", "" + it);
                (activity as DashBoardActivity?)!!.binding!!.popupLoc.currentLocation.setText(it)
                if(sharedHelper!!.selectedaddressid==0){
                    binding!!.location.setText(it)
                    sharedHelper?.currentlocation  = it
                    (activity as DashBoardActivity?)!!.binding!!.popupLoc.currentLocation.setText(sharedHelper!!.currentlocation)


                    myLat = sharedHelper!!.currentLat.toDouble()
                    myLng = sharedHelper!!.currentLng.toDouble()
                    Log.d("qwertyu",""+myLat)
                    Log.d("wertyui",""+myLng)
                    sharedHelper!!.selectedLat = sharedHelper!!.currentLng
                    sharedHelper!!.selectedLng = sharedHelper!!.currentLng
                    Log.d("ujujhyuj",""+sharedHelper!!.selectedLat)
                    Log.d("okkiik",""+sharedHelper!!.selectedLng)
                    sharedHelper!!.seletedaddresslocation = sharedHelper!!.currentlocation
                }
                else{
                    binding!!.location.setText(sharedHelper!!.seletedaddresslocation)
                    myLng = sharedHelper!!.selectedLng.toDouble()
                    myLat = sharedHelper!!.selectedLat.toDouble()

                    Log.d("zsxdcfvgb",""+myLat)
                    Log.d("zsxdcfvgb",""+myLat)
                    Log.d("ereerree",""+myLng)
                }

            }

//            if (value[0].addressComponents?.size != 0) {
//                value[0].addressComponents?.get(0)?.long_name?.let { address ->
//                    location.text = address
//                }
//            }

        }
    }


    private fun showEmergencyDialog() {


        ScheduledBookingSingleton.clearAllValues()

        if (binding!!.location.text.toString().trim() != getString(R.string.fetching)) {

            ScheduledBookingSingleton.getInstance().bookingType = Constants.BookingType.AMBULANCE
            ScheduledBookingSingleton.getInstance().location = binding!!.location.text.toString().trim()
            ScheduledBookingSingleton.getInstance().latitude = myLat.toString().trim()

            ScheduledBookingSingleton.getInstance().longitude = myLng.toString().trim()


            homeData?.speciality?.let {
                for (i in 0 until it.size) {
                    if (it[i].name == Constants.BookingType.AMBULANCE) {
                        ScheduledBookingSingleton.getInstance().specialityId =
                            it[i].id?.toString()
                        ScheduledBookingSingleton.getInstance().dfee =
                            it[i].fee?.toString()
                    }
                }
            }


            //startActivity(Intent(requireContext(), SummaryEmergencyConsultActivity::class.java))
            startActivity(Intent(requireContext(), EmergencyMapBooking::class.java))

        }


    }

    private fun showSuccessDialog() {

        DialogUtils.showSuccessBookingDialog(requireContext(), object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.your_emergency_nbooking_has_been_created))
    }

    private fun pleaseLogin() {


        DialogUtils.showAlertDialog(requireContext(),
                getString(R.string.please_logintoaccess),
                getString(R.string.alert),
                getString(R.string.cancel),
                getString(R.string.login),
                object : DialogCallBack {
                    override fun onPositiveClick() {



                    }

                    override fun onNegativeClick() {
                        startActivity(
                            Intent(requireActivity(), LoginActivity::class.java)
                                .putExtra(Constants.IntentKeys.FINISHACT, true)
                        )
                    }
                })


    }

    fun restrictDoubleTap(view: View) {
        view.isEnabled = false
        Handler().postDelayed({
            view.isEnabled = true
        }, 1000)
    }

    fun showComingSoon() {

        DialogUtils.showAlertWithHeader(requireContext(), object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.comingsoon), getString(R.string.alert))
    }



}