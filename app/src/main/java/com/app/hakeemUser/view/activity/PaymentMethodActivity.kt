package com.app.hakeemUser.view.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityPaymentMethodBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.CompletedData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*
import com.paypal.android.sdk.payments.PaymentActivity.EXTRA_PAYMENT
import org.json.JSONException
import org.json.JSONObject
import java.math.BigDecimal

class PaymentMethodActivity : BaseActivity() {

    var binding: ActivityPaymentMethodBinding? = null
    var viewmodel: BookingViewModel? = null
    var completedData: CompletedData? = null
    var ratingDialog: Dialog? = null
    var config: PayPalConfiguration? = null
    var amountToBePaid: Int? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentMethodBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_method)
        viewmodel = ViewModelProvider(this).get(BookingViewModel::class.java)

        getIntentValues()
        config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(getString(R.string.paypal_client_id))
        startServicePaypal()
        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

    }

    private fun startServicePaypal() {

        val intent = Intent(this, PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        startService(intent)

    }

    private fun processPayment(amount: Int) {

        val payPalPayment = PayPalPayment(
            BigDecimal(amount.toString()), "USD",
            "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(this, PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
        startActivityForResult(intent, Constants.RequestCode.PAYPAL_REQUEST_CODE)
    }


    private fun getIntentValues() {

        intent.extras?.let { bundle ->

            bundle.getParcelable<CompletedData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { data ->

                completedData = data
                data.fee?.let {
                    amountToBePaid = if (data.extraFee != null) {
                        it.toInt() + data.extraFee!!.toInt()
                    } else {
                        it.toInt()
                    }

                }
            }
        }
    }

    fun onWalletClicked(view: View) {
        scheduleBooking("wallet")
    }

    fun onPayfordClicked(view: View) {
        amountToBePaid?.let {

            DialogUtils.showAlertDialog(this,
                getString(R.string.payment_confiration),
                getString(R.string.invoice),
                getString(R.string.ok),
                getString(R.string.cancel),
                object : DialogCallBack {
                    override fun onPositiveClick() {

                        processPayment(it)
                    }

                    override fun onNegativeClick() {

                    }
                })


        }

    }

    fun onPayOnVisitClicked(view: View) {
        scheduleBooking("cod")
    }

    private fun showSuccessDialog() {

        DialogUtils.showSuccessDialog(this, object : SingleTapListener {
            override fun singleTap() {

                val intent = Intent(this@PaymentMethodActivity, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()

                showRatingDialog()
            }
        })
    }

    private fun showRatingDialog() {

        ratingDialog = DialogUtils.getBookingRatingDialog(this)

        var doctorName = ratingDialog?.findViewById<TextView>(R.id.doctorName)
        var ratingBar = ratingDialog?.findViewById<RatingBar>(R.id.ratingBar)
        var docImage = ratingDialog?.findViewById<ImageView>(R.id.docImage)
        var comment = ratingDialog?.findViewById<EditText>(R.id.comment)
        var submit = ratingDialog?.findViewById<CardView>(R.id.submit)
        var dialogRoot = ratingDialog?.findViewById<ConstraintLayout>(R.id.dialogRoot)


        doctorName?.text =
            "${getString(R.string.rate_you_consultation_with)} ${completedData?.providerName}"

        completedData?.profilePic?.let {
            UiUtils.loadImage(docImage, it, getDrawable(R.drawable.place_holder_doctor)!!)
        }

        submit?.setOnClickListener {
            if (ratingBar?.rating != 0.0f) {
                if (comment?.text.toString().trim() != "") {

                    postReview(ratingBar?.rating, comment?.text.toString().trim())

                } else {
                    dialogRoot?.let { UiUtils.showSnack(it, getString(R.string.rate_consultation)) }
                }
            } else {
                dialogRoot?.let {
                    UiUtils.showSnack(
                        it,
                        getString(R.string.provider_rating)
                    )
                }
            }
        }

        ratingDialog?.show()
    }

    private fun postReview(rating: Float?, comment: String) {

        DialogUtils.showLoader(this@PaymentMethodActivity)
        viewmodel?.rateConsultation(
            this@PaymentMethodActivity,
            rating,
            comment,
            completedData?.bookingId
        )
            ?.observe(this@PaymentMethodActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            showSuccessDialog()
                        }
                    }

                }
            })
    }

    private fun scheduleBooking(paymentMode: String) {


        DialogUtils.showAlertDialog(this,
            getString(R.string.payment_confiration),
            getString(R.string.invoice),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    completedData?.let {

                        DialogUtils.showLoader(this@PaymentMethodActivity)
                        viewmodel?.payAmount(this@PaymentMethodActivity, paymentMode, it.bookingId)
                            ?.observe(this@PaymentMethodActivity, Observer {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(binding!!.root, msg)
                                            }
                                        } else {
                                            showRatingDialog()
                                        }
                                    }

                                }
                            })

                    }
                }

                override fun onNegativeClick() {

                }
            })


    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.PAYPAL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.let {
                val confirmation: PaymentConfirmation =
                    it.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
                if (confirmation != null) {
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString(4)

                        try {
                            var paymentDeatils: JSONObject =
                                JSONObject(paymentDetails).get("response") as JSONObject
                            if (paymentDeatils.get("state").toString().equals("approved", true)) {
                                payUsinpPaymentGateWay(paymentDeatils.get("id").toString(),"paypal")
                            }
                        } catch (e: Exception) {

                        }


                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }

    private fun payUsinpPaymentGateWay(tnx_id: String, payment_type: String) {

        completedData?.let {

            DialogUtils.showLoader(this@PaymentMethodActivity)
            viewmodel?.payAmount(this@PaymentMethodActivity, payment_type, it.bookingId,tnx_id)
                ?.observe(this@PaymentMethodActivity, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {
                                showRatingDialog()
                            }
                        }

                    }
                })

        }

    }


}