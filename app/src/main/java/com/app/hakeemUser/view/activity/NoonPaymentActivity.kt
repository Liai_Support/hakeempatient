package com.app.hakeemUser.view.activity

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityPaymentBinding
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.BookingViewModel


class NoonPaymentActivity : BaseActivity() {

    var binding: ActivityPaymentBinding? = null
    var viewModel: BookingViewModel? = null
    var sharedHelper: SharedHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_payment)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment)
        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        sharedHelper = SharedHelper(this)

        getIntentValues()
       // val profileName = intent.getStringExtra("amount")

    }

    private fun getIntentValues() {
        val bundle = intent.extras
        if (bundle != null) {
            val amount = bundle.getDouble("amount", 0.0)
            Log.d("xcxhv", "" + amount)
            if (amount != 0.0) {
                viewModel?.payment(this, amount)?.observe(this, androidx.lifecycle.Observer {
                    it?.let {
                        it.rcode?.let { rcode ->
                            if (rcode != 0) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.webView, msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    Log.d("xcxvxv", "" + data.cdata!!.posturl)

                                    if (data.cdata != null) {
                                        loadUrl(data.cdata!!.posturl!!,this)
                                    }
                                }
                            }
                        }

                    }
                })

            }else{
                UiUtils.showSnack(findViewById(android.R.id.content), getString(R.string.invalidamount))

            }
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUrl(url: String, noonPaymentActivity: NoonPaymentActivity) {
        binding!!.webView.settings.javaScriptEnabled = true
        binding!!.webView.settings.loadWithOverviewMode = true
        binding!!.webView.settings.useWideViewPort = true
        binding!!.webView.settings.builtInZoomControls = true
        binding!!.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {

            }
            override fun onPageFinished(view: WebView, url: String) {
                try {
                    Log.d("URLLOADING ", url)
                    val paymentorderid = Uri.parse(url).getQueryParameter("orderId")
                    Log.d("chvhzcb ",""+paymentorderid)
                    if (url.contains("https://hakeem.com.sa/")) {
                        if(paymentorderid != null){
                            Log.d("vnmvmnnm ","jhjedhjsdhjds")

                            procees(url,paymentorderid.toString())
                        }else{
                          UiUtils.showSnack(binding!!.root,getString(R.string.paymentfailed))
                        }
                    }

                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }

        //Load url in webView
        binding!!.webView.loadUrl(url)
    }

    fun procees(url: String, paymentorderid: String) {
        viewModel?.payment1(this, paymentorderid!!)?.observe(this, androidx.lifecycle.Observer {
            it?.let {
                Log.d(" ccvcvc ","sssssss")
                it.rcode?.let { rcode ->
                    if (rcode != 0) {
                        it.message?.let { msg ->
                            Log.d("cvcvccvcv ","vbvbbvvb")

                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let {data->

                            Log.d("wertyhujikl","hjcfdhdhjdjh")
                            if (!data.order!!.status!!.equals("CANCELLED")&& !data.order!!.status!!.equals("REJECTED") && data.order!!.status!!.equals("INITIATED")||data.order!!.status!!.equals("AUTHORIZED")){

                                Log.d("enter","enter")

                                viewModel?.payment2(this@NoonPaymentActivity, paymentorderid!!)?.observe(this@NoonPaymentActivity, androidx.lifecycle.Observer {
                                    it?.let {
                                        it.rcode?.let { rcode ->
                                            if (rcode != 0) {
                                                it.message?.let { msg ->
                                                    UiUtils.showSnack(binding!!.root, msg)
                                                    Log.d("vnmvmnnm ","vbvbbvvb")

                                                }
                                            } else {
                                                Log.d("vnmvmnnm","bnbnnbbnbn")

                                                val paymenttransactionid = it.result?.transaction?.id
                                                if (url.contains("https://hakeem.com.sa/")) {
                                                    setResult(RESULT_OK,intent.putExtra("paymenttransactionid",paymenttransactionid.toString()).putExtra("paymentorderid",paymentorderid.toString()))
                                                    finish()
                                                }
                                            }
                                        }

                                    }
                                })

                            }else{
                                UiUtils.showSnack(binding!!.root,getString(R.string.paymentfailed))

                                Log.d("exit","exit")
                                setResult(RESULT_CANCELED)
                                finish()
                            }


                        }
                    }
                }

            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(RESULT_CANCELED)
        finish()
    }
}