package com.app.hakeemUser.view.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityLablistBinding
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.LabListAdapter
import com.app.hakeemUser.view.adapter.TestlistAdapter
import com.app.hakeemUser.viewmodel.MembersViewModel
import org.json.JSONArray
import java.util.*


class LablistActivity : BaseActivity() {
    var binding: ActivityLablistBinding? = null
    private var membersViewModel: MembersViewModel? = null
    lateinit var textView: TextView
    var idarray: ArrayList<Int> = ArrayList()
    var sharedHelper: SharedHelper? = null
    var array = JSONArray()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLablistBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_lablist)
        sharedHelper = SharedHelper(this)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)
        gettestlist()

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
        binding!!.getlablist.setOnClickListener(View.OnClickListener { view ->
            if(idarray.isEmpty()==false){
                DialogUtils.showLoader(this)
                array = JSONArray()
                for(i in 0 until idarray.size){
                    array.put(idarray[i])
                }
                membersViewModel?.getlablist(this,array)
                    ?.observe(this,Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        binding!!.labList.visibility = View.GONE
                                        binding!!.labtxt.visibility = View.GONE
                                        UiUtils.showSnack(binding!!.rootLab, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        data.labdata?.let { list ->
                                            if(list.isEmpty()){
                                                binding!!.labList.visibility = View.GONE
                                                binding!!.labtxt.visibility = View.GONE
                                                UiUtils.showSnack(binding!!.rootLab, getString(R.string.labnotavailable))
                                            }
                                            else{
                                                binding!!.labList.visibility = View.VISIBLE
                                                binding!!.labtxt.visibility = View.VISIBLE
                                                binding!!.labList.layoutManager = LinearLayoutManager(this)
                                                binding!!.labList.adapter = LabListAdapter(this@LablistActivity,this, list)
                                            }

                                        }

                                    }

                                }
                            }


                        }

                    })
            }
            else{
                UiUtils.showSnack(binding!!.rootLab, getString(R.string.pleasechoosetestfirst))
            }
        })
    }

    private fun gettestlist(){
        DialogUtils.showLoader(this)
        membersViewModel?.gettestlistDetails(this)
            ?.observe(this,Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.rootLab, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.testDetails?.let { list ->
                                    binding!!.testList.layoutManager = LinearLayoutManager(this)
                                    binding!!.testList.adapter = TestlistAdapter(this@LablistActivity,this, list)

                                }

                            }
                        }


                    }

                }
            })

    }
    
    fun onBackPressed(view: View) {
        onBackPressed()
    }
}

