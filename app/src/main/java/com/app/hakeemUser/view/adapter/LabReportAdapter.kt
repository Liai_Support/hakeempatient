package com.app.hakeemUser.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildLabReportBinding
import com.app.hakeemUser.databinding.ChildMemberListHomeBinding
import com.app.hakeemUser.models.LabReportJson2
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.models.ReportData
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DashBoardActivity
import com.app.hakeemUser.view.activity.ViewLabReportActivity


class LabReportAdapter(
    var activity: ViewLabReportActivity,
    var context: Context,
    var list: ArrayList<ReportData>,
    var testnamee2: java.util.ArrayList<LabReportJson2>?

) :
    RecyclerView.Adapter<LabReportAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childLabReportBinding: ChildLabReportBinding = ChildLabReportBinding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LabReportAdapter.MyViweHolder {
        return MyViweHolder(
            LayoutInflater.from(context).inflate(R.layout.child_lab_report, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return testnamee2!!.size
    }

    override fun onBindViewHolder(holder: LabReportAdapter.MyViweHolder, position: Int) {


        holder.childLabReportBinding.report.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        holder.childLabReportBinding.report.adapter = ReportAdapter(activity,context, testnamee2!![position].Report)
        holder.childLabReportBinding.testname.text = ""+ testnamee2!![position].tesname
    }
}