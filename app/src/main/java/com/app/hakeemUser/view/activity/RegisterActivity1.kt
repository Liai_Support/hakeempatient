package com.app.hakeemUser.view.activity

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityRegister1Binding
import com.app.hakeemUser.models.ClassDetails
import com.app.hakeemUser.models.RegisterData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.AmazonViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import java.io.File
import java.util.ArrayList

class RegisterActivity1 : BaseActivity() {

    var binding: ActivityRegister1Binding? = null
    private var viewmodel: RegisterViewModel? = null
    private var amazonViewModel: AmazonViewModel? = null
    var sharedHelper: SharedHelper? = null
    var uploadFile: File? = null
    private var classData: ArrayList<ClassDetails> = ArrayList()

    var classtype: String? = null
    var classid: Int? = null
    var icname: String? = null
    var icnameid: Int? = null

    var cd1 : MutableList<String> = mutableListOf<String>()
    var cd2 : MutableList<Int> = mutableListOf<Int>()
    var cd3 : MutableList<String> = mutableListOf<String>()
    var cd4 : MutableList<Int> = mutableListOf<Int>()

            var mobileno:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegister1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_register1)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        sharedHelper = SharedHelper(this)
        setView()
        geticnamedata()
        // countryCodePicker1.setDefaultCountryUsingNameCode("SA")




        binding!!.mobileNumber.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {


                mobileno= binding!!.mobileNumber.text.toString()
                Log.d("xscdfvgbhj",""+mobileno)



                if (mobileno.equals("0")){
                    val imm: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                 Log.d("sderftgyhujikol","sdfrgtyuiop[")
                    UiUtils.showSnack(binding!!.root1, getString(R.string.typemobileno))

                }else{
                   mobileno= binding!!.mobileNumber.text.toString()
               }


            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
    }


    private fun setView() {
        binding?.dontHaveAcc1?.text =
                SpannableStringBuilder().append(resources.getString(R.string.already_have_an_acc))
                        .append(" ").append(
                                UiUtils.getBoldUnderlineString(
                                        this,
                                        resources.getString(R.string.signin_now)
                                )
                        )

        val spinner1 = findViewById<Spinner>(R.id.spinner_icname)
        cd3.add(0,getString(R.string.cicn)+" "+getString(R.string.optional))
        cd4.add(0,0)
        if (spinner1 != null) {
            val adapter = ArrayAdapter(this,
                R.layout.spinner_item, cd3)
            spinner1.adapter = adapter

            spinner1.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    icname = cd3[position]
                    icnameid = cd4[position]
                    getClassData(icnameid!!)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action

                }
            }
        }

    }

    fun onSignInClicked(view: View) {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    fun onBackPressed(view: View) {
       // onSignInClicked(dontHaveAcc1)
        super.onBackPressed()
    }

    fun onForgotPasswordClicked(view: View) {
        if (binding!!.emailId.text.toString().trim() == "") {
            UiUtils.showSnack(binding!!.root1,getString(R.string.pleaseenteryouremailid))
        } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
            UiUtils.showSnack(binding!!.root1, getString(R.string.plaeseentervallidemailid))
        } else {
            startActivity(
                    Intent(this, ForgotPasswordActivity::class.java)
                            .putExtra(Constants.IntentKeys.EMAIL, binding!!.emailId.text.toString().trim())
            )
        }
    }

    fun onRegisterClicked(view: View) {

        if (isValidInputs() && isInsuranceValid()) {
            if(uploadFile != null){
                DialogUtils.showLoader(this)
                amazonViewModel?.uploadImage(this, uploadFile!!)?.observe(this, Observer {
                    it.error?.let { value ->
                        if (!value) {
                            proceedSignIn(it.message)
                        } else {
                            DialogUtils.dismissLoader()
                            //proceedSignIn("nil")
                        }
                    }
                })
            }
            else{
                proceedSignIn("Nil")
            }
        }

    }

    private fun getClassData(icompanyid: Int) {
        DialogUtils.showLoader(this)
        viewmodel?.getclassdetails(this,icompanyid.toString())
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.classDetails?.let { list ->
                                    if (list.size != 0) {
                                        classid = 0
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        for ((index, value) in list.withIndex()) {

                                            println("the element at $index is $value")
                                            value.name?.let { it1 -> cd1.add(index+1, it1) }
                                            value.id?.let { it2 -> cd2.add(index+1, it2) }
                                        }


                                        val spinner = findViewById<Spinner>(R.id.spinner)

                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                R.layout.spinner_item, cd1)

                                            spinner.adapter = adapter

                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position]


                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action

                                                }
                                            }
                                        }
                                    }
                                    else{
                                        classid = 0
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        val spinner = findViewById<Spinner>(R.id.spinner)
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                R.layout.spinner_item, cd1)

                                            spinner.adapter = adapter

                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position]


                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action

                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }


    private fun geticnamedata() {
        DialogUtils.showLoader(this)
        viewmodel?.geticnamedetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.companyDetails?.let { list ->
                                    if (list.size != 0) {
                                        if(sharedHelper?.language.equals("en")){
                                            for ((index, value) in list.withIndex()) {
                                                println("the element at $index is $value")
                                                value.name?.let { it1 -> cd3.add(index+1, it1) }
                                                value.id?.let { it2 -> cd4.add(index+1, it2) }
                                            }
                                        }
                                        else{
                                            for ((index, value) in list.withIndex()) {
                                                println("the element at $index is $value")
                                                value.name_ar?.let { it1 -> cd3.add(index+1, it1) }
                                                value.id?.let { it2 -> cd4.add(index+1, it2) }
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }

                }
            })
    }


    private fun proceedSignIn(message: String?) {

        message?.let { pic ->
            viewmodel?.createAccount(
                binding!!.name.text.toString().trim(),
                binding!!.emailId.text.toString().trim(),
                binding!!.countryCodePicker1.selectedCountryCode,
                mobileno!!,
                binding!!.password.text.toString().trim(),
                binding!!.insurenceId.text.toString().trim(),
                    classid.toString().trim(),
                icnameid.toString().trim(),
                binding!!.sauthiId.text.toString().trim(),
                    pic
            )?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })
        }
    }

    private fun handleResponse(data: RegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }
        data.profilePic?.let { value -> sharedHelper?.userImage = value }
        data.istatus?.let { value -> sharedHelper?.istatus = value}

        sharedHelper?.loggedIn = true
        sharedHelper?.selectedaddressid = 0

        val intent =
                    Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
       // onSignInClicked(dontHaveAcc1)

    }

    private fun isValidInputs(): Boolean {
        when {

           /* uploadFile == null -> {
                UiUtils.showSnack(binding!!.root1, "Please upload profile picture")
                return false
            }*/

            binding!!.name.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourname))
                return false
            }
            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.plaeseentervallidemailid))
                return false
            }
            binding!!.mobileNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }
            binding!!.password.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourpassword))
                return false
            }
            binding!!.sauthiId.text.toString().trim() == "" ->{
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryoursaudiid))
                return false
            }
          /* binding!!.insurenceId.text.toString().trim() == "" -> {
                //UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourinsurance))
                return false
            }
            classid == 0 -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleasechooseinsuranceclass))
                return false
            }
            icnameid == 0 -> {
               UiUtils.showSnack(binding!!.root1, getString(R.string.pleasechooseinsurancecompany))
                return false
            }*/
            binding!!.confirmPassword1.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourconfirmpassword))
                return false
            }
            binding!!.confirmPassword1.text.toString().trim() != binding!!.password.text.toString().trim() -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.confirmpasswordshouldbesameaspassword))
                return false
            }
            else -> return true
        }
        return true
    }

    private fun isInsuranceValid(): Boolean {
        if(binding!!.insurenceId.text.toString().trim() != "" || !binding!!.insurenceId.text.toString().trim().equals("") || classid != 0 || icnameid != 0){
            when {
                binding!!.insurenceId.text.toString().trim() == "" -> {
                    UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourinsurance))
                    return false
                }
                classid == 0 -> {
                    UiUtils.showSnack(binding!!.root1, getString(R.string.pleasechooseinsuranceclass))
                    return false
                }
                icnameid == 0 -> {
                    UiUtils.showSnack(binding!!.root1, getString(R.string.pleasechooseinsurancecompany))
                    return false
                }
                else -> return true
            }
        }
        return true
    }

    fun onProfileEditClicked(view: View) {
        DialogUtils.showPictureDialog(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == RESULT_OK) {
                handleCamera()
            }

        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture1,
                        BaseUtils.getRealPathFromUriNew(this, uri),
                        ContextCompat.getDrawable(this, R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root1, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture1,
                    sharedHelper!!.imageUploadPath,
                    ContextCompat.getDrawable(this, R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root1, getString(R.string.unable_to_retrieve))
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {

            Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseUtils.openGallery(this)
            } else {
                UiUtils.showSnack(binding!!.root1, getString(R.string.storage_permission_error))
            }

            Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
            ) {
                BaseUtils.openCamera(this)
            } else {
                UiUtils.showSnack(binding!!.root1, getString(R.string.camera_permission_error))
            }
        }
    }



    fun ShowHidePass(view: View) {
        if(binding!!.password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){

            //Show Password
            binding!!.passBtn.setImageResource(R.drawable.pass_visibility_24);

            binding!!.password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            binding!!.passBtn.setImageResource(R.drawable.pass_visibility_off_24);

            //Hide Password
            binding!!.password.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }
    fun ShowHidePass2(view: View) {
        if(binding!!.confirmPassword1.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){

            //Show Password
            binding!!.confirmPassBtn.setImageResource(R.drawable.pass_visibility_24);

            binding!!.confirmPassword1.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            binding!!.confirmPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

            //Hide Password
            binding!!.confirmPassword1.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

}