package com.app.hakeemUser.view.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildPatientListBinding
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.utils.BaseUtils
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.ScheduledBookingSingleton
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.AddPatientDetailsActivity
import com.app.hakeemUser.view.activity.ChooseDetailsActivity

class SelectPatientAdapter(
    var activity: ChooseDetailsActivity,
    var context: Context,
    var list: ArrayList<MemberData>,
    var selected: (Int) -> Unit
) :
    RecyclerView.Adapter<SelectPatientAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: ChildPatientListBinding = ChildPatientListBinding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectPatientAdapter.MyViweHolder {
        return MyViweHolder(
            LayoutInflater.from(context).inflate(R.layout.child_patient_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SelectPatientAdapter.MyViweHolder, position: Int) {

        holder.childPatientListBinding.name.text = list[position].name
        holder.childPatientListBinding.patientType.text = list[position].type

        if(list[position].type.equals(context.getString(R.string.myself)) || list[position].type.equals("Myself")){
            //holder.childPatientListBinding.edit.visibility = View.GONE
            holder.childPatientListBinding.delete.visibility = View.GONE
        }

        if (list[position].isSelected == true) {
            holder.childPatientListBinding.selected.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.tick
                )
            )
        } else {
            holder.childPatientListBinding.selected.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.unselected_patient
                )
            )
        }

        holder.itemView.setOnClickListener {


            if(list[position].gender.equals("Nil",true)){
                UiUtils.showSnack(holder.itemView,context.getString(R.string.pleaseupdateyourprofile))
            }
            else if(list[position].dob.equals("Nil",true) || list[position].dob == null ||  list[position].dob!!.isEmpty()){
                UiUtils.showSnack(holder.itemView,context.getString(R.string.pleaseupdateyourprofile))
            }
            else{
                var dob = list[position].dob.toString()
                var year = BaseUtils.getFormatedDateUtc(dob.toString(), "yyyy-MM-dd", "yyyy")
                var month = BaseUtils.getFormatedDateUtc(dob.toString(), "yyyy-MM-dd", "MM")
                var day = BaseUtils.getFormatedDateUtc(dob.toString(), "yyyy-MM-dd", "dd")

                ScheduledBookingSingleton.getInstance().patientAge = BaseUtils.getAge(year!!.toInt(),month!!.toInt(),day!!.toInt())
                selected(position)
                for (i in 0 until list.size) {
                    list[i].isSelected = position == i
                }
                notifyDataSetChanged()
            }
          /*  if(list[position].adminapprove.equals("accepted",true)){
                selected(position)
                for (i in 0 until list.size) {
                    list[i].isSelected = position == i
                }
                notifyDataSetChanged()
            }
            else{
                UiUtils.showSnack(holder.itemView,context.getString(R.string.iiuv))
            }*/
        }

        holder.childPatientListBinding.delete.setOnClickListener {
            DialogUtils.showLoader(context)
            activity.membersViewModel?.deletemember(activity,list[position].memberId)
                ?.observe(activity, Observer{
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                     UiUtils.showSnack(holder.itemView, msg)
                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(holder.itemView, msg)
                                    activity.getmemeberlist()
                                }
                            }
                        }

                    }
                })
        }
        holder.childPatientListBinding.edit.setOnClickListener {
            activity.ispatientedited = true
            if(list[position].type.equals(context.getString(R.string.myself)) || list[position].type.equals("Myself")){
                activity.startActivity(
                    Intent(
                        activity,
                        AddPatientDetailsActivity::class.java
                    )
                        .putExtra("editmember", true)
                        .putExtra("myselfedit", true)
                        .putExtra(
                            "memberdata",
                            list[position] as Parcelable
                        )
                )
            }
            else {
                activity.startActivity(
                    Intent(
                        activity,
                        AddPatientDetailsActivity::class.java
                    )
                        .putExtra("editmember", true)
                        .putExtra("myselfedit", false)
                        .putExtra(
                            "memberdata",
                            list[position] as Parcelable
                        )
                )
            }
         }

    }
}