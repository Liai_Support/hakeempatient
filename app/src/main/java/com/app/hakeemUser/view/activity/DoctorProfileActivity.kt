package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityDashboard1Binding
import com.app.hakeemUser.databinding.ActivityDoctorProfileBinding
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.models.DoctorSpecialityList
import com.app.hakeemUser.models.ReviewData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.DcotorSpecialityAdapter
import com.app.hakeemUser.view.adapter.ReviewAdapter
import com.app.hakeemUser.viewmodel.BookingViewModel

class DoctorProfileActivity : BaseActivity() {

    var binding: ActivityDoctorProfileBinding? = null
    var viewmodel: BookingViewModel? = null
    var doctorData: DoctorData? = null
    public var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDoctorProfileBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
      //  binding = DataBindingUtil.setContentView(this, R.layout.activity_doctor_profile)

        viewmodel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        initListener()
        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

    }

    private fun initListener() {


        binding!!.bookNow.setOnClickListener {

            startActivity(
                Intent(
                    this,
                    SummaryDetailsBookingActivity::class.java
                )
                    .putExtra(Constants.IntentKeys.DOCTOR_DATA, doctorData as Parcelable)

            )


//            DialogUtils.showLoader(this)
//            viewmodel?.bookNewConsultation(this)?.observe(this, Observer {
//                DialogUtils.dismissLoader()
//                it?.let {
//                    it.error?.let { error ->
//                        if (error) {
//                            it.message?.let { msg ->
//                                UiUtils.showSnack(root, msg)
//                            }
//                        } else {
//                            showSuccessDialog()
//                        }
//                    }
//                }
//            })


        }
    }

//    private fun showSuccessDialog() {
//
//        DialogUtils.showSuccessDialog(this, object : SingleTapListener {
//            override fun singleTap() {
//                val intent = Intent(this@DoctorProfileActivity, DashBoardActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                startActivity(intent)
//                finish()
//            }
//        })
//    }

    private fun getIntentValues() {
        intent.extras?.let { bundle ->

            bundle.getParcelable<DoctorData>(Constants.IntentKeys.DOCTOR_DATA)?.let { data ->
                doctorData = data
                if(data.averageRating == null || data.averageRating!!.isEmpty()){
                    binding!!.ratingValue.visibility = View.GONE
                    binding!!.rating.visibility = View.GONE
                }
                else{
                    UiUtils.setRating(this, data.averageRating?.toFloat(), binding!!.rating, 35)
                    data.averageRating?.let { binding!!.ratingValue.text = "$it/5" }
                }


                data.status?.let {
                    if (it.equals("online", true))
                        binding!!.isOnline.visibility =
                            View.VISIBLE else binding!!.isOnline.visibility = View.GONE
                }
                data.name?.let { binding!!.doctorName.text = it }
                data.education?.let {
                    binding!!.qualification.text = it
                }

                if(data.expierence == null || data.expierence!!.isEmpty()){
                    binding!!.experience.visibility = View.GONE
                }
                else{
                    binding!!.experience.text =
                        data.expierence + " " + getString(R.string.year) + " " + getString(R.string.experience)
                }



                if(data.hospital == null || data.hospital!!.isEmpty()){
                    binding!!.hospital.visibility = View.GONE
                    binding!!.txtHospital.visibility = View.GONE
                }
                else{
                    data.hospital?.let { binding!!.hospital.text = it }
                }

                if(data.location == null || data.location!!.isEmpty()){
                    binding!!.location.visibility = View.GONE
                    binding!!.txtLocation.visibility = View.GONE
                }
                else{
                    data.location?.let { binding!!.location.text = it }
                }

                if(data.designation == null || data.designation!!.isEmpty()){
                    binding!!.designation.visibility = View.GONE
                    binding!!.txtDesignation.visibility = View.GONE
                }
                else{
                    data.designation?.let { binding!!.designation.text = it }

                }


                data.profilePic?.let {
                    UiUtils.loadImage(
                        binding!!.profileImage,
                        it,
                        ContextCompat.getDrawable(this, R.drawable.splash_image)!!
                    )
                }

                bundle.getParcelableArrayList<ReviewData>(Constants.IntentKeys.REVIEW_LIST)
                    ?.let { list ->

                        var adapter = ReviewAdapter(this, list)
                        binding!!.reviewView.layoutManager = LinearLayoutManager(this)
                        binding!!.reviewView.adapter = adapter

                        if(list.size == 0){
                            binding!!.textView43.visibility = View.GONE
                            binding!!.reviewView.visibility = View.GONE
                        }
                    }

                bundle.getParcelableArrayList<DoctorSpecialityList>(Constants.IntentKeys.SPECIALITYLIST)
                    ?.let { list ->
                        var adapter = DcotorSpecialityAdapter(this, list)
                        binding!!.specialityList.layoutManager = GridLayoutManager(this, 2)
                        binding!!.specialityList.adapter = adapter
                    }

                bundle.getBoolean(Constants.IntentKeys.BOOKINGFLOW).let {
                    if (it) {
                        binding!!.bookNow.visibility = View.VISIBLE
                    } else {
                        binding!!.bookNow.visibility = View.GONE
                    }
                }

            }

        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}