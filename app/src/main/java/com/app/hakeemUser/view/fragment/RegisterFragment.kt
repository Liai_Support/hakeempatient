package com.app.hakeemUser.view.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentConsultationBinding
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.databinding.FragmentRegisterBinding
import com.app.hakeemUser.interfaces.MoveToFragment
import com.app.hakeemUser.models.RegisterData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.AmazonViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import java.io.File


class RegisterFragment(
    var moveToFragment: MoveToFragment,
    var supportFragmentManager: FragmentManager
) : Fragment(R.layout.fragment_register) {

    var binding: FragmentRegisterBinding? = null
    private var viewmodel: RegisterViewModel? = null
    private var amazonViewModel: AmazonViewModel? = null
    var sharedHelper: SharedHelper? = null
    var uploadFile: File? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        setView()
        initListener()
        return view
    }

    private fun initListener() {

        binding!!.back.setOnClickListener {

            val manager =
                requireActivity().supportFragmentManager
            val trans: FragmentTransaction = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()

        }

        binding!!.editPic.setOnClickListener {
            DialogUtils.showPictureDialog(requireActivity())
        }

        binding!!.register.setOnClickListener {
            if (isValidInputs()) {
                DialogUtils.showLoader(requireContext())

                amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                    ?.observe(viewLifecycleOwner, Observer {


                        it.error?.let { value ->
                            if (!value) {
                                proceedSignIn(it.message)
                            } else {
                                DialogUtils.dismissLoader()
                            }
                        }
                    })

            }
        }
        binding!!.forgotPassword.setOnClickListener {
            if (binding!!.emailId.text.toString().trim() == "") {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
            } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
            } else {

                var bundle = Bundle()
                bundle.putString(Constants.IntentKeys.EMAIL, binding!!.emailId.text.toString().trim())

                moveToFragment.moveTo(Constants.IntentFragment.FORGOTPASSWORD, bundle)

            }
        }
        binding!!.forgotImage.setOnClickListener {
            if (binding!!.emailId.text.toString().trim() == "") {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
            } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
            } else {
                var bundle = Bundle()
                bundle.putString(Constants.IntentKeys.EMAIL, binding!!.emailId.text.toString().trim())

                moveToFragment.moveTo(Constants.IntentFragment.FORGOTPASSWORD, bundle)

//            startActivity(
//                Intent(this, ForgotPasswordActivity::class.java)
//                    .putExtra(Constants.IntentKeys.EMAIL, emailId.text.toString().trim())
//            )
            }
        }
        binding!!.dontHaveAcc.setOnClickListener {

            val manager =
                requireActivity().supportFragmentManager
            val trans: FragmentTransaction = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()

        }
    }


    private fun setView() {
        binding!!.dontHaveAcc?.text =
            SpannableStringBuilder().append(resources.getString(R.string.already_have_an_acc))
                .append(" ").append(
                    UiUtils.getBoldUnderlineString(
                        requireContext(),
                        resources.getString(R.string.signin_now)
                    )
                )
    }


    private fun proceedSignIn(message: String?) {

        message?.let {pic ->
            viewmodel?.createAccount(
                binding!!.name.text.toString().trim(),
                binding!!.emailId.text.toString().trim(),
                binding!!.countryCodePicker.selectedCountryCode,
                binding!!.mobileNumber.text.toString().trim(),
                binding!!.password.text.toString().trim(),"","","","",
                pic
            )?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })
        }
    }

    private fun handleResponse(data: RegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }

        val manager =
            requireActivity().supportFragmentManager
        val trans: FragmentTransaction = manager.beginTransaction()
        trans.remove(this)
        trans.commit()
        manager.popBackStack()

    }

    private fun isValidInputs(): Boolean {
        when {

            uploadFile == null -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseuploadprofilepicture))
                return false
            }

            binding!!.name.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourname))
                return false
            }
            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
                return false
            }
            binding!!.mobileNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }
            binding!!.password.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourconfirmpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() != binding!!.password.text.toString().trim() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.confirmpasswordshouldbesameaspassword))
                return false
            }
            else -> return true
        }

        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture,
                    BaseUtils.getRealPathFromUriNew(requireContext(), uri),
                    ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(requireContext(), uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    fun setonRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

    }

    fun setOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == RESULT_OK) {
                handleCamera()
            }

        }

    }

    fun setPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {

            Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseUtils.openGallery(requireActivity())
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
            }

            Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
            ) {
                BaseUtils.openCamera(requireActivity())
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
            }
        }

    }

}