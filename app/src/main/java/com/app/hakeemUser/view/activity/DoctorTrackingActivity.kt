package com.app.hakeemUser.view.activity

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.*
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityDashboard1Binding
import com.app.hakeemUser.databinding.ActivityTrackDoctorBinding
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.AppData
import com.app.hakeemUser.models.BookingData
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.rxbus.RxBusNotification
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.HomeViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import io.reactivex.disposables.Disposable
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.net.URISyntaxException
import java.util.*
import kotlin.collections.ArrayList
import com.google.android.gms.maps.model.LatLng





class DoctorTrackingActivity : AppCompatActivity(), OnMapReadyCallback {

    var binding: ActivityTrackDoctorBinding? = null

    var bookingViewModel: BookingViewModel? = null
    var homeViewModel: HomeViewModel? = null

    private var currentRoute: ArrayList<LatLng> = ArrayList()
    private val expectedTextValue = ""
    private var map: GoogleMap? = null

    private var status = ""

    private var blackPolyline: Polyline? = null
    private var blackTrackingPolyline: Polyline? = null
    var animationHelper: AnimationHelper? = null
    var mapFragment: MapFragment? = null

    var bookingID: Int? = null
    var providerId: Int? = null

    var data: BookingData? = null


    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private var myLat: Double? = null
    private var myLng: Double? = null

    private var source: LatLng? = null
    private var destination: LatLng? = null
    private lateinit var notificationEventListner: Disposable
    var myVehicleMarker: Marker? = null

    private var socket: Socket? = null

    private var sharedHelper = SharedHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrackDoctorBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_track_doctor)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        animationHelper = AnimationHelper()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapFragment = fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment?.getMapAsync(this)

        locationListner()
        askLocationPermission()
        getNotification()
        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

        val languageToLoad = sharedHelper.language

        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
        Log.d("vbvbvbvb",""+sharedHelper.language)


    }


    private fun getNotification() {

        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                runOnUiThread() {
                    if (!(this as Activity).isFinishing) { //show dialog
                        getIntentValues()
                    }
                }
            }

    }

    private fun isActivityRunning(activityClass: Class<*>): Boolean {
        val activityManager =
            baseContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val tasks =
            activityManager.getRunningTasks(Int.MAX_VALUE)
        for (task in tasks) {
            if (activityClass.canonicalName.equals(
                    task.baseActivity!!.className,
                    ignoreCase = true
                )
            ) return true
        }
        return false
    }


    override fun onResume() {

        Log.d("asdefrtghyujikol","ghcdbhdscbh")
        super.onResume()
        map?.let {
            getIntentValues()
        }
    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
//        opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
            Log.d("saghxdvsghdxv","exceptionoccired")
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
//            initSockets()
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }


        socket?.on("provider_$providerId") {
            runOnUiThread {

                if (data?.status == "startvisit") {

                    val value = JSONObject(it[0].toString())

                    addMarkerAndTrack(
                        value.optString(Constants.SocketKey.LATUTUDE),
                        value.optString(Constants.SocketKey.LONGITUDE)
                    )

                }

            }
        }

    }

    private fun addMarkerAndTrack(lat: String, lng: String) {


        if (myVehicleMarker == null) {

            if (lat != "" && lng != "") {
                val markerOptions = MarkerOptions()
                markerOptions.position(LatLng(lat.toDouble(), lng.toDouble())).flat(true)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.track))
                myVehicleMarker = map?.addMarker(markerOptions)
                animateMarker(LatLng(lat.toDouble(), lng.toDouble()))
            }

        } else {
            if (lat != "" && lng != "") {

                myVehicleMarker?.remove()

                val markerOptions = MarkerOptions()
                markerOptions.position(LatLng(lat.toDouble(), lng.toDouble())).flat(true)
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.track))
                myVehicleMarker = map?.addMarker(markerOptions)
                animateMarker(LatLng(lat.toDouble(), lng.toDouble()))


                animateMarker(LatLng(lat.toDouble(), lng.toDouble()))
            }

        }


    }

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }


    private fun getUserLocation() {

        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()

                }
            }
        })
    }

    private fun getLastKnownLocation(){

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                source = LatLng(
                    myLat!!,
                   myLng!!
                )

                Log.d("asdrftyuio",""+myLat)
                Log.d("asdrftyuio",""+myLng)

                getPolyLine()

            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest!!,
                    locationCallback,
                    null
                )

            }
        }

    }

    private fun calculateDiffDistance() {

        Log.d("source!!.latitude",""+source!!.latitude)
        Log.d("source!!.longitude",""+source!!.longitude)
        Log.d("destination!!.latitude",""+destination!!.latitude)
        Log.d("destination!!.longitude",""+destination!!.longitude)
        source?.let { src ->
            destination?.let { dest ->
                binding!!.kmAway.text =
                    "${BaseUtils.calculateDistance(
                        src.latitude,
                        src.longitude,
                        dest.latitude,
                        dest.longitude
                    )} km away"
            }
        }

//        myLat?.let { myLat ->
//            myLng?.let { myLng ->
//                providerLat?.let { proLat ->
//                    providerLng?.let { proLng ->
//
//                    }
//                }
//            }
//        }

    }


    private fun getIntentValues() {


        map?.clear()
        intent.extras?.let {
            getDetails(
                it.getInt(Constants.IntentKeys.BOOKINGID),
                it.getInt(Constants.IntentKeys.PROVIDERID)
            )


        }

    }

    private fun getDetails(bookingID: Int, patientId: Int) {

//        map?.clear()

        DialogUtils.showLoader(this)
        bookingViewModel?.getDetails(this, bookingID, patientId)?.observe(this, Observer {

            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            if (data.size != 0)
                                handleResponse(data, 0)
                        }
                    }
                }


            }

        })

    }

    private fun handleResponse(data: ArrayList<BookingData>, position: Int) {

        this.data = data[position]
        data[position].providerName?.let { binding!!.doctorName.text = it }
        data[position].education?.let { binding!!.qualification.text = it }
        if(data[position].specialityName != null){
            data[position].education?.let { binding!!.qualification.text = it+" - "+data[position].specialityName }
        }

        data[position].expierence?.let { binding!!.experience.text = it +" year experience" }
        data[position].averageRating?.let { binding!!.ratingValue.text = "$it/5" }
        UiUtils.setRating(this, data[position].averageRating?.toFloat(), binding!!.rating, 40)
        data[position].profilePic?.let {
            UiUtils.loadImage(
                binding!!.circleImageView2,
                it,
                ContextCompat.getDrawable(this, R.drawable.splash_image)!!
            )
        }
//        data[position].status?.let {
//            status = it
//
//            when {
//                status.equals("pending", true) -> {
//                    textButton.text = resources.getString(R.string.cancel_consultation)
//                }
//                status.equals("startvisit", true) -> {
////                    textButton.text = resources.getString(R.string.end_trip)
//                }
//                else -> {
//                    textButton.text = it
//                }
//            }
//        }

        bookingID = data[position].bookingId
        providerId = data[position].id

        if (data[position].bookingLatitude != null && data[position].bookingLongitude != null)
            source = LatLng(
                data[position].bookingLatitude?.toDouble()!!,
                data[position].bookingLongitude?.toDouble()!!
            )



        if (data[position].providerLatitude != null && data[position].providerLongitude != null) {
            destination = LatLng(
                data[position].providerLatitude?.toDouble()!!,
                data[position].providerLongitude?.toDouble()!!,


            )
        }

        Log.d("swnjwjwks",""+ data[position].distance)
        data[position].distance?.let {
            binding!!.kmAway.text = "${BaseUtils.numberFormat(it.toDouble())} km away"
        }


        initSockets()
        calculateDiffDistance()
        getPolyLine()

        data[position].status?.let {
            when {
                it.equals("pending", true) -> {
                    binding!!.statusText.text = resources.getString(R.string.cancel_consultation)
                }
                it.equals("startvisit", true) -> {
                    if (data[position].reachedDestination == "0") {
                        binding!!.statusText.text = resources.getString(R.string.start_visit)
                    } else {
                        binding!!.statusText.text = resources.getString(R.string.destination_reached)
                    }

                }
                it.equals("completed", true) -> {
                    binding!!.statusText.text = resources.getString(R.string.end_visit)
                    callAppSettings()
                }
                it.equals("paymentverified", true) -> {
                    binding!!.statusText.text = resources.getString(R.string.end_visit)
                    callAppSettings()
                }
                it.equals("cancelledbyprovider", true) -> {
                    binding!!.statusText.text = resources.getString(R.string.cancelled_by_provider)

                }
            }
        }
    }

    private fun callAppSettings() {

        homeViewModel?.getAppSettings(this)
            ?.observe(this, androidx.lifecycle.Observer {
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            /*it.message?.let { msg ->

                            }*/
                        } else {
                            it.data?.let { data ->
                                handleAppResponse(data)
                            }
                        }
                    }

                }
            })

    }

    private fun handleAppResponse(data: AppData) {
        var position = -1
        data.completed?.let {

            for (i in 0 until it.size) {
                if (it[i].bookingId == this.data?.bookingId) {
                    position = i
                    break
                }
            }

        }

        if (position != -1) {

            data.completed?.get(position)?.let {
                startActivity(
                    Intent(this, SummaryDetailsActivity::class.java)
                        .putExtra(
                            Constants.IntentKeys.PAYMENTDETAIL,
                            it as Parcelable
                        )
                        .putExtra(Constants.IntentKeys.BACKPRESSALLOWED, false)
                )
                finish()
            }
        }


    }

    private fun getPolyLine() {

        Log.d("vbvbvbvb",""+sharedHelper.language)
        Log.d("source",""+source)
        Log.d("destination",""+destination)

        if (source != null && destination != null) {
  /*             Log.d("wjksjksjsms","kakakakaka")
            source = LatLng(28.451307, 77.297691)
         //   destination = LatLng(80.307274, 13.184409)
            destination = LatLng(13.184409, 80.307274)*/
            bookingViewModel?.getRoute(this, source!!, destination!!)
                ?.observe(this,
                    Observer {
                        Log.d("dnmsdnmsnms","kanikashanmugam")
                        currentRoute = ArrayList()
                        currentRoute.add(source!!)
                        currentRoute.addAll(it.points)
                        currentRoute.add(destination!!)


                        generateMarker(source!!, destination!!)
                        Handler(Looper.getMainLooper()).postDelayed({
                            addRouteinMap(currentRoute, getLatLngBounds(source!!, destination!!))
                        }, 500)


                    })
        }
    }

    fun onBackPressed(view: View) {

        val intent = Intent(this, DashBoardActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
//        onBackPressed()
    }


    private fun cancelBooking() {

        bookingID?.let { it ->
            DialogUtils.showLoader(this)

            bookingViewModel?.cancelBooking(this, it)?.observe(this, Observer {

                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            finish()
                        }
                    }

                }

            })
        }
    }

    fun onCancelClicked(view: View) {
        finish()
    }

    private fun generateMarker(
        sourceLatLng: LatLng,
        destinationLatLng: LatLng
    ) {

        val markerOptions = MarkerOptions()
        markerOptions.title(sourceLatLng.latitude.toString() + " : " + sourceLatLng.longitude)

        map?.addMarker(
            MarkerOptions()
                .position(LatLng(sourceLatLng.latitude, sourceLatLng.longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.srcmarker))
        )

        map?.addMarker(
            MarkerOptions()
                .position(LatLng(destinationLatLng.latitude, destinationLatLng.longitude))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        )


    }


    private fun addRouteinMap(
        route: List<LatLng>,
        bounds: LatLngBounds
    ) {
//        if (blackPolyline != null) {
//            if (blackPolyline?.points?.size == 0) {
        generateBlackPolyline(route)
//            } else {
//                blackPolyline?.points = route
//            }
//        } else {
//            generateBlackPolyline(route)
//        }
//        generateGreyPolyline(route)

        Log.d("sjhhjshjshjs","kanikashanmaguagm")
        runOnUiThread {
            map?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20))


        }
        animatePolyLine(route)
    }


    private fun animatePolyLine(route: List<LatLng>) {
//        animationHelper?.animatePolyLine(blackPolyline, route)
    }


    private fun generateBlackPolyline(route: List<LatLng>) {
        val lineOptions = PolylineOptions()
        lineOptions.width(9f)
        lineOptions.color(ContextCompat.getColor(this, R.color.colorPrimary))
        lineOptions.startCap(SquareCap())
        lineOptions.endCap(SquareCap())
        lineOptions.jointType(JointType.ROUND)
        lineOptions.addAll(route)
        blackPolyline = map?.addPolyline(lineOptions)
        blackPolyline?.zIndex = 2f
    }


    private fun getLatLngBounds(
        sourceLatLng: LatLng,
        destinationLatLng: LatLng
    ): LatLngBounds {
        val builder = LatLngBounds.Builder()
        builder.include(sourceLatLng)
        builder.include(destinationLatLng)
        return builder.build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation()
        }
    }


    fun onCancelBookingClicked(view: View) {

        var cancelAmount = 0f

        data?.fee?.let { fee ->

            cancelAmount = if (sharedHelper.cancelInPercetage) {
                fee.toFloat() * (sharedHelper.cancelPercORAmt.toFloat() / 100f)
            } else {
                fee.toFloat() - sharedHelper.cancelPercORAmt.toFloat()
            }

            var content =
                BaseUtils.numberFormat(cancelAmount.toDouble()) + " " + getString(R.string.cancel_string)

            data?.status?.let {
                if (it.equals("pending", true)) {
                    DialogUtils.showAlertWithHeader(this, object : SingleTapListener {
                        override fun singleTap() {
                            cancelBooking()
                        }
                    }, content, getString(R.string.confirm))

                }
            }
        }

    }

    fun animateMarker(toPosition: LatLng?) {
        myVehicleMarker?.let {
            val startPosition: LatLng = it.getPosition()
            val latLngInterpolator: AnimationHelper.LatLngInterpolatorNew =
                AnimationHelper.LatLngInterpolatorNew.LinearFixed()
            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator.duration = 1000 // duration 3 second
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition: LatLng =
                        latLngInterpolator.interpolate(v, startPosition, toPosition)
                    it.setPosition(newPosition)
                    it.setAnchor(0.5f, 0.5f)
                } catch (ex: Exception) {
                    ex.printStackTrace()

                    Log.d("wsydghdwguw",""+ex)
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                }
            })
            valueAnimator.start()
            //            }
        }
    }

    override fun onMapReady(map0: GoogleMap) {
        map0?.let { map = map0 }
        getIntentValues()
    }

}