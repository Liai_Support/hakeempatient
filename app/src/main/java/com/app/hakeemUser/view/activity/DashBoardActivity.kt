package com.app.hakeemUser.view.activity

//import kotlinx.android.synthetic.main.activity_dashboard.drawer
//import kotlinx.android.synthetic.main.activity_dashboard.navigationDrawer
//import kotlinx.android.synthetic.main.activity_dashboard1.bottom_navigation_main
/*import kotlinx.android.synthetic.main.layout_home_drawer1.view.*
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_dashboard1.*
import kotlinx.android.synthetic.main.activity_dashboard1.drawer
import kotlinx.android.synthetic.main.activity_dashboard1.navigationDrawer
import kotlinx.android.synthetic.main.activity_register1.*
import kotlinx.android.synthetic.main.dialog_alert.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile1.*
import kotlinx.android.synthetic.main.layout_home_drawer.*
import kotlinx.android.synthetic.main.layout_home_drawer.endProfile
import kotlinx.android.synthetic.main.layout_home_drawer.logout
import kotlinx.android.synthetic.main.layout_home_drawer.patientId
import kotlinx.android.synthetic.main.layout_home_drawer.patientName
import kotlinx.android.synthetic.main.layout_home_drawer1.*
import kotlinx.android.synthetic.main.popup_location.*
import kotlinx.android.synthetic.main.select_patient_details.**/
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.os.Parcelable
import android.text.Layout
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityDashboard1Binding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.ProfileDetails
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.ListMemberhomeAdapter
import com.app.hakeemUser.view.fragment.*
import com.app.hakeemUser.viewmodel.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.util.*
import kotlin.collections.ArrayList


class DashBoardActivity : BaseActivity() {
    private var profileViewModel: ProfileViewModel? = null

    var binding: ActivityDashboard1Binding? = null
    private var profileFragment: ProfileFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    private var imageList = arrayListOf<ImageView>()
    public var homeFragment: HomeFragment? = null
    public var viewmodel: AddressViewModel? = null
    var doubleBackToExitPressedOnce = false
    private var homeViewModel: HomeViewModel? = null
    private var viewmodel1: RegisterViewModel? = null
    private var sharedHelper: SharedHelper? = null

    private var myLat: Double? = null
    private var myLng: Double? = null
    private lateinit var contain: FrameLayout
    private lateinit var imground: ImageView
    private lateinit var cons: ConstraintLayout
    private lateinit var lay: Layout
    private lateinit var vie: View
    public var membersViewModel: MembersViewModel? = null
    var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
    var cd1: ArrayList<String> = ArrayList()
    var cd2: ArrayList<Int> = ArrayList()
    var cd3: ArrayList<String> = ArrayList()
    var cd4: ArrayList<Int> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboard1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        // binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard1)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewmodel = ViewModelProvider(this).get(AddressViewModel::class.java)
        viewmodel1 = ViewModelProvider(this).get(RegisterViewModel::class.java)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)
        sharedHelper = SharedHelper(this)
        binding!!.navigationDrawer.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        Log.d("nbbbnn", "" + sharedHelper!!.language)

        if (sharedHelper!!.loggedIn == true) {
            updateDeviceToken()
            getmemeberlist()
        }


        val bottomSheet = findViewById<View>(R.id.bottom_sheet)
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        (mBottomSheetBehavior as BottomSheetBehavior<*>).setState(BottomSheetBehavior.STATE_HIDDEN)
        binding!!.popupLoc.closePop.setOnClickListener(View.OnClickListener {
            mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        })


        binding!!.bottomNavigationMain.itemIconTintList = null
        binding!!.bottomNavigationMain.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    setAdapter(0)
                    true
                }
                R.id.appoinment -> {
                    if (sharedHelper?.loggedIn!!)
                        setAdapter(1)
                    else
                        pleaseLogin()
                    true
                }
                R.id.wallet -> {
                    if (sharedHelper?.loggedIn!!)
                        setAdapter(2)
                    else
                        pleaseLogin()
                    true
                }
                R.id.account -> {
                    if (sharedHelper?.loggedIn!!)
                        setAdapter(3)
                    else
                        pleaseLogin()
                    true
                }
                else -> false
            }
        }
        contain = findViewById(R.id.container)
        val imground = findViewById(R.id.iv_add) as View
        cons = findViewById(R.id.cons)
        vie = findViewById(R.id.vi)

        //val mainview = findViewById(R.id.relative) as View


        imground.setOnTouchListener(object : OnSwipeTouchListener(this@DashBoardActivity) {
            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                // pop_loc.visibility = View.VISIBLE
                return super.onTouch(view, motionEvent)
            }

            override fun onSwipeLeft() {
                super.onSwipeLeft()
                /* Toast.makeText(this@DashBoardActivity, "Swipe Left gesture detected",
                        Toast.LENGTH_SHORT)
                        .show()*/
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
                /*Toast.makeText(
                        this@DashBoardActivity,
                        "Swipe Right gesture detected",
                        Toast.LENGTH_SHORT
                ).show()*/
            }

            override fun onSwipeUp() {
                super.onSwipeUp()
/* Toast.makeText(this@DashBoardActivity, "Swipe up gesture detected", Toast.LENGTH_SHORT)
                        .show()*/

                //  pop_loc.visibility = View.VISIBLE

                /* str = "Swipe Up";
                popup_loc2  = (RelativeLayout) findViewById(R.id.popup_loc);
                popup_loc2.setVisibility(View.VISIBLE);
              // botm_nav.setOnNavigationItemSelectedListener(null);*/
                //mBottomSheetBehavior.setPeekHeight(400);
                (mBottomSheetBehavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_COLLAPSED)
                closeDrawer()

            }

            override fun onSwipeDown() {
                super.onSwipeDown()
/* Toast.makeText(this@DashBoardActivity, "Swipe down gesture detected", Toast.LENGTH_SHORT)
                        .show()*/
                (mBottomSheetBehavior as BottomSheetBehavior<View>).setState(BottomSheetBehavior.STATE_HIDDEN)
                closeDrawer()
                //  pop_loc.visibility = View.INVISIBLE
            }


        })


        /*  pop_loc.setOnTouchListener(object : OnSwipeTouchListener(this@DashBoardActivity) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                Toast.makeText(this@DashBoardActivity, "Swipe Left gesture detected",
                        Toast.LENGTH_SHORT)
                        .show()
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
                Toast.makeText(
                        this@DashBoardActivity,
                        "Swipe Right gesture detected",
                        Toast.LENGTH_SHORT
                ).show()
            }

            override fun onSwipeUp() {
                super.onSwipeUp()
                Toast.makeText(this@DashBoardActivity, "Swipe up gesture detected", Toast.LENGTH_SHORT)
                        .show()
                pop_loc.visibility = View.VISIBLE
            }

            override fun onSwipeDown() {
                super.onSwipeDown()
                *//* Toast.makeText(this@DashBoardActivity, "Swipe down gesture detected", Toast.LENGTH_SHORT)
                         .show()*//*
                pop_loc.visibility = View.INVISIBLE
            }
        })

        close_pop.setOnClickListener(View.OnClickListener {
            pop_loc.visibility = View.INVISIBLE
        })*/




        if (intent.extras == null) {
            setAdapter(0)
            if (sharedHelper!!.loggedIn) {
                getmemeberlist()
                geticnamedata()
                getProfileData()
            }
        } else {
            var value = intent.extras!!.getInt(Constants.IntentKeys.HOMETYPE, 0)
            setAdapter(value)
        }



    }


    override fun onResume() {
        super.onResume()
        setLoginUi()
    }

    fun getmemeberlist() {
        DialogUtils.showLoader(this)
        membersViewModel?.getmemberDetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                // UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                for (item in data){
                                    if(item.type.equals("myself",true)){
                                        sharedHelper!!.isMyself = true
                                    }
                                }

                               /* binding!!.homeDrawer.memberRecycler.layoutManager =
                                    LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                                // binding!!.homeDrawer.memberRecycler.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true) )

                                binding!!.homeDrawer.memberRecycler.adapter =
                                    ListMemberhomeAdapter(this@DashBoardActivity, this, data)
                        */    }
                        }
                    }

                }
            })
    }

    fun geticnamedata() {
        DialogUtils.showLoader(this)
        viewmodel1?.geticnamedetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.companyDetails?.let { list ->
                                    if (list.size != 0) {
                                        cd3.add(0, getString(R.string.cicn)+" "+getString(R.string.optional))
                                        cd4.add(0, 0)
                                        if (sharedHelper?.language.equals("en")) {
                                            for ((index, value) in list.withIndex()) {
                                                Log.d("dscd", "" + index + "saa" + "" + value.name);
                                                println("the element at $index is $value")
                                                value.name?.let { it1 ->
                                                    cd3.add(
                                                        index + 1,
                                                        it1
                                                    )
                                                }
                                                value.id?.let { it2 -> cd4.add(index + 1, it2) }
                                            }
                                        } else {
                                            for ((index, value) in list.withIndex()) {
                                                Log.d(
                                                    "dscd",
                                                    "" + index + "saa" + "" + value.name_ar
                                                );
                                                println("the element at $index is $value")
                                                value.name_ar?.let { it1 ->
                                                    cd3.add(
                                                        index + 1,
                                                        it1
                                                    )
                                                }
                                                value.id?.let { it2 -> cd4.add(index + 1, it2) }
                                            }
                                        }
                                        sharedHelper!!.insurancecompanyname.clear()
                                        sharedHelper!!.insurancecompanyid.clear()
                                        sharedHelper!!.insurancecompanyname = cd3
                                        sharedHelper!!.insurancecompanyid = cd4
                                    } else {
                                        sharedHelper!!.insurancecompanyname.clear()
                                        sharedHelper!!.insurancecompanyid.clear()
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }

    /* override fun onClick(view: View?) {
         when(view?.id){
             R.id.iv_add -> {
                 // Toast.makeText(this, "Clicked!", Toast.LENGTH_SHORT).show()
                 pop_loc.visibility = View.VISIBLE
             }
         }
     }*/

    fun onRoundClicked(view: View) {
        //
        //  pop_loc.visibility = View.VISIBLE
    }

    private fun setLoginUi() {

        if (sharedHelper?.loggedIn!!) {
            binding!!.homeDrawer.logout.text = getString(R.string.logout)


            binding!!.homeDrawer.patientName.text = sharedHelper?.name
            // patientId.text = sharedHelper?.id.toString()
            // endProfile.text = getString(R.string.view_and_edit_profile)

            UiUtils.loadImage(binding!!.homeDrawer.circleImageView5, sharedHelper?.userImage)

        } else {

            binding!!.homeDrawer.patientId.text = ""
            binding!!.homeDrawer.endProfile.text = ""


            binding!!.homeDrawer.patientName.text = getString(R.string.guest_login)

            binding!!.homeDrawer.logout.text = getString(R.string.login)
        }
        binding!!.navigationDrawer.bringToFront()
    }

    /*public fun listaddress(){
        DialogUtils.showLoader(this)
        viewmodel?.getlistaddress(this)
                ?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root1, msg)
                                }
                            } else {
                                it.data?.let { data ->

                                    data.addressDetails?.let { list ->
                                        address_recycler.layoutManager = LinearLayoutManager(this)
                                        address_recycler.adapter = ListAddressAdapter(this, list)
                                    }


                                    *//*data.classDetails?.let { list ->
                                        if (list.size != 0) {
                                            for ((index, value) in list.withIndex()) {
                                                Log.d("dscd",""+index+"saa"+""+value.name);
                                                println("the element at $index is $value")
                                                value.name?.let { it1 -> cd1.add(index+1, it1) }
                                                value.id?.let { it2 -> cd2.add(index+1, it2) }
                                            }
                                        }
                                    }*//*

                                }
                            }
                        }

                    }
                })

    }*/

    private fun updateDeviceToken() {
        if (sharedHelper?.loggedIn!!)
            homeViewModel?.updateDeviceToken(this)
    }

    fun setAdapter(position: Int) {
        setUiTab(position)
        when (position) {
            0 -> {

                homeFragment = HomeFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.commit()


            }
            1 -> {
                val consultationFragment = ConsultationFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, consultationFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            2 -> {
                val myWalletFragment = MyWalletFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, myWalletFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
            3 -> {
                profileFragment = ProfileFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, profileFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }

        }
    }

    private fun setUiTab(position: Int) {
        for (i in 0 until imageList.size) {
            if (position == i) {
                imageList[i].background =
                    ContextCompat.getDrawable(this, R.drawable.tab_selected_background)
                imageList[i].setColorFilter(UiUtils.fetchAccentColor(this))
            } else {
                imageList[i].setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
                imageList[i].setColorFilter(ContextCompat.getColor(this, R.color.grey))
            }
        }
    }

    fun onCloseClicked(view: View) {
        closeDrawer()
    }

    fun onHomeClicked(view: View) {
        setAdapter(0)
    }

    /* fun onAppoinmentClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(1)
        else
            pleaseLogin()
        closeDrawer()

    }*/

    fun onWalletClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(2)
        else
            pleaseLogin()
        closeDrawer()

    }

    fun onRatingClicked(view: View) {
        closeDrawer()
        if (sharedHelper?.loggedIn!!) {
            DialogUtils.showRatingDialog(this, object : DialogCallBack {
                override fun onPositiveClick() {
                    launchMarket()
                }

                override fun onNegativeClick() {

                }

            })
        } else
            pleaseLogin()
    }

    private fun launchMarket() {
        val uri: Uri = Uri.parse("market://details?id=$packageName")
        val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(myAppLinkToMarket)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show()
        }
    }

    fun onAllDoctorsClicked(view: View) {
        startActivity(Intent(this, AllDoctorsActivity::class.java))
        closeDrawer()
    }

    fun onLanguageClicked(view: View) {
        startActivity(Intent(this, ChooseLanguageActivity::class.java))
        closeDrawer()
    }

    /* fun onAppointmentClicked(view: View) {
        closeDrawer()
        showComingSoon()
    }*/

    fun onAccountClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            setAdapter(3)
        else
            pleaseLogin()
        closeDrawer()
    }

    fun onHistoryConsultationClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            startActivity(
                Intent(this, ConsultationActivity::class.java)
                    .putExtra(Constants.IntentKeys.CONSULTATIONTYPE, 2)
                    .putExtra(Constants.IntentKeys.HEADER, getString(R.string.consultation_history))
            )
        else
            pleaseLogin()
        closeDrawer()
    }

    fun onDrawerMenuClicked(view: View?) {
        binding!!.drawer.openDrawer(GravityCompat.START)
        binding!!.navigationDrawer.bringToFront()
        mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
    }

    fun onNotificationClicked(view: View) {
        if (sharedHelper?.loggedIn!!)
            startActivity(Intent(this, NotificationActivity::class.java))
        else
            pleaseLogin()
    }


    /*override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            //finish()
            //moveTaskToBack(true);
            //exitProcess(-1)
            if (doubleBackToExitPressedOnce) {
                moveTaskToBack(true);
                exitProcess(-1)
               *//* val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                startActivity(intent)*//*
                return
            }

            doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }*/

    fun onLogoutClicked(view: View) {
        closeDrawer()

        sharedHelper?.loggedIn = false
        sharedHelper?.token = ""
        sharedHelper?.userImage = ""
        sharedHelper?.id = 0
        sharedHelper?.name = ""
        sharedHelper?.email = ""
        sharedHelper?.mobileNumber = ""
        sharedHelper?.countryCode = ""
        sharedHelper?.istatus = "true"

        val intent =
            Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }


    private fun closeDrawer() {
        if (binding!!.drawer.isDrawerOpen(GravityCompat.START)) {
            binding!!.drawer.closeDrawer(GravityCompat.START)
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == Constants.RequestCode.GPS_REQUEST || requestCode == Constants.RequestCode.LOCATION_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                homeFragment?.let {
                    it.onFragmentResult(requestCode, resultCode, data)
                }
            }
        }

        profileFragment?.onActivityResults(requestCode, resultCode, data)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        profileFragment?.onRequestPermissionResult(requestCode, permissions, grantResults)

    }

    private fun pleaseLogin() {

        DialogUtils.showAlertDialog(this,
            getString(R.string.login_feature),
            getString(R.string.please_login),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    startActivity(
                        Intent(this@DashBoardActivity, LoginActivity::class.java)
                        // .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )
                }

                override fun onNegativeClick() {

                }
            })

    }

    fun onAddPatientClicked(view: View) {
        if (sharedHelper?.loggedIn!!) {
            startActivity(Intent(this, AddPatientDetailsActivity::class.java)
                .putExtra("editmember", false)
                .putExtra("myselfedit", false)
            )
            closeDrawer()
        } else {
            pleaseLogin()
            closeDrawer()
        }
    }

    fun onPatientDetailsClicked(view: View) {
        if (sharedHelper?.loggedIn!!) {
            startActivity(Intent(this, PatientDetailsActivity::class.java))
            closeDrawer()
        } else {
            pleaseLogin()
            closeDrawer()
        }
    }

    fun onLabtextClicked(view: View) {
        closeDrawer()
        startActivity(
            Intent(this, LablistActivity::class.java)
        )
    }

    fun onPharmacyClicked(view: View) {
        closeDrawer()
        startActivity(
            Intent(this, PharmachylistActivity::class.java)
        )
    }

    fun onSocialMediaClicked(view: View) {
        closeDrawer()
        showComingSoon()
    }


    fun onContactusclick(view: View) {
        closeDrawer()
        showComingSoon()
    }

    fun onTermsClicked(view: View) {
        closeDrawer()
        //   showComingSoon()
        //https://hakeem.com.sa/terms-and-conditions/
        val intent = Intent(
            "android.intent.action.VIEW",
            Uri.parse("https://hakeem.com.sa/terms-and-conditions/")
        )
        startActivity(intent)


    }

    fun showComingSoon() {

        DialogUtils.showAlertWithHeader(this, object : SingleTapListener {
            override fun singleTap() {

            }
        }, getString(R.string.comingsoon), getString(R.string.alert))
    }

    private fun getProfileData() {
        DialogUtils.showLoader(this)
        profileViewModel?.getProfileDetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.profileDetails?.let { list ->
                                    if (list.size != 0) {
                                        handleResponse(list[0])
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ProfileDetails) {
        if (data.insurenceid == null || data.insurenceid.equals("") || data.insurenceid!!.isEmpty()) {
            sharedHelper?.isInsurance = false
        }
        else {
            sharedHelper?.isInsurance = true
        }
    }
}


