package com.app.hakeemUser.view.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityConsultationBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.PatientActionListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.*
import com.app.hakeemUser.notification.NotificationUtils
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.adapter.ConsultationAdapter
import com.app.hakeemUser.view.adapter.ConsultationHistoryAdapter
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.CallViewModel
import com.app.hakeemUser.viewmodel.ConsultationViewModel
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class ConsultationActivity : BaseActivity() {

    var binding: ActivityConsultationBinding? = null
    var viewmodel: ConsultationViewModel? = null
    var bookingViewModel: BookingViewModel? = null
    var callViewModel: CallViewModel? = null
    var flowType = 0
    var clickedPosition = -1
    var data: ArrayList<ConsultationData> = ArrayList()
    var sharedHelper = SharedHelper(this)
    var list2: java.util.ArrayList<CurrentBookingListJson>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConsultationBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_consultation)
        viewmodel = ViewModelProvider(this).get(ConsultationViewModel::class.java)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        callViewModel = ViewModelProvider(this).get(CallViewModel::class.java)
        getIntentValues()

        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.back.rotation= 180F
        }
    }

    private fun getIntentValues() {


        intent.extras?.let { it ->
            it.getInt(Constants.IntentKeys.CONSULTATIONTYPE).let { values ->
                flowType = values
                getConsultation(values)

            }

            it.getString(Constants.IntentKeys.HEADER)?.let { values ->
                binding!!.header.text = values
            }
        }


    }

    override fun onResume() {
        super.onResume()
        getConsultation(flowType)
    }

    private fun getConsultation(type: Int) {

        DialogUtils.showLoader(this)
        viewmodel?.getConsultation(this, type)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }

            }
        })

    }

    private fun handleResponse(data: ArrayList<ConsultationData>) {

        this.data = data
     //   Log.d("mkmkmkmkmkm",""+list.size)

        if (flowType == 1) {
    //        Log.d("kanikashank8ugam",""+list.size)



            binding!!.consultationList.layoutManager = LinearLayoutManager(this)
            binding!!.consultationList.adapter =
                ConsultationAdapter(this, list2!!, object : OnClickListener {
                    override fun onClickItem(position: Int) {

                        if (data[position].providerId == null) {


                            DialogUtils.showAlertDialog(this@ConsultationActivity,
                                getString(R.string.cancel_message),
                                getString(R.string.confirm),
                                getString(R.string.ok),
                                getString(R.string.cancel),
                                object : DialogCallBack {
                                    override fun onPositiveClick() {
                                        DialogUtils.showLoader(this@ConsultationActivity)
                                        bookingViewModel?.cancelBooking(
                                            this@ConsultationActivity,
                                            data[position].bookingId!!
                                        )
                                            ?.observe(this@ConsultationActivity,
                                                Observer {
                                                    DialogUtils.dismissLoader()
                                                    getConsultation(flowType)
                                                })
                                    }

                                    override fun onNegativeClick() {

                                    }

                                })

                        } else {

                            if (data[position].status.equals("completed", true) ||
                                data[position].status.equals("cancelledbyprovider", true) ||
                                data[position].status.equals("cancelledbypatient", true) ||
                                data[position].status.equals("paymentverified", true)
                            ) {

                                //do nothing
                            }
                            else {

                                if(data[position].is_ins.equals("1")){
                                    DialogUtils.showLoader(this@ConsultationActivity)
                                    viewmodel?.getInsuranceStatus(this@ConsultationActivity, data[position].bookingId!!)?.observe(this@ConsultationActivity, Observer {
                                        DialogUtils.dismissLoader()
                                        it?.let {
                                            it.error?.let { error ->
                                                if (error) {
                                                    it.message?.let { msg ->
                                                        UiUtils.showSnack(binding!!.root, msg)
                                                    }
                                                } else {
                                                    it.data?.let { data1 ->
                                                        if(data1[0].ins_status.equals("pending",true)){
                                                            UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                                        }
                                                        else if(data1[0].ins_status.equals("rejected",true)){
                                                            UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                                        }
                                                        else if(data1[0].ins_status.equals("accepted",true)){
                                                            if(data1[0].isPendingAmount.equals("yes",true)){
                                                                val balance =  data1[0].balance!!.toDouble()
                                                                DialogUtils.showAlertDialogPayment(this@ConsultationActivity,
                                                                    getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                                        getString(R.string.insurance),
                                                                    getString(R.string.ok),
                                                                    getString(R.string.cancel),
                                                                    object : DialogCallBack {
                                                                        override fun onPositiveClick() {
                                                                            startActivity(
                                                                                Intent(
                                                                                    this@ConsultationActivity,
                                                                                    VirtualPaymentActivity::class.java
                                                                                )
                                                                                    .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                                    .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                                    .putExtra("bookingid",data[position].bookingId)
                                                                            )
                                                                        }

                                                                        override fun onNegativeClick() {

                                                                        }

                                                                    })
                                                            }
                                                            else {
                                                                startActivity(
                                                                    Intent(
                                                                        this@ConsultationActivity,
                                                                        DoctorTrackingActivity::class.java
                                                                    )
                                                                        .putExtra(
                                                                            Constants.IntentKeys.PROVIDERID,
                                                                            data[position].providerId
                                                                        )
                                                                        .putExtra(
                                                                            Constants.IntentKeys.BOOKINGID,
                                                                            data[position].bookingId
                                                                        )
                                                                )
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    startActivity(
                                        Intent(
                                            this@ConsultationActivity,
                                            DoctorTrackingActivity::class.java
                                        )
                                            .putExtra(
                                                Constants.IntentKeys.PROVIDERID,
                                                data[position].providerId
                                            )
                                            .putExtra(
                                                Constants.IntentKeys.BOOKINGID,
                                                data[position].bookingId
                                            )
                                    )
                                }
                            }
                        }

                    }

                }, object : PatientActionListener {
                    override fun onChatClicked(position: Int) {
                        if(data[position].is_ins.equals("1") && data[position].status!! == "pending"){
                            DialogUtils.showLoader(this@ConsultationActivity)
                            viewmodel?.getInsuranceStatus(this@ConsultationActivity, data[position].bookingId!!)?.observe(this@ConsultationActivity, Observer {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(binding!!.root, msg)
                                            }
                                        } else {
                                            it.data?.let { data1 ->
                                                if(data1[0].ins_status.equals("pending",true)){
                                                    UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                                }
                                                else if(data1[0].ins_status.equals("rejected",true)){
                                                    UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                                }
                                                else if(data1[0].ins_status.equals("accepted",true)){
                                                    if(data1[0].isPendingAmount.equals("yes",true)){
                                                        val balance =  data1[0].balance!!.toDouble()
                                                        DialogUtils.showAlertDialogPayment(this@ConsultationActivity,
                                                            getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                                        getString(R.string.insurance),
                                                            getString(R.string.ok),
                                                            getString(R.string.cancel),
                                                            object : DialogCallBack {
                                                                override fun onPositiveClick() {
                                                                    startActivity(
                                                                        Intent(
                                                                            this@ConsultationActivity,
                                                                            VirtualPaymentActivity::class.java
                                                                        )
                                                                            .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                            .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                            .putExtra("bookingid",data[position].bookingId)
                                                                    )
                                                                }

                                                                override fun onNegativeClick() {

                                                                }

                                                            })
                                                    }
                                                    else {
                                                        if (data[position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                                                            startActivity(
                                                                Intent(this@ConsultationActivity, ChatActivity::class.java)
                                                                    .putExtra(Constants.IntentKeys.DOCTORSID, data[position].providerId)
                                                                    .putExtra(
                                                                        Constants.IntentKeys.BOOKINGID,
                                                                        data[position].bookingId.toString()
                                                                    )
                                                                    .putExtra(
                                                                        Constants.IntentKeys.DOCTORNAME,
                                                                        data[position].providerName
                                                                    )
                                                                    .putExtra(
                                                                        Constants.IntentKeys.DOCTORIMAGE,
                                                                        data[position].profilePic
                                                                    )
                                                            )
                                                        }
                                                        else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                                                            DialogUtils.showAlert(this@ConsultationActivity, object : SingleTapListener {
                                                                override fun singleTap() {

                                                                }
                                                            }, getString(R.string.wait_alert))
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                        }
                        else{
                            if (data[position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                                startActivity(
                                    Intent(this@ConsultationActivity, ChatActivity::class.java)
                                        .putExtra(Constants.IntentKeys.DOCTORSID, data[position].providerId)
                                        .putExtra(
                                            Constants.IntentKeys.BOOKINGID,
                                            data[position].bookingId.toString()
                                        )
                                        .putExtra(
                                            Constants.IntentKeys.DOCTORNAME,
                                            data[position].providerName
                                        )
                                        .putExtra(
                                            Constants.IntentKeys.DOCTORIMAGE,
                                            data[position].profilePic
                                        )
                                )
                            }
                            else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                                DialogUtils.showAlert(this@ConsultationActivity, object : SingleTapListener {
                                    override fun singleTap() {

                                    }
                                }, getString(R.string.wait_alert))
                            }
                        }
                    }

                    override fun onCallClicked(position: Int) {
                        Log.d("wqwwqw","xanmxamx")

                        if(data[position].is_ins.equals("1") && data[position].status!! == "pending"){
                            DialogUtils.showLoader(this@ConsultationActivity)
                            viewmodel?.getInsuranceStatus(this@ConsultationActivity, data[position].bookingId!!)?.observe(this@ConsultationActivity, Observer {
                                DialogUtils.dismissLoader()
                                Log.d("swhhjw","wioiwowiowi")
                                it?.let {

                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(binding!!.root, msg)
                                            }
                                        } else {
                                            it.data?.let { data1 ->
                                                if(data1[0].ins_status.equals("pending",true)){
                                                    UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                                }
                                                else if(data1[0].ins_status.equals("rejected",true)){
                                                    UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                                }
                                                else if(data1[0].ins_status.equals("accepted",true)){
                                                    if(data1[0].isPendingAmount.equals("yes",true)){
                                                        val balance =  data1[0].balance!!.toDouble()
                                                        DialogUtils.showAlertDialogPayment(this@ConsultationActivity,
                                                            getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                                        getString(R.string.insurance),
                                                            getString(R.string.ok),
                                                            getString(R.string.cancel),
                                                            object : DialogCallBack {
                                                                override fun onPositiveClick() {
                                                                    startActivity(
                                                                        Intent(
                                                                            this@ConsultationActivity,
                                                                            VirtualPaymentActivity::class.java
                                                                        )
                                                                            .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                            .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                            .putExtra("bookingid",data[position].bookingId)
                                                                    )
                                                                }

                                                                override fun onNegativeClick() {

                                                                }

                                                            })
                                                    }
                                                    else {
                                                        Log.d("abaaa","qewqweqewqew")

                                                        if (data[position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                                                            clickedPosition = position
                                                            Log.d("abaaa","qewqweqewqew3")

                                                            if (data[position].stream == "0") {
                                                                Log.d("abaaa","qewqweqewqew2")

                                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                                    Log.d("abaaa","qewqweqewqew67")

                                                                    requestPermissions(
                                                                        Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                                                                        Constants.Permission.AUDIO_CALL_PERMISSION
                                                                    )
                                                                } else {
                                                                    createCall(Constants.ChatTypes.VOICE_CALL)
                                                                }
                                                            } else if (data[position].stream == "1") {
                                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                                    requestPermissions(
                                                                        Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                                                        Constants.Permission.VIDEO_CALL_PERMISSION
                                                                    )
                                                                } else {
                                                                    createCall(Constants.ChatTypes.VIDEO_CALL)
                                                                }
                                                            }
                                                            else if (data[position].stream == "3") {
                                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                                    requestPermissions(
                                                                        Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                                                        Constants.Permission.VIDEO_CALL_PERMISSION
                                                                    )
                                                                } else {
                                                                    createCall(Constants.ChatTypes.VIDEO_CALL)
                                                                }
                                                            }
                                                        }
                                                        else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) { DialogUtils.showAlert(this@ConsultationActivity, object : SingleTapListener {
                                                            override fun singleTap() {

                                                            }

                                                        },getString(R.string.wait_alert)) }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                        }
                        else{
                            if (data[position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                                clickedPosition = position
                             if (data[position].stream == "0") {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                                            Constants.Permission.AUDIO_CALL_PERMISSION
                                        )
                                    } else {
                                        createCall(Constants.ChatTypes.VOICE_CALL)
                                    }
                                } else if (data[position].stream == "1") {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                            Constants.Permission.VIDEO_CALL_PERMISSION
                                        )
                                    } else {
                                        createCall(Constants.ChatTypes.VIDEO_CALL)
                                    }
                                }
                                else if (data[position].stream == "3") {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                            Constants.Permission.VIDEO_CALL_PERMISSION
                                        )
                                    } else {
                                        createCall(Constants.ChatTypes.VIDEO_CALL)
                                    }
                                }
                            }
                            else if (data[position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data[position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) { DialogUtils.showAlert(this@ConsultationActivity, object : SingleTapListener {
                                override fun singleTap() {

                                }

                            },getString(R.string.wait_alert)) }
                        }
                    }

                }, object : OnClickListener {
                    override fun onClickItem(position: Int) {

                        DialogUtils.showLoader(this@ConsultationActivity)
                        bookingViewModel?.cancelBooking(
                            this@ConsultationActivity,
                            data[position].bookingId!!
                        )
                            ?.observe(this@ConsultationActivity,
                                Observer {
                                    DialogUtils.dismissLoader()
                                    getConsultation(flowType)
                                })
                    }

                })

        }
        else {
            Collections.sort(data,
                Comparator<ConsultationData?> { o1, o2 ->
                    Integer.compare(
                        o2!!.bookingId!!,
                        o1!!.bookingId!!
                    )
                })
            binding!!.consultationList.layoutManager = LinearLayoutManager(this)
            binding!!.consultationList.adapter =
                ConsultationHistoryAdapter(this, data, object : OnClickListener {
                    override fun onClickItem(position: Int) {

                        Log.d("erterter",""+data[position].isreviewed.toString())
                        sharedHelper!!.isreview= data[position].isreviewed.toString()
                        if (data[position].status.equals("completed", true) ||
                            data[position].status.equals("cancelledbyprovider", true) ||
                            data[position].status.equals("cancelledbypatient", true) ||
                            data[position].status.equals("paymentverified", true)
                        ) {
                            startActivity(
                                Intent(
                                    this@ConsultationActivity,
                                    InvoiceDetailsActivity::class.java
                                )
                                    .putExtra(
                                        Constants.IntentKeys.PAYMENTDETAIL,
                                        data[position] as Parcelable
                                    )
                                    .putExtra("lab",data[position].suggested_labs as ArrayList<SlabData>)
                                    .putExtra("med",data[position].medicine as ArrayList<MedData>)
                                    .putExtra("test",data[position].test_to_take as ArrayList<TestData>)

                            )
                            //do nothing
                        }
                        else {

                            if(data[position].is_ins.equals("1")){
                                DialogUtils.showLoader(this@ConsultationActivity)
                                viewmodel?.getInsuranceStatus(this@ConsultationActivity, data[position].bookingId!!)?.observe(this@ConsultationActivity, Observer {
                                    DialogUtils.dismissLoader()
                                    it?.let {
                                        it.error?.let { error ->
                                            if (error) {
                                                it.message?.let { msg ->
                                                    UiUtils.showSnack(binding!!.root, msg)
                                                }
                                            } else {
                                                it.data?.let { data1 ->
                                                    if(data1[0].ins_status.equals("pending",true)){
                                                        UiUtils.showSnack(binding!!.root,"Insurance verification by hospital admin is pending")
                                                    }
                                                    else if(data1[0].ins_status.equals("rejected",true)){
                                                        UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                                    }
                                                    else if(data1[0].ins_status.equals("accepted",true)){
                                                        if(data1[0].isPendingAmount.equals("yes",true)){
                                                            val balance =  data1[0].balance!!.toDouble()
                                                            DialogUtils.showAlertDialogPayment(this@ConsultationActivity,
                                                                getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                                        getString(R.string.insurance),
                                                                getString(R.string.ok),
                                                                getString(R.string.cancel),
                                                                object : DialogCallBack {
                                                                    override fun onPositiveClick() {
                                                                        startActivity(
                                                                            Intent(
                                                                                this@ConsultationActivity,
                                                                                VirtualPaymentActivity::class.java
                                                                            )
                                                                                .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                                .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                                .putExtra("bookingid",data[position].bookingId)
                                                                        )
                                                                    }

                                                                    override fun onNegativeClick() {

                                                                    }

                                                                })
                                                        }
                                                        else {
                                                            startActivity(
                                                                Intent(
                                                                    this@ConsultationActivity,
                                                                    InvoiceDetailsActivity::class.java
                                                                )
                                                                    .putExtra(
                                                                        Constants.IntentKeys.PAYMENTDETAIL,
                                                                        data[position] as Parcelable
                                                                    )
                                                                    .putExtra("lab",data[position].suggested_labs as ArrayList<SlabData>)
                                                                    .putExtra("med",data[position].medicine as ArrayList<MedData>)
                                                                    .putExtra("test",data[position].test_to_take as ArrayList<TestData>)

                                                            )
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                            else {
                                startActivity(
                                    Intent(
                                        this@ConsultationActivity,
                                        InvoiceDetailsActivity::class.java
                                    )
                                        .putExtra(
                                            Constants.IntentKeys.PAYMENTDETAIL,
                                            data[position] as Parcelable
                                        )
                                        .putExtra("lab",data[position].suggested_labs as ArrayList<SlabData>)
                                        .putExtra("med",data[position].medicine as ArrayList<MedData>)
                                        .putExtra("test",data[position].test_to_take as ArrayList<TestData>)

                                )
                            }
                        }
                    }
                })
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.Permission.AUDIO_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                createCall(Constants.ChatTypes.VOICE_CALL)

            }

        } else if (requestCode == Constants.Permission.VIDEO_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                createCall(Constants.ChatTypes.VIDEO_CALL)

            }
        }
    }

    private fun createCall(callType: String) {
             Log.d("uwuwuuwu","iwowiwowiwow")
        var intent = Intent(this, VideoCallActivity::class.java)
            .putExtra(Constants.NotificationIntentValues.CALL_TYPE, callType)
            .putExtra(
                Constants.NotificationIntentValues.BOOKINGID,
                data[clickedPosition].bookingId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.CALL_FROM,
                Constants.ChatTypes.OUTGOING_CALL
            )
            .putExtra(
                Constants.NotificationIntentValues.CHANNEL_ID,
                data[clickedPosition].providerId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.TO_ID,
                data[clickedPosition].providerId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.FROM_ID,
                sharedHelper.id.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.ID,
                data[clickedPosition].providerId.toString()
            )
            .putExtra(Constants.NotificationIntentValues.NAME, data[clickedPosition].providerName)
            .putExtra(Constants.NotificationIntentValues.IMAGE, data[clickedPosition].profilePic)

            .putExtra(
                Constants.NotificationIntentValues.BOOKINGDATETIME,
                data[clickedPosition].bookingDate
            )

//        startActivity(intent)

        callViewModel?.createCall(
            data[clickedPosition].providerId.toString(),
            data[clickedPosition].providerId.toString(),
            callType,
            sharedHelper.id.toString()
        )?.observe(this, Observer {

            if (it.error == "true") {
                UiUtils.showSnack(binding!!.root, it.message)
            } else if (it.error == "false") {
                NotificationUtils(this).notificationHangupCall(
                    data[clickedPosition].providerName!!,
                    "outgoing $callType call"
                )
//                intent.putExtra(Constants.NotificationIntentValues.ID, it.data?.id)
                startActivity(intent)
            }
        })

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}