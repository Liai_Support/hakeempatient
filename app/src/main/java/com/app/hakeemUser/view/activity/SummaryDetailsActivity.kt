package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityResetPasswordBinding
import com.app.hakeemUser.databinding.ActivitySummaryBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.models.CompletedData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils

class SummaryDetailsActivity : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var completedData: CompletedData? = null
    var backPressAllowed = false
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_summary)

        getIntentValues()

        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
    }

    private fun getIntentValues() {

        intent.extras?.let { bundle ->
            bundle.getParcelable<CompletedData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { data ->
                completedData = data
                UiUtils.setRating(this, data.averageRating?.toFloat(), binding!!.rating, 35)
                data.status?.let {

                    if (it.equals("online", true))
                        View.VISIBLE
                    else View.GONE
                }
                data.providerName?.let { binding!!.doctorName.text = it }
                data.education?.let { binding!!.qualification.text = it+"-"+""+ data.specialityName!! }
                data.expierence?.let { binding!!.experience.text = it+" "+getString(R.string.year)+" "+getString(R.string.experience) }
                data.averageRating?.let { binding!!.ratingValue.text = "$it/5" }

                data.status?.let {
                    if (it.equals("online", true))
                        binding!!.isOnline.visibility =
                            View.VISIBLE else binding!!.isOnline.visibility = View.GONE
                }

                data.profilePic?.let {
                    UiUtils.loadImage(
                        binding!!.profileImage,
                        it,
                        ContextCompat.getDrawable(this, R.drawable.splash_image)!!
                    )
                }

                binding!!.extraChargeText.text = getString(R.string.extra_charges_by_doc)

                data.fee?.let {
                    if (data.extraFee != null) {

                        binding!!.singleConsultation.text = "${it.toInt()} SAR"
                        binding!!.consultationAmount.text = "${data.extraFee} SAR"
                        binding!!.totalAmount.text = "${it.toInt() + data.extraFee!!.toInt()} SAR"

                    } else {
                        binding!!.singleConsultation.text = "$it SAR"
                        binding!!.consultationAmount.text = "0 SAR"
                        binding!!.totalAmount.text = "$it SAR"
                    }

                }
            }

            backPressAllowed = bundle.getBoolean(Constants.IntentKeys.BACKPRESSALLOWED)
        }
    }

    fun onConfirmClicked(view: View) {
        completedData?.let {
            startActivity(
                Intent(this, PaymentMethodActivity::class.java)
                    .putExtra(Constants.IntentKeys.PAYMENTDETAIL, it as Parcelable)
            )
        }

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }


    override fun onBackPressed() {
        if (backPressAllowed) {
            finish()
        } else {
            DialogUtils.showAlertDialog(this,
                getString(R.string.pay_consultation_fee),
                getString(R.string.oops),
                getString(R.string.ok),
                getString(R.string.cancel),
                object : DialogCallBack {
                    override fun onPositiveClick() {
                        completedData?.let {
                            startActivity(
                                Intent(
                                    this@SummaryDetailsActivity,
                                    PaymentMethodActivity::class.java
                                )
                                    .putExtra(Constants.IntentKeys.PAYMENTDETAIL, it as Parcelable)
                            )
                        }

                    }

                    override fun onNegativeClick() {

                    }
                })
        }
    }
}