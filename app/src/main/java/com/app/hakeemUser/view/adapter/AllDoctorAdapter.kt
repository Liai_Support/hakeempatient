package com.app.hakeemUser.view.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildAllDoctorBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DoctorProfileActivity
import com.app.hakeemUser.view.activity.LoginActivity

class AllDoctorAdapter(
    var context: Context
) : PagedListAdapter<DoctorData, AllDoctorAdapter.ViewHolder>(DIFF_CALLBACK) {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildAllDoctorBinding = ChildAllDoctorBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_all_doctor,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data: DoctorData? = getItem(position)

        Log.d("kanika",""+itemCount)


        data?.let { data ->

//            UiUtils.setRating(context, data.averageRating?.toFloat(), holder.binding.rating, 40)

            data.profilePic?.let {
                UiUtils.loadImage(
                    holder.binding.profileImage,
                    it,
                    ContextCompat.getDrawable(context, R.drawable.splash_image)!!
                )
            }

            data.status?.let {
                if (it.equals("online", true))
                    holder.binding.isOnline.visibility =
                        View.VISIBLE else holder.binding.isOnline.visibility = View.GONE
            }

            data.name?.let { holder.binding.doctorName.text = it }
            data.education?.let { holder.binding.qualification.text = it }
            data.expierence?.let { holder.binding.experience.text = it }
            data.averageRating?.let { holder.binding.ratingValue.text = "$it+" }


            holder.binding.root.setOnClickListener{
                if (SharedHelper(context).loggedIn) {
                    context.startActivity(
                        Intent(
                            context,
                            DoctorProfileActivity::class.java
                        )
                            .putExtra(Constants.IntentKeys.DOCTOR_DATA, data as Parcelable)
                            .putExtra(
                                Constants.IntentKeys.SPECIALITYLIST,
                                data.speciality as ArrayList<Parcelable>
                            )
                            .putExtra(Constants.IntentKeys.REVIEW_LIST, data.review)
                            .putExtra(Constants.IntentKeys.BOOKINGFLOW, false)
                    )
                } else {
                    pleaseLogin()
                }
            }

            holder.binding.viewProfile.setOnClickListener {

                if (SharedHelper(context).loggedIn) {
                    context.startActivity(
                        Intent(
                            context,
                            DoctorProfileActivity::class.java
                        )
                            .putExtra(Constants.IntentKeys.DOCTOR_DATA, data as Parcelable)
                            .putExtra(
                                Constants.IntentKeys.SPECIALITYLIST,
                                data.speciality as ArrayList<Parcelable>
                            )
                            .putExtra(Constants.IntentKeys.REVIEW_LIST, data.review)
                            .putExtra(Constants.IntentKeys.BOOKINGFLOW, false)
                    )
                } else {
                    pleaseLogin()
                }


            }

        }


    }

    companion object {

        private val DIFF_CALLBACK: DiffUtil.ItemCallback<DoctorData> =
            object : DiffUtil.ItemCallback<DoctorData>() {
                override fun areItemsTheSame(
                    oldItem: DoctorData,
                    newItem: DoctorData
                ): Boolean {
                    return oldItem.providerId === newItem.providerId
                }

                override fun areContentsTheSame(
                    oldItem: DoctorData,
                    newItem: DoctorData
                ): Boolean {
                    return oldItem.providerId === newItem.providerId &&
                            oldItem.distance == newItem.distance
                }
            }
    }

    private fun pleaseLogin() {


        DialogUtils.showAlertDialog(context,
            context.getString(R.string.login_feature),
            context.getString(R.string.please_login),
            context.getString(R.string.ok),
            context.getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    context.startActivity(
                        Intent(context, LoginActivity::class.java)
                            .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )

                }

                override fun onNegativeClick() {

                }
            })


    }
}