package com.app.hakeemUser.view.adapter


import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildPatientListBinding
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.AddPatientDetailsActivity
import com.app.hakeemUser.view.activity.PatientDetailsActivity

class SelectPatientAdapter1(
    var activity: PatientDetailsActivity,
    var context: Context,
    var list: ArrayList<MemberData>
) :
    RecyclerView.Adapter<SelectPatientAdapter1.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: ChildPatientListBinding = ChildPatientListBinding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectPatientAdapter1.MyViweHolder {
        return MyViweHolder(
            LayoutInflater.from(context).inflate(R.layout.child_patient_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SelectPatientAdapter1.MyViweHolder, position: Int) {

        holder.childPatientListBinding.name.text = list[position].name
        holder.childPatientListBinding.patientType.text = ""+(position+1)+"."+list[position].type

        if(list[position].type.equals(context.getString(R.string.myself)) || list[position].type.equals("Myself")){
            //holder.childPatientListBinding.edit.visibility = View.GONE
            holder.childPatientListBinding.delete.visibility = View.GONE
        }

        holder.childPatientListBinding.selected.visibility = View.GONE
        holder.childPatientListBinding.delete.setOnClickListener {
            DialogUtils.showLoader(context)
            activity.membersViewModel?.deletemember(activity,list[position].memberId)
                ?.observe(activity, Observer{
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(holder.itemView, msg)
                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(holder.itemView, msg)
                                    activity.getmemeberlist()
                                }
                            }
                        }

                    }
                })
        }
        holder.childPatientListBinding.edit.setOnClickListener {
            if(list[position].type.equals(context.getString(R.string.myself)) || list[position].type.equals("Myself")){
                activity.startActivity(
                    Intent(
                        activity,
                        AddPatientDetailsActivity::class.java
                    )
                        .putExtra("editmember", true)
                        .putExtra("myselfedit", true)
                        .putExtra(
                            "memberdata",
                            list[position] as Parcelable
                        )
                )
            }
            else {
                activity.startActivity(
                    Intent(
                        activity,
                        AddPatientDetailsActivity::class.java
                    )
                        .putExtra("editmember", true)
                        .putExtra("myselfedit", false)
                        .putExtra(
                            "memberdata",
                            list[position] as Parcelable
                        )
                )
            }
        }

    }
}