package com.app.hakeemUser.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildTestListBinding
import com.app.hakeemUser.models.TestDetails
import com.app.hakeemUser.view.activity.LablistActivity
import java.util.*


class TestlistAdapter(var lablistactivity: LablistActivity,var context: Context, var list: ArrayList<TestDetails>,) : RecyclerView.Adapter<TestlistAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: ChildTestListBinding = ChildTestListBinding.bind(view)

    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): TestlistAdapter.MyViweHolder {

        return MyViweHolder(
                LayoutInflater.from(context).inflate(R.layout.child_test_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TestlistAdapter.MyViweHolder, position: Int) {


        if (lablistactivity.sharedHelper?.language.equals("ar")){
            holder.childPatientListBinding.test.text = list[position].aname

        }else{
            holder.childPatientListBinding.test.text = list[position].ename

        }

        holder.childPatientListBinding.test.setChecked(list.get(position).isseleted!!);

        holder.childPatientListBinding.test.setTag(position);

        holder.childPatientListBinding.test.setOnClickListener(View.OnClickListener {
            val pos = holder.childPatientListBinding.test.getTag() as Int
           // Toast.makeText(context, list.get(pos).ename.toString() + " clicked!", Toast.LENGTH_SHORT).show()
            if (list.get(pos).isseleted!!) {
                list.get(pos).isseleted = false
                lablistactivity.idarray.remove(list[position].id!!)
            }
            else {
                list.get(pos).isseleted = true
                lablistactivity.idarray.add(list[position].id!!)
            }

        })

        /*holder.childPatientListBinding.test.setOnClickListener {

            Log.d("gh",""+holder.childPatientListBinding.test.isChecked)
             if(holder.childPatientListBinding.test.isChecked == true){
                 holder.childPatientListBinding.test.isChecked = true
                 lablistactivity.idarray.add(list[position].id!!)
                 //notifyDataSetChanged()
                }
             else if(holder.childPatientListBinding.test.isChecked == false){
                 holder.childPatientListBinding.test.isChecked = false
                 lablistactivity.idarray.remove(list[position].id!!)
                 //notifyDataSetChanged()
                }

            Log.d("ngv",""+lablistactivity.idarray)




        }*/


    }



}








