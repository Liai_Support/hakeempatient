package com.app.hakeemUser.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.models.LabData
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildTestListBinding
import com.app.hakeemUser.databinding.LablistshowBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.PharmachyDetails
import com.app.hakeemUser.view.activity.LablistActivity
import com.app.hakeemUser.view.activity.PharmachylistActivity
import java.util.*

class PharmachylistAdapter(
    var pharmachylistActivity: PharmachylistActivity, var context: Context, var list: ArrayList<PharmachyDetails>

) :
    RecyclerView.Adapter<PharmachylistAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var lablistshowBinding: LablistshowBinding = LablistshowBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.lablistshow,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        if(pharmachylistActivity.sharedHelper?.language.equals("ar")){
            holder.lablistshowBinding.lab.text = list[position].Pharmacy_ar_name
        }else{
            holder.lablistshowBinding.lab.text = list[position].Pharmacy_name

        }
        holder.lablistshowBinding.map.setOnClickListener {
            var lat = list[position].latitude
            var lng = list[position].longitude
            val uri = String.format(Locale.ENGLISH,
                "google.navigation:q=$lat,$lng")
            val gmmIntentUri = Uri.parse(uri)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            pharmachylistActivity.startActivity(mapIntent)
        }
    }



}