package com.app.hakeemUser.view.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityAlldoctorBinding
import com.app.hakeemUser.databinding.ActivityDashboard1Binding
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.view.adapter.AllDoctorAdapter
import com.app.hakeemUser.viewmodel.AvailableDoctorViewModel

class AllDoctorsActivity : BaseActivity() {

    var binding: ActivityAlldoctorBinding? = null
    var viewModel: AvailableDoctorViewModel? = null
    var allDoctorAdapter: AllDoctorAdapter? = null
    var sharedHelper: SharedHelper? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlldoctorBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
          if (sharedHelper!!.language == "ar"){
              //binding!!.imageView14.setImageResource(R.drawable.arrow_right)
              binding!!.imageView14.rotation= 180F
          }

        //setContentView(R.layout.activity_alldoctor)

        viewModel = ViewModelProvider(this).get(AvailableDoctorViewModel::class.java)

        initAdapter()

        viewModel?.configDataSourceAllDoctor(this)

        viewModel?.allItemPagedList?.observe(this, Observer {
            allDoctorAdapter?.submitList(it)
        })
    }

    private fun initAdapter() {

        allDoctorAdapter = AllDoctorAdapter(this)
        binding!!.doctorList.layoutManager = LinearLayoutManager(this)
        binding!!.doctorList.adapter = allDoctorAdapter

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

}