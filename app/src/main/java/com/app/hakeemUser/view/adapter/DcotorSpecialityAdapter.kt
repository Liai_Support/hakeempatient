package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildSpeciality1Binding
import com.app.hakeemUser.databinding.ChildSpecialityBinding
import com.app.hakeemUser.models.DoctorSpecialityList
import com.app.hakeemUser.utils.UiUtils
import java.util.*

class DcotorSpecialityAdapter(
    var context: Context,
    var list: ArrayList<DoctorSpecialityList>
) :
    RecyclerView.Adapter<DcotorSpecialityAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildSpeciality1Binding = ChildSpeciality1Binding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_speciality1,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        list[position].name?.let { holder.binding.type.text = it }
        list[position].image?.let {
            UiUtils.loadImage(
                holder.binding.doctor,
                it,
                ContextCompat.getDrawable(context, R.drawable.splash_image)!!
            )
        }

        holder.binding.selection.visibility = View.GONE
    }

}