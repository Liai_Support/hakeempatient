package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivitySummaryBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.CompletedData
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.models.vData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.BookingViewModel

class SummaryDetailsBookingActivity : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var completedData: CompletedData? = null
    var bookingViewModel: BookingViewModel? = null
    var viewmodel: BookingViewModel? = null
    var amountTobePaid = ""
    var selectedType = 0
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_summary)
        viewmodel = ViewModelProvider(this).get(BookingViewModel::class.java)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        initListener()
        binding!!.cardView10.visibility = View.GONE

        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
    }

    private fun insvirtualfee(){
        DialogUtils.showLoader(this@SummaryDetailsBookingActivity)
        bookingViewModel?.newvirtualfee(this@SummaryDetailsBookingActivity)
            ?.observe(this@SummaryDetailsBookingActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })
    }

    private fun handleResponse(data: vData) {

        if(data.status.equals("no")){
            binding!!.cardView4.visibility = View.VISIBLE
            binding!!.cardView30.visibility = View.GONE
            binding!!.singleConsultation.text = " ${data.vamt} SAR"
            binding!!.consultationAmount.text = " ${data.vamt} SAR"
            binding!!.totalAmount.text = " ${data.vamt} SAR"
            selectedType = 3
            ScheduledBookingSingleton.getInstance().vfee = data.vamt
            amountTobePaid = data.vamt.toString()
           // ScheduledBookingSingleton.getInstance().paymenttype = "nothing"
            UiUtils.showSnack(binding!!.cardView4,getString(R.string.yip))
        }
        else{
            binding!!.cardView4.visibility = View.GONE
            binding!!.cardView30.visibility = View.VISIBLE
            binding!!.singleConsultation.text = " ${data.vamt} SAR"
            binding!!.singleConsultationv.text = " ${data.iamt} SAR"
            binding!!.consultationAmount.text = " ${data.iamt} SAR"
            binding!!.totalAmount.text = " ${data.iamt} SAR"
            selectedType = 3
            amountTobePaid = data.iamt.toString()
            ScheduledBookingSingleton.getInstance().vfee = data.iamt
          //  ScheduledBookingSingleton.getInstance().paymenttype = "insurance"

        }

      /*  data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }*/
    }


    private fun initListener() {

        binding!!.chat.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                ScheduledBookingSingleton.getInstance().chatFee?.let {
                    amountTobePaid = it
                    selectedType = 1
                    binding!!.singleConsultation.text = " $it SAR"
                    binding!!.consultationAmount.text = " $it SAR"
                    binding!!.totalAmount.text = " $it SAR"
                }
            }
        }

        binding!!.voiceCall.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                ScheduledBookingSingleton.getInstance().audioFee?.let {
                    amountTobePaid = it
                    selectedType = 2
                    binding!!.singleConsultation.text = " $it SAR"
                    binding!!.consultationAmount.text = " $it SAR"
                    binding!!.totalAmount.text = " $it SAR"
                }
            }
        }

        binding!!.videoCall.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                ScheduledBookingSingleton.getInstance().videoFee?.let {
                    amountTobePaid = it
                    selectedType = 3
                    binding!!.singleConsultation.text = " $it SAR"
                    binding!!.consultationAmount.text = " $it SAR"
                    binding!!.totalAmount.text = " $it SAR"
                }
            }
        }

    }

    private fun getIntentValues() {

        intent.extras?.let { bundle ->
            bundle.getParcelable<DoctorData>(Constants.IntentKeys.DOCTOR_DATA)?.let { data ->

                UiUtils.setRating(this, data.averageRating?.toFloat(), binding!!.rating, 35)
                data.status?.let {

                    if (it.equals("online", true))
                        View.VISIBLE
                    else View.GONE
                }
                data.name?.let { binding!!.doctorName.text = it }


                if (data.speciality!!.isNotEmpty()){
                    data.education?.let { binding!!.qualification.text = it+"-"+""+ data.speciality!![0].name!! }

                   }
                data.expierence?.let { binding!!.experience.text = it+" "+getString(R.string.year)+" "+getString(R.string.experience) }
                data.averageRating?.let { binding!!.ratingValue.text = "$it/5" }

                data.status?.let {
                    if (it.equals("online", true))
                        binding!!.isOnline.visibility =
                            View.VISIBLE else binding!!.isOnline.visibility = View.GONE
                }

                data.profilePic?.let {
                    UiUtils.loadImage(
                        binding!!.profileImage,
                        it,
                        ContextCompat.getDrawable(this, R.drawable.splash_image)!!
                    )
                }

                if (ScheduledBookingSingleton.getInstance().isVirtualBooking == 1) {
                   // insvirtualfee()
                   /* chatTypeView.visibility = View.VISIBLE
                    ScheduledBookingSingleton.getInstance().chatFee?.let {
                        amountTobePaid = it
                        selectedType = 1
                        chat.isChecked = true
                        singleConsultation.text = " $it SAR"
                        consultationAmount.text = " $it SAR"
                        totalAmount.text = " $it SAR"
                    }*/

                    ScheduledBookingSingleton.getInstance().vfee?.let {
                        binding!!.singleConsultation.text = " $it SAR"
                        binding!!.consultationAmount.text = " $it SAR"
                        binding!!.totalAmount.text = " $it SAR"
                        amountTobePaid = it
                        ScheduledBookingSingleton.getInstance().vfee = it
                        selectedType = 3
                    }
                }
                else if(ScheduledBookingSingleton.getInstance().isGlobalBooking == 1){
                    ScheduledBookingSingleton.getInstance().dfee?.let {
                        binding!!.singleConsultation.text = " $it SAR"
                        binding!!.consultationAmount.text = " $it SAR"
                        binding!!.totalAmount.text = " $it SAR"
                        amountTobePaid = it
                        ScheduledBookingSingleton.getInstance().vfee = it
                        selectedType = 3
                    }
                }
                else {
                    ScheduledBookingSingleton.getInstance().dfee?.let {
                        binding!!.singleConsultation.text = " $it SAR"
                        binding!!.consultationAmount.text = " $it SAR"
                        binding!!.totalAmount.text = " $it SAR"
                        amountTobePaid = it
                        ScheduledBookingSingleton.getInstance().dfee = it

                    }
                }


            }
        }
    }

    fun onConfirmClicked(view: View) {

        if (SharedHelper(this).loggedIn!!) {
            DialogUtils.showAlertDialog(this,
                getString(R.string.booking_confirmation),
                getString(R.string.confirm),
                getString(R.string.ok),
                getString(R.string.cancel),
                object : DialogCallBack {
                    override fun onPositiveClick() {

                        if(ScheduledBookingSingleton.getInstance().chooseoption == 1){
                            proccedd("","","Insurance")
                        }
                        else{
                            if (ScheduledBookingSingleton.getInstance().isVirtualBooking == 1 || ScheduledBookingSingleton.getInstance().isGlobalBooking == 1) {

                                /*  if(ScheduledBookingSingleton.getInstance().paymenttype.equals("insurance",true) ){
                                      placeVirtualBookig("nothing")
                                  }
                                  else {*/
                                startActivity(
                                    Intent(
                                        this@SummaryDetailsBookingActivity,
                                        VirtualPaymentActivity::class.java
                                    )
                                        .putExtra(Constants.IntentKeys.AMOUNT, amountTobePaid)
                                        .putExtra(Constants.IntentKeys.BOOKINGTYPE, selectedType)
                                )
                                //  }
                            }
                            else {

                                /* DialogUtils.showLoader(this@SummaryDetailsBookingActivity)
                                 bookingViewModel?.bookNewConsultation(this@SummaryDetailsBookingActivity)
                                     ?.observe(this@SummaryDetailsBookingActivity, Observer {
                                         DialogUtils.dismissLoader()
                                         it?.let {
                                             it.error?.let { error ->
                                                 if (error) {
                                                     it.message?.let { msg ->
                                                         UiUtils.showSnack(root, msg)
                                                     }
                                                 } else {
                                                     showSuccessDialog()
                                                 }
                                             }
                                         }
                                     })*/

                                startActivity(
                                    Intent(
                                        this@SummaryDetailsBookingActivity,
                                        VirtualPaymentActivity::class.java
                                    )
                                        .putExtra(Constants.IntentKeys.AMOUNT, ScheduledBookingSingleton.getInstance().dfee)
                                        .putExtra(Constants.IntentKeys.BOOKINGTYPE, 0)
                                )


                            }
                        }
                    }

                    override fun onNegativeClick() {

                    }
                })
        } else {
            pleaseLogin()
        }


    }

    fun proccedd(porderid: String,paymenttransactionId: String, paymentMethod: String) {
        ScheduledBookingSingleton.getInstance().paymentMethod = paymentMethod
        ScheduledBookingSingleton.getInstance().transactionId = paymenttransactionId
        ScheduledBookingSingleton.getInstance().torderId = porderid
        if(selectedType == 0){
            placeNormalBooking()
        }
        else{
            placeVirtualBookig()
        }
    }

    private fun placeVirtualBookig() {

        DialogUtils.showLoader(this)
        viewmodel?.bookVirtualConsultation(this, selectedType)
                ?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {
                                showSuccessDialog1()
                            }
                        }
                    }
                })

    }

    private fun placeNormalBooking(){
        DialogUtils.showLoader(this@SummaryDetailsBookingActivity)
        viewmodel?.bookNewConsultation(this@SummaryDetailsBookingActivity)
            ?.observe(this@SummaryDetailsBookingActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            showSuccessDialog()
                        }
                    }
                }
            })
    }

    private fun showSuccessDialog1() {

        DialogUtils.showSuccessDialog(this, object : SingleTapListener {
            override fun singleTap() {

                val intent = Intent(this@SummaryDetailsBookingActivity, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()

            }
        })
    }

    private fun showSuccessDialog() {

        DialogUtils.showSuccessBookingDialog(this, object : SingleTapListener {
            override fun singleTap() {
                val intent =
                    Intent(this@SummaryDetailsBookingActivity, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        }, getString(R.string.confirm_booking))
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }


    private fun pleaseLogin() {


        DialogUtils.showAlertDialog(this,
            getString(R.string.login_feature),
            getString(R.string.please_login),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    startActivity(
                        Intent(this@SummaryDetailsBookingActivity, LoginActivity::class.java)
                            .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )

                }

                override fun onNegativeClick() {

                }
            })


    }
}