package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.app.hakeemUser.R
import com.app.hakeemUser.utils.SharedHelper

class GetStartedActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)

        
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed({

            if (SharedHelper(this).loggedIn) {
                startActivity(Intent(this, DashBoardActivity::class.java))
                finish()
            }

        }, 300)



    }

    fun onStartedClicked() {

        startActivity(Intent(this,OnBoardActivity::class.java))
        finish()
    }

}