package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildHome1Binding
import com.app.hakeemUser.databinding.ChildHomeBinding
import com.app.hakeemUser.models.BannerImageData
import com.app.hakeemUser.utils.UiUtils

class HomeHeaderAdapter(
    var context: Context,
    var listBanner: ArrayList<BannerImageData>
) :
    RecyclerView.Adapter<HomeHeaderAdapter.HomeHeaderViewHolder>() {

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildHome1Binding = ChildHome1Binding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_home1,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listBanner.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        listBanner[position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.imgdoc, imageUrl)
        }

        listBanner[position].image?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.imgtemp, imageUrl)
        }
       // holder.binding.imgdoc.setImageResource(listBanner[position].img)
        holder.binding.txt1.setText(listBanner[position].txt1)

    }
}