package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildMyWalletBinding
import com.app.hakeemUser.models.TransactionHistory
import com.app.hakeemUser.utils.BaseUtils

class MyWalletAdapter(
    var context: Context,
    var data: ArrayList<TransactionHistory>
) :
    RecyclerView.Adapter<MyWalletAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildMyWalletBinding = ChildMyWalletBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_my_wallet,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        data[position].id?.let {
            holder.binding.id.text = "ID : $it"
        }

        data[position].amount?.let {
            if (data[position].type == "credit") {
                holder.binding.amount.text = "+$it SAR"
                holder.binding.amount.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
            } else {
                holder.binding.amount.text = "-$it SAR"
                holder.binding.amount.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.google_color
                    )
                )
            }
        }

        data[position].createdAt?.let {
            holder.binding.date.text =
                BaseUtils.getFormatedDateUtc(it, "yyyy-MM-dd'T'HH:mm:ss", "dd/MM/yyyy hh:mm a")
        }

    }

    fun setValues(list: java.util.ArrayList<TransactionHistory>) {

        data = list
        notifyDataSetChanged()
    }
}