package com.app.hakeemUser.view.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.databinding.FragmentSocialLoginDetailsBinding
import com.app.hakeemUser.interfaces.MoveToFragment
import com.app.hakeemUser.models.SocialRegisterData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.AmazonViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import java.io.File

class SocialLoginDetailsFragment(
    var moveToFragment: MoveToFragment,
    supportFragmentManager: FragmentManager,
) : Fragment(R.layout.fragment_social_login_details) {

    var binding: FragmentSocialLoginDetailsBinding? = null
    var socialToken = ""
    var loginType = ""
    var profileImage = ""
    var uploadFile: File? = null
    var sharedHelper: SharedHelper? = null
    private var amazonViewModel: AmazonViewModel? = null
    private var viewmodel: RegisterViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSocialLoginDetailsBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        getIntentValues()

        binding!!.editPic.setOnClickListener {
            DialogUtils.showPictureDialog(requireActivity())
        }

        binding!!.submit.setOnClickListener {
            if (isValidInputs()) {
                DialogUtils.showLoader(requireContext())
                if (uploadFile != null) {

                    amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                        ?.observe(viewLifecycleOwner, Observer {


                            it.error?.let { value ->
                                if (!value) {
                                    proceedSignIn(it.message)
                                } else {
                                    DialogUtils.dismissLoader()
                                }
                            }
                        })
                } else {
                    proceedSignIn("")
                }
            }
        }

        binding!!.back.setOnClickListener {
            val manager =
                requireActivity().supportFragmentManager
            val trans: FragmentTransaction = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()
        }
        return view
    }

    private fun getIntentValues() {

        arguments?.let { extra ->

            extra.getString(Constants.IntentKeys.EMAIL)?.let { value ->
                if (value.isNotEmpty()) {
                    binding!!.emailId.setText(value)
                    binding!!.emailId.isEnabled = false
                } else {
                    binding!!.emailId.isEnabled = true
                }

            }

            extra.getString(Constants.IntentKeys.NAME)?.let { value ->
                binding!!.name.setText(value)
            }

            extra.getString(Constants.IntentKeys.SOCIALTOKEN)?.let { value ->
                socialToken = value
            }

            extra.getString(Constants.IntentKeys.LOGINTYPE)?.let { value ->
                loginType = value
            }

            extra.getString(Constants.IntentKeys.PROFILE_PICTURE)?.let { value ->
                if (value.isNotEmpty()) {
                    profileImage = value
                    UiUtils.loadImage(binding!!.profilePicture, value)
                }
            }


        }

    }


    private fun proceedSignIn(imageUrlString: String?) {

        imageUrlString?.let { image ->


            var imageurl = if (image.isEmpty()) {
                profileImage
            } else {
                imageUrlString
            }

            viewmodel?.socialRegister(
                binding!!.emailId.text.toString().trim(),
                imageurl,
                binding!!.name.text.toString().trim(),
                socialToken,
                binding!!.countryCodePicker.selectedCountryCode,
                binding!!.phoneNumber.text.toString().trim(), loginType,"hbc","","",""
            )?.observe(viewLifecycleOwner, Observer {
                it?.let {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })

        }

    }

    private fun handleResponse(data: SocialRegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.token?.let { sharedHelper?.token = it }

        sharedHelper?.loggedIn = true


//        moveToFragment.onFragBackPressed()

        requireActivity().finish()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture,
                    BaseUtils.getRealPathFromUriNew(requireContext(), uri),
                    ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(requireContext(), uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

    }


    private fun isValidInputs(): Boolean {
        when {

            uploadFile == null && profileImage.isEmpty() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseuploadprofilepicture))
            }

            binding!!.name.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourname))
                return false
            }
            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
                return false
            }
            binding!!.phoneNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourmobilenumber))
                return false
            }

            else -> return true
        }

        return true
    }

    fun setOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == RESULT_OK) {
                handleCamera()
            }

        }

    }

    fun setPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when (requestCode) {

            Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseUtils.openGallery(requireActivity())
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
            }

            Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
            ) {
                BaseUtils.openCamera(requireActivity())
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
            }
        }
    }
}