package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildMemberListHomeBinding
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DashBoardActivity


class ListMemberhomeAdapter(
    var activity: DashBoardActivity,
    var context: Context,
    var list: ArrayList<MemberData>,
) :
    RecyclerView.Adapter<ListMemberhomeAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childMemberListHomeBinding: ChildMemberListHomeBinding = ChildMemberListHomeBinding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListMemberhomeAdapter.MyViweHolder {
        return MyViweHolder(
            LayoutInflater.from(context).inflate(R.layout.child_member_list_home, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ListMemberhomeAdapter.MyViweHolder, position: Int) {

        holder.childMemberListHomeBinding.name.text = list[position].name

        if(list[position].type.equals(context.getString(R.string.myself)) || list[position].type.equals("Myself")){
            holder.childMemberListHomeBinding.removemember.visibility = View.GONE
        }

        holder.childMemberListHomeBinding.removemember.setOnClickListener {
            DialogUtils.showLoader(activity)
            activity.membersViewModel?.deletemember(activity,list[position].memberId)
                ?.observe(activity, Observer{
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(binding!!.root, msg)
                                }
                            }
                            else {
                                it.message?.let { msg ->
                                     UiUtils.showSnack(holder.childMemberListHomeBinding.root, msg)
                                    activity.getmemeberlist()
                                }
                            }
                        }

                    }
                })
        }

       /* holder.childMemberListHomeBinding.circleImageView1.setOnClickListener {
            activity.startActivity(
                Intent(
                    activity,
                    AddPatientDetailsActivity::class.java
                )
                    .putExtra("editmember",true)
                    .putExtra(
                        "memberdata",
                        list[position] as Parcelable
                    )
            )
        }*/


    }
}