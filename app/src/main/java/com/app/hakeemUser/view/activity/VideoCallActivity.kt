package com.app.hakeemUser.view.activity

import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.SurfaceView
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityCallBinding
import com.app.hakeemUser.databinding.ActivityResetPasswordBinding
import com.app.hakeemUser.databinding.ActivitySearchLocationBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.notification.NotificationUtils
import com.app.hakeemUser.rxbus.RxBusNotification
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.CallViewModel
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import io.agora.rtc.video.VideoCanvas
import io.agora.rtc.video.VideoEncoderConfiguration
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.Dispatchers.Default

class VideoCallActivity : BaseActivity(), LifecycleOwner {

    var binding: ActivityCallBinding? = null

    private var isMute: Boolean = false
    private var isVideoMute: Boolean = false
    private var isFrontCamera: Boolean = false

    private var isSpeakerOnAudio: Boolean = false

    private var mRtcEngine: RtcEngine? = null

    private var callType: String = ""
    private var incomingOutGoing: String = ""
    private var channelId: String = ""
    private var toId: String = ""
    private var fromId: String = ""
    private var doctorname: String = ""
    private var chatId: String = ""
    private var doctorImage: String = ""
    private var bookingId: String = ""

    private var callViewModel: CallViewModel? = null
    private lateinit var notificationEventListner: Disposable

    var ringTone: Ringtone? = null
    var mPlayer: MediaPlayer? = null

    var incomingConstraintSet: ConstraintSet? = null
    var acceptedConstraintSet: ConstraintSet? = null
    var bookingTime = ""

    var handler: Handler? = null
    lateinit var runnable: Runnable
    var sharedHelper: SharedHelper? = null

    private val mRtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        // Tutorial Step 1
        override fun onFirstRemoteVideoDecoded(
            uid: Int,
            width: Int,
            height: Int,
            elapsed: Int
        ) { // Tutorial Step 5
            runOnUiThread { setupRemoteVideo(uid) }
        }

        override fun onUserJoined(uid: Int, elapsed: Int) {
            super.onUserJoined(uid, elapsed)
            runOnUiThread {
                stopRingtone()
                stopEarpiceRingtone()
                onCallAccepted()
            }
        }

        override fun onUserOffline(uid: Int, reason: Int) { // Tutorial Step 7
            runOnUiThread { onRemoteUserLeft() }
        }

        override fun onUserMuteVideo(uid: Int, muted: Boolean) { // Tutorial Step 10
            runOnUiThread { onRemoteUserVideoMuted(uid, muted) }
        }


    }


    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        binding = ActivityCallBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_call)
        sharedHelper = SharedHelper(this)
        callViewModel = ViewModelProvider(this).get(CallViewModel::class.java);
        getIntentValues()
        initListener()

    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setUi() {

        if (callType == Constants.ChatTypes.VOICE_CALL) {
            if (incomingOutGoing == Constants.ChatTypes.INCOMING_CALL) {
                setIncomingVoice()
                startRigtone()
            } else if (incomingOutGoing == Constants.ChatTypes.OUTGOING_CALL) {
                setOutgoingVoice()
            }

        } else if (callType == Constants.ChatTypes.VIDEO_CALL) {
            if (incomingOutGoing == Constants.ChatTypes.INCOMING_CALL) {
                setIncomingVideo()
                startRigtone()
            } else if (incomingOutGoing == Constants.ChatTypes.OUTGOING_CALL) {
                setOutgoingVideo()
            }

        }

    }

    override fun onResume() {
        super.onResume()

        runnable = Runnable {

            if (BaseUtils.isConsultationTimeOver(
                    BaseUtils.getFormatedDateUtc(
                        bookingTime,
                        "yyyy-MM-dd'T'HH:mm:ss",
                        "dd/MM/yyyy hh:mm a"
                    )!!, "dd/MM/yyyy hh:mm a",
                    sharedHelper?.chatTiming!!
                )
            ) {
                endCall(true)
                handler?.removeCallbacks(runnable)
            } else {
                handler?.postDelayed(runnable, 5000)
            }

        }

        handler?.removeCallbacks(runnable)
        handler?.postDelayed(runnable, 5000)
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    private fun setOutgoingVideo() {

        binding!!.audio.callLayout.visibility = View.GONE
        binding!!.video.videoLayout.visibility = View.VISIBLE

        binding!!.video.groupCommonVideo.visibility = View.VISIBLE
        binding!!.video.groupIncomingVideo.visibility = View.GONE
        binding!!.video.groupOutGoingVideo.visibility = View.VISIBLE

        startEarpeiceRingtone()


    }

    private fun setOutgoingVoice() {

        binding!!.audio.callLayout.visibility = View.VISIBLE
        binding!!.video.videoLayout.visibility = View.GONE

        binding!!.audio.groupOutGoing.visibility = View.VISIBLE
        binding!!.audio.groupInComing.visibility = View.GONE
        startEarpeiceRingtone()
    }

    private fun setIncomingVideo() {

        binding!!.audio.callLayout.visibility = View.GONE
        binding!!.video.videoLayout.visibility = View.VISIBLE

        binding!!.video.groupCommonVideo.visibility = View.VISIBLE
        binding!!.video.groupIncomingVideo.visibility = View.VISIBLE
        binding!!.video.groupOutGoingVideo.visibility = View.GONE

    }

    private fun setIncomingVoice() {
        binding!!.audio.callLayout.visibility = View.VISIBLE
        binding!!.video.videoLayout.visibility = View.GONE

        binding!!.audio.groupOutGoing.visibility = View.GONE
        binding!!.audio.groupInComing.visibility = View.VISIBLE

    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequiresApi(Build.VERSION_CODES.P)
    private fun getIntentValues() {

        intent.extras?.let {
            callType = it.getString(Constants.NotificationIntentValues.CALL_TYPE, "")

            incomingOutGoing = it.getString(Constants.NotificationIntentValues.CALL_FROM, "")
            channelId = it.getString(Constants.NotificationIntentValues.CHANNEL_ID, "")
            Log.d("ghjuik",""+channelId)

            toId = it.getString(Constants.NotificationIntentValues.TO_ID, "")
            fromId = it.getString(Constants.NotificationIntentValues.FROM_ID, "")
            doctorname = it.getString(Constants.NotificationIntentValues.NAME, "")

            chatId = it.getString(Constants.NotificationIntentValues.ID, "")
            doctorImage = it.getString(Constants.NotificationIntentValues.IMAGE, "")
            bookingTime = it.getString(Constants.NotificationIntentValues.BOOKINGDATETIME, "")
            bookingId = it.getString(Constants.NotificationIntentValues.BOOKINGID, "")
        }

        binding!!.audio.doctoreName.text = doctorname
        binding!!.video.doctoreNameVideo.text = doctorname

        UiUtils.loadImage(
            binding!!.video.doctorImageVideo,
            doctorImage,
            getDrawable(R.drawable.place_holder_doctor)!!
        )
        UiUtils.loadImage(
            binding!!.audio.doctorImageAudio,
            doctorImage,
            getDrawable(R.drawable.place_holder_doctor)!!
        )

        askPermission()
        setUi()
    }

    private fun askPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (callType == Constants.ChatTypes.VOICE_CALL) {
                requestPermissions(
                    Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                    Constants.Permission.AUDIO_CALL_PERMISSION
                )
            } else if (callType == Constants.ChatTypes.VIDEO_CALL) {
                requestPermissions(
                    Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                    Constants.Permission.VIDEO_CALL_PERMISSION
                )
            }

        } else {
            initAgoraEngine()
        }

    }

    private fun initListener() {


        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                runOnUiThread {
                    when {
                        //made by user
                        it.equals(Constants.EventBusKeys.ACCEPT_CALL, true) -> {
                            onCallAccepted()
                        }
                        //made by user
                        it.equals(Constants.EventBusKeys.REJECT_CALL, true) -> {
                            endCall(false)
                        }
                        //made by both
                        it.equals(Constants.EventBusKeys.END_CALL, true) -> {
                            endCall(false)
                        }
                        //made by both
                        it.equals(Constants.EventBusKeys.HANGUP_CALL, true) -> {
                            endCall(false)
                        }
                    }
                }
            }

    }


    private fun initAgoraEngine() {
       // Toast.makeText(applicationContext,"agoraengineinitialized", Toast.LENGTH_LONG).show()

        initializeAgoraEngine() // Tutorial Step 1

        setupVideoProfile() // Tutorial Step 2

        setupLocalVideo() // Tutorial Step 3

        if (incomingOutGoing == Constants.ChatTypes.OUTGOING_CALL) {
            joinChannel(channelId)
        }

        setSpeakerUi()
        setMicUi()
        setVideoMuteUi()

    }

    // Tutorial Step 1
    private fun initializeAgoraEngine() {

        try {
            mRtcEngine =
                RtcEngine.create(baseContext, getString(R.string.agora_app_id), mRtcEventHandler)
        } catch (e: Exception) {
            throw RuntimeException(
                "NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(
                    e
                )
            )
        }
    }

    // Tutorial Step 2
    private fun setupVideoProfile() {
        mRtcEngine!!.enableAudio()
        changeSpeakerAudio()

        //mRtcEngine!!.setAudioProfile(io.agora.rtc.Constants.AUDIO_PROFILE_SPEECH_STANDARD, io.agora.rtc.Constants.AUDIO_SCENARIO_DEFAULT)

        mRtcEngine!!.setVideoProfile(io.agora.rtc.Constants.VIDEO_PROFILE_240P_3, false)
        if (callType == Constants.ChatTypes.VOICE_CALL) {
            mRtcEngine!!.disableVideo()
        } else {
            mRtcEngine!!.enableVideo()
        }

        mRtcEngine!!.setVideoEncoderConfiguration(
            VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
            )
        )
    }


    // Tutorial Step 3
    private fun setupLocalVideo() {
        val surfaceView = RtcEngine.CreateRendererView(baseContext)
        surfaceView.setZOrderMediaOverlay(true)
        binding!!.video.localVideoViewContainer.addView(surfaceView)
        mRtcEngine!!.setupLocalVideo(VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, 0))
    }

    // Tutorial Step 4
    private fun joinChannel(channelId: String?) {
        mRtcEngine!!.joinChannel(
            null,
            channelId,
            "Extra Optional Data",
            0
        ) // if you do not specify the uid, we will generate the uid for you
//        mRtcEngine!!.joinChannel(null, "demo", "Extra Optional Data", 0) // if you do not specify the uid, we will generate the uid for you
    }


    // Tutorial Step 5
    private fun setupRemoteVideo(uid: Int) {

        if (binding!!.video.remoteVideoViewContainer.childCount >= 1) {
            return
        }
        val surfaceView = RtcEngine.CreateRendererView(baseContext)
        binding!!.video.remoteVideoViewContainer.addView(surfaceView)
        mRtcEngine!!.setupRemoteVideo(VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, uid))
        surfaceView.tag = uid // for mark purpose

    }

    // Tutorial Step 7
    private fun onRemoteUserLeft() {
        binding!!.video.remoteVideoViewContainer.removeAllViews()
        endCall(false)
    }

    // Tutorial Step 10
    private fun onRemoteUserVideoMuted(uid: Int, muted: Boolean) {
        val surfaceView = binding!!.video.remoteVideoViewContainer.getChildAt(0) as SurfaceView
        val tag = surfaceView.tag
        if (tag != null && tag as Int == uid) {
            surfaceView.visibility = if (muted) View.GONE else View.VISIBLE
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
     //   Toast.makeText(applicationContext,""+requestCode+"=>permissions=>"+permissions+"grant"+grantResults,Toast.LENGTH_LONG).show()

        if (requestCode == Constants.Permission.VIDEO_CALL_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                    Constants.Permission.AUDIO_CALL_PERMISSION

                )
            } else {
                initAgoraEngine()
            }
        } else if (requestCode == Constants.Permission.AUDIO_CALL_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                    Constants.Permission.VIDEO_CALL_PERMISSION
                )
            } else {
                initAgoraEngine()
            }
        }
    }


    //user actions
    fun onVideoMuteClicked2(view: View) {

        val iv = view as ImageView
        if (iv.isSelected) {
            iv.isSelected = false
            iv.clearColorFilter()
        } else {
            iv.isSelected = true
            iv.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
        }

        mRtcEngine!!.muteLocalVideoStream(iv.isSelected)

        val surfaceView = binding!!.video.localVideoViewContainer.getChildAt(0) as SurfaceView
        surfaceView.setZOrderMediaOverlay(!iv.isSelected)
        surfaceView.visibility = if (iv.isSelected) View.GONE else View.VISIBLE
    }

    fun onAudioMuteClicked(view: View) {
        val iv = view as ImageView
        if (iv.isSelected) {
            iv.isSelected = false
            iv.clearColorFilter()
        } else {
            iv.isSelected = true
            iv.setColorFilter(
                ContextCompat.getColor(this, R.color.colorPrimary),
                PorterDuff.Mode.MULTIPLY
            )
        }

        mRtcEngine!!.muteLocalAudioStream(iv.isSelected)
    }

    fun onSwitchCameraClicked2(view: View) {
        mRtcEngine!!.switchCamera()
    }

    fun onEndCallClicked(view: View) {
        finish()
    }

    fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(permission),
                requestCode
            )
            return false
        }
        return true
    }

//    ---------------------------------------------------------------------------------------------------------------------------------

    //voice call actions
    public fun onEndVoiceCall(view: View) {
        DialogUtils.showAlertDialog(this,
            getString(R.string.complete_booking),
            getString(R.string.confirm),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    endCall(true)
                }

                override fun onNegativeClick() {

                }
            })

    }

    public fun onVoiceCallAccepted(view: View) {
        onCallAccepted()
    }

    //video call actions
    public fun onVideoCallAccepted(view: View) {
        onCallAccepted()
    }

    public fun onEndVideoCall(view: View) {

        DialogUtils.showAlertDialog(this,
            getString(R.string.complete_booking),
            getString(R.string.confirm),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    endCall(true)
                }

                override fun onNegativeClick() {
                    endCall2()
                }
            })
    }

    //    ---------------------------------------------------------------------------------------------------------------------------------

    private fun onCallAccepted() {

        joinChannel(channelId)

        if (incomingOutGoing == Constants.ChatTypes.INCOMING_CALL) {
            acceptCall()
        }

        if (callType == Constants.ChatTypes.VOICE_CALL) {

            binding!!.audio.groupInComing.visibility = View.GONE
            binding!!.audio.groupOutGoing.visibility = View.VISIBLE

            binding!!.audio.status.text = resources.getString(R.string.connected)


        } else if (callType == Constants.ChatTypes.VIDEO_CALL) {

            binding!!.video.groupCommonVideo.visibility = View.GONE
            binding!!.video.groupIncomingVideo.visibility = View.GONE
            binding!!.video.groupOutGoingVideo.visibility = View.VISIBLE


//            setCameraAnimate()
        }

    }

    //    ---------------------------------------------------------------------------------------------------------------------------------

    private fun endCall2() {
        notificationEventListner.dispose()
        callViewModel?.endCall(channelId, fromId, toId,callType)?.observe(this, Observer {
            if (it.error!!) {
                UiUtils.showSnack(binding!!.root, it.message!!)
            } else if (!it.error!!) {
                stopRingtone()
                stopEarpiceRingtone()
                NotificationUtils(this).removeCallNotifications()
                mRtcEngine?.leaveChannel()
                RtcEngine.destroy()

                finish()
            }
        })

    }
    private fun endCall(completedBooking: Boolean) {
        notificationEventListner.dispose()
        if (completedBooking) {
            callViewModel?.completeBooking(bookingId)
        }
        callViewModel?.endCall(channelId, fromId, toId, callType)?.observe(this, Observer {
            if (it.error!!) {
                UiUtils.showSnack(binding!!.root, it.message!!)
            } else if (!it.error!!) {
                stopRingtone()
                stopEarpiceRingtone()
                NotificationUtils(this).removeCallNotifications()
                mRtcEngine?.leaveChannel()
                RtcEngine.destroy()

                finish()
            }
        })

    }

    private fun acceptCall() {
        NotificationUtils(this).notificationEndCall(doctorname)
        callViewModel?.acceptCall(channelId, fromId, toId)
    }


    //    ---------------------------------------------------------------------------------------------------------------------------------


    override fun onDestroy() {
        super.onDestroy()
        stopRingtone()
        stopEarpiceRingtone()
        NotificationUtils(this).removeCallNotifications()
    }


    override fun onBackPressed() {

    }

    //    ---------------------------------------------------------------------------------------------------------------------------------

    @RequiresApi(Build.VERSION_CODES.P)
    private fun startRigtone() {
        val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        ringTone = RingtoneManager.getRingtone(applicationContext, notification)
        ringTone?.isLooping = true
        ringTone?.play()
    }

    private fun stopRingtone() {
        ringTone?.let { tone ->
            if (tone.isPlaying) {
                tone.stop()
            }
        }
    }

    private fun startEarpeiceRingtone() {

        mPlayer = MediaPlayer()

        var mUri = Uri.parse(
            "android.resource://"
                    + packageName + "/raw/ringsound"
        );
        mPlayer?.setDataSource(this, mUri)
        mPlayer?.setAudioStreamType(AudioManager.STREAM_VOICE_CALL)
        mPlayer?.isLooping = true
        mPlayer?.prepare()
        mPlayer?.start()
    }

    private fun stopEarpiceRingtone() {
        mPlayer?.let { player ->
            if (player.isPlaying) {
                player.stop()
            }
        }
    }

    //    ---------------------------------------------------------------------------------------------------------------------------------


    public fun onSpeakerChangeClicked(view: View) {
        changeSpeakerAudio()
    }

    public fun onMicChangeClicked(view: View) {
        changeMicSettings()
    }

    fun onVideoMuteClicked(view: View) {
        changeVideo()
    }

    fun onSwitchCameraClicked(view: View) {
        setCameraFacing()
    }

    //    ---------------------------------------------------------------------------------------------------------------------------------

    private fun setSpeakerUi() {
        mRtcEngine?.let {

            mRtcEngine?.setEnableSpeakerphone(isSpeakerOnAudio)
            if (isSpeakerOnAudio) {
                binding!!.audio.speaker.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.speaker_on))
            } else {
                binding!!.audio.speaker.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.speaker_off))
            }
        }
    }

    private fun changeSpeakerAudio() {
        isSpeakerOnAudio = !isSpeakerOnAudio
        setSpeakerUi()
    }


    private fun setMicUi() {

        mRtcEngine?.let {
            it.muteLocalAudioStream(isMute)
            if (isMute) {
                binding!!.audio.mic.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.mic_off))
                binding!!.video.micVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.mic_off))
            } else {
                binding!!.audio.mic.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.mic_on))
                binding!!.video.micVideo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.mic_on))
            }
        }
    }

    private fun changeMicSettings() {
        isMute = !isMute
        setMicUi()
    }

    private fun setCameraFacing() {
        mRtcEngine?.let {
            it.switchCamera()
        }
    }

    private fun setVideoMuteUi() {
        mRtcEngine?.let {
            it.muteLocalVideoStream(isVideoMute)
            if (isVideoMute) {
                binding!!.video.camera.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.video_off))
            } else {
                binding!!.video.camera.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.video_on))
            }
        }
    }

    private fun changeVideo() {
        isVideoMute = !isVideoMute
        setVideoMuteUi()
    }
}