package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityArticleBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.ArticlesData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.HomeArticleAdapter

class ArticleActivity : BaseActivity() {

    var binding: ActivityArticleBinding? = null
    var list: ArrayList<ArticlesData> = ArrayList()
    var selectedId = 0
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)

        //binding = DataBindingUtil.setContentView(this, R.layout.activity_article)
        getIntentValues()
        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

    }

    private fun getIntentValues() {

        intent.extras?.let {
            it.getParcelableArrayList<ArticlesData>(Constants.IntentKeys.ARTICLELIST)?.let { lis ->
                list = lis
            }

            it.getInt(Constants.IntentKeys.ARTICLEID).let { value ->
                selectedId = value
            }
        }


        setArticleData()
    }

    private fun setArticleData() {

        for (i in 0 until list.size) {
            if (list[i].id == selectedId) {
                setData(i)
                break
            }
        }
    }

    private fun setData(position: Int) {

        binding!!.titleArticle.text = list[position].title
        binding!!.content.text = list[position].description

        UiUtils.loadImage(binding!!.articleImage, list[position].articleImage)

        var listShowUp = list
        listShowUp.removeAt(position)
        setAdapter(listShowUp)


    }

    private fun setAdapter(listShowUp: ArrayList<ArticlesData>) {

        binding!!.relatedArticle.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding!!.relatedArticle.adapter = HomeArticleAdapter(this, listShowUp, object : OnClickListener {
            override fun onClickItem(position: Int) {


                startActivity(
                    Intent(this@ArticleActivity, ArticleActivity::class.java)
                        .putExtra(Constants.IntentKeys.ARTICLEID, list[position].id)
                        .putExtra(Constants.IntentKeys.ARTICLELIST, list)
                )
                finish()

            }

        })
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}