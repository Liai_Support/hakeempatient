package com.app.hakeemUser.view.activity

import android.R.attr
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityLogin1Binding
import com.app.hakeemUser.databinding.ActivityLoginBinding
import com.app.hakeemUser.models.CheckOtpData
import com.app.hakeemUser.models.GetOtpData
import com.app.hakeemUser.models.LoginData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.RegisterViewModel
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import org.json.JSONObject


class LoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    var binding: ActivityLogin1Binding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    private var mGoogleSignInClient: GoogleApiClient? = null
    lateinit var callbackManager: CallbackManager
    var finishAct = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLogin1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_login1)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)
        FacebookSdk.sdkInitialize(applicationContext)
        setView()
        getIntentValues()
        initFacebookLogin()
        initGoogleLogin()
        Log.d("edrftgyhujikolp;",""+ sharedHelper!!.language)

    }

    private fun showDialog1(mail: String) {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.findViewById<EditText>(R.id.mail).setText(mail)
        //login button click of custom layout
        mDialogView.findViewById<Button>(R.id.send).setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            //get text from EditTexts of custom layout
            val mail = mDialogView.findViewById<EditText>(R.id.mail).text.toString()
            getOtp(mail)
        }
    }

    private fun getOtp(email: String) {
        DialogUtils.showLoader(this)
        viewmodel?.getOtp(email)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root1, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: GetOtpData) {
        data.otp?.let {
          //  otp.setText(it)
            showDialog2(it)

        }
    }

    private fun showDialog2(s: String) {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget_otp, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView).setCancelable(false)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //login button click of custom layout
        //mDialogView.otp.setText(s)
        mDialogView.findViewById<TextView>(R.id.getStarted).setOnClickListener {
            //dismiss dialog
            //mAlertDialog.dismiss()
            if (mDialogView.findViewById<EditText>(R.id.otp).text.toString().trim() == "") {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourotp))
            } else {
                DialogUtils.showLoader(this)
                viewmodel?.verifyOtp(binding!!.emailId.text.toString(), mDialogView.findViewById<EditText>(R.id.otp).text.toString())?.observe(this,
                        Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            mAlertDialog.dismiss()
                                            UiUtils.showSnack(binding!!.root1, msg)
                                        }
                                    } else {
                                        it.data?.let { data ->
                                            mAlertDialog.dismiss()
                                            handleCheckotpResponse(data)
                                        }
                                    }
                                }

                            }

                        })
            }

            //get text from EditTexts of custom layout
        }
    }

    private fun handleCheckotpResponse(data: CheckOtpData) {
        data.token?.let { sharedHelper?.token = it }
       /* startActivity(Intent(this, ResetPasswordActivity::class.java))
        finish()*/
        showDialog3()
    }

    private fun showDialog3(){
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget_new, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
        //show dialog
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //login button click of custom layout
        mDialogView.findViewById<Button>(R.id.onResetPasswordClicked).setOnClickListener {
            //dismiss dialog
            if(mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().equals("") || mAlertDialog.findViewById<EditText>(R.id.confirmPassword).text.toString().equals("")){
                UiUtils.showSnack(mDialogView,getString(R.string.pleaseenterpassword))
            }
            else{
                if(mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().trim().equals(mAlertDialog.findViewById<EditText>(R.id.confirmPassword).text.toString().trim())){
                    mAlertDialog.dismiss()
                    DialogUtils.showLoader(this)
                    viewmodel?.resetPassword(mAlertDialog.findViewById<EditText>(R.id.newPassword).text.toString().trim())?.observe(this, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root1, msg)
                                    }
                                } else {
                                    val intent =
                                        Intent(this, LoginActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
                                }
                            }

                        }
                    })
                }
                else{
                    UiUtils.showSnack(mDialogView,getString(R.string.passwordmismatch))
                }
            }
            //get text from EditTexts of custom layout
        }

        mDialogView.findViewById<ImageView>(R.id.show_pass_btn1).setOnClickListener {
            if(mDialogView.findViewById<EditText>(R.id.newPassword).getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

                //Show Password
                mDialogView.findViewById<EditText>(R.id.newPassword).setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

                //Hide Password
                mDialogView.findViewById<EditText>(R.id.newPassword).setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
        mDialogView.findViewById<ImageView>(R.id.show_pass_btn2).setOnClickListener {
            if(mDialogView.findViewById<EditText>(R.id.confirmPassword).getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

                //Show Password
                mDialogView.findViewById<EditText>(R.id.confirmPassword).setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

                //Hide Password
                mDialogView.findViewById<EditText>(R.id.confirmPassword).setTransformationMethod(PasswordTransformationMethod.getInstance());

            }
        }
    }



    private fun getIntentValues() {

        intent.extras?.let {
            it.getBoolean(Constants.IntentKeys.FINISHACT).let { value ->
                finishAct = value

                if (finishAct) {
                    //forgotPassword.isEnabled = false
                    //forgotImage.isEnabled = false
                   // dontHaveAcc.isEnabled = false
                    //textView2.isEnabled = false

                    //forgotPassword.visibility = View.INVISIBLE
                   // forgotImage.visibility = View.INVISIBLE
                    //dontHaveAcc.visibility = View.INVISIBLE
                    //textView2.visibility = View.INVISIBLE

                }
            }
        }

    }

    private fun setView() {
        binding?.dontHaveAcc?.text =
            SpannableStringBuilder().append(resources.getString(R.string.don_t_have_an_account))
                .append(" ").append(
                    UiUtils.getBoldUnderlineString(
                        this,
                        resources.getString(R.string.sign_up_now)
                    )
                )
    }

    fun onSignUpClicked(view: View) {
        startActivity(Intent(this, RegisterActivity1::class.java))
    }

    fun onLoginClicked(view: View) {
/*

        // Initialize a new layout inflater instance
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.popup_signup,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.RIGHT
            popupWindow.exitTransition = slideOut

        }

        // Get the widgets reference from custom view
        val tv = view.findViewById<TextView>(R.id.text_view)
        val buttonPopup = view.findViewById<Button>(R.id.button_popup)

        // Set click listener for popup window's text view
        tv.setOnClickListener{
            // Change the text color of popup window's text view
            tv.setTextColor(Color.RED)
        }

        // Set a click listener for popup's button widget
        buttonPopup.setOnClickListener{
            // Dismiss the popup window
            popupWindow.dismiss()
        }

        // Set a dismiss listener for popup window
        popupWindow.setOnDismissListener {
            Toast.makeText(applicationContext,"Popup closed",Toast.LENGTH_SHORT).show()
        }


        // Finally, show the popup window on app
        TransitionManager.beginDelayedTransition(root)
        popupWindow.showAtLocation(
                root, // Location to display popup window
                Gravity.CENTER, // Exact position of layout to display popup
                0, // X offset
                0 // Y offset
        )
*/

        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.signin(binding!!.emailId.text.toString().trim(), binding!!.password.text.toString().trim())
                ?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root1, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        handleResponse(data)
                                    }
                                }
                            }

                        }
                    })
        }


    }

    fun onForgotPasswordClicked(view: View) {
        if (binding!!.emailId.text.toString().trim() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryouremailid))
        } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
            UiUtils.showSnack(binding!!.root1, getString(R.string.plaeseentervallidemailid))
        } else {
            showDialog1(binding!!.emailId.text.toString().trim())

          /*  startActivity(
                Intent(this, ForgotPasswordActivity::class.java)
                    .putExtra(Constants.IntentKeys.EMAIL, emailId.text.toString().trim())
            )*/
        }

    }

    private fun handleResponse(data: LoginData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }
        data.countryCode?.let { value -> sharedHelper?.countryCode = value }
        data.profilePic?.let { value -> sharedHelper?.userImage = value }
        data.istatus?.let { value -> sharedHelper?.istatus = value}

        sharedHelper?.loggedIn = true
        sharedHelper?.selectedaddressid = 0

        if (finishAct) {
            finish()
        } else {
            val intent =
                Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

    }

    private fun isValidInputs(): Boolean {
        when {

            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.plaeseentervallidemailid))
                return false
            }

            binding!!.password.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root1, getString(R.string.pleaseenteryourpassword))
                return false
            }

            else -> return true
        }


    }

    private fun initGoogleLogin() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("552818571547-jmmviiopg8tbq8g5erp9a15dvo9sfuns.apps.googleusercontent.com")//you can also use R.string.default_web_client_id
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    private fun googlesignIn() {

        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient)
        startActivityForResult(signInIntent, 100)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {

            if (requestCode == 100 || data.getData()!=null) {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

                result?.let {
                    mGoogleSignInClient?.clearDefaultAccountAndReconnect()
                    handleSignInResult(it)
                }

            } else {
                callbackManager.onActivityResult(requestCode, resultCode, data)
            }
        }
        else{
            UiUtils.showSnack(binding!!.root1, getString(R.string.invalllid))
        }
    }


    private fun get_data_for_facebook(loginResult: LoginResult) {

        val request = GraphRequest.newMeRequest(loginResult.accessToken) { value, response ->
            handleFacebookSuccessResponse(
                value,
                response
            )
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, locale ")
        request.parameters = parameters
        request.executeAsync()
    }

    private fun handleFacebookSuccessResponse(value: JSONObject, response: GraphResponse) {

        val email_json = value.optString("email")
        val first_name = value.optString("first_name")
       // val last_name = value.optString("last_name")
        val id = value.optString("id")
       // val type = "facebook"
        val imageUrl = "https://graph.facebook.com/$id/picture?type=large"

        LoginManager.getInstance().logOut()


        viewmodel?.socialLogin(id, Constants.SocialLogin.FACEBOOK)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root1, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            data.userExists?.let { exist ->
                                if (exist) {
                                    data.email?.let { value -> sharedHelper?.email = value }
                                    data.id?.let { value -> sharedHelper?.id = value }
                                    data.name?.let { value -> sharedHelper?.name = value }
                                    data.mobileNumber?.let { value ->
                                        sharedHelper?.mobileNumber = value
                                    }
                                    data.token?.let { value -> sharedHelper?.token = value }
                                    data.countryCode?.let { value ->
                                        sharedHelper?.countryCode = value
                                    }
                                    data.profilePic?.let { value ->
                                        sharedHelper?.userImage = value
                                    }
                                    data.istatus?.let { value -> sharedHelper?.istatus = value}


                                    sharedHelper?.loggedIn = true
                                    sharedHelper?.selectedaddressid = 0
                                    if (finishAct) {
                                        finish()
                                    } else {
                                        val intent =
                                            Intent(this, DashBoardActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()
                                    }

                                } else {

                                    startActivity(
                                        Intent(this, SocialLoginDetailsActivity::class.java)
                                            .putExtra(Constants.IntentKeys.EMAIL, email_json)
                                            .putExtra(Constants.IntentKeys.NAME, first_name)
                                            .putExtra(
                                                Constants.IntentKeys.PROFILE_PICTURE,
                                                imageUrl
                                            )
                                            .putExtra(Constants.IntentKeys.SOCIALTOKEN, id)
                                            .putExtra(
                                                Constants.IntentKeys.LOGINTYPE,
                                                Constants.SocialLogin.FACEBOOK
                                            )
                                    )

                                }
                            }


                        }
                    }
                }
            }
        })


    }

    private fun handleSignInResult(result: GoogleSignInResult) {
       // var ha = true
        if (result.isSuccess) {
            val googleSignInAccount = result.signInAccount
            val email = googleSignInAccount!!.email
            val first_name: String
            val last_name: String
            val type = "google"
            val id = googleSignInAccount.id
            val imageUrl = googleSignInAccount.photoUrl.toString()
            val full_name = googleSignInAccount.displayName
            if (full_name!!.contains(" ")) {
                val names =
                    full_name.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                first_name = names[0]
                last_name = names[1]
            } else {
                first_name = full_name
                last_name = ""
            }

            Auth.GoogleSignInApi.signOut(mGoogleSignInClient)

            DialogUtils.showLoader(this)
            viewmodel?.socialLogin(id, Constants.SocialLogin.GOOGLE)?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.userExists?.let { exist ->
                                    if (exist) {
                                        data.email?.let { value -> sharedHelper?.email = value }
                                        data.id?.let { value -> sharedHelper?.id = value }
                                        data.name?.let { value -> sharedHelper?.name = value }
                                        data.mobileNumber?.let { value ->
                                            sharedHelper?.mobileNumber = value
                                        }
                                        data.token?.let { value -> sharedHelper?.token = value }
                                        data.countryCode?.let { value ->
                                            sharedHelper?.countryCode = value
                                        }
                                        data.profilePic?.let { value ->
                                            sharedHelper?.userImage = value
                                        }
                                        data.istatus?.let { value -> sharedHelper?.istatus = value}


                                        sharedHelper?.loggedIn = true
                                        sharedHelper?.selectedaddressid = 0
                                        if (finishAct) {
                                            finish()
                                        } else {
                                            val intent =
                                                Intent(this, DashBoardActivity::class.java)
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            intent.flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            startActivity(intent)
                                            finish()
                                        }

                                    } else {

                                        startActivity(
                                            Intent(this, SocialLoginDetailsActivity::class.java)
                                                .putExtra(Constants.IntentKeys.EMAIL, email)
                                                .putExtra(Constants.IntentKeys.NAME, first_name)
                                                .putExtra(
                                                    Constants.IntentKeys.PROFILE_PICTURE,
                                                    imageUrl
                                                )
                                                .putExtra(Constants.IntentKeys.SOCIALTOKEN, id)
                                                .putExtra(
                                                    Constants.IntentKeys.LOGINTYPE,
                                                    Constants.SocialLogin.GOOGLE
                                                )
                                        )

                                    }
                                }


                            }
                        }
                    }
                }
            })


        }
        else{
           // UiUtils.showSnack(root1, "Invalllid")
        }
    }

    private fun initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()

        binding!!.loginButtonFacebook.setReadPermissions("email")
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                get_data_for_facebook(loginResult)
            }

            override fun onCancel() {
                UiUtils.showLog("facebook", "")
            }

            override fun onError(exception: FacebookException) {
                UiUtils.showLog("facebook", exception.toString())
            }
        })
    }


    fun onFacebookClicked(view: View) {
        if (NetworkUtils.isNetworkConnected(this))
            binding!!.loginButtonFacebook.performClick()
        else
            UiUtils.showSnack(
                findViewById(android.R.id.content),
                resources.getString(R.string.no_internet_connection)
            )
    }

    fun OnGoogleClicked(view: View) {
        googlesignIn()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    fun ShowHidePass(view: View) {
        if(binding!!.password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){

            //Show Password
            binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_24);

            binding!!.password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{

            //Hide Password
            binding!!.showPassBtn.setImageResource(R.drawable.pass_visibility_off_24);

            binding!!.password.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }




}