package com.app.hakeemUser.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildDoctorConsultationBinding
import com.app.hakeemUser.databinding.ChildDoctorVirtualConsultationBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.PatientActionListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.ConsultationData
import com.app.hakeemUser.models.CurrentBookingListJson
import com.app.hakeemUser.utils.*

class ConsultationAdapter(
    var context: Context,
    var list: ArrayList<CurrentBookingListJson>,
    var onClickListener: OnClickListener,
    var action: PatientActionListener,
    var cancelClick: OnClickListener
) :
    RecyclerView.Adapter<ConsultationAdapter.ViewHolder>() {
    var testnamee2:ArrayList<CurrentBookingListJson>? = null

    var sharedHelper: SharedHelper? = null
    var viewModelStoreOwner: ViewModelStoreOwner? = null

    init {
        sharedHelper = SharedHelper(context)
    }

    inner class ViewHolder(view: View, type: Int) : RecyclerView.ViewHolder(view) {

        var binding: ChildDoctorConsultationBinding? = null
        var virtualBinding: ChildDoctorVirtualConsultationBinding? = null

        init {
            if (type == 0) {
                binding = ChildDoctorConsultationBinding.bind(view)
            } else {
                virtualBinding =
                    ChildDoctorVirtualConsultationBinding.bind(view)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == 0) {
            return ViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.child_doctor_consultation,
                    parent,
                    false
                )
                , 0
            )
        } else {
            return ViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.child_doctor_virtual_consultation,
                    parent,
                    false
                )
                , 1
            )
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (list[position].isVirtualBooking == null || list[position].isVirtualBooking == 0) {
            holder.binding?.bookingid1!!.text =  list[position].bookingIds.toString()
            holder.binding?.root?.setOnClickListener {
                onClickListener.onClickItem(position)
            }

            list[position].profilePic?.let {
                UiUtils.loadImage(
                    holder.binding?.circleImageView2,
                    it,
                    context.getDrawable(R.drawable.place_holder_doctor)!!
                )
            }


            holder.binding?.doctorName?.text = list[position].providerName
          //  holder.binding?.qualification?.text = list[position].education+"-"+list[position].specialityName
            holder.binding?.qualification?.text = list[position].specialityName

            if(list[position].previousIssue.equals("not mentioned",true)){
                holder.binding?.issuee?.text = "Not mentioned"
            }else  if (list[position].previousIssue==""){
                holder.binding!!.issuee.text = "Not mentioned"

            }
            else {
                holder.binding?.issuee?.text = list[position].previousIssue
            }

            holder.binding!!.healthIssue.text = list[position].expierence +" year experience"
            holder.binding?.fee?.text = " : "+list[position].fee + " SAR"

        /*    if (list[position].previousIssue==""){
                holder.binding!!.issuee.text = context.getString(R.string.nil)

            }else{
                holder.binding!!.issuee.text = list[position].previousIssue

            }*/


            if ( list[position].booking_type=="home_visit"){

                holder.binding!!.textViewststuss.text =" Home visit "

            }else if ( list[position].booking_type=="virtual"){
                holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"

            }else if ( list[position].booking_type=="emergency"){
                holder.binding!!.textViewststuss.text =" Emergency"

            }else if ( list[position].booking_type=="home_health_care"){
                holder.binding!!.textViewststuss.text =" Home Health Care"

            }else if ( list[position].booking_type=="global"){
                holder.binding!!.textViewststuss.text =" Global"

            }
            list[position].status?.let {
                when {
                    it.equals("pending", true) -> {

                        holder.binding?.status?.text =
                           " "+ context.resources.getString(R.string.pending)
                    }
                    it.equals("startvisit", true) -> {
                        holder.binding?.status?.text = " "+ context.getString(R.string.start_visit)

                    }
                    it.equals("completed", true) -> {
                        holder.binding?.status?.text = " "+ context.getString(R.string.visit_completed)

                    }
                    it.equals("cancelledbyprovider", true) -> {
                        holder.binding?.status?.text =
                            " "+  context.getString(R.string.cancelled_by_provider)

                    }
                    it.equals("cancelledbypatient", true) -> {
                        holder.binding?.status?.text =
                            " "+ context.getString(R.string.cancelled_by_paitent)

                    }
                    it.equals("paymentverified", true) -> {
                        holder.binding?.status?.text = " "+ context.getString(R.string.payment_verified)

                    }
                }
            }

            if (list[position].isFeatureBooking == "false") {
               // holder.binding?.healthIssue?.text = context.getString(R.string.emergency_booking)
                   Log.d("dscsd",""+ list[position].bookingPeriod)
                list[position].bookingDate?.let {
                    list[position].bookingTime?.let { time ->
                        holder.binding?.dateAndTime?.text =
                                "${BaseUtils.getFormatedDateUtc(
                                        "${BaseUtils.getFormatedDate(
                                                it,
                                                "yyyy-MM-dd HH:mm",
                                                "yyyy-MM-dd"
                                        )} $time",
                                        "yyyy-MM-dd hh:mm:ss",
                                        "dd/MM/yyyy hh:mm a"
                                )}"
                       /* holder.binding?.dateAndTime?.text =
                            BaseUtils.getFormatedDate(it, "yyyy-MM-dd ", "dd/MM/yyyy") + " " +
                                    BaseUtils.getFormatedDate(time, "hh:mm:ss", "hh:mm") + " " +
                                    list[position].bookingPeriod*/
                    }

                }
            }
            else if(list[position].isFeatureBooking == "true"){
                Log.d("dscsd",""+ list[position].bookingPeriod)

                list[position].bookingDate?.let {
                    list[position].bookingTime?.let { time ->
                        holder.binding?.dateAndTime?.text =
                            "${BaseUtils.getFormatedDateUtc(
                                "${BaseUtils.getFormatedDate(
                                    it,
                                    "yyyy-MM-dd HH:mm",
                                    "yyyy-MM-dd"
                                )} $time",
                                "yyyy-MM-dd hh:mm:ss",
                                "dd/MM/yyyy hh:mm"
                            )}"+" "+list[position].bookingPeriod
                    }

                }
            }

        }
        else {
            holder.virtualBinding?.bookingid1!!.text =list[position].bookingIds.toString()
            if (list[position].chat == "1") {
                holder.virtualBinding?.call?.visibility = View.GONE
                holder.virtualBinding?.callText?.visibility = View.GONE
              /*  holder.virtualBinding?.chat?.visibility = View.GONE
                holder.virtualBinding?.textView35?.visibility = View.GONE*/
            } else {
                holder.virtualBinding?.call?.visibility = View.VISIBLE
                holder.virtualBinding?.callText?.visibility = View.VISIBLE
               /* holder.virtualBinding?.chat?.visibility = View.VISIBLE
                holder.virtualBinding?.textView35?.visibility = View.VISIBLE*/
            }

            list[position].profilePic?.let {
                UiUtils.loadImage(
                    holder.virtualBinding?.circleImageView2,
                    it,
                    context.getDrawable(R.drawable.place_holder_doctor)!!
                )
            }

            holder.virtualBinding?.name?.text = list[position].providerName
           holder.virtualBinding?.qualification?.text = list[position].specialityName
           // holder.virtualBinding?.qualification?.text = list[position].education+"-"+list[position].specialityName

            holder.virtualBinding?.fee?.text = " : "+list[position].fee + " SAR"

            holder.virtualBinding!!.healthIssue.text = list[position].expierence +" year experience"
            if(list[position].previousIssue.equals("not mentioned",true)){
                holder.virtualBinding?.issuee?.text = "Not mentioned"
            }else  if (list[position].previousIssue==""){
                holder.virtualBinding!!.issuee.text = "Not mentioned"

            }
            else {
                holder.virtualBinding?.issuee?.text = list[position].previousIssue
            }

            if ( list[position].booking_type=="home_visit"){
                holder.virtualBinding!!.textViewststuss.text =" Home visit"

            }else   if ( list[position].booking_type=="virtual"){
                holder.virtualBinding!!.textViewststuss.text =" Virtual Consult"

            }else if ( list[position].booking_type=="global"){
                holder.virtualBinding!!.textViewststuss.text =" Global"

            }
            list[position].bookingDate?.let {
                list[position].bookingTime?.let { time ->
                    holder.virtualBinding?.startdate?.text =
                        "${BaseUtils.getFormatedDateUtc(
                            it,
                            "yyyy-MM-dd HH:mm",
                            "dd/MM/yyyy "
                        )}"
                }

            }

            list[position].bookingDate?.let {
                list[position].bookingTime?.let { time ->
                    holder.virtualBinding?.startTime?.text =
                        "${BaseUtils.getFormatedDateUtc(
                            it,
                            "yyyy-MM-dd HH:mm",
                            "dd-MM-yyyy hh:mm a"
                        )}"
                }

            }


        /*    list[position].bookingDate?.let {
                list[position].bookingTime?.let { time ->
                    holder.virtualBinding?.enddate?.text =
                        "${BaseUtils.getFormatedDateUtc(
                            it,
                            "yyyy-MM-dd HH:mm",
                            "dd/MM/yyyy "
                        )}"
                }

            }*/



           /* list[position].bookingDate?.let {
                list[position].bookingTime?.let { time ->
                    holder.virtualBinding?.endTime?.text = BaseUtils.addEndTime(
                        "${
                            BaseUtils.getFormatedDateUtc(
                                it,
                                "yyyy-MM-dd HH:mm",
                                "hh:mm a "
                            )
                        }",
                        "dd/MM/yyyy hh:mm a ",
                        "hh:mm a",
                        sharedHelper?.chatTiming!!
                    )
                }
            }
*/




            list[position].bookingDate?.let {
                list[position].bookingTime?.let { time ->
                    holder.virtualBinding?.endTime?.text = BaseUtils.addEndTime(
                        "${BaseUtils.getFormatedDateUtc(
                            it,
                            "yyyy-MM-dd HH:mm",
                            "dd/MM/yyyy hh:mm a"
                        )}",
                        "dd/MM/yyyy hh:mm a",
                        "dd/MM/yyyy hh:mm a",
                        sharedHelper?.chatTiming!!
                    )
                }
            }


            holder.virtualBinding?.chat?.setOnClickListener {
                action.onChatClicked(position)
            }

            holder.virtualBinding?.call?.setOnClickListener {
                action.onCallClicked(position)
                        Log.d("second",""+BaseUtils.isConsultationTime(
                            BaseUtils.getFormatedDateUtc(
                                list[position].bookingDate,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a"
                            , sharedHelper?.chatTiming!!
                        ))

            }

            holder.virtualBinding?.cancel?.setOnClickListener {

                if (list[position].status!! == "pending")
                    if (BaseUtils.isValidCancelTime(
                            BaseUtils.getFormatedDateUtc(
                                list[position].bookingDate,
                                "yyyy-MM-dd HH:mm",
                                "dd/MM/yyyy hh:mm a"
                            )!!, "dd/MM/yyyy hh:mm a"
                            , sharedHelper?.chatTiming!!
                        )
                    ) {
                        cancelClick.onClickItem(position)
                    } else {
                        if (!BaseUtils.isConsultationTimeOver(
                                BaseUtils.getFormatedDateUtc(
                                    list[position].bookingDate,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a"
                                , sharedHelper?.chatTiming!!
                            )
                            && list[position].status == "pending"
                        )
                            DialogUtils.showAlert(
                                context,
                                object : SingleTapListener {
                                    override fun singleTap() {

                                    }

                                },
                                context.getString(R.string.cancellation_alert1)
                                        + " " + sharedHelper?.chatTiming
                                        + " " + context.getString(
                                    R.string.cancellation_alert2
                                )

                            )
                    }

            }

            list[position].status?.let {
                when {
                    it.equals("pending", true) -> {

                        Log.d("xvxcxc xc",""+ BaseUtils.getFormatedDateUtc(
                            list[position].bookingDate,
                            "yyyy-MM-dd HH:mm",
                            "dd/MM/yyyy hh:mm a"
                        )!!)
                        if (BaseUtils.isConsultationTimeOver(
                                BaseUtils.getFormatedDateUtc(
                                    list[position].bookingDate,
                                    "yyyy-MM-dd HH:mm",
                                    "dd/MM/yyyy hh:mm a"
                                )!!, "dd/MM/yyyy hh:mm a"
                                , sharedHelper?.chatTiming!!
                            )
                        ) {
                            holder.virtualBinding?.status?.text =
                                context.resources.getString(R.string.time_expired)
                            holder.virtualBinding?.call?.visibility = View.GONE
                            holder.virtualBinding?.callText?.visibility = View.GONE
                            holder.virtualBinding?.chat?.visibility = View.GONE
                            holder.virtualBinding?.textView35?.visibility = View.GONE

                        } else {
                            holder.virtualBinding?.status?.text =
                                " "+  context.resources.getString(R.string.cancel)
                        }

                    }
                    it.equals("startvisit", true) -> {
                        holder.virtualBinding?.status?.text =
                            " "+ context.getString(R.string.start_visit)

                    }
                    it.equals("virtualbookingcompleted", true) -> {
                        holder.virtualBinding?.status?.text =
                            " "+ context.getString(R.string.booking_completed)

                    }
                    it.equals("cancelledbyprovider", true) -> {
                        holder.virtualBinding?.status?.text =
                            " "+  context.getString(R.string.cancelled_by_provider)

                    }
                    it.equals("cancelledbypatient", true) -> {
                        holder.virtualBinding?.status?.text =
                            " "+  context.getString(R.string.cancelled_by_paitent)

                    }
                    it.equals("paymentverified", true) -> {
                        holder.virtualBinding?.status?.text =
                            " "+    context.getString(R.string.payment_verified)

                    }


                }
            }

        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].isVirtualBooking == 1) {
            1
        } else {
            0
        }
    }

    fun notifyDataChangedAdapter(data: java.util.ArrayList<CurrentBookingListJson>) {
        list = data
        notifyDataSetChanged()
    }

}