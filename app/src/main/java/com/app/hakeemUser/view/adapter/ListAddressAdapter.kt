package com.app.hakeemUser.view.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.CardLocationBinding
import com.app.hakeemUser.models.AddressDetails
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DashBoardActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.view.fragment.HomeFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior


class ListAddressAdapter(
        var context: Context,
        var list: ArrayList<AddressDetails>,
        var homefragment: HomeFragment
) :
        RecyclerView.Adapter<ListAddressAdapter.MyViweHolder>() {

    class MyViweHolder(view: View) : RecyclerView.ViewHolder(view) {
        var childPatientListBinding: CardLocationBinding = CardLocationBinding.bind(view)
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): ListAddressAdapter.MyViweHolder {
        return MyViweHolder(
                LayoutInflater.from(context).inflate(R.layout.card_location, parent, false)

        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ListAddressAdapter.MyViweHolder, position: Int) {

        Log.d("sabhsxbhsaxbhsxbhsx",""+list[position].lat!!.toString())
        Log.d("sabhsxbhsaxbhsxbhsx",""+list[position].lng!!.toString())
        holder.childPatientListBinding.address.text = list[position].address
       // holder.childPatientListBinding.patientType.text = list[position].type

        holder.itemView.setOnClickListener {
          //  (context as DashBoardActivity).
            homefragment.sharedHelper!!.Iscurrentlocationseleted = false
            homefragment.sharedHelper!!.Islocationseleted = true
            homefragment.binding!!.location.setText(""+list[position].address)
            homefragment.myLat = list[position].lat!!
            homefragment.myLng = list[position].lng!!

            homefragment.sharedHelper?.selectedLat = list[position].lat!!.toString()
            homefragment.sharedHelper?.selectedLng = list[position].lng!!.toString()

            Log.d("yewuyuywsww",""+homefragment.sharedHelper?.selectedLat )
            Log.d("yewuyuywsww",""+homefragment.sharedHelper?.selectedLng )


            homefragment.sharedHelper!!.selectedaddressid = list[position].id!!
            homefragment.sharedHelper!!.seletedaddresslocation = list[position].address!!
            homefragment.botomclose()
        }

        holder.childPatientListBinding.delete.setOnClickListener(View.OnClickListener {

            if(holder.childPatientListBinding.address.text.toString().equals(homefragment.binding!!.location.text.toString())){
                UiUtils.showSnack(holder.childPatientListBinding.delete,context.getString(R.string.selectedaddressshouldnotdelete))
            }
            else{
                DialogUtils.showLoader(context)
                list[position].id?.let { it1 ->
                    (context as DashBoardActivity).viewmodel?.deleteaddresss(context, it1)
                        ?.observe(context as DashBoardActivity, Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { message -> UiUtils.showSnack(holder.childPatientListBinding.delete, message) }
                                    } else {
                                        it.message?.let { message -> UiUtils.showSnack(holder.childPatientListBinding.delete, message)
                                            homefragment.listaddress()
                                        }
                                    }
                                }

                            }
                        })
                }

            }

        })

    }

}