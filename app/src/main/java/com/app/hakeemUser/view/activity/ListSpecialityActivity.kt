package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivitySelectSpecialityBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.SpecialityImageData
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.ScheduledBookingSingleton
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.SpecialityAdapter
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException

class ListSpecialityActivity : BaseActivity() {

    var binding: ActivitySelectSpecialityBinding? = null
    var selectedSpeciality: Int? = null
    var fee: String? = null
    var sharedHelper: SharedHelper? = null

    private var socket: Socket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectSpecialityBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_select_speciality)
        sharedHelper = SharedHelper(this)
        getIntentValues()
        initSockets()

        if (sharedHelper!!.language == "ar"){
            binding!!.back.rotation= 180F
        }

    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
//        opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected")
//            initSockets()
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }


    }


    private fun getIntentValues() {

        intent.extras?.let {
            it.getParcelableArrayList<SpecialityImageData>(Constants.IntentKeys.SPECIALITYLIST)
                ?.let { list ->
                    var i = 0
                    while (i < list.size) {

                        if (list[i].name.equals("ambulance", true) ||
                            list[i].name.equals("nurse", true) ||
                            list[i].name.equals("Physiotherapy", true)
                        ) {
                            list.removeAt(i)
                        } else {
                            i++
                        }

                    }


                    var adapter = SpecialityAdapter(this, list, object : OnClickListener {
                        override fun onClickItem(position: Int) {
                            selectedSpeciality = list[position].id
                            fee = list[position].fee.toString()

                        }
                    })
                    binding!!.specialityList.layoutManager = GridLayoutManager(this, 2)
                    binding!!.specialityList.adapter = adapter

                }
        }

    }


    fun onConfirmClicked(view: View) {

        if (validInputs()) {
            selectedSpeciality?.let {
                ScheduledBookingSingleton.getInstance().specialityId = it.toString()
                ScheduledBookingSingleton.getInstance().dfee = fee
                ScheduledBookingSingleton.getInstance().bookingType = Constants.BookingType.DOCTOR

                startActivity(Intent(this, SummaryEmergencyConsultActivity::class.java))
            }


        }


    }

    private fun validInputs(): Boolean {

        if (selectedSpeciality == null) {
            UiUtils.showSnack(binding!!.root, getString(R.string.selectasplecaility))
            return false
        }
        return true
    }

    private fun showSnack(message: String) {
        UiUtils.showSnack(binding!!.root, message)
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}