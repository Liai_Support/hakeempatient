package com.app.hakeemUser.view.activity

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityLoginBinding
import com.app.hakeemUser.interfaces.MoveToFragment
import com.app.hakeemUser.models.LoginData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.fragment.ForgotPasswordFragment
import com.app.hakeemUser.view.fragment.RegisterFragment
import com.app.hakeemUser.view.fragment.ResetPasswordFragment
import com.app.hakeemUser.view.fragment.SocialLoginDetailsFragment
import com.app.hakeemUser.viewmodel.RegisterViewModel
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import org.json.JSONObject

class GuestLoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    var binding: ActivityLoginBinding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    private var mGoogleSignInClient: GoogleApiClient? = null
    private lateinit var callbackManager: CallbackManager


    private var registerFragment: RegisterFragment? = null
    private var socialoLoginFragment: SocialLoginDetailsFragment? = null
    private var resetPasswordFragment: ResetPasswordFragment? = null
    var forgotPassword: ForgotPasswordFragment? = null

    private var currentFragment = Constants.IntentFragment.LOGIN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(this)
        FacebookSdk.sdkInitialize(applicationContext)
        setView()
        initFacebookLogin()
        initGoogleLogin()
        inifragment()

    }


    private fun setView() {
        binding?.dontHaveAcc?.text =
            SpannableStringBuilder().append(resources.getString(R.string.don_t_have_an_account))
                .append(" ").append(
                    UiUtils.getBoldUnderlineString(
                        this,
                        resources.getString(R.string.sign_up_now)
                    )
                )
    }

    fun onSignUpClicked(view: View) {
        moveToFragment(Constants.IntentFragment.REGISTER, Bundle())
    }

    fun onLoginClicked(view: View) {

        if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.signin(binding!!.emailId.text.toString().trim(), binding!!.password.text.toString().trim())
                ?.observe(this,
                    Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    it.data?.let { data ->
                                        handleResponse(data)
                                    }
                                }
                            }

                        }
                    })
        }


    }

    fun onForgotPasswordClicked(view: View) {


        if (binding!!.emailId.text.toString().trim() == "") {
            UiUtils.showSnack(binding!!.root, "Please enter your emailid")
        } else if (!BaseUtils.isValidEmail(binding!!.emailId.text.toString().trim())) {
            UiUtils.showSnack(binding!!.root, "Please enter your valid emailid")
        } else {

            var bundle = Bundle()
            bundle.putString(Constants.IntentKeys.EMAIL, binding!!.emailId.text.toString().trim())
            moveToFragment(Constants.IntentFragment.FORGOTPASSWORD, bundle)
        }

    }

    private fun handleResponse(data: LoginData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.mobileNumber?.let { sharedHelper?.mobileNumber = it }
        data.token?.let { sharedHelper?.token = it }
        data.countryCode?.let { value -> sharedHelper?.countryCode = value }

        sharedHelper?.loggedIn = true

        finish()

    }

    private fun isValidInputs(): Boolean {
        when {

            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }

            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
                return false
            }

            binding!!.password.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourpassword))
                return false
            }

            else -> return true
        }


    }

    private fun initGoogleLogin() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleApiClient.Builder(this)
            .enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .build()
    }

    private fun googlesignIn() {

        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient)
        startActivityForResult(signInIntent, 100)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            result?.let {
                mGoogleSignInClient?.clearDefaultAccountAndReconnect()
                handleSignInResult(it)
            }

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }

        if (Constants.IntentFragment.REGISTER == currentFragment) {
            registerFragment?.setOnActivityResult(requestCode, resultCode, data)
        } else if (Constants.IntentFragment.SOCIALLOGINDETAILS == currentFragment) {
            socialoLoginFragment?.setOnActivityResult(requestCode, resultCode, data)
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Constants.IntentFragment.REGISTER == currentFragment) {
            registerFragment?.setPermissionResult(requestCode, permissions, grantResults)
        } else if (Constants.IntentFragment.SOCIALLOGINDETAILS == currentFragment) {
            socialoLoginFragment?.setPermissionResult(requestCode, permissions, grantResults)
        }
    }

    private fun get_data_for_facebook(loginResult: LoginResult) {

        val request = GraphRequest.newMeRequest(loginResult.accessToken) { value, response ->
            handleFacebookSuccessResponse(
                value,
                response
            )
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, locale ")
        request.parameters = parameters
        request.executeAsync()
    }

    private fun handleFacebookSuccessResponse(value: JSONObject, response: GraphResponse) {

        val email_json = value.optString("email")
        val first_name = value.optString("first_name")
       // val last_name = value.optString("last_name")
        val id = value.optString("id")
      //  val type = "facebook"
        val imageUrl = "https://graph.facebook.com/$id/picture?type=large"

        LoginManager.getInstance().logOut()


        viewmodel?.socialLogin(id, Constants.SocialLogin.FACEBOOK)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            data.userExists?.let { exist ->
                                if (exist) {
                                    data.email?.let { value -> sharedHelper?.email = value }
                                    data.id?.let { value -> sharedHelper?.id = value }
                                    data.name?.let { value -> sharedHelper?.name = value }
                                    data.mobileNumber?.let { value ->
                                        sharedHelper?.mobileNumber = value
                                    }
                                    data.token?.let { value -> sharedHelper?.token = value }
                                    data.countryCode?.let { value ->
                                        sharedHelper?.countryCode = value
                                    }

                                    sharedHelper?.loggedIn = true
                                    finish()
                                } else {


                                    var bundle = Bundle()
                                    bundle.putString(Constants.IntentKeys.EMAIL, email_json)
                                    bundle.putString(Constants.IntentKeys.NAME, first_name)
                                    bundle.putString(
                                        Constants.IntentKeys.PROFILE_PICTURE,
                                        imageUrl
                                    )
                                    bundle.putString(Constants.IntentKeys.SOCIALTOKEN, id)
                                    bundle.putString(
                                        Constants.IntentKeys.LOGINTYPE,
                                        Constants.SocialLogin.FACEBOOK
                                    )

                                    moveToFragment(
                                        Constants.IntentFragment.SOCIALLOGINDETAILS,
                                        bundle
                                    )


                                }
                            }


                        }
                    }
                }
            }
        })


    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val googleSignInAccount = result.signInAccount
            val email = googleSignInAccount!!.email
            val first_name: String
          //  val last_name: String
            // val type = "google"
            val id = googleSignInAccount.id
            val imageUrl = googleSignInAccount.photoUrl.toString()
            val full_name = googleSignInAccount.displayName
            if (full_name!!.contains(" ")) {
                val names =
                    full_name.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                first_name = names[0]
               // last_name = names[1]
            } else {
                first_name = full_name
                //last_name = ""
            }

            Auth.GoogleSignInApi.signOut(mGoogleSignInClient)

            DialogUtils.showLoader(this)
            viewmodel?.socialLogin(id, Constants.SocialLogin.GOOGLE)?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.userExists?.let { exist ->
                                    if (exist) {
                                        data.email?.let { value -> sharedHelper?.email = value }
                                        data.id?.let { value -> sharedHelper?.id = value }
                                        data.name?.let { value -> sharedHelper?.name = value }
                                        data.mobileNumber?.let { value ->
                                            sharedHelper?.mobileNumber = value
                                        }
                                        data.token?.let { value -> sharedHelper?.token = value }
                                        data.countryCode?.let { value ->
                                            sharedHelper?.countryCode = value
                                        }

                                        sharedHelper?.loggedIn = true
                                        finish()

                                    } else {


                                        var bundle = Bundle()
                                        bundle.putString(Constants.IntentKeys.EMAIL, email)
                                        bundle.putString(Constants.IntentKeys.NAME, first_name)
                                        bundle.putString(
                                            Constants.IntentKeys.PROFILE_PICTURE,
                                            imageUrl
                                        )
                                        bundle.putString(Constants.IntentKeys.SOCIALTOKEN, id)
                                        bundle.putString(
                                            Constants.IntentKeys.LOGINTYPE,
                                            Constants.SocialLogin.GOOGLE
                                        )

                                        moveToFragment(
                                            Constants.IntentFragment.SOCIALLOGINDETAILS,
                                            bundle
                                        )

                                    }
                                }


                            }
                        }
                    }
                }
            })


        }
    }

    private fun initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()

        binding!!.loginButtonFacebook.setReadPermissions("email")
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                get_data_for_facebook(loginResult)
            }

            override fun onCancel() {
                UiUtils.showLog("facebook", "")
            }

            override fun onError(exception: FacebookException) {
                UiUtils.showLog("facebook", exception.toString())
            }
        })
    }


    fun onFacebookClicked(view: View) {
        if (NetworkUtils.isNetworkConnected(this))
            binding!!.loginButtonFacebook.performClick()
        else
            UiUtils.showSnack(
                findViewById(android.R.id.content),
                resources.getString(R.string.no_internet_connection)
            )
    }

    fun OnGoogleClicked(view: View) {
        googlesignIn()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    fun inifragment() {

        registerFragment = RegisterFragment(object : MoveToFragment {

            override fun moveTo(fragName: String, bundle: Bundle) {
                moveToFragment(fragName, bundle)
            }

            override fun onFragBackPressed() {

            }

        }, supportFragmentManager)


        socialoLoginFragment = SocialLoginDetailsFragment(object : MoveToFragment {

            override fun moveTo(fragName: String, bundle: Bundle) {
                moveToFragment(fragName, bundle)
            }

            override fun onFragBackPressed() {

            }

        }, supportFragmentManager)


        resetPasswordFragment = ResetPasswordFragment(object : MoveToFragment {

            override fun moveTo(fragName: String, bundle: Bundle) {
                moveToFragment(fragName, bundle)
            }

            override fun onFragBackPressed() {

            }

        }, supportFragmentManager)


        forgotPassword = ForgotPasswordFragment(object : MoveToFragment {

            override fun moveTo(fragName: String, bundle: Bundle) {
                moveToFragment(fragName, bundle)
            }

            override fun onFragBackPressed() {

            }

        }, supportFragmentManager)


    }


    fun moveToRegisterFragment(bundle: Bundle) {


        registerFragment?.let {
            it.arguments = bundle
            BaseUtils.navigateToFragment(
                R.id.containerView,
                it,
                supportFragmentManager
            )
        }


    }

    fun moveToSocialDetailFragment(bundle: Bundle) {


        socialoLoginFragment?.let {
            it.arguments = bundle
            BaseUtils.navigateToFragment(
                R.id.containerView,
                it,
                supportFragmentManager
            )
        }


    }

    fun moveToResetPasswordFragment(bundle: Bundle) {


        resetPasswordFragment?.let {
            it.arguments = bundle
            BaseUtils.navigateToFragment(
                R.id.containerView,
                it,
                supportFragmentManager
            )
        }


    }

    fun moveToOtpFragment(bundle: Bundle) {


        forgotPassword?.let {
            it.arguments = bundle
            BaseUtils.navigateToFragment(
                R.id.containerView,
                it,
                supportFragmentManager
            )
        }


    }


    fun moveToFragment(fragName: String, bundle: Bundle) {

        when (fragName) {

            Constants.IntentFragment.REGISTER -> {
                currentFragment = Constants.IntentFragment.REGISTER
                moveToRegisterFragment(bundle)
            }

            Constants.IntentFragment.FORGOTPASSWORD -> {
                currentFragment = Constants.IntentFragment.FORGOTPASSWORD
                moveToOtpFragment(bundle)
            }

            Constants.IntentFragment.RESETPASSWORD -> {
                currentFragment = Constants.IntentFragment.RESETPASSWORD
                moveToResetPasswordFragment(bundle)
            }

            Constants.IntentFragment.SOCIALLOGINDETAILS -> {
                currentFragment = Constants.IntentFragment.SOCIALLOGINDETAILS
                moveToSocialDetailFragment(bundle)
            }

        }

    }


}