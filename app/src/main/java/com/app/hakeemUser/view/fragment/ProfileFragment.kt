package com.app.hakeemUser.view.fragment

import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentProfile2Binding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.ProfileDetails
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.activity.DashBoardActivity
import com.app.hakeemUser.viewmodel.AmazonViewModel
import com.app.hakeemUser.viewmodel.ProfileViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.io.File
import java.util.*

class ProfileFragment : Fragment(R.layout.fragment_profile2) {

    var binding: FragmentProfile2Binding? = null

    private var profileViewModel: ProfileViewModel? = null
    private var amazonViewModel: AmazonViewModel? = null
    var sharedHelper: SharedHelper? = null
    var uploadFile: File? = null
    var ageDif: Int = 0
    private var profileImageUrl = "nil"
    var cCode = ""
    var mobile = ""
    var email = ""
    var status: String? = null
    var classtype: String? = null
    var classid: String? = "0"
    var nclassid: String? = "0"
    var icname: String? = null
    var icnameid: String? = "0"
    var nicnameid: String? = "0"
    lateinit var spinner: Spinner
    lateinit var spinner1: Spinner
    private var viewmodel: RegisterViewModel? = null
    var cd1 : ArrayList<String> = ArrayList()
    var cd2 : ArrayList<Int> = ArrayList()
    var check = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProfile2Binding.inflate(inflater, container, false)
        val view = binding!!.root
        sharedHelper = SharedHelper(requireContext())
        Log.d("wertyuiop",""+sharedHelper!!.language)

        sharedHelper?.language?.let {
            if (it == "en") {
            } else {
                binding!!.contactNumber1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.emailId1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.bloodGroup1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.dob1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.height1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.weight1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.insurenceId.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.txtSaudiid.gravity = View.TEXT_ALIGNMENT_VIEW_START

                binding!!.edtContactNumber1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtEmailId1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtBloodGroup1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtDob1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtHeight1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtWeight1.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtInsurenceId.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.edtSaudiid.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.txtIcname.gravity = View.TEXT_ALIGNMENT_VIEW_START
                binding!!.insurenceType.gravity = View.TEXT_ALIGNMENT_VIEW_START

            }
        }
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
       // getClassData()
       // geticnamedata()

        initListeners()
        enableEdit(false)
        getProfileData()
        (activity as DashBoardActivity?)!!.binding!!.popupLoc.closePop.setOnClickListener(View.OnClickListener {
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        })
        return view
    }

    private fun initListeners() {
        /* val cd = arrayOf("")
        cd.set(0,"Class A")
        cd.set(1,"Class B")*/

        // access the spinner
        spinner1 = binding!!.edtIcname
        if (spinner1 != null) {
            val adapter = ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_item, sharedHelper!!.insurancecompanyname
            )
            spinner1.adapter = adapter
            // spinner1.setSelection(2)
            spinner1.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?, position: Int, id: Long
                ) {
                    icname = sharedHelper!!.insurancecompanyname[position]
                    icnameid = sharedHelper!!.insurancecompanyid[position].toString()
                    if(check > 0){
                        getClassData(icnameid!!,0)
                    }
                    else{
                        check++
                        //getClassData(icnameid!!,0)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }

        binding!!.editImage.setOnClickListener {
            DialogUtils.showPictureDialog(requireActivity())
        }

        binding!!.edtBloodGroup1.setOnClickListener {
            showBloodGrpDialog()
        }

        binding!!.edtDob1.setOnClickListener {
            selectDob()
        }

        binding!!.editProfile1.setOnClickListener {
            if (binding!!.editProfile1.text.toString() == resources.getString(R.string.edit_profile)) {
                binding!!.editImage.visibility = View.VISIBLE
                enableEdit(true)
            } else {
                if (isValidInputs() && isInsuranceValid()) {
                    DialogUtils.showLoader(requireContext())
                    if (uploadFile != null) {
                        amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                            ?.observe(viewLifecycleOwner, Observer {

                                it.error?.let { value ->
                                    if (!value) {
                                        updateProfile(it.message)
                                    } else {
                                        DialogUtils.dismissLoader()
                                        //updateProfile("nil")
                                    }
                                }
                            })
                    }
                    else {
                        updateProfile(profileImageUrl)
                    }

                    /*if (binding!!.insurenceId.text.toString().equals(binding!!.edtInsurenceId.text.toString()) && classid == nclassid && icnameid == nicnameid) {
                        DialogUtils.showLoader(requireContext())
                        if (uploadFile != null) {
                            amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                                ?.observe(viewLifecycleOwner, Observer {

                                    it.error?.let { value ->
                                        if (!value) {
                                            updateProfile(it.message)
                                        } else {
                                            DialogUtils.dismissLoader()
                                            updateProfile(it.message)
                                        }
                                    }
                                })
                        } else {
                            updateProfile(profileImageUrl)
                        }
                    }
                    else {
                        DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                            override fun singleTap() {
                                status = "true"
                                DialogUtils.showLoader(requireContext())
                                if (uploadFile != null) {
                                    amazonViewModel?.uploadImage(requireContext(), uploadFile!!)
                                        ?.observe(viewLifecycleOwner, Observer {

                                            it.error?.let { value ->
                                                if (!value) {
                                                    updateProfile(it.message)
                                                } else {
                                                    DialogUtils.dismissLoader()
                                                }
                                            }
                                        })
                                } else {
                                    updateProfile(profileImageUrl)
                                }
                            }
                        }, "Are You Sure to Change Insurance")
                    }*/
                }
            }

        }

        binding!!.male.setOnClickListener {
            binding!!.male.isChecked = true
            binding!!.female.isChecked = false
        }

        binding!!.female.setOnClickListener {
            binding!!.male.isChecked = false
            binding!!.female.isChecked = true
        }

    }

    private fun getClassData(icompanyid: String,iclassid: Int) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.getclassdetails(requireContext(),icompanyid)
            ?.observe(requireActivity(), Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.classDetails?.let { list ->
                                    if (list.size != 0) {
                                        classid = "0"
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        for ((index, value) in list.withIndex()) {
                                            println("the element at $index is $value")
                                            value.name?.let { it1 -> cd1.add(index + 1, it1) }
                                            value.id?.let { it2 -> cd2.add(index + 1, it2) }
                                        }

                                        spinner = binding!!.edtInsurenceType
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(
                                                requireContext(),
                                                android.R.layout.simple_spinner_item, cd1
                                            )
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(
                                                    parent: AdapterView<*>?,
                                                    view: View?, position: Int, id: Long
                                                ) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position].toString()

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }

                                            for ((index, value) in cd2.withIndex()) {
                                                if(cd2[index] == iclassid){
                                                    spinner.setSelection(index)
                                                    binding!!.insurenceType.text = cd1[index]
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        classid = "0"
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        spinner = binding!!.edtInsurenceType
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(
                                                requireContext(),
                                                android.R.layout.simple_spinner_item, cd1
                                            )
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(
                                                    parent: AdapterView<*>?,
                                                    view: View?, position: Int, id: Long
                                                ) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position].toString()

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }

                                            for ((index, value) in cd2.withIndex()) {
                                                if(cd2[index] == iclassid){
                                                    spinner.setSelection(index)
                                                    binding!!.insurenceType.text = cd1[index]
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }


    private fun selectDob() {


        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(
            requireActivity(),
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                binding!!.dob1.text = StringBuilder().append(year).append("-")
                    .append(BaseUtils.numberFormat(monthOfYear + 1)).append("-")
                    .append(BaseUtils.numberFormat(dayOfMonth))

                var ghvhg = StringBuilder().append(year).append("-")
                    .append(BaseUtils.numberFormat(monthOfYear + 1)).append("-")
                    .append(BaseUtils.numberFormat(dayOfMonth))

                // edt_dob1.text = ghvhg.toString()
                binding!!.edtDob1.setText(ghvhg)
                ageDif = BaseUtils.getAge(year, monthOfYear, dayOfMonth)!!.toInt()
            },
            year,
            month,
            day
        )

        dpd.show()
    }

    private fun showBloodGrpDialog() {

       // var list = arrayListOf("A+ve", "A-ve", "B+ve", "B-ve", "AB+ve", "AB-ve", "O+ve", "O-ve")
       var list=  resources.getStringArray(R.array.BloodGroup)
        DialogUtils.showListDialog(requireContext(), list, getString(R.string.choosebloodgroup), object :
            OnClickListener {
            override fun onClickItem(position: Int) {
                binding!!.bloodGroup1.text = list[position]
                binding!!.edtBloodGroup1.setText(binding!!.bloodGroup1.text.toString())
            }

        })

    }

    private fun updateProfile(profilePic: String?) {


        DialogUtils.showLoader(requireContext())
        profileViewModel?.updateProfile(
            requireContext(),
            binding!!.edtName1.text.toString().trim(),
            email,
            cCode,
            mobile,
            getGender(),
            binding!!.edtDob1.text.toString(),
            binding!!.edtBloodGroup1.text.toString(),
            binding!!.edtHeight1.text.toString(),
            binding!!.edtWeight1.text.toString(),
            classid, binding!!.edtInsurenceId.text.toString(), icnameid, status,binding!!.edtSaudiid.text.toString(),
            profilePic,
            ageDif
        )?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { message -> UiUtils.showSnack(binding!!.root1, message) }
                } else {
                    sharedHelper?.userImage = profilePic!!
                    it.message?.let { message -> UiUtils.showSnack(binding!!.root1, message) }
                    successResponse()
                }
            }
        })

    }

    private fun successResponse() {

        binding!!.editImage.visibility = View.GONE
        enableEdit(false)
        getProfileData()
    }

    private fun getGender(): String {
        return when {
            binding!!.male.isChecked -> {
                "male"
            }
            binding!!.female.isChecked -> {
                "female"
            }
            else -> {
                ""
            }
        }
    }

    private fun isValidInputs(): Boolean {

        if (binding!!.edtName1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.enteryourname))
            return false
        } else if (!binding!!.male.isChecked && !binding!!.female.isChecked) {
            UiUtils.showSnack(binding!!.root1, getString(R.string.selectyourgender))
            return false
        } else if (binding!!.edtDob1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.selectyourdateofbirth))
            return false
        }/* else if (binding!!.edtBloodGroup1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.selectyourbloodgroup))
            return false
        }*/
        else if (binding!!.edtSaudiid.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.enteryoursaudiid))
            return false
        }
        /*else if (binding!!.edtInsurenceId.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.enteryourinsuranceid))
            return false
        }else if (classid == 0) {
            UiUtils.showSnack(binding!!.root1, getString(R.string.selectyourinsuranceclass))
            return false
        } else if (icnameid == 0) {
            UiUtils.showSnack(binding!!.root1, getString(R.string.selectyourinsurancecompany))
            return false
        } else if (binding!!.edtHeight1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.enteryourheight))
            return false
        } else if (binding!!.edtWeight1.text.toString() == "") {
            UiUtils.showSnack(binding!!.root1, getString(R.string.enteryourweight))
            return false
        }*/ else {
            return true

        }

    }

    private fun isInsuranceValid(): Boolean {
        if(binding!!.edtInsurenceId.text.toString().trim() != "" || !binding!!.edtInsurenceId.text.toString().trim().equals("") || !classid.equals("0") || !icnameid.equals("0")){
            when {
                binding!!.edtInsurenceId.text.toString().trim() == "" -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourinsurance))
                    return false
                }
                classid.equals("0") -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsuranceclass))
                    return false
                }
                icnameid.equals("0") -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsurancecompany))
                    return false
                }
                else -> return true
            }
        }
        return true
    }



    private fun getProfileData() {
        DialogUtils.showLoader(requireContext())
        profileViewModel?.getProfileDetails(requireContext())
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root1, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.profileDetails?.let { list ->
                                    if (list.size != 0) {
                                        handleResponse(list[0])
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }

    private fun handleResponse(data: ProfileDetails) {

        if (data.insurenceid == null || data.insurenceid.equals("") || data.insurenceid!!.isEmpty()) {
            sharedHelper?.isInsurance = true

        } else {
            sharedHelper?.isInsurance = false
        }
        data.name?.let {
            binding!!.name1.setText(it)
            binding!!.edtName1.setText(it)
            sharedHelper?.name = it
            (activity as DashBoardActivity?)!!.binding!!.homeDrawer.patientName.text = it
        }
        data.countryCode?.let { ccpVal ->
            cCode = ccpVal
            data.mobileNumber?.let {
                binding!!.contactNumber1.text = "+$ccpVal  $it"
                mobile = it
            }
        }

        data.profilePic?.let {
            if(!it.equals("Nil",true)) {
                profileImageUrl = it
                sharedHelper?.userImage = it
                UiUtils.loadImage(binding!!.profilePicture1, it)
                UiUtils.loadImage(
                    (activity as DashBoardActivity?)!!.binding!!.homeDrawer.circleImageView5,
                    sharedHelper?.userImage
                )
            }
        }
        data.email?.let {
            binding!!.emailId1.text = it
            email = it
        }

        data.insurenceid?.let {
            binding!!.insurenceId.text = it
            binding!!.edtInsurenceId.setText(it)
        }

        data.sid?.let {
            binding!!.txtSaudiid.text = it
            binding!!.edtSaudiid.setText(it)
        }

        data.insurencecompanyid?.let {
            if (it == null) {
                UiUtils.showSnack(binding!!.root1, "Insurance class is null")
            } else {
                if(it.equals("")){
                    binding!!.txtIcname.text = ""
                    spinner1.setSelection(0)
                }
                else {
                    for ((index, value) in sharedHelper!!.insurancecompanyid.withIndex()) {
                        icnameid = it
                        nicnameid = it
                        if (value.toString().equals(it)) {
                            binding!!.txtIcname.text = sharedHelper!!.insurancecompanyname[index]
                            spinner1.setSelection(index)
                            getClassData(sharedHelper!!.insurancecompanyid[index].toString(),data.insurenceclassid!!.toInt())
                        }
                    }
                }
            }
        }

        data.DOB?.let {
            if (!it.contains("not", true))
                binding!!.dob1.text = it
            binding!!.edtDob1.setText(it)
        }

        data.age?.let {
            ageDif = it.toInt()
        }

        data.bloodGroup?.let {
            if (!it.contains("not", true))
                binding!!.bloodGroup1.text = it
            binding!!.edtBloodGroup1.setText(it)
        }
        data.height?.let {
            if (!it.contains("not", true))
                binding!!.height1.setText(it)
            binding!!.edtHeight1.setText(it)
        }
        data.weight?.let {
            if (!it.contains("not", true))
                binding!!.weight1.setText(it)
            binding!!.edtWeight1.setText(it)
        }
        data.gender?.let {
            if (it.equals("male", true)) {
                binding!!.male.isChecked = true
            } else if (it.equals("female", true)) {
                binding!!.female.isChecked = true
            }
        }
        data.istatus?.let { value ->
            sharedHelper?.istatus = value
            status = value
        }

        enableEdit(false)
    }

    private fun enableEdit(isEnable: Boolean) {

        binding!!.name1.isEnabled = isEnable
        binding!!.male.isEnabled = isEnable
        binding!!.female.isEnabled = isEnable
        binding!!.dob1.isEnabled = isEnable
        binding!!.bloodGroup1.isEnabled = isEnable
        binding!!.height1.isEnabled = isEnable
        binding!!.weight1.isEnabled = isEnable
        if (isEnable)
            binding!!.editProfile1.text = resources.getString(R.string.update)
        else
            binding!!.editProfile1.text = resources.getString(R.string.edit_profile)

        binding!!.editImage.visibility =
            if (isEnable)
                View.VISIBLE
            else
                View.GONE

        if (isEnable) {
            binding!!.name1.visibility = View.GONE
            binding!!.dob1.visibility = View.GONE
            binding!!.bloodGroup1.visibility = View.GONE
            binding!!.height1.visibility = View.GONE
            binding!!.weight1.visibility = View.GONE
            binding!!.insurenceId.visibility = View.GONE
            binding!!.insurenceType.visibility = View.GONE
            binding!!.txtIcname.visibility = View.GONE
            binding!!.txtSaudiid.visibility = View.GONE

            binding!!.edtName1.visibility = View.VISIBLE
            binding!!.edtDob1.visibility = View.VISIBLE
            binding!!.edtBloodGroup1.visibility = View.VISIBLE
            binding!!.edtHeight1.visibility = View.VISIBLE
            binding!!.edtWeight1.visibility = View.VISIBLE
            binding!!.edtInsurenceId.visibility = View.VISIBLE
            binding!!.edtInsurenceType.visibility = View.VISIBLE
            binding!!.edtIcname.visibility = View.VISIBLE
            binding!!.edtSaudiid.visibility = View.VISIBLE

            if (sharedHelper?.istatus.equals("true", true)) {
                binding!!.insurenceId.visibility = View.VISIBLE
                binding!!.insurenceType.visibility = View.VISIBLE
                binding!!.txtIcname.visibility = View.VISIBLE
                binding!!.edtInsurenceId.visibility = View.GONE
                binding!!.edtInsurenceType.visibility = View.GONE
                binding!!.edtIcname.visibility = View.GONE
            }
        } else {
            binding!!.name1.isEnabled = true
            binding!!.dob1.isEnabled = true
            binding!!.bloodGroup1.isEnabled = true
            binding!!.height1.isEnabled = true
            binding!!.weight1.isEnabled = true

            binding!!.name1.visibility = View.VISIBLE
            binding!!.dob1.visibility = View.VISIBLE
            binding!!.bloodGroup1.visibility = View.VISIBLE
            binding!!.height1.visibility = View.VISIBLE
            binding!!.weight1.visibility = View.VISIBLE
            binding!!.insurenceId.visibility = View.VISIBLE
            binding!!.txtSaudiid.visibility = View.VISIBLE
            binding!!.insurenceType.visibility = View.VISIBLE
            binding!!.txtIcname.visibility = View.VISIBLE

            binding!!.edtName1.visibility = View.GONE
            binding!!.edtContactNumber1.visibility = View.GONE
            binding!!.edtEmailId1.visibility = View.GONE
            binding!!.edtDob1.visibility = View.GONE
            binding!!.edtBloodGroup1.visibility = View.GONE
            binding!!.edtHeight1.visibility = View.GONE
            binding!!.edtWeight1.visibility = View.GONE
            binding!!.edtInsurenceId.visibility = View.GONE
            binding!!.edtSaudiid.visibility = View.GONE
            binding!!.edtInsurenceType.visibility = View.GONE
            binding!!.edtIcname.visibility = View.GONE

        }
    }

    fun onActivityResults(requestCode: Int, resultCode: Int, data: Intent?) {
        if(!isDetached)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                handleCamera()
            }

        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture1,
                    BaseUtils.getRealPathFromUriNew(requireContext(), uri),
                    ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(requireContext(), uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture1,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(requireContext(), R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    fun onRequestPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (!isDetached)
            when (requestCode) {

                Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    BaseUtils.openGallery(requireActivity())
                } else {
                    UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
                }

                Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                ) {
                    BaseUtils.openCamera(requireActivity())
                } else {
                    UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
                }
            }
    }
}