package com.app.hakeemUser.view.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityPaymentMethodBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.CompletedData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.paypal.android.sdk.payments.*

class VirtualPaymentActivity : BaseActivity() {

    var binding: ActivityPaymentMethodBinding? = null
    var viewmodel: BookingViewModel? = null
    var completedData: CompletedData? = null
    var ratingDialog: Dialog? = null
    var config: PayPalConfiguration? = null
    var amountTobePaid = ""
    var selectedType = 0
    var bookingId = 0
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentMethodBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_method)
        viewmodel = ViewModelProvider(this).get(BookingViewModel::class.java)
        sharedHelper = SharedHelper(this)


        getIntentValues()
        config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(getString(R.string.paypal_client_id))

        startServicePaypal()


        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }



    }

    private fun startServicePaypal() {

        val intent = Intent(this, PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        startService(intent)

    }

    private fun processPayment(amount: Double) {

        /* val payPalPayment = PayPalPayment(
             BigDecimal(amount.toString()), "USD",
             "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
         )

         val intent = Intent(this, PaymentActivity::class.java)
         intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
         intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
         startActivityForResult(intent, Constants.RequestCode.PAYPAL_REQUEST_CODE)*/
        Log.d("kjvhhjxv",""+amount)
        startActivityForResult(
            Intent(this, NoonPaymentActivity::class.java)
                .putExtra(
                    "amount",
                    amount.toDouble()
                ), 100
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(Intent(this, PayPalService::class.java))
    }

    private fun getIntentValues() {


        intent.extras?.let { bundle ->

            bundle.getParcelable<CompletedData>(Constants.IntentKeys.PAYMENTDETAIL)?.let { data ->

                completedData = data
            }

            bundle.getString(Constants.IntentKeys.AMOUNT)?.let {
                amountTobePaid = it
            }
            bundle.getInt(Constants.IntentKeys.BOOKINGTYPE).let {
                selectedType = it
            }
            bundle.getInt("bookingid").let {
                bookingId = it
            }
        }

        if(selectedType == 0){
            binding!!.payVisit.visibility = View.GONE
        }
        else if(selectedType == -1){
          //  binding!!.payVisit.visibility = View.VISIBLE
        }
        else{
            binding!!.payVisit.visibility = View.GONE
        }




    }

    fun onWalletClicked(view: View) {

        if (SharedHelper(this).loggedIn) {
            DialogUtils.showAlertDialog(this,
                getString(R.string.make_payment_content),
                getString(R.string.payment_request),
                getString(R.string.ok),
                getString(R.string.cancel),
                object : DialogCallBack {
                    override fun onPositiveClick() {

                        if (amountTobePaid != "") {
                            payUsingWallet(amountTobePaid)
                        }
                    }

                    override fun onNegativeClick() {

                    }
                })
        } else {
            pleaseLogin()
        }

    }

    private fun payUsingWallet(amount: String) {

        DialogUtils.showLoader(this)
        viewmodel?.payUsingWallet(this, amount)?.observe(this, Observer {

            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                            /* if(selectedType == 0){
                                 placeNormalBooking("nothing")
                             }
                             else{
                             placeVirtualBookig("nothing")
                             }*/
                        }
                    } else {
                        it.data?.let { data ->
                            data.transactionId?.let { txtId ->
                                //ScheduledBookingSingleton.getInstance().transactionId = txtId
                                //ScheduledBookingSingleton.getInstance().paymentMethod = "Wallet"
                                proccedd("",txtId,"Wallet")
                            }
                        }

                    }
                }
            }
        })

    }

    private fun placeVirtualBookig() {

        DialogUtils.showLoader(this)
        viewmodel?.bookVirtualConsultation(this, selectedType)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            showSuccessDialog()
                        }
                    }
                }
            })

    }

    private fun placeNormalBooking(){
        DialogUtils.showLoader(this@VirtualPaymentActivity)
        viewmodel?.bookNewConsultation(this@VirtualPaymentActivity)
            ?.observe(this@VirtualPaymentActivity, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            showSuccessDialog()
                        }
                    }
                }
            })
    }

    fun onPayfordClicked(view: View) {

        if (SharedHelper(this).loggedIn) {
            DialogUtils.showAlertDialog(this,
                getString(R.string.make_payment_content),
                getString(R.string.payment_request),
                getString(R.string.ok),
                getString(R.string.cancel),
                object : DialogCallBack {
                    override fun onPositiveClick() {
                        if (amountTobePaid != "") {
                            //proccedd("12345","12345", "Card")
                              processPayment(amountTobePaid.toDouble())
                        }

                    }

                    override fun onNegativeClick() {

                    }
                })
        } else {
            pleaseLogin()
        }


    }

    private fun pleaseLogin() {


        DialogUtils.showAlertDialog(this,
            getString(R.string.login_feature),
            getString(R.string.please_login),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    startActivity(
                        Intent(this@VirtualPaymentActivity, LoginActivity::class.java)
                            .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )

                }

                override fun onNegativeClick() {

                }
            })


    }

    fun onPayOnVisitClicked(view: View) {
        ScheduledBookingSingleton.getInstance().paymentMethod = "Cash on delivery (COD)"
        if(selectedType == 0){
            placeNormalBooking()
        }
        else if(selectedType == -1){
            startActivity(Intent(this, SummaryEmergencyConsultActivity::class.java)
                .putExtra("payment", true))
        }
//        scheduleBooking("cod")
    }

    private fun showSuccessDialog() {

        DialogUtils.showSuccessDialog(this, object : SingleTapListener {
            override fun singleTap() {

                val intent = Intent(this@VirtualPaymentActivity, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()

            }
        })
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d("sderftghyujikol",""+resultCode)
        Log.d("sderftghyujikol",""+requestCode)

        /* if (requestCode == Constants.RequestCode.PAYPAL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
             data?.let {
                 val confirmation: PaymentConfirmation =
                     it.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
                 if (confirmation != null) {
                     try {
                         val paymentDetails = confirmation.toJSONObject().toString(4)

                         try {
                             var paymentDeatils: JSONObject =
                                 JSONObject(paymentDetails).get("response") as JSONObject
                             if (paymentDeatils.get("state").toString().equals("approved", true)) {
                                 placeVirtualBookig(paymentDeatils.get("id").toString())
                             }
                         } catch (e: Exception) {

                         }


                     } catch (e: JSONException) {
                         e.printStackTrace()
                     }
                 }
             }
         }
 */
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                proccedd(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString(), "Card")
                // sendPaymentDetails("")
            }else{
                UiUtils.showSnack(binding!!.root,getString(R.string.paymentfailed))

            }
        }
    }

    fun proccedd(porderid: String,paymenttransactionId: String, paymentMethod: String) {
        ScheduledBookingSingleton.getInstance().paymentMethod = paymentMethod
        ScheduledBookingSingleton.getInstance().transactionId = paymenttransactionId
        ScheduledBookingSingleton.getInstance().torderId = porderid

        if(selectedType == 0){
            placeNormalBooking()
        }
        else if(selectedType == -1){
            startActivity(Intent(this, SummaryEmergencyConsultActivity::class.java)
                .putExtra("payment", true))
        }
        else if(selectedType == 5){
            updateInsBooking(bookingId)
        }
        else{
            placeVirtualBookig()
        }
    }

    private fun updateInsBooking(bookingId: Int) {
        DialogUtils.showLoader(this)
        viewmodel?.updateInsBooking(this, bookingId)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            showSuccessDialog()
                        }
                    }
                }
            })
    }


}