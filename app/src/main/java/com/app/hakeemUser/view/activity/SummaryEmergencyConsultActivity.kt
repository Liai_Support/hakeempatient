package com.app.hakeemUser.view.activity

import android.app.Dialog
import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.cardview.widget.CardView
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivitySummaryBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.CompletedData
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.MapViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.socket.client.Ack
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.io.IOException
import java.net.URISyntaxException
import java.util.*

class SummaryEmergencyConsultActivity : BaseActivity() {

    var binding: ActivitySummaryBinding? = null
    var completedData: CompletedData? = null
    var bookingViewModel: BookingViewModel? = null
    var sharedHelper: SharedHelper? = null
    var socket: Socket? = null
    var payment: Boolean? = false
    private var mapViewModel: MapViewModel? = null
    private var handler: Handler? = null
    var runnable: Runnable? = null
    var dialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySummaryBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_summary)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        handler = Handler(Looper.getMainLooper())
        sharedHelper = SharedHelper(this)
        initSockets()

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

        binding!!.cardView.visibility = View.GONE
        binding!!.cardView10.visibility = View.GONE


        ScheduledBookingSingleton.getInstance().dfee?.let {
            binding!!.singleConsultation.text = "$it SAR"
            binding!!.consultationAmount.text = "$it SAR"
            binding!!.totalAmount.text = "$it SAR"
        }
        intent.extras?.let { bundle ->
            bundle.getBoolean("payment")?.let {
                payment = it
            }
        }

        if(payment == true){
            hitEmergencyApi()
        }


    }


    fun onConfirmClicked(view: View) {
        Log.d("cancelledclick","yesstruee")
        if (sharedHelper?.loggedIn!!) {

            Log.d("cancelledclick","yesstruee1")

            if (ScheduledBookingSingleton.getInstance().bookingType == Constants.BookingType.AMBULANCE) {
                showEmergencyDialog()
                Log.d("cancelledclick","yesstruee11")

            } else if (ScheduledBookingSingleton.getInstance().bookingType == Constants.BookingType.DOCTOR) {
                consultDocNow()
                Log.d("cancelledclick","yesstruee22")

            }else{
                Log.d("cancelledclick","yesstruee244")

            }
        } else {
            pleaseLogin()
        }

    }

    private fun initSockets() {
        val opts = IO.Options()
        opts.forceNew = true
        opts.reconnection = false
        //opts.query = Constants.ApiKeys.AUTHORIZATION + "=" + sharedHelper!!.token
        try {
            socket = IO.socket(UrlHelper.SOCKETURL, opts)
        } catch (e: URISyntaxException) {
            e.printStackTrace()

        }

        socket?.on(Socket.EVENT_DISCONNECT) {
            Log.d(" Socket ", " DisConnected")
            initSockets()
        }

        socket?.on(Socket.EVENT_CONNECT) {
            Log.d(" Socket ", " Connected"+it)
        }

        socket?.let {
            if (!it.connected())
                socket?.connect()
        }

    }

    fun showEmergencyDialog() {
        DialogUtils.showAlertDialog(this,
            getString(R.string.emergency_content),
            getString(R.string.emergency),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {
                    startActivity(
                        Intent(
                            this@SummaryEmergencyConsultActivity,
                            VirtualPaymentActivity::class.java
                        )
                            .putExtra(Constants.IntentKeys.AMOUNT, ScheduledBookingSingleton.getInstance().dfee)
                            .putExtra(Constants.IntentKeys.BOOKINGTYPE, -1)
                    )
                    Log.d("VirtualPaymentActivity","yesstruee11")

                    //hitEmergencyApi()
                }

                override fun onNegativeClick() {

                }
            })
    }

    private fun showSuccessDialog() {

        dialog = DialogUtils.showSearchingDialog(this)

        dialog?.show()

        dialog?.findViewById<CardView>(R.id.getStarted)?.setOnClickListener {
            Log.d("kkkkkk","1")
            dialog?.dismiss()
            finish()
            runnable?.let { handler?.removeCallbacks(it) }
            runnable = null
            socket = null
        }


        Glide.with(this).asGif().load(R.raw.search)
            .listener(object : RequestListener<GifDrawable> {

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.setLoopCount(50)
                    return false;
                }

            }).into(dialog!!.findViewById(R.id.anim_view))

        runnable = Runnable {
            Log.d("kkkkkk","2")

            dialog?.dismiss()
            finish()
            runnable = null
            socket = null


        }


        runnable?.let { handler?.postDelayed(it, 50000)

        }

    }


    fun onBackPressed(view: View) {
        // startActivity(Intent(this, EmergencyMapBooking::class.java))
        onBackPressed()
    }


    private fun pleaseLogin() {


        DialogUtils.showAlertDialog(this,
            getString(R.string.login_feature),
            getString(R.string.please_login),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {

                    startActivity(
                        Intent(this@SummaryEmergencyConsultActivity, LoginActivity::class.java)
                            .putExtra(Constants.IntentKeys.FINISHACT, true)
                    )

                }

                override fun onNegativeClick() {

                }
            })


    }


    private fun hitEmergencyApi() {


        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"

        var p1 = c.get(Calendar.HOUR_OF_DAY)
        var p2 = c.get(Calendar.MINUTE)

        var time = ""
        var period = ""

        if (p1 <= 11) {
            if (p1 == 0) {
                period = "AM"
                time =
                    "12:${BaseUtils.numberFormat(p2)}"
            } else {
                period = "AM"
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            }

        } else {
            period = "PM"
            if (p1 < 13) {
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            } else {
                time =
                    "${BaseUtils.numberFormat(p1 - 12)}:${BaseUtils.numberFormat(p2)}"
            }

        }


        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PATIENTID, sharedHelper?.id)
        jsonObject.put(
            Constants.ApiKeys.SPECIALITYID,"2"
        )
        jsonObject.put(
            Constants.ApiKeys.PAYMENTMETHOD, ScheduledBookingSingleton.getInstance().paymentMethod

        )
        jsonObject.put(
            Constants.ApiKeys.TRANSACTIONID,ScheduledBookingSingleton.getInstance().transactionId

        )
        jsonObject.put(
            Constants.ApiKeys.EFees,ScheduledBookingSingleton.getInstance().dfee
        )
        jsonObject.put(Constants.ApiKeys.BOOKINGDATE, date)
        jsonObject.put(Constants.ApiKeys.BOOKINGTIME, time)
        jsonObject.put(Constants.ApiKeys.BOOKINGPERIOD, period)
        jsonObject.put(
            Constants.ApiKeys.LOCATION,
            ScheduledBookingSingleton.getInstance().location
        )
        jsonObject.put(
            Constants.ApiKeys.LATITUDE,
            ScheduledBookingSingleton.getInstance().latitude
        )
        jsonObject.put(
            Constants.ApiKeys.LONGITUDE,
            ScheduledBookingSingleton.getInstance().longitude
        )
        jsonObject.put(Constants.ApiKeys.PROVIDERTYPE, Constants.BookingType.AMBULANCE)
        jsonObject.put(Constants.ApiKeys.ISVIRTUALBOOKING, 0)
//        jsonObject.put(
//            "transactionId",
//            ScheduledBookingSingleton.getInstance().transactionId
//        )
//        jsonObject.put(
//            "paymentMethod",
//            ScheduledBookingSingleton.getInstance().paymentMethod
//        )


        checkActivityBookingRandom(jsonObject)

        /*  socket?.on("request_completed-${sharedHelper?.id}") {
                      ThreadUtils.runOnUiThread {
                  DialogUtils.dismissLoader()
                  showSuccessDialog()
              }
          }
          DialogUtils.showLoader(this)*/


    }

    private fun checkActivityBookingRandom(jsonObject: JSONObject) {

        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"

        DialogUtils.showLoader(this)
        bookingViewModel?.getActiveBooking(this, date)?.observe(this, androidx.lifecycle.Observer { it ->
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { msg ->
                        UiUtils.showSnack(binding!!.root, msg)
                    }
                } else {

                    it.data?.let { list ->
                        Log.d("wertyuiol",""+list.size)
                        if (list.size == 0) {
                            // socket?.emit(UrlHelper.SENDRANDOMREQUEST,jsonObject)
//                            socket?.emit(UrlHelper.SENDRANDOMREQUEST, jsonObject)
                            socket?.emit(UrlHelper.SENDRANDOMREQUEST, jsonObject, Ack {
                                Log.d("sdfghyjuki",it.toString())
                                val temp=it
                                Log.d("sdfghyjuki",""+temp)

                            })
                            //  socket?.disconnect();


                            Log.d("qwedfrt", "werrrrrthy")

                            startListening()
                            startListening()
                            showSuccessDialog()

                        } else {

                            DialogUtils.showAlert(this, object : SingleTapListener {
                                override fun singleTap() {

                                    /*    val intent =
                                                Intent(
                                                        this@SummaryEmergencyConsultActivity,
                                                        DashBoardActivity::class.java
                                                )
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()*/

                                }

                            }, getString(R.string.booking_alert))
                        }
                    }

                }
            }
        })


    }

    private fun startListening() {
        Log.d("aaqwertyui","qwsertgyhu")
        Log.d("socket1",""+socket)
        Log.d("socket",""+socket?.on("request_accepted-${sharedHelper?.id}")
        {

        })

        socket?.on("request_accepted-${sharedHelper?.id}") {

            Log.d("wsedrftgyuiop[","zxdcfvghjkl;'")
            showNoSearchNotification("Doctor found")
            Log.d("kkkkkk","3")

            dialog?.dismiss()
            runnable?.let { handler?.removeCallbacks(it) }
            runnable = null
            socket = null
            // finish()
            val intent =
                Intent(
                    this@SummaryEmergencyConsultActivity,
                    DashBoardActivity::class.java
                )
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()



        }


        socket?.on("request_completed-${sharedHelper?.id}") {
            Log.d("saed[","asdf;'")

            showNoSearchNotification("No doctor found")
            Log.d("kkkkkk","4")

            dialog?.dismiss()
            runnable?.let { handler?.removeCallbacks(it) }
            runnable = null
            socket = null

            //   finish()
            val intent =
                Intent(
                    this@SummaryEmergencyConsultActivity,
                    DashBoardActivity::class.java
                )
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()


        }


    }

    private fun checkActivityBookingDoctor(jsonObject: JSONObject) {

        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"


        DialogUtils.showLoader(this)
        bookingViewModel?.getActiveBooking(this, date)?.observe(this, androidx.lifecycle.Observer {
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { msg ->
                        UiUtils.showSnack(binding!!.root, msg)
                    }
                } else {

                    it.data?.let { list ->
                        if (list.size == 0) {
                            socket?.emit(UrlHelper.SENDRANDOMREQUEST, jsonObject, Ack {

                            })

                            DialogUtils.showSuccessBookingDialog(
                                this,
                                object : SingleTapListener {
                                    override fun singleTap() {

                                        val intent =
                                            Intent(
                                                this@SummaryEmergencyConsultActivity,
                                                DashBoardActivity::class.java
                                            )
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()
                                    }
                                },
                                getString(R.string.booking_success)
                            )


                        } else {
                            DialogUtils.showAlert(this, object : SingleTapListener {
                                override fun singleTap() {
                                    val intent =
                                        Intent(
                                            this@SummaryEmergencyConsultActivity,
                                            DashBoardActivity::class.java
                                        )
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
                                    finish()

                                }

                            }, getString(R.string.booking_alert))
                        }
                    }

                }
            }
        })


    }

    fun consultDocNow() {
        DialogUtils.showAlertDialog(this,
            getString(R.string.booking_confirmation),
            getString(R.string.confirm),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {
                    sendNewBookingRequest()
                    Log.d("xscdsdcddec","yesstruee11")

                }

                override fun onNegativeClick() {

                }
            })
    }


    private fun sendNewBookingRequest() {

        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"

        var p1 = c.get(Calendar.HOUR_OF_DAY)
        var p2 = c.get(Calendar.MINUTE)

        var time = ""
        var period = ""

        if (p1 <= 11) {
            if (p1 == 0) {
                period = "AM"
                time =
                    "12:${BaseUtils.numberFormat(p2)}"
            } else {
                period = "AM"
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            }

        } else {
            period = "PM"
            if (p1 < 13) {
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            } else {
                time =
                    "${BaseUtils.numberFormat(p1 - 12)}:${BaseUtils.numberFormat(p2)}"
            }

        }

        var utcTime = BaseUtils.getUtcTime(
            ScheduledBookingSingleton.getInstance().bookingDate + " " + ScheduledBookingSingleton.getInstance().bookingTime + " " + ScheduledBookingSingleton.getInstance().bookingPeriod,
            "yyyy/MM/dd hh:mm a"
        )

//        jsonObject.put(
//            Constants.ApiKeys.BOOKINGDATE,
//            BaseUtils.getFormatedDate(utcTime, "yyyy/MM/dd hh:mm a", "yyyy/MM/dd")
//        )
//        jsonObject.put(
//            Constants.ApiKeys.BOOKINGTIME,
//            BaseUtils.getFormatedDate(utcTime, "yyyy/MM/dd hh:mm a", "hh:mm")
//        )
//        jsonObject.put(
//            Constants.ApiKeys.BOOKINGPERIOD,
//            BaseUtils.getFormatedDate(utcTime, "yyyy/MM/dd hh:mm a", "a")
//        )


        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PATIENTID, sharedHelper?.id)
        jsonObject.put(
            Constants.ApiKeys.SPECIALITYID,"2"
        )
        jsonObject.put(
            Constants.ApiKeys.PAYMENTMETHOD,"Cash on delivery (COD)"

        )
        jsonObject.put(
            Constants.ApiKeys.TRANSACTIONID,"2"

        )
        jsonObject.put(
            Constants.ApiKeys.EFees,"250"
        )
        jsonObject.put(Constants.ApiKeys.BOOKINGDATE, date)
        jsonObject.put(Constants.ApiKeys.BOOKINGTIME, time)
        jsonObject.put(Constants.ApiKeys.BOOKINGPERIOD, period)
        jsonObject.put(Constants.ApiKeys.LOCATION, ScheduledBookingSingleton.getInstance().location)
        jsonObject.put(Constants.ApiKeys.LATITUDE, ScheduledBookingSingleton.getInstance().latitude)
        jsonObject.put(
            Constants.ApiKeys.LONGITUDE,
            ScheduledBookingSingleton.getInstance().longitude
        )
        jsonObject.put(Constants.ApiKeys.PROVIDERTYPE, Constants.BookingType.DOCTOR)
        jsonObject.put(Constants.ApiKeys.ISVIRTUALBOOKING, 0)

        var listenerValue = "request_accepted-${sharedHelper?.id}"



        checkActivityBookingDoctor(jsonObject)

    }

    private fun showNoSearchNotification(content : String) {


        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, "Alert")
            .setContentText(content)
            .setContentTitle(getString(R.string.app_name))
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setSmallIcon(R.drawable.splash_image)
            .setSound(defaultSoundUri)




        notificationManager.notify(System.currentTimeMillis().toInt(), notification.build())
    }


    override fun onStop() {
        super.onStop()
        try {
            socket?.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            socket?.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}