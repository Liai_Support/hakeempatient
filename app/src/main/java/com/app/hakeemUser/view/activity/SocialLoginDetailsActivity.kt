package com.app.hakeemUser.view.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityResetPasswordBinding
import com.app.hakeemUser.databinding.ActivitySocialLoginDetailsBinding
import com.app.hakeemUser.models.SocialRegisterData
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.AmazonViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel

import java.io.File

class SocialLoginDetailsActivity : BaseActivity() {

    var binding: ActivitySocialLoginDetailsBinding? = null

    var socialToken = ""
    var loginType = ""
    var profileImage = ""
    var uploadFile: File? = null
    var sharedHelper: SharedHelper? = null
    private var amazonViewModel: AmazonViewModel? = null
    private var viewmodel: RegisterViewModel? = null
    var classtype: String? = null
    var classid: Int? = 0
  /*  val cd1 = arrayOf("","","","","")
    val cd2 = arrayOf(0,0,0,0,0)*/
    var icname: String? = null
    var icnameid: Int? = null
   /* val cd3 = arrayOf("","","","","")
    val cd4 = arrayOf(0,0,0,0,0)*/


    var cd1 : MutableList<String> = mutableListOf<String>()
    var cd2 : MutableList<Int> = mutableListOf<Int>()
    var cd3 : MutableList<String> = mutableListOf<String>()
    var cd4 : MutableList<Int> = mutableListOf<Int>()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySocialLoginDetailsBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_social_login_details)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        amazonViewModel = ViewModelProvider(this).get(AmazonViewModel::class.java)
        geticnamedata()
        getIntentValues()

        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.back.rotation= 180F
        }
    }

    private fun geticnamedata() {
        DialogUtils.showLoader(this)
        viewmodel?.geticnamedetails(this)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.companyDetails?.let { list ->
                                    if (list.size != 0) {
                                        if(sharedHelper?.language.equals("en")){
                                            for ((index, value) in list.withIndex()) {
                                                Log.d("dscd",""+index+"saa"+""+value.name);
                                                println("the element at $index is $value")
                                                value.name?.let { it1 -> cd3.add(index+1, it1) }
                                                value.id?.let { it2 -> cd4.add(index+1, it2) }
                                            }
                                        }
                                        else{
                                            for ((index, value) in list.withIndex()) {
                                                Log.d("dscd",""+index+"saa"+""+value.name_ar);
                                                println("the element at $index is $value")
                                                value.name_ar?.let { it1 -> cd3.add(index+1, it1) }
                                                value.id?.let { it2 -> cd4.add(index+1, it2) }
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }

                }
            })
    }


    private fun getClassData(icompanyid: Int) {
        DialogUtils.showLoader(this)
        viewmodel?.getclassdetails(this,icompanyid.toString())
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.classDetails?.let { list ->
                                    if (list.size != 0) {
                                        classid = 0
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        for ((index, value) in list.withIndex()) {
                                            Log.d("dscd",""+index+"saa"+""+value.name);
                                            println("the element at $index is $value")
                                            value.name?.let { it1 -> cd1.add(index+1, it1) }
                                            value.id?.let { it2 -> cd2.add(index+1, it2) }
                                        }

                                        val spinner = findViewById<Spinner>(R.id.spinner)
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                android.R.layout.simple_spinner_item, cd1)
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position]

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        classid = 0
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        val spinner = findViewById<Spinner>(R.id.spinner)
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                android.R.layout.simple_spinner_item, cd1)
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position]

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
            })
    }


    private fun getIntentValues() {

        // access the spinner
        val spinner1 = findViewById<Spinner>(R.id.spinner_icname)
        cd3.add(0,"")
        cd4.add(0,0)
        if (spinner1 != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, cd3)
            spinner1.adapter = adapter
            spinner1.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    icname = cd3[position]
                    icnameid = cd4[position]
                    getClassData(icnameid!!)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }




        intent.extras?.let { extra ->

            extra.getString(Constants.IntentKeys.EMAIL)?.let { value ->
                if (value.isNotEmpty()) {
                    binding!!.emailId.setText(value)
                    binding!!.emailId.isEnabled = false
                } else {
                    binding!!.emailId.isEnabled = true
                }

            }

            extra.getString(Constants.IntentKeys.NAME)?.let { value ->
                binding!!.name.setText(value)
            }

            extra.getString(Constants.IntentKeys.SOCIALTOKEN)?.let { value ->
                socialToken = value
            }

            extra.getString(Constants.IntentKeys.LOGINTYPE)?.let { value ->
                loginType = value
            }

            extra.getString(Constants.IntentKeys.PROFILE_PICTURE)?.let { value ->
                if (value.isNotEmpty()) {
                    profileImage = value
                    UiUtils.loadImage(binding!!.profilePicture, value)
                }
            }
        }

    }

    fun onSubmitClicked(view: View) {

        if (isValidInputs() && isInsuranceValid()) {
            DialogUtils.showLoader(this)
            if (uploadFile != null) {

                amazonViewModel?.uploadImage(this, uploadFile!!)?.observe(this, Observer {


                    it.error?.let { value ->
                        if (!value) {
                            proceedSignIn(it.message)
                        } else {
                            DialogUtils.dismissLoader()
                        }
                    }
                })
            } else {
                proceedSignIn("")
            }
        }

    }

    private fun proceedSignIn(imageUrlString: String?) {

        imageUrlString?.let { image ->

            var imageurl = ""

            imageurl = if (image.isEmpty()) {
                profileImage
            } else {
                imageUrlString
            }

            viewmodel?.socialRegister(
                binding!!.emailId.text.toString().trim(),
                imageurl,
                binding!!.name.text.toString().trim(),
                socialToken,
                binding!!.countryCodePicker.selectedCountryCode,
                binding!!.phoneNumber.text.toString().trim(), loginType,binding!!.insurenceId.text.toString().trim(),classid.toString().trim(),icnameid.toString().trim(),binding!!.saudiId.text.toString()
            )?.observe(this, Observer {
                it?.let {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                handleResponse(data)
                            }
                        }
                    }
                }
            })

        }

    }

    private fun handleResponse(data: SocialRegisterData) {

        data.email?.let { sharedHelper?.email = it }
        data.id?.let { sharedHelper?.id = it }
        data.name?.let { sharedHelper?.name = it }
        data.token?.let { sharedHelper?.token = it }
        data.istatus?.let { value -> sharedHelper?.istatus = value}

        sharedHelper?.loggedIn = true
        sharedHelper?.selectedaddressid = 0

        val intent =
            Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == RESULT_OK) {
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == RESULT_OK) {
                handleCamera()
            }

        }
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                UiUtils.loadImage(
                    binding!!.profilePicture,
                    BaseUtils.getRealPathFromUriNew(this, uri),
                    ContextCompat.getDrawable(this, R.drawable.doctor)!!
                )
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_select))
            }
        }
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper!!.imageUploadPath)
        if (uploadFile!!.exists()) {
            UiUtils.loadImage(
                binding!!.profilePicture,
                sharedHelper!!.imageUploadPath,
                ContextCompat.getDrawable(this, R.drawable.doctor)!!
            )
        } else {
            UiUtils.showSnack(binding!!.root, getString(R.string.unable_to_retrieve))
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {

            Constants.Permission.READ_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseUtils.openGallery(this)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.storage_permission_error))
            }

            Constants.Permission.CAMERA_STORAGE_PERMISSIONS -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
            ) {
                BaseUtils.openCamera(this)
            } else {
                UiUtils.showSnack(binding!!.root, getString(R.string.camera_permission_error))
            }
        }
    }


    private fun isValidInputs(): Boolean {
        when {

            uploadFile == null && profileImage.isEmpty() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseuploadprofilepicture))
            }

            binding!!.name.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourname))
                return false
            }
            binding!!.emailId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryouremailid))
                return false
            }
            !BaseUtils.isValidEmail(binding!!.emailId.text.toString()) -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.plaeseentervallidemailid))
                return false
            }
            binding!!.phoneNumber.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root,getString(R.string.pleaseenteryourmobilenumber))
                return false
            }
            binding!!.saudiId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryoursaudiid))
                return false
            }
           /* binding!!.insurenceId.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourinsurance))
                return false
            }
            classid == 0 -> {
                UiUtils.showSnack(binding!!.root,getString(R.string.pleasechooseinsuranceclass))
                return false
            }
            icnameid == 0 -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsurancecompany))
                return false
            }*/

            else -> return true
        }

        return true
    }

    private fun isInsuranceValid(): Boolean {
        if(binding!!.insurenceId.text.toString().trim() != "" || !binding!!.insurenceId.text.toString().trim().equals("") || classid != 0 || icnameid != 0){
            when {
                binding!!.insurenceId.text.toString().trim() == "" -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourinsurance))
                    return false
                }
                classid == 0 -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsuranceclass))
                    return false
                }
                icnameid == 0 -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsurancecompany))
                    return false
                }
                else -> return true
            }
        }
        return true
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onProfileEditClicked(view: View) {
        DialogUtils.showPictureDialog(this)
    }
}