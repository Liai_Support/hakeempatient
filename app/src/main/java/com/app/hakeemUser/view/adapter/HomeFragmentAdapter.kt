package com.app.hakeemUser.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class HomeFragmentAdapter(fm: FragmentManager, var fragment: ArrayList<Fragment?>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        fragment[position]?.let { frag ->
            return frag
        }
        return fragment[position]!!
    }

    override fun getCount(): Int {
        return fragment.size
    }
}