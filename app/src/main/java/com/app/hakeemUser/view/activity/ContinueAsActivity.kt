package com.app.hakeemUser.view.activity

import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityContinueAs1Binding
import com.app.hakeemUser.databinding.ActivityContinueAsBinding
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityLoginBinding
import com.app.hakeemUser.utils.SharedHelper
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task

class ContinueAsActivity : BaseActivity() {
    var binding: ActivityContinueAs1Binding? = null
    public var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContinueAs1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_continue_as1)
        sharedHelper = SharedHelper(this)

        /*registerUser.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
*/
        binding!!.guestUser.setOnClickListener {
            sharedHelper!!.selectedaddressid = 0
            startActivity(Intent(this, DashBoardActivity::class.java))
            //finish()
        }

        binding!!.login.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            //finish()
        }

        binding!!.signup.setOnClickListener {
            startActivity(Intent(this, RegisterActivity1::class.java))
            //finish()
        }




    }



}