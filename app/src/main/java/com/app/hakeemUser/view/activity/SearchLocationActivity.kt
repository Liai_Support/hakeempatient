package com.app.hakeemUser.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityLogin1Binding
import com.app.hakeemUser.databinding.ActivityResetPasswordBinding
import com.app.hakeemUser.databinding.ActivitySearchLocationBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.Prediction
import com.app.hakeemUser.models.PredictionAutoCompleted
import com.app.hakeemUser.network.ApiInput
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.adapter.AutoCompleteAdapter
import com.app.hakeemUser.viewmodel.AddressViewModel
import com.app.hakeemUser.viewmodel.MapViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng

class SearchLocationActivity : BaseActivity(), OnMapReadyCallback {
    var binding: ActivitySearchLocationBinding? = null

    var mapViewModel: MapViewModel? = null

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var addressViewModel: AddressViewModel? = null


    private var myLat: Double? = null
    private var myLng: Double? = null

    private var adapter: AutoCompleteAdapter? = null
    private var data: ArrayList<PredictionAutoCompleted> = ArrayList()

    var map: GoogleMap? = null
    var mapFragment: MapFragment? = null
    var idleLat: Double? = 0.0
    var idleLng: Double? = 0.0
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchLocationBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_search_location)


        sharedHelper = SharedHelper(this)
        addressViewModel = ViewModelProvider(this).get(AddressViewModel::class.java)

        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapFragment = fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment?.getMapAsync(this)
        initListener()
        locationListner()
        askLocationPermission()
        setAdapter()


        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

    }

    private fun setAdapter() {

        adapter = AutoCompleteAdapter(this, data, object : OnClickListener {
            override fun onClickItem(position: Int) {

                getLatLngDetails(data, position)

            }
        })
        binding!!.locationList.layoutManager = LinearLayoutManager(this)
        binding!!.locationList.adapter = adapter

    }

    private fun getLatLngDetails(data: ArrayList<PredictionAutoCompleted>, position: Int) {

        DialogUtils.showLoader(this)
        mapViewModel?.getLatLngDetails(data[position].placeId.toString())?.observe(this, Observer {
            DialogUtils.dismissLoader()

            it.result?.let {

                data[position].description?.let { value -> sharedHelper?.location = value }
                it.geometry?.location?.lat?.let { value ->
                    sharedHelper?.selectedLat = value.toString()
                }
                it.geometry?.location?.lng?.let { value ->
                    sharedHelper?.selectedLng = value.toString()
                }
                finish()
            }
        })

    }

    private fun initListener() {

        binding!!.searchText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0?.toString()?.length != 0) {
                    if (binding!!.listContainer.visibility == View.VISIBLE)
                        autoCompleteSearch(p0?.toString())
                } else {
                    data = ArrayList()
                    loadValues(ArrayList())
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        binding!!.clear.setOnClickListener {
            binding!!.searchText.setText("")
        }

        binding!!.searchText.setOnFocusChangeListener { _, focus ->

            if (focus) {
                if (binding!!.listContainer.visibility == View.GONE) {
                    binding!!.listContainer.visibility = View.VISIBLE
                    binding!!.mapContainer.visibility = View.GONE
                }
            } else {
                if (binding!!.mapContainer.visibility == View.GONE) {
                    binding!!.listContainer.visibility = View.GONE
                    binding!!.mapContainer.visibility = View.VISIBLE
                    getLastKnownLocation()
                }
            }

        }

        binding!!.pickFromMap.setOnClickListener {
            if (currentFocus?.id == R.id.searchText)
                binding!!.searchText.clearFocus()
            else
                if (binding!!.mapContainer.visibility == View.GONE) {
                    binding!!.listContainer.visibility = View.GONE
                    binding!!.mapContainer.visibility = View.VISIBLE
                    getLastKnownLocation()
                }
        }

        binding!!.confirmLocation.setOnClickListener {
            if (binding!!.searchText.text.toString().isNotEmpty()) {

               // sharedHelper?.location = searchText.text.toString().trim()
               // sharedHelper?.selectedLat = idleLat.toString()
               // sharedHelper?.selectedLng = idleLng.toString()
                sharedHelper?.selectedLat = idleLat.toString()
                sharedHelper?.selectedLng = idleLng.toString()
                sharedHelper!!.selectedaddressid = -1
                sharedHelper!!.seletedaddresslocation = binding!!.searchText.text.toString().trim()

                DialogUtils.showLoader(this)
                addressViewModel?.addaddresss(
                        this,
                    binding!!.searchText.text.toString().trim(),
                idleLat,
                idleLng
                )?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { message -> UiUtils.showSnack(binding!!.root5, message) }
                        } else {
                            it.message?.let { message -> UiUtils.showSnack(binding!!.root5, message) }
                            startActivity(Intent(this, DashBoardActivity::class.java))
                            finish()
                            //successResponse()
                        }
                    }
                })

            }
        }

    }

    private fun autoCompleteSearch(place: String?) {

        if (myLat != null && myLng != null)
            place?.let {
                mapViewModel?.searchAutoComplete(place, myLat.toString(), myLng.toString())
                    ?.observe(this, Observer {

                        it?.error?.let { boo ->
                            if (boo) {
                                it.errorMessage?.let { error ->
                                    //                                    UiUtils.showSnack(findViewById(android.R.id.content), error)
                                }

                            } else {
                                it.predictions?.let { list ->
                                    loadValues(list)
                                }
                            }
                        }

                    })
            }

    }

    private fun loadValues(list: List<PredictionAutoCompleted>) {
        data = list as ArrayList<PredictionAutoCompleted>
        adapter?.notifyDataSetChangedValues(list)
    }

    fun onBackPressed(view: View) {
        finish()
    }

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()

                    }
                }
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }


    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }

    private fun getUserLocation() {

        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    private fun getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude

                if (binding!!.mapContainer.visibility == View.VISIBLE) {
                    map?.let {
                        idleLat = myLat
                        idleLng = myLng
                        searchPlaces()
                        locateMyLocation()
                    }
                }


            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    private fun locateMyLocation() {

        myLat?.let { lat ->
            myLng?.let { lng ->
                if (binding!!.mapContainer.visibility == View.VISIBLE)
                    map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 20.0f))
            }
        }

    }

    private fun searchPlaces() {
        val url = UrlHelper.getAddress(idleLat?.toDouble()!!, idleLng?.toDouble()!!)
        val inputForAPI = ApiInput()
        inputForAPI.context = this
        inputForAPI.url = url
        mapViewModel?.getAddress(this, inputForAPI)
            ?.observe(this, androidx.lifecycle.Observer {
                it.let { responses ->
                    responses.error?.let { errorValue ->
                        if (!errorValue) {
                            responses.predictions?.let { value ->
                                getAddressFromResponse(value)
                            }
                        }
                    }

                }

            })
    }

    private fun getAddressFromResponse(value: List<Prediction>) {
        if (value.isNotEmpty()) {

            value[0].formatted_address?.let {
                binding!!.searchText.setText(it)
            }

        }
    }



    override fun onMapReady(p0: GoogleMap) {
        map = p0
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        map?.isMyLocationEnabled = true
        myLat?.let {
            myLng?.let {
                if (binding!!.mapContainer.visibility == View.VISIBLE)
                    getLastKnownLocation()
            }
        }

        map?.setOnCameraIdleListener {

            var centerLatLng = map?.cameraPosition?.target
            idleLat = centerLatLng?.latitude
            idleLng = centerLatLng?.longitude

            idleLat?.let {
                idleLng?.let {
                    searchPlaces()
                }
            }

        }
    }


}