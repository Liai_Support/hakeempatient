package com.app.hakeemUser.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.ActivityLoginBinding
import com.app.hakeemUser.databinding.ActivityPharmachylistBinding
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.PharmachylistAdapter
import com.app.hakeemUser.view.adapter.TestlistAdapter
import com.app.hakeemUser.viewmodel.MembersViewModel
import java.util.ArrayList

class PharmachylistActivity : BaseActivity() {
    var binding: ActivityPharmachylistBinding? = null
    private var membersViewModel: MembersViewModel? = null
    lateinit var textView: TextView
    var idarray: ArrayList<Int> = ArrayList()
    var sharedHelper: SharedHelper? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPharmachylistBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_pharmachylist)
        sharedHelper = SharedHelper(this)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)
        getpharmachylist()

        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
    }

    private fun getpharmachylist(){
        DialogUtils.showLoader(this)
        membersViewModel?.getpharmachylistDetails(this)
                ?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.rootPharmachy, msg)
                                }
                            } else {
                                it.data?.let { data ->
                                    data.pharmachyDetails?.let { list ->
                                        if(list.isEmpty()){
                                            UiUtils.showSnack(binding!!.rootPharmachy, getString(R.string.noresultfound))
                                        }
                                        else{
                                            binding!!.PharmachyList.layoutManager = LinearLayoutManager(this)
                                            binding!!.PharmachyList.adapter = PharmachylistAdapter(this@PharmachylistActivity,this, list)
                                        }
                                    }

                                }
                            }


                        }

                    }
                })

    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

}