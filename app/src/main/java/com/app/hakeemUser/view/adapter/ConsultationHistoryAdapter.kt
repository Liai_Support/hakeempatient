package com.app.hakeemUser.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildDoctorConsultationHistoryBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.ConsultationData
import com.app.hakeemUser.utils.BaseUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils

class ConsultationHistoryAdapter(
    var context: Context,
    var list: ArrayList<ConsultationData>,
    var onClickListener: OnClickListener
) :
    RecyclerView.Adapter<ConsultationHistoryAdapter.ViewHolder>() {
    var sharedHelper: SharedHelper? = null


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildDoctorConsultationHistoryBinding =
            ChildDoctorConsultationHistoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_doctor_consultation_history,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


       // holder.binding?.bookingid1!!.text = "" + context.getString(R.string.bookingid) + "" + list[position].bookingIds.toString()
        holder.binding?.bookingid1!!.text = "" +list[position].bookingIds.toString()
        if(list[position].medicine!!.isEmpty()== false && list[position].test_to_take!!.isEmpty()== false && list[position].suggested_labs!!.isEmpty()==false){
            Log.d("dvsd",""+ list[position].medicine!!.get(0).medicine)
            Log.d("dhvsd",""+ list[position].test_to_take!!.get(0).Test_name)
            Log.d("d",""+ list[position].suggested_labs!!.get(0).lab_name)
        }


        holder.binding.root.setOnClickListener {
            onClickListener.onClickItem(position)
        }

        list[position].profilePic?.let {
            UiUtils.loadImage(
                holder.binding.circleImageView2,
                it,
                context.getDrawable(R.drawable.place_holder_doctor)!!
            )
        }

        holder.binding.doctorName.text = list[position].providerName
      //  holder.binding.qualification.text = list[position].education+"-"+list[position].specialityName
        holder.binding.qualification.text = list[position].education

        if (list[position].previousIssue==""){
            holder.binding.issue.text = context.getString(R.string.nil)

        }else{
            holder.binding.issue.text = list[position].previousIssue

        }

        list[position].fee?.let {

            if (list[position].extraFee != null) {
                holder.binding.fee.text =" : "+"${it.toInt() + list[position].extraFee?.toInt()!!} SAR"
            } else {
                holder.binding.fee.text =" : "+"${list[position].fee} SAR"
            }
        }


        if(list[position].previousIssue.equals("not mentioned",true)){
            holder.binding?.textView13?.text = ""
        }else if ( holder.binding?.textView13?.text.equals("")){
            holder.binding?.textView13?.text = context.getString(R.string.notmentioined)

        }
        else {
            holder.binding?.textView13?.text = list[position].previousIssue
        }

        list[position].bookingDate?.let {
            list[position].bookingTime?.let { time ->
                holder.binding.dateAndTime.text =
                        "${BaseUtils.getFormatedDateUtc(
                                "${BaseUtils.getFormatedDate(
                                        it,
                                        "yyyy-MM-dd HH:mm",
                                        "yyyy-MM-dd"
                                )} $time",
                                "yyyy-MM-dd hh:mm:ss",
                                "dd/MM/yyyy hh:mm a"
                        )}"
                  /*  "${BaseUtils.getFormatedDateUtc(
                        time,
                        "HH:mm:ss",
                        "hh:mm"
                    )} ${list[position].bookingPeriod} ${BaseUtils.getFormatedDateUtc(
                        it,
                        "yyyy-MM-dd",
                        "dd/MM/yyyy"
                    )}"*/
            }

        }

        if (list[position].status=="completed"){
            list[position].status?.let {
                holder.binding.status.text = " "+context.getString(R.string.consultation)+" "+it
            }
        }else{
            list[position].status?.let {
                holder.binding.status.text = it
            }
        }


        holder.binding.healthIssue.text = list[position].expierence+" year expirence"


        if ( list[position].booking_type=="home_visit"){
            holder.binding.textViewststuss.text =" Home visit"

        }else   if ( list[position].booking_type=="virtual"){
            holder.binding.textViewststuss.text =" Virtual"

        }else   if ( list[position].booking_type=="emergency"){

            holder.binding.textViewststuss.text =" Emergency"

        }

        if (list[position].isFeatureBooking == "false") {
           // holder.binding.healthIssue.text = context.getString(R.string.emergency_booking)
        }
        else if(list[position].isFeatureBooking == "true"){

        }

    }
}