package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildArticleBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.ArticlesData
import com.app.hakeemUser.models.SpecialityImageData
import com.app.hakeemUser.utils.UiUtils

class HomeArticleAdapter(
    var context: Context,
    var listSpeciality: ArrayList<ArticlesData>,
    var clickListener: OnClickListener
) : RecyclerView.Adapter<HomeArticleAdapter.HomeHeaderViewHolder>() {

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildArticleBinding = ChildArticleBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_article,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listSpeciality.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        listSpeciality[position].articleImage?.let { imageUrl ->
            UiUtils.loadImage(holder.binding.image, imageUrl)
        }


        holder.binding.root.setOnClickListener {
            clickListener.onClickItem(position)
        }

    }

}