package com.app.hakeemUser.view.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityAvailableDoctorBinding
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.view.adapter.AvailableDoctorAdapter
import com.app.hakeemUser.viewmodel.AvailableDoctorViewModel
import com.app.hakeemUser.viewmodel.BookingViewModel

class AvailableDoctorActivity : BaseActivity() {

    var binding: ActivityAvailableDoctorBinding? = null
    var viewmodel: AvailableDoctorViewModel? = null
    var bookingViewModel: BookingViewModel? = null
    var adapter: AvailableDoctorAdapter? = null
    var value: Int = 0
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_available_doctor)
        viewmodel = ViewModelProvider(this).get(AvailableDoctorViewModel::class.java)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        getIntentValues()
        setAdapter()
        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
    }

    private fun getIntentValues() {
        intent.extras?.let {
            it.getInt(Constants.IntentKeys.SPECIALITYID).let { value ->
                viewmodel?.configDataSource(this, value.toString())
                initListner()
            }
            }
    }

    private fun initListner() {

        viewmodel?.itemPagedList?.observe(this, Observer { list ->

            adapter?.let {
                it.submitList(list)
               /* if(list.size > 0){
                    value = 1
                }
                else{
                    Log.d("hgvh",""+value)
                    if(value == 0){
                        DialogUtils.showAlert(this, object : SingleTapListener {
                            override fun singleTap() {
                            }

                        }, "No Doctors Available")
                    }

                }*/
            }
        })

    }

    private fun setAdapter() {

        adapter = AvailableDoctorAdapter(this)
        binding!!.availableDoctors.layoutManager = LinearLayoutManager(this)
        binding!!.availableDoctors.adapter = adapter


    }




    fun onBackPressed(view: View) {
        onBackPressed()
    }
}