package com.app.hakeemUser.view.activity

import android.Manifest.permission
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityOnboardBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.utils.BaseUtils
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.GpsUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.view.adapter.OnBoardAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task

class OnBoardActivity : BaseActivity() {
    var binding: ActivityOnboardBinding? = null

    var dots = arrayOf<TextView>()
    lateinit var mViewpager: ViewPager2
    //var imageList = arrayListOf(R.drawable.onboard1, R.drawable.onboard2, R.drawable.onboard3)
    var imageList1 = arrayListOf(R.drawable.ic_group_1210, R.drawable.ic_group_1212__1_, R.drawable.ic_group_1213,R.drawable.ic_group_1214)

    var descriptionList = ArrayList<String>()
    var isSkipclicked = false
    var descriptionhead = ArrayList<String>()
    var sharedHelper: SharedHelper? = null
    var  positionofadapter :Int=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedHelper = SharedHelper(this)
        Log.d("azbhazhj",""+sharedHelper!!.language)

        binding = ActivityOnboardBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)

/*

        binding!!.skip.setOnClickListener(View.OnClickListener {
            proceed()

        })

        binding!!.skip2.setOnClickListener(View.OnClickListener {

            Log.d("wertyuio",""+ positionofadapter)
            positionofadapter-1
            binding!!.onBoard.currentItem = positionofadapter
        })
*/

        //setContentView(R.layout.activity_onboard)
        if(!BaseUtils.isPermissionsEnabled(this) || !BaseUtils.isGpsEnabled(this)){
            BaseUtils.permissionsEnableRequest(this)
            //BaseUtils.gpsEnableRequest(this)
        }

        descriptionList = arrayListOf(
            getString(R.string.onboarddesc1),
            getString(R.string.onboarddesc2),
            getString(R.string.onboarddesc3),
            getString(R.string.onboarddesc4)
        )

        descriptionhead = arrayListOf(
            getString(R.string.virtualconsultaionhome),
            getString(R.string.labtesting),
            getString(R.string.emergency),
            getString(R.string.onboarddeschead4)
        )

        val boardAdapter =
            OnBoardAdapter(this@OnBoardActivity, this, imageList1, descriptionList, descriptionhead, object : OnClickListener {

                override fun onClickItem(position: Int) {
                    Log.d("dfghjkl",""+position)
                    binding!!.onBoard.currentItem = position
                    positionofadapter=position
                }

            }, object : SingleTapListener {
                override fun singleTap() {
                    isSkipclicked = true
                    if(!BaseUtils.isPermissionsEnabled(this@OnBoardActivity)){
                        BaseUtils.permissionsEnableRequest(this@OnBoardActivity)
                    }
                    else {
                        if(!BaseUtils.isGpsEnabled(this@OnBoardActivity)){
                            BaseUtils.gpsEnableRequest(this@OnBoardActivity)
                        }
                        else{
                            proceed()
                        }
                    }
                }
            })
        binding!!.onBoard.adapter = boardAdapter
        binding!!.indicator.setViewPager(binding!!.onBoard)
        TabLayoutMediator(binding!!.tabLayout, binding!!.onBoard) { tab, position ->
        }.attach()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("xcbnc",""+requestCode)
        val code: Int = 200
        when (requestCode) {
            code -> if (grantResults.size > 0) {
                val locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED
                if (locationAccepted && storageAccepted && cameraAccepted) {
                    //all permissions granted
                    if(!BaseUtils.isGpsEnabled(this@OnBoardActivity)){
                        BaseUtils.gpsEnableRequest(this@OnBoardActivity)
                    }
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(
                                permission.CAMERA
                            ) || shouldShowRequestPermissionRationale(permission.WRITE_EXTERNAL_STORAGE)
                        ) {
                            BaseUtils.permissionsEnableRequest(this)
                            return
                        } else {
                            BaseUtils.displayManuallyEnablePermissionsDialog(this)
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("xcbnc",""+requestCode)
        if(requestCode == 51){
            if(!BaseUtils.isGpsEnabled(this)){
                //BaseUtils.gpsEnableRequest(this)
            }
            else {
                if(isSkipclicked == true){
                    proceed()
                }
            }
        }
    }

    fun proceed(){
        startActivity(Intent(this@OnBoardActivity, ContinueAsActivity::class.java))
        finish()
    }




}