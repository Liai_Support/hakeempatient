package com.app.hakeemUser.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityForgotPasswordBinding
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.interfaces.MoveToFragment
import com.app.hakeemUser.models.CheckOtpData
import com.app.hakeemUser.models.GetOtpData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.RegisterViewModel


class ForgotPasswordFragment(
    var moveToFragment: MoveToFragment,
    var supportFragmentManager: FragmentManager
) : Fragment(R.layout.fragment_forgot_password) {

    var binding: ActivityForgotPasswordBinding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = ActivityForgotPasswordBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())

        getIntentValues()

        binding!!.verify.setOnClickListener {
            if (binding!!.otp.text.toString().trim() == "") {
                UiUtils.showSnack(binding!!.root, "Please enter your otp")
            } else {
                DialogUtils.showLoader(requireContext())
                viewmodel?.verifyOtp(binding!!.emailId.text.toString(), binding!!.otp.text.toString())
                    ?.observe(viewLifecycleOwner,
                        Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(binding!!.root, msg)
                                        }
                                    } else {
                                        it.data?.let { data ->
                                            handleCheckotpResponse(data)
                                        }
                                    }
                                }

                            }

                        })
            }
        }

        binding!!.back.setOnClickListener {

            val manager = requireActivity().supportFragmentManager
            val trans: FragmentTransaction = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()

        }
        return view
    }

    private fun getIntentValues() {
        arguments?.let {
            it.getString(Constants.IntentKeys.EMAIL)?.let { email ->
                binding!!.emailId.setText(email)
                getOtp(email)
            }
        }
    }

    private fun getOtp(email: String) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.getOtp(email)?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data)
                        }
                    }
                }
            }
        })
    }

    private fun handleResponse(data: GetOtpData) {
        data.otp?.let {
            binding!!.otp.setText(it)
        }
    }

    private fun handleCheckotpResponse(data: CheckOtpData) {

        data.token?.let { sharedHelper?.token = it }
        val manager = requireActivity().supportFragmentManager
        val trans: FragmentTransaction = manager.beginTransaction()
        trans.remove(this)
        trans.commit()
        manager.popBackStack()

        moveToFragment.moveTo(Constants.IntentFragment.RESETPASSWORD, Bundle())


//        startActivity(Intent(this, ResetPasswordActivity::class.java))

    }

}