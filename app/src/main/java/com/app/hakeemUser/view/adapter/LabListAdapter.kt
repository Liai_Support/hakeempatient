package com.app.hakeemUser.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemDoctor.models.LabData
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildTestListBinding
import com.app.hakeemUser.databinding.LablistshowBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.view.activity.LablistActivity
import java.util.*

class LabListAdapter(var lablistActivity: LablistActivity, var context: Context, var list: ArrayList<LabData>) : RecyclerView.Adapter<LabListAdapter.ViewHolder>() {
    var testnamearray: ArrayList<String> = ArrayList()
    var ha: String? = ""
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var lablistshowBinding: LablistshowBinding = LablistshowBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(context).inflate(
                        R.layout.lablistshow,
                        parent,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lablistshowBinding.map.setOnClickListener {
            var lat = list[position].latitude
            var lng = list[position].longitude
            val uri = String.format(Locale.ENGLISH,
                    "google.navigation:q=$lat,$lng")
            val gmmIntentUri = Uri.parse(uri)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            lablistActivity.startActivity(mapIntent)
        }

        testnamearray.clear()
        for (i in 0 until list[position].matched_test!!.size) {
            testnamearray.add(list[position].matched_test!![i].Test_name!!)
        }

        ha = ""
        for(i in 0 until testnamearray.size){
            var h = i+1
            if(h == testnamearray.size){
                ha = ha+testnamearray[i]
            }
            else{
                ha = ha+testnamearray[i]+","
            }
        }

        holder.lablistshowBinding.lab.text = list[position].lab_name+"("+ha+")"
    }



}