package com.app.hakeemUser.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityEmergencyMapBookingBinding
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.LiveDoctorData
import com.app.hakeemUser.network.UrlHelper
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.MapViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.json.JSONObject
import java.util.*


class EmergencyMapBooking : BaseActivity(), OnMapReadyCallback {
    var binding: ActivityEmergencyMapBookingBinding? = null

    var mapFragment: MapFragment? = null
    private lateinit var mMap: GoogleMap
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    var map: GoogleMap? = null
    var mapView: View? = null
    private var mapViewModel: MapViewModel? = null
    private var doctorData: ArrayList<LiveDoctorData> = ArrayList()
    var points: List<LatLng> = ArrayList()
    var cd4 : MutableList<LatLng> = mutableListOf<LatLng>()
    private var sharedHelper: SharedHelper? = null
    var bookingViewModel: BookingViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmergencyMapBookingBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_emergency_map_booking)
        sharedHelper = SharedHelper(this)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)

        mapFragment = fragmentManager.findFragmentById(R.id.map1) as MapFragment
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapView = mapFragment!!.getView();
        mapFragment?.getMapAsync(this)
        locationListner()
        askLocationPermission()

        getdoctorlist()

        checkActivityBookingRandom()
        sharedHelper = SharedHelper(this)
        if (sharedHelper!!.language == "ar"){

            binding!!.imageView14.rotation= 180F
        }
    }

    private fun checkActivityBookingRandom() {

        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"

        Log.d("kanikashanmugam",""+date)

        DialogUtils.showLoader(this)
        bookingViewModel?.getActiveBooking(this, date)?.observe(this, androidx.lifecycle.Observer { it ->
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { msg ->
                        UiUtils.showSnack(binding!!.root, msg)
                    }
                } else {

                    it.data?.let { list ->
                        Log.d("wertyuiol",""+list.size)
                        if (list.size == 0) {


                        } else {

                            DialogUtils.showAlertt(this, object : SingleTapListener {
                                override fun singleTap() {

                                    /*    val intent =
                                                Intent(
                                                        this@SummaryEmergencyConsultActivity,
                                                        DashBoardActivity::class.java
                                                )
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()*/
                                    onBackPressed()
                                }

                            }, getString(R.string.booking_alert))
                        }
                    }

                }
            }
        })


    }


    private fun getdoctorlist() {
        DialogUtils.showLoader(this)
        mapViewModel?.listdoctorlive(this, sharedHelper!!.currentLat, sharedHelper!!.currentLng, "1")
            ?.observe(this, Observer{
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                            binding!!.status.visibility = View.GONE
                        } else {
                            it.data?.let { data ->
                                this.doctorData = data.doctors!!
                                if(data.doctors!!.size == 0){
                                    it.message?.let { it1 -> UiUtils.showSnack(binding!!.root, it1) }
                                }
                                else{
                                    for ((index, value) in doctorData.withIndex()) {
                                        value.lat?.let { it1 -> value.lng?.let { it2 ->
                                            LatLng(it1,
                                                    it2
                                            )
                                        } }?.let { it2 ->
                                            cd4.add(index,
                                                    it2
                                            )
                                        }
                                    }

                                    ScheduledBookingSingleton.getInstance().dfee = data.em_fees.toString()
                                    locateMyLocation()
                                }

                            }
                        }
                    }

                }
            })

    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        // Add a marker in Sydney and move the camera
      //  val sydney = LatLng(23.6611815, 42.9249046)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
       // map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9.0f))

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        map?.isMyLocationEnabled = true

        map!!.setPadding(50, 50, 30, 105)

      /*  val locationButton = (mapView.findViewById("1".toInt()).getParent() as View).findViewById<View>("2".toInt())
        val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        rlp.setMargins(0, 180, 180, 0)*/


        myLat?.let {
            myLng?.let {
                    getLastKnownLocation()
            }
        }

        map?.setOnCameraIdleListener {

          //  var centerLatLng = map?.cameraPosition?.target
           /* idleLat = centerLatLng?.latitude
            idleLng = centerLatLng?.longitude

            idleLat?.let {
                idleLng?.let {
                    searchPlaces()
                }
            }*/

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.Permission.LOCATION_PERMISSION) {

            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }

    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_PERMISSION_PERMISSON_LIST,
                    Constants.Permission.LOCATION_PERMISSION
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }

    private fun getUserLocation() {

        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }

    private fun getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {

                myLat = location.latitude
                myLng = location.longitude


                    map?.let {
                      //  idleLat = myLat
                      //  idleLng = myLng
                        locateMyLocation()
                    }


            } else {

                fusedLocationClient?.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    null
                )

            }
        }

    }

    private fun locateMyLocation() {

        myLat?.let { lat ->
            myLng?.let { lng ->
                    map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 10.0f))
            }
        }

        //cd4.size


        for (item in cd4){
            //map?.addMarker(MarkerOptions().position(item))
            map?.addMarker(MarkerOptions().position(item).icon(BitmapDescriptorFactory.fromResource(R.drawable.doct)))
           // a[item] =
        }

    }



    fun onBookingClicked(view: View) {
        startActivity(Intent(this, SummaryEmergencyConsultActivity::class.java))
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }




    private fun hitEmergencyApi() {


        var c = Calendar.getInstance()

        var p3 = c.get(Calendar.YEAR)
        var p4 = c.get(Calendar.MONTH)
        var p5 = c.get(Calendar.DAY_OF_MONTH)

        var date = "$p3/${BaseUtils.numberFormat(p4 + 1)}/${BaseUtils.numberFormat(p5)}"

        var p1 = c.get(Calendar.HOUR_OF_DAY)
        var p2 = c.get(Calendar.MINUTE)

        var time = ""
        var period = ""

        if (p1 <= 11) {
            if (p1 == 0) {
                period = "AM"
                time =
                    "12:${BaseUtils.numberFormat(p2)}"
            } else {
                period = "AM"
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            }

        } else {
            period = "PM"
            if (p1 < 13) {
                time =
                    "${BaseUtils.numberFormat(p1)}:${BaseUtils.numberFormat(p2)}"
            } else {
                time =
                    "${BaseUtils.numberFormat(p1 - 12)}:${BaseUtils.numberFormat(p2)}"
            }

        }


        var jsonObject = JSONObject()
        jsonObject.put(Constants.ApiKeys.PATIENTID, sharedHelper?.id)
        jsonObject.put(
            Constants.ApiKeys.SPECIALITYID,"2"
        )
        jsonObject.put(
            Constants.ApiKeys.PAYMENTMETHOD, ScheduledBookingSingleton.getInstance().paymentMethod

        )
        jsonObject.put(
            Constants.ApiKeys.TRANSACTIONID,ScheduledBookingSingleton.getInstance().transactionId

        )
        jsonObject.put(
            Constants.ApiKeys.EFees,ScheduledBookingSingleton.getInstance().dfee
        )
        jsonObject.put(Constants.ApiKeys.BOOKINGDATE, date)

        Log.d("ajsnajszna",""+ time)
        Log.d("ajsnajszna",""+ date)
        jsonObject.put(Constants.ApiKeys.BOOKINGTIME, time)
        jsonObject.put(Constants.ApiKeys.BOOKINGPERIOD, period)
        jsonObject.put(
            Constants.ApiKeys.LOCATION,
            ScheduledBookingSingleton.getInstance().location
        )
        jsonObject.put(
            Constants.ApiKeys.LATITUDE,
            ScheduledBookingSingleton.getInstance().latitude
        )
        jsonObject.put(
            Constants.ApiKeys.LONGITUDE,
            ScheduledBookingSingleton.getInstance().longitude
        )
        jsonObject.put(Constants.ApiKeys.PROVIDERTYPE, Constants.BookingType.AMBULANCE)
        jsonObject.put(Constants.ApiKeys.ISVIRTUALBOOKING, 0)
//        jsonObject.put(
//            "transactionId",
//            ScheduledBookingSingleton.getInstance().transactionId
//        )
//        jsonObject.put(
//            "paymentMethod",
//            ScheduledBookingSingleton.getInstance().paymentMethod
//        )


        /*  socket?.on("request_completed-${sharedHelper?.id}") {
                      ThreadUtils.runOnUiThread {
                  DialogUtils.dismissLoader()
                  showSuccessDialog()
              }
          }
          DialogUtils.showLoader(this)*/


    }

}