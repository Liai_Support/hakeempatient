package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildReviewBinding
import com.app.hakeemUser.models.ReviewData
import com.app.hakeemUser.utils.UiUtils

class ReviewAdapter(
    var context: Context,
    var list: ArrayList<ReviewData>
) :
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildReviewBinding = ChildReviewBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_review,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        list[position].name?.let { holder.binding.name.text = it }
        list[position].rating?.let {
            UiUtils.setRating(
                context,
                it.toFloat(),
                holder.binding.rating,
                45
            )
        }
        list[position].rating?.let { holder.binding.ratingValue.text = "$it/5" }
        list[position].review?.let { holder.binding.comment.text = it }


    }

}