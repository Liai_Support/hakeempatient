package com.app.hakeemUser.view.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildOnboard1Binding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.view.activity.*

class OnBoardAdapter(
        var activity: OnBoardActivity,
    var context: Context,
    var list: ArrayList<Int>,
    var description: ArrayList<String>,
    var descriptionhead: ArrayList<String>,
    var next: OnClickListener,
    var skip: SingleTapListener
) :
    RecyclerView.Adapter<OnBoardAdapter.OnBoardViewHolder>() {

    inner class OnBoardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildOnboard1Binding = ChildOnboard1Binding.bind(view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OnBoardViewHolder {
        return OnBoardViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_onboard1,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: OnBoardViewHolder, position: Int) {

/*     if (position == list.size - 1) {
         activity.binding!!.skip.visibility = View.GONE
       }else{

         activity.binding!!.skip.visibility = View.VISIBLE
     }*/
//
//        holder.binding.next.setOnClickListener {
//            next.onClickItem(position + 1)
//        }


        holder.binding.skip.setOnClickListener {
            skip.singleTap()
        }
        holder.binding.description.text = description[position]

        holder.binding.descriptionhead.text = descriptionhead[position]


        holder.binding.continuebutton2.setOnClickListener {
            context.startActivity(
                Intent(
                    context,
                    RegisterActivity1::class.java
                )
            )
        }

        holder.binding.continuebutton.setOnClickListener {
            next.onClickItem(position + 1)
                  }


        holder.binding.img1.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                list[position]
            )
        )


        holder.binding!!.skip2.setOnClickListener(View.OnClickListener {

            next.onClickItem(position - 1)


        })
        /*if(position == 0){
            activity.ind1()
        }
        else if(position == 1){
            activity.ind2()
        }
        else if(position == 2){
            activity.ind3()
        }*/
        Log.d("pos",""+position)




        if (position==3){
           holder.binding.continuebutton.visibility=View.GONE
            holder.binding.continuebutton2.visibility=View.VISIBLE
            holder.binding!!.skip.visibility=View.GONE
            holder.binding!!.skip2.visibility=View.VISIBLE

        } else if(position==2){

            holder.binding!!.skip.visibility=View.GONE
            holder.binding!!.skip2.visibility=View.VISIBLE

        }else if(position==1){

            holder.binding!!.skip.visibility=View.GONE
            holder.binding!!.skip2.visibility=View.VISIBLE

        }else if(position==0){

            holder.binding!!.skip.visibility=View.VISIBLE
            holder.binding!!.skip2.visibility=View.GONE
          //  activity.binding!!.skip.text=""
           // activity.binding!!.skip.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_arrow_back_ios_24, 0, 0, 0);
      /*
*/
        }

    }
}