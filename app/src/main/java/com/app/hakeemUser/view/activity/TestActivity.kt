package com.app.hakeemUser.view.activity

import android.content.Intent
import android.net.sip.SipAudioCall
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityResetPasswordBinding
import com.app.hakeemUser.databinding.ActivityTestBinding
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.HomeViewModel
import org.json.JSONObject


class TestActivity : AppCompatActivity() {
    var binding: ActivityTestBinding? = null;

    private var homeViewModel: HomeViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTestBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_test)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

    }


}