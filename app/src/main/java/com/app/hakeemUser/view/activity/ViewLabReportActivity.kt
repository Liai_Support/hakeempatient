package com.app.hakeemUser.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityViewLabReportBinding
import com.app.hakeemUser.models.LabReportJson2
import com.app.hakeemUser.models.ReportData
import com.app.hakeemUser.models.SlabData
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.view.adapter.LabReportAdapter

class ViewLabReportActivity : BaseActivity() {
    var binding: ActivityViewLabReportBinding? = null
    var sharedHelper: SharedHelper? = null
    var labReportJson2: java.util.ArrayList<LabReportJson2>? =
        java.util.ArrayList<LabReportJson2>()
    var testnamee: MutableList<String> = mutableListOf<String>()
    var testnamee2: MutableList<String> = mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewLabReportBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
       // setContentView(R.layout.activity_view_lab_report)
        sharedHelper = SharedHelper(this)

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
        val lab: ArrayList<ReportData> = intent.getSerializableExtra("labreports") as ArrayList<ReportData>


        for (i in 0 until  lab.size){

            testnamee.add(lab[i].test_name.toString())
            Log.d("etdyteywe",""+testnamee)
        }

        testnamee2=testnamee.distinct().toMutableList()
        Log.d("sdxfggvbhn",""+testnamee2)



        for (i in 0 until testnamee2.size){
            var reportname: MutableList<String> = mutableListOf<String>()


            for (k in 0 until lab.size){


                if (testnamee2[i]==lab[k].test_name){

                    if (reportname.size==0){
                        reportname.add(lab[k].report!!)

                        Log.d("qweewewew",""+reportname)


                    }else{
                        for (j in 0 until reportname.size){
                            val m:Int=0
                            Log.d("ettete",""+lab[k])
                            Log.d("ettete",""+k)
                            Log.d("ettete",""+j)
                            Log.d("tetettete", (""+reportname[j]==lab[k].report).toString())
                            if (reportname[j]==lab[k].report){

                            }else{
                                reportname.add(lab[k].report!!)
                                break
                            }
                        }


                    }


                }


            }


            reportname=reportname.distinct().toMutableList()
            labReportJson2!!.add(LabReportJson2(testnamee2[i], reportname))
            Log.d("ccccc",""+labReportJson2)




        }


        binding!!.labreports.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding!!.labreports.adapter = LabReportAdapter(this@ViewLabReportActivity,this, lab,labReportJson2)
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }
}