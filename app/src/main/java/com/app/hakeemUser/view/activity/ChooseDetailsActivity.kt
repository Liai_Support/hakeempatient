package com.app.hakeemUser.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.*
import com.app.hakeemUser.interfaces.ChooseOptionCallBack
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.*
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.adapter.SelectPatientAdapter
import com.app.hakeemUser.view.adapter.SpecialityAdapter
import com.app.hakeemUser.viewmodel.MembersViewModel
import java.util.ArrayList
import android.view.inputmethod.InputMethodManager
import com.google.android.play.core.internal.v

class ChooseDetailsActivity : BaseActivity() {

    var binding: ActivitySelectPatientDetailsBinding? = null
    var selectedSpeciality: Int? = null
    var selectedSpecialityGender: String? = null
    var selectedDetails: MemberData? = null
    var membersViewModel: MembersViewModel? = null
    private var memberData: ArrayList<MemberData> = ArrayList()
    var selectedoption: Boolean? = null
    public var ispatientedited: Boolean? = false
    var sharedHelper: SharedHelper? = null

    var descrip:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectPatientDetailsBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        sharedHelper = SharedHelper(this)
        Log.d("sharedHelper!!.language",""+sharedHelper!!.language)
        //setContentView(R.layout.activity_select_patient_details)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)

        getIntentValues()

        getmemeberlist()

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }
        Log.d("sharedHelper2language",""+sharedHelper!!.language)




    }

    override fun onResume() {
        super.onResume()
        if(ScheduledBookingSingleton.getInstance().isGlobalBooking == 1){

        }
        else {
            selectedoption = null
            if (ispatientedited == true) {
                ispatientedited = false
                getmemeberlist()
            }
        }
    }


    public fun getmemeberlist(){
        DialogUtils.showLoader(this)
        membersViewModel?.getmemberDetails(this)
                ?.observe(this, Observer{
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(binding!!.root, msg)
                                }
                            } else {
                                it.data?.let {
                                    data ->
                                    for (item in data){
                                        if(item.type.equals("myself",true)){
                                            sharedHelper!!.isMyself = true
                                        }
                                    }
                                    this.memberData = data
                                    setAdapter1()
                                    for(item in memberData){
                                       /* var details = PatientDetails()
                                        details.name = item.name.toString()
                                        details.patientType = item.type.toString()
                                        details.dob = item.dob.toString()
                                        details.bloodGroup = item.bloodgroup.toString()
                                        details.insurance = item.insurance.toString()
                                        details.gender = item.gender.toString()
                                       // details.age = item.type.toString()
                                        details.memberid = item.memberId!!.toInt()

                                        var list = SharedHelper(this).patientList
                                        list.add(details)
                                        SharedHelper(this).patientList = list*/
                                    }

                                   /* memberData[].

                                    var details = PatientDetails()
                                    details.patientType =
                                    details.name = name.text.toString()
                                    details.dob = age.text.toString()
                                    details.bloodGroup = bloodGroup1.text.toString()
                                    details.insurance = insurance.text.toString()
                                    details.gender = getgender()
                                    details.age = ageDif
                                    details.memberid = memberid

                                    var list = SharedHelper(this).patientList
                                    list.add(details)
                                    SharedHelper(this).patientList = list
                                    finish()*/

                                }
                            }
                        }

                    }
                })


        binding!!.previousHealthIssue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {

                descrip  =  binding!!.previousHealthIssue?.text.toString()

                if (descrip.length>=100){

                    Log.d("shxbdsbxj","yesssss")
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                    imm?.hideSoftInputFromWindow(binding!!.root1.windowToken, 0)

                    UiUtils.showSnack(
                        binding!!.root1,
                        getString(R.string.pleasegiveashortdescription)
                    )

                   // Toast.makeText(applicationContext,getString(R.string.pleasegiveashortdescription),Toast.LENGTH_LONG)
                }else{

                }
                Log.d("descripwertyuio",""+descrip)

                Log.d("wertyuio",""+descrip.length)





            }
        })
    }


    private fun getIntentValues() {

        intent.extras?.let {
            it.getParcelableArrayList<SpecialityImageData>(Constants.IntentKeys.SPECIALITYLIST)
                ?.let { list ->

                    var i = 0
                    while (i < list.size) {

                        if (ScheduledBookingSingleton.getInstance().isElderlyFlow)
                            if (!(list[i].name.equals("nurse", true) || list[i].name.equals("Physiotherapy", true)
                                            || list[i].name.equals("Dressing Changes", true) || list[i].name.equals("Elderly Care", true))
                            ) {
                                list.removeAt(i)
                            } else {
                                i++
                            }
                        else if (list[i].name.equals("ambulance", true) ||
                            list[i].name.equals("nurse", true) ||
                            list[i].name.equals("Dressing Changes", true) ||
                            list[i].name.equals("Elderly Care", true) ||
                            list[i].name.equals("Physiotherapy", true)
                        ) {
                            list.removeAt(i)
                        } else {
                            i++
                        }


                    }


                    var adapter = SpecialityAdapter(this, list, object : OnClickListener {
                        override fun onClickItem(position: Int) {
                            selectedSpeciality = list[position].id
                            if(selectedSpeciality == 3){
                                binding!!.lin.visibility = View.VISIBLE
                                binding!!.male.setOnClickListener{
                                    binding!!.male.isChecked = true
                                    binding!!.female.isChecked = false
                                    selectedSpecialityGender = "male"
                                }

                                binding!!.female.setOnClickListener{
                                    binding!!.male.isChecked = false
                                    binding!!.female.isChecked = true
                                    selectedSpecialityGender = "female"

                                }
                            }
                            else{
                                binding!!.lin.visibility = View.GONE
                                selectedSpecialityGender = ""
                            }
                        }
                    })
                    binding!!.specialityList.layoutManager = GridLayoutManager(this, 2)
                    binding!!.specialityList.adapter = adapter

                    if(ScheduledBookingSingleton.getInstance().isGlobalBooking == 1){
                        selectedoption = true
                        ScheduledBookingSingleton.getInstance().chooseoption = 0
                    }

                }
            Log.d("yhhyhuhh",""+selectedSpeciality)
            Log.d("mkkkm",""+it)

        }


    }


    public fun setAdapter1() {

        binding!!.patientList.layoutManager = LinearLayoutManager(this)
        binding!!.patientList.adapter = SelectPatientAdapter(this,this, memberData) { pos ->
            var list = memberData
            selectedDetails = list[pos]
            if(selectedDetails?.insuranceid == null || selectedDetails?.insuranceid.equals("") || selectedDetails?.insuranceid!!.isEmpty()){
                sharedHelper?.isInsurance = false;
            }
            else{
                sharedHelper?.isInsurance = true;
            }
        }
        selectedDetails = null

    }

    /*private fun setAdapter() {

        patientList.layoutManager = LinearLayoutManager(this)
        patientList.adapter = SelectPatientAdapter(this, SharedHelper(this).patientList) { pos ->
            var list = SharedHelper(this).patientList
            selectedDetails = list[pos]
        }
        selectedDetails = null

    }*/

    fun onConfirmClicked(view: View) {
        if (selectedSpeciality!=null){
            selectedSpeciality?.let {
                if (validInputs()) {
                    ScheduledBookingSingleton.getInstance().specialityId =
                        selectedSpeciality?.toString()
                    ScheduledBookingSingleton.getInstance().specialityGender =
                        selectedSpecialityGender?.toString()
                    ScheduledBookingSingleton.getInstance().patientType = selectedDetails?.type
                    ScheduledBookingSingleton.getInstance().patientName = selectedDetails?.name
                    ScheduledBookingSingleton.getInstance().patientAge = selectedDetails?.age.toString()

                    ScheduledBookingSingleton.getInstance().patientGender =
                        selectedDetails?.gender
                    ScheduledBookingSingleton.getInstance().insurance =
                        selectedDetails?.insuranceid
                    ScheduledBookingSingleton.getInstance().memberid =
                        selectedDetails?.memberId.toString()
                  /*  ScheduledBookingSingleton.getInstance().previousIssue =
                        binding!!.previousHealthIssue.text.toString()*/
                    ScheduledBookingSingleton.getInstance().previousIssue = descrip
                    startActivity(
                        Intent(this, AvailableDoctorActivity::class.java)
                            .putExtra(Constants.IntentKeys.SPECIALITYID, it)
                    )
                }
                else{
                    //showSnack("Fill all fields")
                }

            }
        }else{

            showSnack(getString(R.string.selectasplecaility))

        }





    }

    private fun validInputs(): Boolean {
        when {
            selectedSpeciality == null -> {
                showSnack(getString(R.string.selectasplecaility))
                return false
            }
            selectedDetails == null -> {
                showSnack(getString(R.string.selectapatient))
                return false
            }
            selectedSpecialityGender == null -> {
                if(selectedSpeciality == 3){
                    showSnack(getString(R.string.selctagender))
                    return false
                }
            }
            //            previousHealthIssue.text.toString().trim().isEmpty() -> {
            //                showSnack("Enter previous health")
            //                return false
            //            }
            selectedoption == null -> {
               DialogUtils.chooseoptionDialog(this, object : ChooseOptionCallBack {
                   override fun onPositiveClick(value: Boolean) {
                       selectedoption = value
                       if(selectedoption == true){
                           ScheduledBookingSingleton.getInstance().chooseoption = 0
                           onConfirmClicked(binding!!.view)
                       }
                       else{
                           if(sharedHelper?.isInsurance == true){
                               ScheduledBookingSingleton.getInstance().chooseoption = 1
                               onConfirmClicked(binding!!.view)
                           }
                           else{
                               showSnack(getString(R.string.pleaseupdatepatientinsurancedetails1))
                               selectedoption = null
                           }
                       }
                   }

                   override fun onNegativeClick() {

                   }

               })
                return false
            }
        }
        return true
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    private fun showSnack(message: String) {
        UiUtils.showSnack(binding!!.root, message)
    }

    fun onAddPatientClicked(view: View) {
        ispatientedited = true
        startActivity(
            Intent(this, AddPatientDetailsActivity::class.java)
        )
    }

}