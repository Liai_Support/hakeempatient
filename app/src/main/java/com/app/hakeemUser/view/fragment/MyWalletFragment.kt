package com.app.hakeemUser.view.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.databinding.FragmentMyWalletBinding
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.interfaces.WalletRechargeCallBack
import com.app.hakeemUser.models.TransactionData
import com.app.hakeemUser.models.TransactionHistory
import com.app.hakeemUser.utils.Constants.RequestCode.PAYPAL_REQUEST_CODE
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DashBoardActivity
import com.app.hakeemUser.view.activity.NoonPaymentActivity
import com.app.hakeemUser.view.adapter.MyWalletAdapter
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.paypal.android.sdk.payments.*
import org.json.JSONObject
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

class MyWalletFragment : Fragment(R.layout.fragment_my_wallet) {

    var binding: FragmentMyWalletBinding? = null
    var config: PayPalConfiguration? = null
    private var amount = 0.0
    var sharedHelper: SharedHelper? = null

    var viewModel: BookingViewModel? = null

    var data: ArrayList<TransactionHistory> = ArrayList()
    var walletAdapter: MyWalletAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMyWalletBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())

        setWalletAdapter()

        initListener()

        config = PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(getString(R.string.paypal_client_id))

        startServicePaypal()
        getWalletDetails()
        (activity as DashBoardActivity?)!!.binding!!.popupLoc.closePop.setOnClickListener(View.OnClickListener {
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        })
        return view
    }

    private fun startServicePaypal() {

        val intent = Intent(requireContext(), PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        requireContext().startService(intent)

    }


    private fun initListener() {

        binding!!.addMoney.setOnClickListener {

            DialogUtils.showExtraFeeDialog(requireContext(), object : WalletRechargeCallBack {
                override fun onPositiveClick(value: Int) {
                    amount = value.toDouble()
                    // processPayment(value)
                    startActivityForResult(
                        Intent(requireContext(), NoonPaymentActivity::class.java)
                            .putExtra(
                                "amount",
                                amount
                            ), 100
                    )
                }

                override fun onNegativeClick() {

                }

            })
        }
    }

    private fun processPayment(amount: Int) {

        val payPalPayment = PayPalPayment(
            BigDecimal(amount.toString()), "USD",
            "Purchase Goods", PayPalPayment.PAYMENT_INTENT_SALE
        )

        val intent = Intent(requireContext(), PaymentActivity::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment)
        startActivityForResult(intent, PAYPAL_REQUEST_CODE)
    }



    private fun setWalletAdapter() {

        walletAdapter = MyWalletAdapter(requireContext(), data)
        binding!!.walletList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding!!.walletList.adapter = walletAdapter

    }

    override fun onDestroy() {
        super.onDestroy()
        requireContext().stopService(Intent(requireContext(), PayPalService::class.java))
    }

   /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PAYPAL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.let {
                val confirmation: PaymentConfirmation =
                    it.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)!!
                if (confirmation != null) {
                    try {
                        val paymentDetails = confirmation.toJSONObject().toString(4)

                        sendDetails(paymentDetails)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }

    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                sendPaymentDetails(data!!.getStringExtra("paymentorderid").toString(),data!!.getStringExtra("paymenttransactionid").toString())
            }else{
                UiUtils.showSnack(binding!!.root,getString(R.string.paymentfailed))

            }
        }
    }

    private fun sendDetails(paymentDetails: String) {

        try {
            var paymentDeatils: JSONObject = JSONObject(paymentDetails).get("response") as JSONObject
            if (paymentDeatils.get("state").toString().equals("approved", true)) {
                //sendPaymentDetails(paymentDeatils.get("id").toString())
            }
        } catch (e: Exception) {

        }


    }

    private fun sendPaymentDetails(porderid: String,ptransactionid: String) {
        DialogUtils.showLoader(requireContext())
        viewModel?.addAmountToWallet(requireContext(), ptransactionid, amount)
            ?.observe(viewLifecycleOwner, Observer {
                DialogUtils.dismissLoader()
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                            viewModel?.refundpayment(requireContext(), amount,porderid)?.observe(this, androidx.lifecycle.Observer {
                                it?.let {
                                      it.rcode?.let { rcode ->
                                          if (rcode != 0) {
                                              it.message?.let { msg ->
                                                  UiUtils.showSnack(binding!!.root, msg)
                                              }
                                          } else {
                                              it.message?.let { msg ->
                                                  UiUtils.showSnack(binding!!.root, msg)
                                              }
                                          }
                                      }
                                }
                            })
                        }
                    } else {
                        DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                            override fun singleTap() {
                                getWalletDetails()
                            }
                        }, getString(R.string.wallet_alert))
                    }
                }
            })
    }

    fun getWalletDetails() {

        DialogUtils.showLoader(requireContext())
        viewModel?.getWalletDetails(requireContext())?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it.error?.let { error ->
                if (error) {
                    it.message?.let { msg ->
                        UiUtils.showSnack(binding!!.root, msg)
                    }
                } else {
                    it.data?.let { data -> setResponse(data) }

                }
            }
        })

    }

    private fun setResponse(data: TransactionData) {

        data.availableCredits?.let { amount ->
            binding!!.totalAmount.text = ""+String.format(Locale.ENGLISH,"%.2f",amount)+" SAR"
        }

        data.history?.let { list ->
            this.data = list
            walletAdapter?.setValues(list)
        }


    }
}