package com.app.hakeemUser.view.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentConsultationBinding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.PatientActionListener
import com.app.hakeemUser.interfaces.SingleTapListener
import com.app.hakeemUser.models.ConsultationData
import com.app.hakeemUser.models.CurrentBookingListJson
import com.app.hakeemUser.notification.NotificationUtils
import com.app.hakeemUser.rxbus.RxBusNotification
import com.app.hakeemUser.utils.*
import com.app.hakeemUser.view.activity.*
import com.app.hakeemUser.view.adapter.ConsultationAdapter
import com.app.hakeemUser.viewmodel.BookingViewModel
import com.app.hakeemUser.viewmodel.CallViewModel
import com.app.hakeemUser.viewmodel.ConsultationViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import io.reactivex.disposables.Disposable
import java.util.*

class ConsultationFragment : Fragment(R.layout.fragment_consultation) {

    var binding: FragmentConsultationBinding? = null
    var viewmodel: ConsultationViewModel? = null
    var bookingViewModel: BookingViewModel? = null
    private lateinit var notificationEventListner: Disposable
    private var isAttached = false
   private var list2: ArrayList<ConsultationData> = ArrayList()
    var adapter: ConsultationAdapter? = null
    var clickedPosition = -1
    var list: ArrayList<ConsultationData> = ArrayList()
    var data:ArrayList<CurrentBookingListJson>? =ArrayList<CurrentBookingListJson>()

    var callViewModel: CallViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentConsultationBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewmodel = ViewModelProvider(this).get(ConsultationViewModel::class.java)
        callViewModel = ViewModelProvider(this).get(CallViewModel::class.java)
        bookingViewModel = ViewModelProvider(this).get(BookingViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        listernNotification()

         setAdapter()
        (activity as DashBoardActivity?)!!.binding!!.popupLoc.closePop.setOnClickListener(View.OnClickListener {
            (activity as DashBoardActivity?)!!.mBottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        })

        Log.d("vbbbvbb",""+ sharedHelper!!.language)

        return view
    }

    private fun setAdapter() {


        Log.d("kanikadhnmugam",""+data!!.size)
        adapter = ConsultationAdapter(requireContext(), data!!, object : OnClickListener {
            override fun onClickItem(position: Int) {
                Log.d("kanikadhnmugam",""+data!!.size)

                if (data!![position].providerId == 0) {

                    DialogUtils.showAlertDialog(requireContext(),
                        getString(R.string.cancel_message),
                        getString(R.string.confirm),
                        getString(R.string.ok),
                        getString(R.string.cancel),
                        object : DialogCallBack {
                            override fun onPositiveClick() {
                                DialogUtils.showLoader(requireContext())
                                bookingViewModel?.cancelBooking(
                                    requireContext(),
                                    data!![position].bookingId!!
                                )
                                    ?.observe(viewLifecycleOwner,
                                        Observer {
                                            DialogUtils.dismissLoader()
                                            getConsultation()
                                        })
                            }

                            override fun onNegativeClick() {

                            }

                        })

                }
                else {
                    if (data!![position].status.equals("completed", true) ||
                        data!![position].status.equals("cancelledbyprovider", true) ||
                        data!![position].status.equals("cancelledbypatient", true) ||
                        data!![position].status.equals("paymentverified", true)
                    ) {

                        //do nothing
                    } else {
                        Log.d("kanika","log1kanika")

                        if(data!![position].is_ins.equals("1")){
                            DialogUtils.showLoader(requireContext())
                            viewmodel?.getInsuranceStatus(requireContext(), data!![position].bookingId!!)?.observe(viewLifecycleOwner, Observer {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(binding!!.root, msg)
                                            }
                                        } else {

                                            Log.d( "erererer","loggedinn")

                                            it.data?.let { data1 ->
                                               if(data1[0].ins_status.equals("pending",true)){
                                                   UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                               }
                                               else if(data1[0].ins_status.equals("rejected",true)){
                                                   UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                               }
                                               else if(data1[0].ins_status.equals("accepted",true)){
                                                   if(data1[0].isPendingAmount.equals("yes",true)){
                                                       val balance =  data1[0].balance!!.toDouble()
                                                       DialogUtils.showAlertDialogPayment(requireContext(),
                                                           getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                               getString(R.string.insurance),
                                                           getString(R.string.ok),
                                                           getString(R.string.cancel),
                                                           object : DialogCallBack {
                                                               override fun onPositiveClick() {

                                                                   if (data1[0].balance.equals("0")){
                                                                       showSuccessDialog()
                                                                   }else{
                                                                       startActivity(
                                                                           Intent(
                                                                               requireActivity(),
                                                                               VirtualPaymentActivity::class.java
                                                                           )
                                                                               .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                               .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                               .putExtra("bookingid",data!![position].bookingId)
                                                                       )
                                                                   }
                                                                /*   startActivity(
                                                                       Intent(
                                                                           requireActivity(),
                                                                           VirtualPaymentActivity::class.java
                                                                       )
                                                                           .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                           .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                           .putExtra("bookingid",data!![position].bookingId)
                                                                   )*/
                                                               }

                                                               override fun onNegativeClick() {

                                                               }

                                                           })
                                                   }
                                                   else {

                                                       Log.d("dhbchb","dvheevbadhbc122")

                                                       startActivity(
                                                           Intent(
                                                               requireContext(),
                                                               DoctorTrackingActivity::class.java
                                                           )
                                                               .putExtra(
                                                                   Constants.IntentKeys.PROVIDERID,
                                                                   data!![position].providerId
                                                               )
                                                               .putExtra(
                                                                   Constants.IntentKeys.BOOKINGID,
                                                                   data!![position].bookingId
                                                               )
                                                       )
                                                   }
                                               } else if(data1[0].ins_status.equals("paid",true)){

                                                   Log.d("dhbchb","dvheevbadhbc11")

                                                   startActivity(
                                                       Intent(
                                                           requireContext(),
                                                           DoctorTrackingActivity::class.java
                                                       )
                                                           .putExtra(
                                                               Constants.IntentKeys.PROVIDERID,
                                                               data!![position].providerId
                                                           )
                                                           .putExtra(
                                                               Constants.IntentKeys.BOOKINGID,
                                                               data!![position].bookingId
                                                           )
                                                   )
                                               }
                                            }
                                        }
                                    }
                                }
                            })
                        }
                        else{
                            startActivity(
                                Intent(requireContext(), DoctorTrackingActivity::class.java)
                                    .putExtra(
                                        Constants.IntentKeys.PROVIDERID,
                                        data!![position].providerId
                                    )
                                    .putExtra(Constants.IntentKeys.BOOKINGID, data!![position].bookingId)
                            )
                           /* if (data!![position].providerId!="null"){

                                Log.d("dhbchb","kanikashanmugam")
                                startActivity(
                                    Intent(requireContext(), DoctorTrackingActivity::class.java)
                                        .putExtra(
                                            Constants.IntentKeys.PROVIDERID,
                                            data!![position].providerId
                                        )
                                        .putExtra(Constants.IntentKeys.BOOKINGID, data!![position].bookingId)
                                )
                              //  UiUtils.showSnack()

                                Log.d("vshgxvdhxsbdxyuhswdxg","dsghwvdshwv")
                            }
                            else{
                                DialogUtils.showAlertDialog(requireContext(),
                                    getString(R.string.cancel_message),
                                    getString(R.string.confirm),
                                    getString(R.string.ok),
                                    getString(R.string.cancel),
                                    object : DialogCallBack {
                                        override fun onPositiveClick() {
                                            DialogUtils.showLoader(requireContext())
                                            bookingViewModel?.cancelBooking(
                                                requireContext(),
                                                data!![position].bookingId!!
                                            )
                                                ?.observe(viewLifecycleOwner,
                                                    Observer {
                                                        DialogUtils.dismissLoader()
                                                        getConsultation()
                                                    })
                                        }

                                        override fun onNegativeClick() {

                                        }

                                    })
                            }*/

                        }
                    }

                }
            }

        }, object : PatientActionListener {
            override fun onChatClicked(position: Int) {

                if(data!![position].is_ins.equals("1") && data!![position].status!! == "pending"){
                    DialogUtils.showLoader(requireContext())
                    viewmodel?.getInsuranceStatus(requireContext(), data!![position].bookingId!!)?.observe(viewLifecycleOwner, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    it.data?.let { data1 ->
                                        if(data1[0].ins_status.equals("pending",true)){
                                            UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                        }
                                        else if(data1[0].ins_status.equals("rejected",true)){
                                            UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                        }
                                        else if(data1[0].ins_status.equals("accepted",true)){
                                            if(data1[0].isPendingAmount.equals("yes",true)){
                                                val balance =  data1[0].balance!!.toDouble()
                                                DialogUtils.showAlertDialogPayment(requireContext(),
                                                    getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                               getString(R.string.insurance),
                                                    getString(R.string.ok),
                                                    getString(R.string.cancel),
                                                    object : DialogCallBack {
                                                        override fun onPositiveClick() {

                                                            if (data1[0].balance.equals("0")){
                                                                showSuccessDialog()
                                                            }else{
                                                                startActivity(
                                                                    Intent(
                                                                        requireActivity(),
                                                                        VirtualPaymentActivity::class.java
                                                                    )
                                                                        .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                        .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                        .putExtra("bookingid",data!![position].bookingId)
                                                                )
                                                            }
                                                          /*  startActivity(
                                                                Intent(
                                                                    requireActivity(),
                                                                    VirtualPaymentActivity::class.java
                                                                )
                                                                    .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                    .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                    .putExtra("bookingid", data!![position].bookingId)
                                                            )*/
                                                        }

                                                        override fun onNegativeClick() {

                                                        }

                                                    })
                                            }
                                            else {
                                                if (data!![position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {

                                                    Log.d("shbxbnjasxhn11",""+Constants.IntentKeys.DOCTORSID+""+data!![position].providerId)
                                                    startActivity(
                                                        Intent(requireContext(), ChatActivity::class.java)
                                                            .putExtra(Constants.IntentKeys.DOCTORSID, data!![position].providerId)
                                                            .putExtra(Constants.IntentKeys.BOOKINGID, data!![position].bookingId.toString())
                                                            .putExtra(
                                                                Constants.IntentKeys.DOCTORNAME,
                                                                data!![position].providerName
                                                            )
                                                            .putExtra(
                                                                Constants.IntentKeys.DOCTORIMAGE,
                                                                data!![position].profilePic
                                                            )
                                                    )
                                                }
                                                else if (data!![position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                                                    DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                                                        override fun singleTap() {

                                                        }
                                                    }, requireContext().getString(R.string.wait_alert))
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
                else{
                    if (data!![position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {

                        Log.d("shbxbnjasxhn12",""+Constants.IntentKeys.DOCTORSID+""+data!![position].providerId)

                        startActivity(
                        Intent(requireContext(), ChatActivity::class.java)
                            .putExtra(Constants.IntentKeys.DOCTORSID, data!![position].providerId)
                            .putExtra(Constants.IntentKeys.BOOKINGID, data!![position].bookingId.toString())
                            .putExtra(
                                Constants.IntentKeys.DOCTORNAME,
                                data!![position].providerName
                            )
                            .putExtra(
                                Constants.IntentKeys.DOCTORIMAGE,
                                data!![position].profilePic
                            )
                    )
                }
                    else if (data!![position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(
                            data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                    DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                        override fun singleTap() {

                        }
                    }, requireContext().getString(R.string.wait_alert))
                }
                }
            }

            override fun onCallClicked(position: Int) {

                Log.d("kanika","log1")
                Log.d("kanika","log1"+""+position)
                Log.d("kanika","log1"+""+data!![position].is_ins)
                if(data!![position].is_ins.equals("1") && data!![position].status!! == "pending"){
                    DialogUtils.showLoader(requireContext())
                    viewmodel?.getInsuranceStatus(requireContext(), data!![position].bookingId!!)?.observe(viewLifecycleOwner, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    it.data?.let { data1 ->
                                          Log.d("kanika","log2")
                                          Log.d("kanika","log2"+data1[0].ins_status)
                                        if(data1[0].ins_status.equals("pending",true)){
                                            UiUtils.showSnack(binding!!.root,getString(R.string.ivbhaip))
                                        }
                                        else if(data1[0].ins_status.equals("rejected",true)){
                                            UiUtils.showSnack(binding!!.root,""+data1[0].message.toString())
                                        }
                                        else if(data1[0].ins_status.equals("accepted",true)){
                                            if(data1[0].isPendingAmount.equals("yes",true)){
                                                val balance =  data1[0].balance!!.toDouble()
                                                DialogUtils.showAlertDialogPayment(requireContext(),
                                                    getString(R.string.yiailyspfba)+" "+balance+" SAR",                                                               getString(R.string.insurance),
                                                    getString(R.string.ok),
                                                    getString(R.string.cancel),
                                                    object : DialogCallBack {
                                                        override fun onPositiveClick() {


                                                            if (data1[0].balance.equals("0")){
                                                                showSuccessDialog()
                                                            }else{
                                                                startActivity(
                                                                    Intent(
                                                                        requireActivity(),
                                                                        VirtualPaymentActivity::class.java
                                                                    )
                                                                        .putExtra(Constants.IntentKeys.AMOUNT, balance.toString())
                                                                        .putExtra(Constants.IntentKeys.BOOKINGTYPE, 5)
                                                                        .putExtra("bookingid",data!![position].bookingId)
                                                                )
                                                            }

                                                        }

                                                        override fun onNegativeClick() {

                                                        }

                                                    })
                                            }
                                            else {
                                                Log.d("kanika","log3")

                                                if (data!![position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                                                    clickedPosition = position
                                                    if (data!![position].stream == "0") {
                                                        requestPermissions(
                                                            Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                                                            Constants.Permission.AUDIO_CALL_PERMISSION
                                                        )
                                                    } else if (data!![position].stream == "1") {
                                                        requestPermissions(
                                                            Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                                            Constants.Permission.VIDEO_CALL_PERMISSION
                                                        )
                                                    }
                                                }
                                                else if (data!![position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                                                    DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                                                        override fun singleTap() {

                                                        }

                                                    }, requireContext().getString(R.string.wait_alert))
                                                }
                                            }
                                        }
                                        else if(data1[0].ins_status.equals("paid",true) && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!) ){
                                            clickedPosition = position
                                            if (data!![position].stream == "0") {
                                                requestPermissions(
                                                    Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                                                    Constants.Permission.AUDIO_CALL_PERMISSION
                                                )
                                            } else if (data!![position].stream == "1") {
                                                requestPermissions(
                                                    Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                                                    Constants.Permission.VIDEO_CALL_PERMISSION
                                                )
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    })
                }
                else{
                    Log.d("kanika","log4")

                    if (data!![position].status!! == "pending" && BaseUtils.isConsultationTime(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)){
                    clickedPosition = position
                    if (data!![position].stream == "0") {
                        requestPermissions(
                            Constants.Permission.AUDIO_CALL_PERMISSION_LIST,
                            Constants.Permission.AUDIO_CALL_PERMISSION
                        )
                    } else if (data!![position].stream == "1") {
                        requestPermissions(
                            Constants.Permission.VIDEO_CALL_PERMISSION_LIST,
                            Constants.Permission.VIDEO_CALL_PERMISSION
                        )
                    }
                }
                    else if (data!![position].status!! == "pending" && !BaseUtils.isConsultationTimeOver(BaseUtils.getFormatedDateUtc(data!![position].bookingDate, "yyyy-MM-dd HH:mm", "dd/MM/yyyy hh:mm a")!!, "dd/MM/yyyy hh:mm a", sharedHelper?.chatTiming!!)) {
                    DialogUtils.showAlert(requireContext(), object : SingleTapListener {
                        override fun singleTap() {

                        }

                    }, requireContext().getString(R.string.wait_alert))
                }
                }
            }

        }, object : OnClickListener {

            override fun onClickItem(position: Int) {
                Log.d("kanika","log6")

                DialogUtils.showLoader(requireContext())
                bookingViewModel?.cancelBooking(requireContext(), data!![position].bookingId!!)
                    ?.observe(viewLifecycleOwner,
                        Observer {
                            DialogUtils.dismissLoader()
                            getConsultation()
                        })
            }

        })

        binding!!.consultationList.layoutManager = LinearLayoutManager(requireContext())
        binding!!.consultationList.adapter = adapter


    }

    private fun listernNotification() {

        notificationEventListner =
            RxBusNotification.listen(String::class.java).doOnError {
            }.subscribe {
                runOnUiThread {
                    if (isAttached)
                        getConsultation()
                }
            }
    }

    override fun onResume() {
        super.onResume()
        isAttached = true
        getConsultation()
    }

    override fun onPause() {
        super.onPause()
        isAttached = false
    }


    private fun getConsultation() {

        DialogUtils.showLoader(requireContext())
        viewmodel?.getConsultation(requireContext(), 1)?.observe(viewLifecycleOwner, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root, msg)
                        }
                    } else {
                        it.data?.let { data1 ->
                           // handleResponse(data)
                              data!!.clear()
                            for (i in 0 until data1.size){
                                if(data1[i].status=="pending"||data1[i].status=="startvisit"){
                                    Log.d("hxdeuesuue","true")
                                    Log.d("hxdeuesuue","true"+data1[i].status)
                                    Log.d("hxdeuesuue","true"+ sharedHelper?.chatTiming!!)
                                      var timee:String=""
                                    data1[i].bookingDate!!.let {
                                        data1[i].bookingTime?.let { time ->
                                            timee = BaseUtils.addEndTime(
                                                "${BaseUtils.getFormatedDateUtc(
                                                    it,
                                                    "yyyy-MM-dd HH:mm",
                                                    "dd/MM/yyyy hh:mm a"
                                                )}",
                                                "dd/MM/yyyy hh:mm a",
                                                "dd/MM/yyyy hh:mm a",
                                                sharedHelper?.chatTiming!!
                                            ).toString()
                                        }
                                    }
                                    Log.d("azsaza","QASQQQQ"+timee)

                                    if (data1!![i].booking_type=="virtual"||data1!![i].booking_type=="global"){
                                        if (BaseUtils.isConsultationTimeOver(
                                                BaseUtils.getFormatedDateUtc(
                                                    data1[i].bookingDate!!,
                                                    "yyyy-MM-dd HH:mm",
                                                    "dd/MM/yyyy hh:mm a"
                                                )!!, "dd/MM/yyyy hh:mm a"
                                                , sharedHelper?.chatTiming!!
                                            )
                                        ){
                                            Log.d("virtualbookingidd",""+data1[i].bookingId)
                                        }
                                        else{
                                           /* if (data1[i].previousIssue==null){
                                                data1[i].previousIssue=""

                                            }*/


                                            data!!.add(
                                                CurrentBookingListJson(
                                                    data1[i].bookingId!!,
                                                    data1[i].bookingIds!!,
                                                    data1[i].isVirtualBooking!!,
                                                    data1[i].profilePic!!,
                                                    data1[i].providerName!!,
                                                    data1[i].providerId!!,
                                                    data1[i].expierence!!,
                                                    data1[i].previousIssue!!,
                                                    data1[i].fee!!,
                                                    data1[i].bookingDate!!,
                                                    data1[i].bookingTime!!,
                                                    data1[i].booking_type!!,
                                                    data1[i].chat!!,
                                                    data1[i].status!!,
                                                    data1[i].specialityName!!,
                                                    data1[i].is_ins!!,
                                                    data1[i].stream!!,
                                                    data1[i].isFeatureBooking!!,
                                                    data1[i].bookingPeriod!!,
                                                )
                                            )
                                        }
                                    }
                                    else{
                                        if (data1[i].previousIssue==null){
                                            data1[i].previousIssue=""
                                            if (data1[i].profilePic==null){
                                                data1[i].profilePic=""

                                                if (data1[i].providerName==null){
                                                    data1[i].providerName=""
                                                    data1[i].expierence=""
                                                    if (data1[i].providerId==null){
                                                        data1[i].providerId=0
                                                        data1[i].fee=""

                                                        data!!.add(
                                                            CurrentBookingListJson(
                                                                data1[i].bookingId!!,
                                                                data1[i].bookingIds!!,
                                                                data1[i].isVirtualBooking!!,
                                                                data1[i].profilePic!!,
                                                                data1[i].providerName!!,
                                                                data1[i].providerId!!,
                                                                data1[i].expierence!!,
                                                                data1[i].previousIssue!!,
                                                                data1[i].fee!!,
                                                                data1[i].bookingDate!!,
                                                                data1[i].bookingTime!!,
                                                                data1[i].booking_type!!,
                                                                data1[i].chat!!,
                                                                data1[i].status!!,
                                                                data1[i].specialityName!!,
                                                                data1[i].is_ins!!,
                                                                data1[i].stream!!,
                                                                data1[i].isFeatureBooking!!,
                                                                data1[i].bookingPeriod!!,
                                                            )
                                                        )
                                                    }else{
                                                        data!!.add(
                                                            CurrentBookingListJson(
                                                                data1[i].bookingId!!,
                                                                data1[i].bookingIds!!,
                                                                data1[i].isVirtualBooking!!,
                                                                data1[i].profilePic!!,
                                                                data1[i].providerName!!,
                                                                data1[i].providerId!!,
                                                                data1[i].expierence!!,
                                                                data1[i].previousIssue!!,
                                                                data1[i].fee!!,
                                                                data1[i].bookingDate!!,
                                                                data1[i].bookingTime!!,
                                                                data1[i].booking_type!!,
                                                                data1[i].chat!!,
                                                                data1[i].status!!,
                                                                data1[i].specialityName!!,
                                                                data1[i].is_ins!!,
                                                                data1[i].stream!!,
                                                                data1[i].isFeatureBooking!!,
                                                                data1[i].bookingPeriod!!,
                                                            )
                                                        )
                                                    }
                                                }else{
                                                   Log.d("jkjkjkkjkj","log1")
                                                }


                                            }else{
                                                Log.d("jkjkjkkjkj","log11")

                                            }

                                            Log.d("dfxcgfc",""+data1[i].providerId)
                                            Log.d("dfxcgfc",""+data1[i].providerId)



                                        }
                                        else{
                                            if (data1[i].profilePic==null){
                                                data1[i].profilePic=""
                                                data1[i].expierence=""

                                                if (data1[i].providerId==null){
                                                    data1[i].providerId=0

                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].providerId!!,
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].chat!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].stream!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                        )
                                                    )
                                                }else{
                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].providerId!!,
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].chat!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].stream!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                        )
                                                    )
                                                }

                                            }else{
                                                Log.d("jkjkjkkjkj","log12")
                                                data!!.add(
                                                    CurrentBookingListJson(
                                                        data1[i].bookingId!!,
                                                        data1[i].bookingIds!!,
                                                        data1[i].isVirtualBooking!!,
                                                        data1[i].profilePic!!,
                                                        data1[i].providerName!!,
                                                        data1[i].providerId!!,
                                                        data1[i].expierence!!,
                                                        data1[i].previousIssue!!,
                                                        data1[i].fee!!,
                                                        data1[i].bookingDate!!,
                                                        data1[i].bookingTime!!,
                                                        data1[i].booking_type!!,
                                                        data1[i].chat!!,
                                                        data1[i].status!!,
                                                        data1[i].specialityName!!,
                                                        data1[i].is_ins!!,
                                                        data1[i].stream!!,
                                                        data1[i].isFeatureBooking!!,
                                                        data1[i].bookingPeriod!!,
                                                    )
                                                )
                                            }

                                        }


                                    }

                            /*        if (BaseUtils.isConsultationTimeOver(
                                            BaseUtils.getFormatedDateUtc(
                                                data1[i].bookingDate!!,
                                                "yyyy-MM-dd HH:mm",
                                                "dd/MM/yyyy hh:mm a"
                                            )!!, "dd/MM/yyyy hh:mm a"
                                            , sharedHelper?.chatTiming!!
                                        )
                                    ){

                                        if (data1[i].booking_type=="emergency"){
                                            Log.d("emergencybooking","swhdgxbshydg")
                                            Log.d("emergencybooking","swhdgxbshydg"+data1[i])
                                            Log.d("emergencybooking","swhdgxbshydg"+data1[i].providerName)


                                             if (data1[i].providerName==null&&data1[i].profilePic==null&&data1[i].previousIssue==null&&data1[i].education==null&&data1[i].expierence==null&&data1[i].previousIssue==null){
                                                 data1[i].providerName=""
                                                 data1[i].profilePic=""
                                                 data1[i].previousIssue=""
                                                 data1[i].education=""
                                                 data1[i].expierence=""
                                                 data!!.add(
                                                     CurrentBookingListJson(
                                                         data1[i].bookingId!!,
                                                         data1[i].bookingIds!!,
                                                         data1[i].isVirtualBooking!!,
                                                         data1[i].profilePic!!,
                                                         data1[i].providerName!!,
                                                         data1[i].providerId.toString(),
                                                         data1[i].expierence!!,
                                                         data1[i].previousIssue!!,
                                                         data1[i].fee!!,
                                                         data1[i].bookingDate!!,
                                                         data1[i].bookingTime!!,
                                                         data1[i].booking_type!!,
                                                         data1[i].chat!!,
                                                         data1[i].status!!,
                                                         data1[i].specialityName!!,
                                                         data1[i].is_ins!!,
                                                         data1[i].stream!!,
                                                         data1[i].isFeatureBooking!!,
                                                         data1[i].bookingPeriod!!,
                                                     )
                                                 )
                                             }else{
                                                if (data1[i].previousIssue==null){
                                                    data1[i].previousIssue=""
                                                    data!!.add(
                                                        CurrentBookingListJson(
                                                            data1[i].bookingId!!,
                                                            data1[i].bookingIds!!,
                                                            data1[i].isVirtualBooking!!,
                                                            data1[i].profilePic!!,
                                                            data1[i].providerName!!,
                                                            data1[i].providerId.toString(),
                                                            data1[i].expierence!!,
                                                            data1[i].previousIssue!!,
                                                            data1[i].fee!!,
                                                            data1[i].bookingDate!!,
                                                            data1[i].bookingTime!!,
                                                            data1[i].booking_type!!,
                                                            data1[i].chat!!,
                                                            data1[i].status!!,
                                                            data1[i].specialityName!!,
                                                            data1[i].is_ins!!,
                                                            data1[i].stream!!,
                                                            data1[i].isFeatureBooking!!,
                                                            data1[i].bookingPeriod!!,
                                                        )
                                                    )
                                                }

                                             }
                                         *//*   if (data1[i].bookingId==null){
                                                data1[i].bookingId=0

                                            }else if(data1[i].bookingIds==null){
                                                data1[i].bookingIds=""

                                            }else if(data1[i].isVirtualBooking==null){
                                                data1[i].isVirtualBooking=0

                                            }else if(data1[i].profilePic==null){
                                                data1[i].profilePic=""
                                            }
                                            else if(data1[i].providerName==null){

                                                Log.d("sdxsdxdcse","swhdgxbshydg")
                                                data1[i].providerName=""

                                            }else if(data1[i].providerId==null){
                                                data1[i].providerId=0

                                            }else if(data1[i].expierence==null){
                                                data1[i].expierence=""

                                            }else if(data1[i].previousIssue==null){
                                                data1[i].previousIssue=""

                                            }else if(data1[i].fee==null){
                                                data1[i].fee=""

                                            }else if(data1[i].bookingDate==null){
                                                data1[i].bookingDate=""

                                            }else if(data1[i].bookingTime==null){
                                                data1[i].bookingTime=""

                                            }else if(data1[i].booking_type==null){
                                                data1[i].booking_type=""

                                            }else if(data1[i].chat==null){
                                                data1[i].chat=""

                                            }else if(data1[i].status==null){
                                                data1[i].status=""

                                            }else if(data1[i].specialityName==null){
                                                data1[i].specialityName=""

                                            }else if(data1[i].is_ins==null){
                                                data1[i].is_ins=""

                                            }else if(data1[i].stream==null){
                                                data1[i].stream=""

                                            }else if(data1[i].isFeatureBooking==null){
                                                data1[i].isFeatureBooking=""

                                            }else if(data1[i].bookingPeriod==null){
                                                data1[i].bookingPeriod=""

                                            }else{



                                                this.data = data

                                                Log.d("hxdeuesuue","true"+ data!!.size)
                                                Log.d("hxdeuesuue","true"+ data)
                                                handleResponse(data!!)
                                            }
*//*

                                        }
                                        Log.d("swhgbxywhxg","whsuitrueeeee")
                                    }
                                    else if (data1[i].booking_type=="home_visit"){



                                        this.data = data

                                        Log.d("hxdeuesuue","true"+ data!!.size)
                                        Log.d("hxdeuesuue","true"+ data)
                                    }*/

                                }

                                handleResponse(data!!)
                            }






                        }


                    }
                }
            }
        })

    }

    private fun handleResponse(data1: ArrayList<CurrentBookingListJson>) {


/*
        for (i in 0 until data1.size){
            if(data1[i].status=="pending"){
                Log.d("hxdeuesuue","true")
                Log.d("hxdeuesuue","true"+data1[i].status)
                data!!.add(
                    CurrentBookingListJson(
                        data1[i].bookingId!!,
                        data1[i].bookingIds!!,
                        data1[i].isVirtualBooking!!,
                        data1[i].profilePic!!,
                        data1[i].providerName!!,
                        data1[i].providerId.toString(),
                        data1[i].expierence!!,
                        data1[i].previousIssue!!,
                        data1[i].fee!!,
                        data1[i].bookingDate!!,
                        data1[i].bookingTime!!,
                        data1[i].booking_type!!,
                        data1[i].chat!!,
                        data1[i].status!!,
                        data1[i].specialityName!!,
                        data1[i].is_ins!!,
                        data1[i].stream!!,
                        data1[i].isFeatureBooking!!,
                        data1[i].bookingPeriod!!,
                )
                )

                this.data = data

                Log.d("hxdeuesuue","true"+ list2!!.size)
            }
        }
*/
                   this.data=data1
//        adapter?.notifyDataSetChanged()
        adapter?.notifyDataChangedAdapter(data!!)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.Permission.AUDIO_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                createCall(Constants.ChatTypes.VOICE_CALL)

            }

        } else if (requestCode == Constants.Permission.VIDEO_CALL_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.d("kanikashanmugam","kanikashnauag")

                createCall(Constants.ChatTypes.VIDEO_CALL)

            }
        }
    }


    private fun createCall(callType: String) {
          Log.d("dgbvsdgvbhdsghdghsgdh","dsyuewyudgdyuedgyud")
        var intent = Intent(context, VideoCallActivity::class.java)
            .putExtra(Constants.NotificationIntentValues.CALL_TYPE, callType)
            .putExtra(
                Constants.NotificationIntentValues.BOOKINGID,
                data!![clickedPosition].bookingId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.CALL_FROM,
                Constants.ChatTypes.OUTGOING_CALL
            )
            .putExtra(
                Constants.NotificationIntentValues.CHANNEL_ID,
                data!![clickedPosition].providerId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.TO_ID,
                data!![clickedPosition].providerId.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.FROM_ID,
                sharedHelper?.id.toString()
            )
            .putExtra(
                Constants.NotificationIntentValues.ID,
                data!![clickedPosition].providerId.toString()
            )
            .putExtra(Constants.NotificationIntentValues.NAME, data!![clickedPosition].providerName)
            .putExtra(Constants.NotificationIntentValues.IMAGE, data!![clickedPosition].profilePic)
            .putExtra(
                Constants.NotificationIntentValues.BOOKINGDATETIME,
                data!![clickedPosition].bookingDate
            )

//        startActivity(intent)

        callViewModel?.createCall(
            data!![clickedPosition].providerId.toString(),
            data!![clickedPosition].providerId.toString(),
            callType,
            sharedHelper?.id.toString()
        )?.observe(viewLifecycleOwner, Observer {

            if (it.error == "true") {
                UiUtils.showSnack(binding!!.root, it.message)
            } else if (it.error == "false") {
                NotificationUtils(requireContext()).notificationHangupCall(
                    data!![clickedPosition].providerName!!,
                    "outgoing $callType call"
                )
//                intent.putExtra(Constants.NotificationIntentValues.ID, it.data?.id)
                startActivity(intent)
            }
        })

    }


    private fun showSuccessDialog() {

        DialogUtils.showSuccessDialog(requireContext(), object : SingleTapListener {
            override fun singleTap() {

                val intent = Intent(requireContext(), DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
        })
    }

}