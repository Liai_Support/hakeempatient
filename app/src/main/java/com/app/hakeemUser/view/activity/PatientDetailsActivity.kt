package com.app.hakeemUser.view.activity

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityPatientDetails2Binding
import com.app.hakeemUser.databinding.ActivityPatientDetailsBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.utils.BaseUtils
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.MembersViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import java.util.*
import kotlin.collections.ArrayList
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.view.adapter.SelectPatientAdapter
import com.app.hakeemUser.view.adapter.SelectPatientAdapter1

class PatientDetailsActivity : BaseActivity() {

    var binding: ActivityPatientDetails2Binding? = null
    var sharedHelper: SharedHelper? = null
    var membersViewModel: MembersViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPatientDetails2Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_patient_details2)
        sharedHelper = SharedHelper(this)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)
        getmemeberlist()

        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

    }

    override fun onResume() {
        super.onResume()
        getmemeberlist()
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    public fun getmemeberlist(){
        DialogUtils.showLoader(this)
        membersViewModel?.getmemberDetails(this)
            ?.observe(this, Observer{
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                for (item in data){
                                    if(item.type.equals("myself",true)){
                                        sharedHelper!!.isMyself = true
                                    }
                                }
                                binding!!.recycler.layoutManager = LinearLayoutManager(this)
                                binding!!.recycler.adapter = SelectPatientAdapter1(this,this, data)
                            }
                        }
                    }

                }
            })
    }

    private fun showSnack(message: String) {
        UiUtils.showSnack(binding!!.root, message)
    }

}