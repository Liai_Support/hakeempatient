package com.app.hakeemUser.view.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.MembersViewModel
import com.app.hakeemUser.viewmodel.RegisterViewModel
import java.util.*
import androidx.lifecycle.Observer
import com.app.hakeemUser.databinding.ActivityPatientDetailsBinding
import com.app.hakeemUser.models.MemberData
import com.app.hakeemUser.utils.BaseUtils
import kotlin.collections.ArrayList


class AddPatientDetailsActivity : BaseActivity() {

    var binding: ActivityPatientDetailsBinding? = null
    var selectedSpeciality: Int? = null
    var ageDif: Int = 0
    var sharedHelper: SharedHelper? = null
    private var viewmodel: RegisterViewModel? = null

    var classtype: String? = null
    var classid: String? = "0"
    var icname: String? = null
    var icnameid: String? = "0"

    /* var cd1 : MutableList<String> = mutableListOf<String>()
     var cd2 : MutableList<Int> = mutableListOf<Int>()
     var cd3 : MutableList<String> = mutableListOf<String>()
     var cd4 : MutableList<Int> = mutableListOf<Int>()
 */
    var cd1 : ArrayList<String> = ArrayList()
    var cd2 : ArrayList<Int> = ArrayList()
    var cd3 : ArrayList<String> = ArrayList()
    var cd4 : ArrayList<Int> = ArrayList()

    private var membersViewModel: MembersViewModel? = null
    var memberId = ""
    var check = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPatientDetailsBinding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_patient_details)
        sharedHelper = SharedHelper(this)
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        membersViewModel = ViewModelProvider(this).get(MembersViewModel::class.java)
        //getClassData()
        //geticnamedata()
        initListener()
        getIntentValues()
        if (sharedHelper!!.language == "ar"){
            binding!!.imageView14.rotation= 180F
        }

        if(sharedHelper!!.isMyself == true){
            binding!!.myself.visibility = View.GONE
            binding!!.textView11.visibility = View.GONE

        }
    }

    private fun getIntentValues() {
        intent.extras?.let {
            if(it.getBoolean("editmember") == true){
                binding!!.confirm.text = getString(R.string.update)
                intent.extras?.let {
                    it.getParcelable<MemberData>("memberdata")?.let { data ->
                        memberId = data.memberId.toString()
                        if(data.type.equals("Myself",true)){
                            binding!!.myself.isChecked = true
                        }
                        else if(data.type.equals("Family",true)){
                            binding!!.family.isChecked = true
                        }
                        else if(data.type.equals("Friends",true)){
                            binding!!.friends.isChecked = true
                        }
                        else if(data.type.equals("Others",true)){
                            binding!!.friends.isChecked = true
                        }

                        binding!!.age.setText(BaseUtils.getFormatedDate(data.dob.toString(), "yyyy-MM-dd","yyyy-MM-dd"))
                        if(data.age == null){
                            ageDif = 0
                        }
                        else{
                            ageDif = data.age!!.toInt()
                        }
                        binding!!.bloodGroup1.setText(data.bloodgroup)
                        binding!!.edtMob.setText(data.mobile.toString())
                        binding!!.edtMail.setText(data.email.toString())
                        binding!!.edtSauthi.setText(data.sauthiId.toString())
                        binding!!.name.setText(data.name.toString())
                        binding!!.insurance.setText(data.insuranceid.toString())

                        /*for ((index, value) in sharedHelper!!.insuranceclassid.withIndex()) {
                            Log.d("xgxcbv",""+value+"=="+data.insuranceclass)
                            if(value.toString().equals(data.insuranceclass)){
                                binding!!.spinner.setSelection(index)
                            }
                        }*/

                        for ((index, value) in sharedHelper!!.insurancecompanyid.withIndex()) {
                            if(value.toString().equals(data.insurancecompany)){
                                binding!!.spinnerIcname.setSelection(index)
                                getClassData(sharedHelper!!.insurancecompanyid[index].toString(),data.insuranceclass!!.toInt())
                            }
                        }

                        if(data.gender.equals("male",true)){
                            binding!!.maleRadio.isChecked = true
                        }
                        else if (data.gender.equals("female", true)){
                            binding!!.femaleRadio.isChecked = true
                        }
                    }
                }
                if(it.getBoolean("myselfedit") == true){
                    binding!!.textView1.visibility = View.GONE
                    binding!!.radioGroup.visibility = View.GONE
                    binding!!.edtMail.isEnabled = false
                    binding!!.edtMob.isEnabled = false
                }
                else{
                    if(sharedHelper!!.isMyself == true){
                        binding!!.myself.visibility = View.GONE
                    }
                }
            }
            else{
                if(sharedHelper!!.isMyself == true){
                    binding!!.myself.visibility = View.GONE
                }
            }
        }

    }

    private fun getClassData(icompanyid: String,iclassid: Int) {
        DialogUtils.showLoader(this)
        viewmodel?.getclassdetails(this,icompanyid)
            ?.observe(this, Observer {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->
                                UiUtils.showSnack(binding!!.root, msg)
                            }
                        } else {
                            it.data?.let { data ->
                                data.classDetails?.let { list ->
                                    if (list.size != 0) {
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        for ((index, value) in list.withIndex()) {
                                            println("the element at $index is $value")
                                            value.name?.let { it1 -> cd1.add(index + 1, it1) }
                                            value.id?.let { it2 -> cd2.add(index + 1, it2) }
                                        }

                                        val spinner = findViewById<Spinner>(R.id.spinner)
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                android.R.layout.simple_spinner_item, cd1)
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position].toString()

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }

                                            for ((index, value) in cd2.withIndex()) {
                                                if(cd2[index] == iclassid){
                                                    spinner.setSelection(index)
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        cd1.clear()
                                        cd2.clear()
                                        cd1.add(0,getString(R.string.cic)+" "+getString(R.string.optional))
                                        cd2.add(0,0)
                                        val spinner = findViewById<Spinner>(R.id.spinner)
                                        if (spinner != null) {
                                            val adapter = ArrayAdapter(this,
                                                android.R.layout.simple_spinner_item, cd1)
                                            spinner.adapter = adapter
                                            spinner.onItemSelectedListener = object :
                                                AdapterView.OnItemSelectedListener {
                                                override fun onItemSelected(parent: AdapterView<*>,
                                                                            view: View, position: Int, id: Long) {
                                                    classtype = cd1[position]
                                                    classid = cd2[position].toString()

                                                }

                                                override fun onNothingSelected(parent: AdapterView<*>) {
                                                    // write code to perform some action
                                                }
                                            }

                                            for ((index, value) in cd2.withIndex()) {
                                                if(cd2[index] == iclassid){
                                                    spinner.setSelection(index)
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }

                }
            })
    }


    private fun initListener() {

        binding!!.bloodGroup1.setOnClickListener {
            showBloodGrpDialog()
        }

        binding!!.age.setOnClickListener {
            selectDob()
        }

        binding!!.maleRadio.setOnClickListener {
            binding!!. maleRadio.isChecked = true
            binding!!.femaleRadio.isChecked = false
        }
        binding!!.femaleRadio.setOnClickListener {
            binding!!.femaleRadio.isChecked = true
            binding!!.maleRadio.isChecked = false
        }

        //val classes = resources.getStringArray(R.array.Classes)

        // access the spinner
        val spinner1 = findViewById<Spinner>(R.id.spinner_icname)
        if (spinner1 != null) {
            val adapter = ArrayAdapter(this,
                android.R.layout.simple_spinner_item, sharedHelper!!.insurancecompanyname)
            spinner1.adapter = adapter
            spinner1.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    icname = sharedHelper!!.insurancecompanyname[position]
                    icnameid = sharedHelper!!.insurancecompanyid[position].toString()
                    if(check > 0){
                        getClassData(icnameid!!,0)
                    }
                    else{
                        check++
                       // getClassData(icnameid!!,0)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }

    }



    private fun selectDob() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                binding!!.age.setText("$year-${monthOfYear + 1}-$dayOfMonth")
                ageDif = BaseUtils.getAge(year, monthOfYear, dayOfMonth)!!.toInt()
            },
            year,
            month,
            day
        )

        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    private fun showBloodGrpDialog() {

        // var list = arrayListOf("A+ve", "A-ve", "B+ve", "B-ve", "AB+ve", "AB-ve", "O+ve", "O-ve")
        var list=  resources.getStringArray(R.array.BloodGroup)
        DialogUtils.showListDialog(this, list, getString(R.string.choosebloodgroup), object :
            OnClickListener {
            override fun onClickItem(position: Int) {
                binding!!.bloodGroup1.setText(list[position])
            }

        })

    }


    fun onConfirmClicked(view: View) {
        if(binding!!.confirm.text.equals(getString(R.string.update))){
            if (validInputs() && isInsuranceValid()) {
                DialogUtils.showLoader(this)
                membersViewModel?.updatememberDetails(
                    this,
                    memberId,
                    getSelectedPatientType(),
                    binding!!.name.text.toString(),
                    binding!!.age.text.toString(),
                    binding!!.bloodGroup1.text.toString(),
                    binding!!.insurance.text.toString(),
                    getgender(),
                    ageDif,
                    classid.toString().trim(),
                    icnameid.toString().trim(),
                    binding!!.edtMail.text.toString().trim(),
                    binding!!.edtMob.text.toString().trim(),
                    binding!!.edtSauthi.text.toString().trim()
                )
                    ?.observe(this, {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                }
                                else {
                                    it.message?.let { it1 ->
                                        UiUtils.showSnack(binding!!.root,
                                            it1
                                        )
                                    }
                                    finish()
                                }
                            }

                        }
                    })
            }
        }
        else {
            if (validInputs() && isInsuranceValid()) {
                /*  var details = PatientDetails()
            details.patientType = getSelectedPatientType()
            details.name = name.text.toString()
            details.dob = age.text.toString()
            details.bloodGroup = bloodGroup1.text.toString()
            details.insurance = insurance.text.toString()
            details.gender = getgender()
            details.age = ageDif
            //details.memberid = memberid

            var list = SharedHelper(this).patientList
            list.add(details)
            SharedHelper(this).patientList = list
            finish()*/


                DialogUtils.showLoader(this)
                membersViewModel?.addmemberDetails(
                    this,
                    getSelectedPatientType(),
                    binding!!.name.text.toString(),
                    binding!!.age.text.toString(),
                    binding!!.bloodGroup1.text.toString(),
                    binding!!.insurance.text.toString(),
                    getgender(),
                    ageDif,
                    classid.toString().trim(),
                    icnameid.toString().trim(),
                    binding!!.edtMail.text.toString().trim(),
                    binding!!.edtMob.text.toString().trim(),
                    binding!!.edtSauthi.text.toString().trim()
                )
                    ?.observe(this, {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {
                                    finish()

                                    /*  it.data?.let {
                                            data ->
                                        //var memberid = data
                                      *//*  var details = PatientDetails()
                                        details.patientType = getSelectedPatientType()
                                        details.name = name.text.toString()
                                        details.dob = age.text.toString()
                                        details.bloodGroup = bloodGroup1.text.toString()
                                        details.insurance = insurance.text.toString()
                                        details.gender = getgender()
                                        details.age = ageDif
                                        details.memberid = memberid

                                        var list = SharedHelper(this).patientList
                                        list.add(details)
                                        SharedHelper(this).patientList = list
                                        finish()*//*
                                    }*/
                                }
                            }

                        }
                    })


//                ScheduledBookingSingleton.getInstance().specialityId =
//                    selectedSpeciality?.toString()
//
//                ScheduledBookingSingleton.getInstance().patientType = getSelectedPatientType()
//                ScheduledBookingSingleton.getInstance().patientName = name.text.toString()
//                ScheduledBookingSingleton.getInstance().patientAge = age.text.toString()
//                ScheduledBookingSingleton.getInstance().patientGender = getgender()
//                ScheduledBookingSingleton.getInstance().previousIssue =
//                    previousHealthIssue.text.toString()
//
//
//                startActivity(
//                    Intent(this, AvailableDoctorActivity::class.java)
//                        .putExtra(Constants.IntentKeys.SPECIALITYID, it)
//                )
            }
        }
    }

    fun validInputs(): Boolean {
        when {
            getSelectedPatientType() == "" -> {
                showSnack(getString(R.string.selectpatienttype))
                return false
            }
            binding!!.name.text.toString().trim() == "" -> {
                showSnack(getString(R.string.enteryourname))
                return false
            }
            binding!!.age.text.toString().trim() == "" -> {
                showSnack(getString(R.string.selectyourdateofbirth))
                return false
            }
            binding!!.edtSauthi.text.toString().trim() == "" -> {
                showSnack(getString(R.string.enteryoursaudiid))
                return false
            }
           /* binding!!.bloodGroup1.text.toString().trim() == "" -> {
                showSnack(getString(R.string.selectyourbloodgroup))
                return false
            }*/
            /* binding!!.insurance.text.toString().trim() == "" -> {
                 showSnack(getString(R.string.enteryourinsuranceid))
                 return false
             }
             classid == 0 -> {
                 UiUtils.showSnack(binding!!.root, getString(R.string.selectyourinsuranceclass))
                 return false
             }
             icnameid == 0 -> {
                 UiUtils.showSnack(binding!!.root, getString(R.string.selectyourinsurancecompany))
                 return false
             }*/
            getgender() == "" -> {
                showSnack(getString(R.string.selectyourgender))
                return false
            }

            else -> return true

        }
        //return true
    }

    private fun isInsuranceValid(): Boolean {
        if(binding!!.insurance.text.toString().trim() != "" || !binding!!.insurance.text.toString().trim().equals("") || !classid.equals("0") || !icnameid.equals("0")){
            when {
                binding!!.insurance.text.toString().trim() == "" -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourinsurance))
                    return false
                }
                classid.equals("0") -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsuranceclass))
                    return false
                }
                icnameid.equals("0") -> {
                    UiUtils.showSnack(binding!!.root, getString(R.string.pleasechooseinsurancecompany))
                    return false
                }
                else -> return true
            }
        }
        return true
    }



    fun getSelectedPatientType(): String {

        return when {
            binding!!.myself.isChecked -> {
                "Myself"
            }
            binding!!.family.isChecked -> {
                "Family"
            }
            binding!!.friends.isChecked -> {
                "Friends"
            }
            binding!!.others.isChecked -> {
                "Others"
            }
            else -> ""
        }
    }

    fun getgender(): String {

        return when {
            binding!!.maleRadio.isChecked -> {
                "Male"
            }
            binding!!.femaleRadio.isChecked -> {
                "Female"
            }
            binding!!.otherRadio.isChecked -> {
                "Others"
            }

            else -> ""
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    private fun showSnack(message: String) {
        UiUtils.showSnack(binding!!.root, message)
    }

}