package com.app.hakeemUser.view.activity

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ActivityNotification1Binding
import com.app.hakeemUser.interfaces.DialogCallBack
import com.app.hakeemUser.models.NotificationListData
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.adapter.NotificationAdapter
import com.app.hakeemUser.viewmodel.ConsultationViewModel

class NotificationActivity : BaseActivity() {

    var binding: ActivityNotification1Binding? = null

    var viewModel: ConsultationViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotification1Binding.inflate(layoutInflater)
        val view = binding!!.root
        setContentView(view)
        //setContentView(R.layout.activity_notification1)

        viewModel = ViewModelProvider(this).get(ConsultationViewModel::class.java)

        getNotification()
    }

    private fun getNotification() {
        DialogUtils.showLoader(this)
        viewModel?.getNotificationData(this)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        it.message?.let { msg ->
                            UiUtils.showSnack(binding!!.root1, msg)
                        }
                    } else {
                        it.data?.let { data ->
                            handleResponse(data.data)
                        }
                    }
                }

            }
        })
    }

    private fun handleResponse(data: ArrayList<NotificationListData>?) {

        data?.let {
            if (it.size != 0) {

                var adapter = NotificationAdapter(this, it)
                binding!!.listView.layoutManager = LinearLayoutManager(this)
                binding!!.listView.adapter = adapter
            }
        }
    }

    fun onBackPressed(view: View) {
        onBackPressed()
    }

    fun onClearClicked(view: View) {


        DialogUtils.showAlertDialog(this,
            getString(R.string.clear_notification),
            getString(R.string.confirm),
            getString(R.string.ok),
            getString(R.string.cancel),
            object : DialogCallBack {
                override fun onPositiveClick() {
                    DialogUtils.showLoader(this@NotificationActivity)
                    viewModel?.clearNotification(this@NotificationActivity)
                        ?.observe(this@NotificationActivity, Observer {
                            DialogUtils.dismissLoader()
                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(binding!!.root1, msg)
                                        }
                                    } else {
                                        var adapter =
                                            NotificationAdapter(
                                                this@NotificationActivity,
                                                ArrayList()
                                            )
                                        binding!!.listView.layoutManager =
                                            LinearLayoutManager(this@NotificationActivity)
                                        binding!!.listView.adapter = adapter
                                    }
                                }

                            }
                        })
                }

                override fun onNegativeClick() {

                }

            })

    }

}
