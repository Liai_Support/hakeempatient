package com.app.hakeemUser.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildAutocompleteListBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.models.PredictionAutoCompleted

class AutoCompleteAdapter(
    var context: Context,
    var list: ArrayList<PredictionAutoCompleted>,
    var listener: OnClickListener
) :
    RecyclerView.Adapter<AutoCompleteAdapter.HomeHeaderViewHolder>() {

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildAutocompleteListBinding = ChildAutocompleteListBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_autocomplete_list,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        holder.binding.title.text = list[position].terms?.get(0)?.value
        holder.binding.content.text = list[position].description

        holder.binding.root.setOnClickListener {
            listener.onClickItem(position)
        }

    }

    fun notifyDataSetChangedValues(list: ArrayList<PredictionAutoCompleted>) {
        this.list = list
        notifyDataSetChanged()
    }
}