package com.app.hakeemUser.view.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildAvailableDoctorBinding
import com.app.hakeemUser.models.DoctorData
import com.app.hakeemUser.utils.Constants
import com.app.hakeemUser.utils.ScheduledBookingSingleton
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.view.activity.DoctorProfileActivity
import com.app.hakeemUser.view.activity.SummaryDetailsBookingActivity

class AvailableDoctorAdapter(
    var context: Context
) : PagedListAdapter<DoctorData, AvailableDoctorAdapter.ViewHolder>(DIFF_CALLBACK) {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildAvailableDoctorBinding = ChildAvailableDoctorBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_available_doctor,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data: DoctorData? = getItem(position)



        data?.let { data ->


            //UiUtils.setRating(context, data.averageRating?.toFloat(), holder.binding.rating, 40)

            data.profilePic?.let {
                UiUtils.loadImage(
                    holder.binding.profileImage,
                    it,
                    ContextCompat.getDrawable(context, R.drawable.splash_image)!!
                )
            }

            data.status?.let {
                if (it.equals("online", true))
                    holder.binding.isOnline.visibility =
                        View.VISIBLE else holder.binding.isOnline.visibility = View.GONE
            }

            data.name?.let { holder.binding.doctorName.text = it }
            data.education?.let { holder.binding.qualification.text = it }
            data.expierence?.let { holder.binding.experience.text = it }
            data.averageRating?.let { holder.binding.ratingValue.text = "$it/5" }


            holder.binding.root.setOnClickListener {

                data.providerId?.let {
                    ScheduledBookingSingleton.getInstance().providerId = it.toString()
                }

                data.default_fees?.let {
                    ScheduledBookingSingleton.getInstance().dfee = it
                }

                data.virtual_fees?.let {
                    ScheduledBookingSingleton.getInstance().vfee = it
                }

             /*  data.speciality?.get(0)?.fee?.let {
                    ScheduledBookingSingleton.getInstance().fee = it
                }

               data.speciality?.get(0)?.chatVirtualFee?.let {
                    ScheduledBookingSingleton.getInstance().chatFee = it
                }

               data.speciality?.get(0)?.virtualFee?.let {
                    ScheduledBookingSingleton.getInstance().audioFee = it
                }

               data.speciality?.get(0)?.virtualStreamFee?.let {
                    ScheduledBookingSingleton.getInstance().videoFee = it
                }
*/
                context.startActivity(
                    Intent(
                        context,
                        DoctorProfileActivity::class.java
                    )
                        .putExtra(Constants.IntentKeys.DOCTOR_DATA, data as Parcelable)
                        .putExtra(
                            Constants.IntentKeys.SPECIALITYLIST,
                            data.speciality as ArrayList<Parcelable>
                        )
                        .putExtra(Constants.IntentKeys.REVIEW_LIST, data.review)
                        .putExtra(Constants.IntentKeys.BOOKINGFLOW, true)
                )
            }

            holder.binding.bookNow.setOnClickListener {

                data.providerId?.let {
                    ScheduledBookingSingleton.getInstance().providerId = it.toString()
                }
                data.default_fees?.let {
                    ScheduledBookingSingleton.getInstance().dfee = it
                }

                data.virtual_fees?.let {
                    ScheduledBookingSingleton.getInstance().vfee = it
                }

              /*  data.speciality?.get(0)?.fee?.let {
                    ScheduledBookingSingleton.getInstance().fee = it
                }

                data.speciality?.get(0)?.chatVirtualFee?.let {
                    ScheduledBookingSingleton.getInstance().chatFee = it
                }

                data.speciality?.get(0)?.virtualFee?.let {
                    ScheduledBookingSingleton.getInstance().audioFee = it
                }

                data.speciality?.get(0)?.virtualStreamFee?.let {
                    ScheduledBookingSingleton.getInstance().videoFee = it
                }*/

                context.startActivity(
                    Intent(
                        context,
                        SummaryDetailsBookingActivity::class.java
                    )
                        .putExtra(Constants.IntentKeys.DOCTOR_DATA, data as Parcelable)

                )


//                onClickListener.onClickItem(position)

            }


            holder.binding.viewProfile.setOnClickListener {

                data.providerId?.let {
                    ScheduledBookingSingleton.getInstance().providerId = it.toString()
                }


                data.speciality?.get(0)?.fee?.let {
                    ScheduledBookingSingleton.getInstance().dfee = it
                }

                data.speciality?.get(0)?.chatVirtualFee?.let {
                    ScheduledBookingSingleton.getInstance().chatFee = it
                }

                data.speciality?.get(0)?.virtualFee?.let {
                    ScheduledBookingSingleton.getInstance().audioFee = it
                }

                data.speciality?.get(0)?.virtualStreamFee?.let {
                    ScheduledBookingSingleton.getInstance().videoFee = it
                }

                context.startActivity(
                    Intent(
                        context,
                        DoctorProfileActivity::class.java
                    )
                        .putExtra(Constants.IntentKeys.DOCTOR_DATA, data as Parcelable)
                        .putExtra(
                            Constants.IntentKeys.SPECIALITYLIST,
                            data.speciality as ArrayList<Parcelable>
                        )
                        .putExtra(Constants.IntentKeys.REVIEW_LIST, data.review)
                        .putExtra(Constants.IntentKeys.BOOKINGFLOW, true)
                )
            }

        }

    }

    companion object {

        private val DIFF_CALLBACK: DiffUtil.ItemCallback<DoctorData> =
            object : DiffUtil.ItemCallback<DoctorData>() {
                override fun areItemsTheSame(
                    oldItem: DoctorData,
                    newItem: DoctorData
                ): Boolean {
                    return oldItem.providerId === newItem.providerId
                }

                override fun areContentsTheSame(
                    oldItem: DoctorData,
                    newItem: DoctorData
                ): Boolean {
                    return oldItem.providerId === newItem.providerId &&
                            oldItem.distance == newItem.distance
                }
            }
    }
}