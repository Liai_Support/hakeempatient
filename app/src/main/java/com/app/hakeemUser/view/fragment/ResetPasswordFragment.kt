package com.app.hakeemUser.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.FragmentConsultationBinding
import com.app.hakeemUser.databinding.FragmentHome1Binding
import com.app.hakeemUser.databinding.FragmentResetPasswordBinding
import com.app.hakeemUser.interfaces.MoveToFragment
import com.app.hakeemUser.utils.DialogUtils
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.app.hakeemUser.viewmodel.RegisterViewModel

class ResetPasswordFragment(
    var moveToFragment: MoveToFragment,
    supportFragmentManager: FragmentManager
) : Fragment(R.layout.fragment_reset_password) {

    var binding: FragmentResetPasswordBinding? = null
    private var viewmodel: RegisterViewModel? = null
    var sharedHelper: SharedHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResetPasswordBinding.inflate(inflater, container, false)
        val view = binding!!.root
        viewmodel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        sharedHelper = SharedHelper(requireContext())
        binding!!.resetPassword.setOnClickListener {
            if (isValidInputs()) {
                DialogUtils.showLoader(requireContext())
                viewmodel?.resetPassword(binding!!.newPassword.text.toString().trim())
                    ?.observe(viewLifecycleOwner, Observer {
                        DialogUtils.dismissLoader()
                        it?.let {
                            it.error?.let { error ->
                                if (error) {
                                    it.message?.let { msg ->
                                        UiUtils.showSnack(binding!!.root, msg)
                                    }
                                } else {

                                    val manager =
                                        requireActivity().supportFragmentManager
                                    val trans: FragmentTransaction = manager.beginTransaction()
                                    trans.remove(this)
                                    trans.commit()
                                    manager.popBackStack()

//                            val intent =
//                                Intent(this, LoginActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                            intent.flags =
//                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                            startActivity(intent)

                                }
                            }

                        }
                    })
            }
        }

        binding!!.back.setOnClickListener {
            val manager =
                requireActivity().supportFragmentManager
            val trans: FragmentTransaction = manager.beginTransaction()
            trans.remove(this)
            trans.commit()
            manager.popBackStack()
        }
        return view
    }

    private fun isValidInputs(): Boolean {

        when {

            binding!!.newPassword.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() == "" -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.pleaseenteryourconfirmpassword))
                return false
            }
            binding!!.confirmPassword.text.toString().trim() != binding!!.newPassword.text.toString().trim() -> {
                UiUtils.showSnack(binding!!.root, getString(R.string.confirmpasswordshouldbesameaspassword))
                return false
            }
            else -> return true
        }

    }


}