package com.app.hakeemUser.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.databinding.ChildSpecialityBinding
import com.app.hakeemUser.interfaces.OnClickListener
import com.app.hakeemUser.interfaces.PatientActionListener
import com.app.hakeemUser.models.SpecialityImageData
import com.app.hakeemUser.utils.SharedHelper
import com.app.hakeemUser.utils.UiUtils
import com.facebook.FacebookSdk.getApplicationContext
import java.util.*

class SpecialityAdapter(
    var context: Context,
    var list: ArrayList<SpecialityImageData>,
    var onclickListener: OnClickListener
) :
    RecyclerView.Adapter<SpecialityAdapter.ViewHolder>() {
    var sharedHelper: SharedHelper? = null
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ChildSpecialityBinding = ChildSpecialityBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.child_speciality,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        sharedHelper = SharedHelper(context)

        Log.d("wertg",""+list[position].image)
        Log.d("wertg",""+list[position].name)

        holder.binding.selection1.setOnClickListener {
            setSelection(position)
        }

        if (list[position].isSelected) {
            holder.binding.selection.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.check_speciality
                )
            )
            holder.binding.selection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
            holder.binding.selection1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary)
            holder.binding.type.setTextColor(Color.WHITE)
          // holder.binding.doctor.imageTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.white)
        }
        else {

            holder.binding.selection.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.uncheck_speciality
                )
            )
            holder.binding.selection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                )
            )
            holder.binding.selection1.backgroundTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.white)
            holder.binding.type.setTextColor(ContextCompat.getColor(context,R.color.textcolor_1))
          // holder.binding.doctor.imageTintList = ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary)
        }


        if (sharedHelper?.language.equals("ar")){
            list[position].aname?.let {
                holder.binding.type.text = it
            }
        }
        else{
            list[position].name?.let {
                holder.binding.type.text = it
            }
        }


        list[position].image?.let {
            UiUtils.loadImage(
                holder.binding.doctor,
                it,
                ContextCompat.getDrawable(context, R.drawable.splash_image)!!
            )
        }


    }

    private fun setSelection(position: Int) {

        for (i in 0 until list.size) {
            if (position == i) {
                list[position].isSelected = true
                onclickListener.onClickItem(position)
            } else {
                list[i].isSelected = false
            }
        }
        notifyDataSetChanged()

    }
}