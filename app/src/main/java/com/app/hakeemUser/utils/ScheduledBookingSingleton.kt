package com.app.hakeemUser.utils

class ScheduledBookingSingleton private constructor() {

    companion object {

        private var instane: ScheduledBookingSingleton? = null

        fun getInstance(): ScheduledBookingSingleton {
            if (instane == null) {
                instane = ScheduledBookingSingleton()
            }
            return instane!!
        }

        fun clearAllValues() {
            instane = ScheduledBookingSingleton()
        }
    }


    var providerId: String? = null
    var specialityId: String? = null
    var specialityGender: String? = null
    var bookingDate: String? = null
    var bookingTime: String? = null
    var bookingPeriod: String? = null
    var location: String? = null
    var patientType: String? = null
    var patientName: String? = null
    var patientAge: String? = null
    var patientGender: String? = null
    var insurance: String? = null
    var previousIssue: String? = null
    var latitude: String? = null
    var longitude: String? = null
    var isVirtualBooking: Int = 0
    var isGlobalBooking: Int = 0
    var chooseoption: Int = 0


    var bookingType: String? = null

    var isElderlyFlow: Boolean = false

    var dfee: String? = null
    var chatFee: String? = null
    var audioFee: String? = null
    var videoFee: String? = null
    var vfee: String? = null
   // var paymenttype: String? = null
    var memberid: String? = null
    var transactionId: String? = null
    var torderId: String? = null
    var paymentMethod: String? = null


}