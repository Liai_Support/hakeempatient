package com.app.hakeemUser.utils

import android.content.Context
import com.app.hakeemUser.models.PatientDetails
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)



    var isInsurance: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("isInsurance")
        }
        set(value) {
            sharedPreference.putBoolean("isInsurance", value)
        }


    var isreview: String
        get() : String {
            return sharedPreference.getKey("isreview")
        }
        set(value) {
            sharedPreference.putKey("isreview", value)
        }

    var isMyself: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("isMyself")
        }
        set(value) {
            sharedPreference.putBoolean("isMyself", value)
        }


    var token: String
        get() : String {
            return sharedPreference.getKey("token")
        }
        set(value) {
            sharedPreference.putKey("token", value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey("fcmToken")
        }
        set(value) {
            sharedPreference.putKey("fcmToken", value)
        }
    var language: String
        get() : String {
            return if (sharedPreference.getKey("language") == "") {
                "en"
            } else {
                sharedPreference.getKey("language")
            }

        }
        set(value) {
            sharedPreference.putKey("language", value)
        }


    var id: Int
        get() : Int {
            return sharedPreference.getInt("id")
        }
        set(value) {
            sharedPreference.putInt("id", value)
        }
    var name: String
        get() : String {
            return sharedPreference.getKey("name")
        }
        set(value) {
            sharedPreference.putKey("name", value)
        }

    var istatus: String
        get() : String {
            return sharedPreference.getKey("istatus")
        }
        set(value) {
            sharedPreference.putKey("istatus", value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey("email")
        }
        set(value) {
            sharedPreference.putKey("email", value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey("mobileNumber")
        }
        set(value) {
            sharedPreference.putKey("mobileNumber", value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey("imageUploadPath")
        }
        set(value) {
            sharedPreference.putKey("imageUploadPath", value)
        }
    var countryCode: String
        get() : String {
            return sharedPreference.getKey("countryCode")
        }
        set(value) {
            sharedPreference.putKey("countryCode", value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("loggedIn")
        }
        set(value) {
            sharedPreference.putBoolean("loggedIn", value)
        }

    var Iscurrentlocationseleted: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("iscurrentlocation")
        }
        set(value) {
            sharedPreference.putBoolean("iscurrentlocation", value)
        }

    var Islocationseleted: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("islocation")
        }
        set(value) {
            sharedPreference.putBoolean("islocation", value)
        }

    var selectedaddressid: Int
        get() : Int {
            return sharedPreference.getInt("addressid")
        }
        set(value) {
            sharedPreference.putInt("addressid", value)
        }

    var seletedaddresslocation: String
        get() : String {
            return sharedPreference.getKey("seletedaddresslocation")
        }
        set(value) {
            sharedPreference.putKey("seletedaddresslocation", value)
        }




    var currentLat: String
        get() : String {
            return sharedPreference.getKey("currentLat")
        }
        set(value) {
            sharedPreference.putKey("currentLat", value)
        }

    var currentLng: String
        get() : String {
            return sharedPreference.getKey("currentLng")
        }
        set(value) {
            sharedPreference.putKey("currentLng", value)
        }

    var location: String
        get() : String {
            return sharedPreference.getKey("location")
        }
        set(value) {
            sharedPreference.putKey("location", value)
        }

    var selectedLat: String
        get() : String {
            return sharedPreference.getKey("selectedLat")
        }
        set(value) {
            sharedPreference.putKey("selectedLat", value)
        }

    var selectedLng: String
        get() : String {
            return sharedPreference.getKey("selectedLng")
        }
        set(value) {
            sharedPreference.putKey("selectedLng", value)
        }

    var userImage: String
        get() : String {
            return sharedPreference.getKey("userImage")
        }
        set(value) {
            sharedPreference.putKey("userImage", value)
        }

    var currentlocation: String
        get() : String {
            return sharedPreference.getKey("currentLocation")
        }
        set(value) {
            sharedPreference.putKey("currentLocation", value)
        }


    var chatTiming: Int
        get() : Int {
            return sharedPreference.getInt("chatTiming")
        }
        set(value) {
            sharedPreference.putInt("chatTiming", value)
        }

    var calendarTiming: Int
        get() : Int {
            return sharedPreference.getInt("calendarTiming")
        }
        set(value) {
            sharedPreference.putInt("calendarTiming", value)
        }

    var cancelInPercetage: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("cancelInPercetage")
        }
        set(value) {
            sharedPreference.putBoolean("cancelInPercetage", value)
        }

    var cancelPercORAmt: String
        get() : String {
            return sharedPreference.getKey("cancelPercORAmt")
        }
        set(value) {
            sharedPreference.putKey("cancelPercORAmt", value)
        }
    var patientList: ArrayList<PatientDetails>
        get() : ArrayList<PatientDetails> {
            val myType = object : TypeToken<List<PatientDetails>>() {}.type
            val vsl = sharedPreference.getKey("patientList")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<PatientDetails> = Gson().fromJson<List<PatientDetails>>(vsl, myType)
            return logs as ArrayList<PatientDetails>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("patientList", jsonString)
        }

  /*  var insuranceclassname: ArrayList<String>
        get() : ArrayList<String> {
            val myType = object : TypeToken<List<String>>() {}.type
            val vsl = sharedPreference.getKey("cd1")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<String> = Gson().fromJson<List<String>>(vsl, myType)
            return logs as ArrayList<String>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cd1", jsonString)
        }

    var insuranceclassid: ArrayList<Int>
        get() : ArrayList<Int> {
            val myType = object : TypeToken<List<Int>>() {}.type
            val vsl = sharedPreference.getKey("cd2")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Int> = Gson().fromJson<List<Int>>(vsl, myType)
            return logs as ArrayList<Int>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cd2", jsonString)
        }*/

    var insurancecompanyname: ArrayList<String>
        get() : ArrayList<String> {
            val myType = object : TypeToken<List<String>>() {}.type
            val vsl = sharedPreference.getKey("cd3")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<String> = Gson().fromJson<List<String>>(vsl, myType)
            return logs as ArrayList<String>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cd3", jsonString)
        }

    var insurancecompanyid: ArrayList<Int>
        get() : ArrayList<Int> {
            val myType = object : TypeToken<List<Int>>() {}.type
            val vsl = sharedPreference.getKey("cd4")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Int> = Gson().fromJson<List<Int>>(vsl, myType)
            return logs as ArrayList<Int>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cd4", jsonString)
        }

}
