package com.app.hakeemUser.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.hakeemUser.R
import com.app.hakeemUser.interfaces.*
import com.app.hakeemUser.view.adapter.CommonListAdapter
import kotlin.math.roundToInt


object DialogUtils {

    var loaderDialog: Dialog? = null
    var noInternetDialog: Dialog? = null
    var paymentDialog: Dialog? = null
    private var isShowingDialog = false


    fun showPictureDialog(activity: Activity) {

        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle(activity.getString(R.string.choose_your_option))
        val items =
            arrayOf(activity.getString(R.string.gallery), activity.getString(R.string.camera))

        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.READ_STORAGE_PERM_LIST,
                        Constants.Permission.READ_STORAGE_PERMISSIONS
                    )
                    //BaseUtils.openGallery(activity)
                } else {
                    BaseUtils.openGallery(activity)
                }
                1 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(
                        Constants.Permission.CAMERA_PERM_LIST,
                        Constants.Permission.CAMERA_STORAGE_PERMISSIONS
                    )
                    //BaseUtils.openCamera(activity)
                } else {
                    BaseUtils.openCamera(activity)
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }

    fun showListDialog(
        context: Context,
        list: Array<String>,
        headerVal: String,
        listener: OnClickListener
    ): Dialog {

        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        dialog.setContentView(R.layout.dialog_selection)

        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var listView = dialog.findViewById<RecyclerView>(R.id.listView)
        var header = dialog.findViewById<TextView>(R.id.header)

        header.text = headerVal

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, height)

        listView.layoutManager = LinearLayoutManager(context)
        listView.adapter = CommonListAdapter(context, list, object : OnClickListener {
            override fun onClickItem(position: Int) {
                listener.onClickItem(position)
                dialog.dismiss()
            }

        })

        dialog.show()

        return dialog
    }

    fun showSuccessDialog(
        context: Context,
        singleTapListener: SingleTapListener
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_success_consultation)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, height)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        Handler().postDelayed({

            if (dialog.isShowing) {
                dialog.dismiss()
                singleTapListener.singleTap()
            }

        }, 2000)

        dialog.show()

    }


    fun showSuccessBookingDialog(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_success_booking)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)

        contentView.text = content

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, height)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        Handler().postDelayed({

            if (dialog.isShowing) {
                dialog.dismiss()
                singleTapListener.singleTap()
            }

        }, 2000)

        dialog.show()

    }

    fun showAlert(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }




    fun showAlertt(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }

    fun showAlertWithHeader(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String,
        headerVal: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var header = dialog.findViewById<TextView>(R.id.header)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerVal


        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        dialog.show()

    }

    fun showAlertDialog(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
       // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = dialog.findViewById<TextView>(R.id.header)
        var contentTextView = dialog.findViewById<TextView>(R.id.content)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick()
            dialog.dismiss()
        }



        dialog.show()

    }

    fun showAlertDialogPayment(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {

        paymentDialog?.let {
            if (it.isShowing) {
                return
            }
        }

        paymentDialog = Dialog(context)
        paymentDialog?.setCancelable(false)
        paymentDialog?.setCanceledOnTouchOutside(false)
        paymentDialog?.setContentView(R.layout.dialog_alert)
        paymentDialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
      //  var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        paymentDialog?.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = paymentDialog?.findViewById<TextView>(R.id.header)
        var contentTextView = paymentDialog?.findViewById<TextView>(R.id.content)

        var cancel = paymentDialog?.findViewById<TextView>(R.id.cancel)
        var ok = paymentDialog?.findViewById<TextView>(R.id.ok)

        headerTextView?.text = title
        contentTextView?.text = content

        cancel?.text = negativeText
        ok?.text = positiveText

        cancel?.setOnClickListener {
            callBack.onNegativeClick()
            paymentDialog?.dismiss()
        }

        ok?.setOnClickListener {
            callBack.onPositiveClick()
            paymentDialog?.dismiss()
        }



        paymentDialog?.show()

    }

    fun getBookingRatingDialog(context: Context): Dialog {

        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_consult_rating)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.95).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        return dialog

    }


//    fun showDialog(
//        context: Context,
//        header: String,
//        content: String,
//        positiveText: String,
//        negativeText: String,
//        dialogCallback: DialogCallback
//    ) {
//
//        var alertDialog = AlertDialog.Builder(context)
//        alertDialog.setTitle(header)
//        alertDialog.setMessage(content)
//        alertDialog.setCancelable(false)
//        alertDialog.setPositiveButton(positiveText) { _, _ ->
//            dialogCallback.positiveClickListner()
//        }
//        alertDialog.setNegativeButton(negativeText) { _, _ ->
//            dialogCallback.negativeClickListner()
//        }
//        alertDialog.show()
//    }

    @SuppressLint("ResourceType")
    fun showLoader(context: Context) {

        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

        // loaderDialog!!.anim_view.playAnimation()
        loaderDialog!!.findViewById<com.airbnb.lottie.LottieAnimationView>(R.id.anim_view).playAnimation()


        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }

    }

    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }

//    fun noInternetDialog(context: Context, singleTapListener: SingleTapListener) {
//
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//
//            }
//        }
//
//        if (!isShowingDialog) {
//            noInternetDialog = Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
//            noInternetDialog?.setCancelable(false)
//            noInternetDialog?.setCanceledOnTouchOutside(false)
//            noInternetDialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT)
//
//            val inflater = LayoutInflater.from(context)
//            val view = inflater?.inflate(R.layout.dialog_no_internet, null)
//            if (view != null) {
//                noInternetDialog!!.setContentView(view)
//            }
//
//            noInternetDialog!!.tryAgain.setOnClickListener {
//                if (NetworkUtils.isNetworkConnected(context)) {
//                    singleTapListener.singleTap()
//                    dismissInternetDialog(object : SingleTapListener {
//                        override fun singleTap() {
//                            singleTapListener.singleTap()
//                        }
//
//                    })
//                }
//            }
//
//
//            noInternetDialog?.show()
//            isShowingDialog = true
//        }
//    }
//
//    fun dismissInternetDialog(singleTapListener: SingleTapListener) {
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//                noInternetDialog!!.dismiss()
//                singleTapListener.singleTap()
//                isShowingDialog = false
//            }
//        }
//    }

    fun checkGpsIsEnabled(context: Context): Boolean {
        val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun showRatingDialog(context: Context, callBack: DialogCallBack) {


        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_rating)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
       // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)


        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick()
            dialog.dismiss()
        }



        dialog.show()

    }


    fun showExtraFeeDialog(
        context: Context,
        callBack: WalletRechargeCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_extra_amount)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
       // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var extraAmount = dialog.findViewById<EditText>(R.id.extraAmount)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)


        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            if (extraAmount.text.toString().trim().isNotEmpty() && extraAmount.text.toString().trim() != "0") {
                callBack.onPositiveClick(extraAmount.text.toString().toInt())
                dialog.dismiss()
            }
        }



        dialog.show()

    }

    fun showInsurancebalanceDialog(
        context: Context,
        callBack: WalletRechargeCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_extra_amount)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var extraAmount = dialog.findViewById<EditText>(R.id.extraAmount)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)


        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            if (extraAmount.text.toString().trim().isNotEmpty() && extraAmount.text.toString().trim() != "0") {
                callBack.onPositiveClick(extraAmount.text.toString().toInt())
                dialog.dismiss()
            }
        }



        dialog.show()

    }

    fun showSearchingDialog(context: Context): Dialog {

        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_searching_doctor)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.95).roundToInt()
        var height: Int = (context.resources.displayMetrics.heightPixels * 0.75).roundToInt()

        dialog.window?.setLayout(width, height)

       // var extraAmount = dialog.findViewById<EditText>(R.id.extraAmount)


        return dialog
    }

    fun chooseoptionDialog(
        context: Context,
        callBack: ChooseOptionCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_chooseoption)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
       // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        var radiogroup = dialog.findViewById<RadioGroup>(R.id.radiogroup)
        var normal = dialog.findViewById<RadioButton>(R.id.normal)
      //  var insurance = dialog.findViewById<RadioButton>(R.id.insurance)
        var normal1: Boolean? = null

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        radiogroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
            normal1 = normal.isChecked
        })

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            if (normal1 != null) {
                callBack.onPositiveClick(normal1!!)
                dialog.dismiss()
            }
            else{

            }
        }



        dialog.show()

    }


}