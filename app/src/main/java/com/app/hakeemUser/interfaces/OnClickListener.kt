package com.app.hakeemUser.interfaces

interface OnClickListener {
    fun onClickItem(position: Int)
}
