package com.app.hakeemUser.interfaces

interface SingleTapListener {
    fun singleTap()
}