package com.app.hakeemUser.interfaces

interface ChooseOptionCallBack {
    fun onPositiveClick(value: Boolean)
    fun onNegativeClick()
}