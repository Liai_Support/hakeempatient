package com.app.hakeemUser.interfaces

interface PatientActionListener {

    fun onChatClicked(position: Int)
    fun onCallClicked(position: Int)
}