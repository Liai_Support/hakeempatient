package com.app.hakeemUser.interfaces

interface WalletRechargeCallBack {
    fun onPositiveClick(value: Int)
    fun onNegativeClick()
}