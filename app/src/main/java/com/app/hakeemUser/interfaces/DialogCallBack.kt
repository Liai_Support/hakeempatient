package com.app.hakeemUser.interfaces

interface DialogCallBack {
    fun onPositiveClick()
    fun onNegativeClick()
}