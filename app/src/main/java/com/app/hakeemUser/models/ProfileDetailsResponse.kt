
package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProfileDetailsResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: detailsContainer? = null

}

class detailsContainer : Serializable {

    @SerializedName("profileDetails")
    var profileDetails: ArrayList<ProfileDetails>? = null
}


class ProfileDetails : Serializable {


    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("loginType")
    var loginType: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("DOB")
    var DOB: String? = null

    @SerializedName("bloodGroup")
    var bloodGroup: String? = null

    @SerializedName("height")
    var height: String? = null

    @SerializedName("weight")
    var weight: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null


    @SerializedName("Iqama_No")
    var sid: String? = null

    @SerializedName("insurence_id")
    var insurenceid: String? = null

    @SerializedName("insurence_class")
    var insurenceclassid: String? = null

    @SerializedName("class_name")
    var insurenceclassname: String? = null

    @SerializedName("admin_approve")
    var istatus: String? = "true"

    @SerializedName("ins_name")
    var insurencecompanyname: String? = null

    @SerializedName("insurence_com")
    var insurencecompanyid: String? = null

    @SerializedName("addressid")
    var addressid: Int? = 0

    @SerializedName("age")
    var age: String? = "0"




}