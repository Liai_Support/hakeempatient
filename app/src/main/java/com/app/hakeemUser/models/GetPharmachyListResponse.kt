package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class GetPharmachyListResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Presult1? = null
}

class Presult1 : Serializable {

    @SerializedName("results")
    var pharmachyDetails: ArrayList<PharmachyDetails>? = null
}


class PharmachyDetails : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("Pharmacy_name")
    var Pharmacy_name: String? = null

    @SerializedName("Pharmacy_ar_name")
    var Pharmacy_ar_name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

}
