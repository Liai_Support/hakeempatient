package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetInsuranceStatusResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<InsuranceData>? = null


}

class InsuranceData : Serializable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("bookingId")
    var bookingId: String? = null

    @SerializedName("isPendingAmount")
    var isPendingAmount: String? = null

    @SerializedName("balance")
    var balance: String? = "0"

    @SerializedName("modeOfBalance")
    var modeOfBalance: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("ins_status")
    var ins_status: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

}