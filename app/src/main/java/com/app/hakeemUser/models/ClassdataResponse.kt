package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ClassdataResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Classdata? = null

}

class Classdata : Serializable {

    @SerializedName("results")
    var classDetails: ArrayList<ClassDetails>? = null
}


class ClassDetails : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("class_name")
    var name: String? = null

}
