package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AllDoctorResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("data")
    var data: DoctorListDetails? = null

}