package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ActivityBookingResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<ActivieBookingData>? = null


}


class ActivieBookingData : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("providerId")
    var providerId: Int? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("bookingDate")
    var bookingDate: String? = null

    @SerializedName("bookingTime")
    var bookingTime: String? = null

    @SerializedName("bookingPeriod")
    var bookingPeriod: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("bookingLatitude")
    var bookingLatitude: String? = null

    @SerializedName("bookingLongitude")
    var bookingLongitude: String? = null

    @SerializedName("isFeatureBooking")
    var isFeatureBooking: String? = null

    @SerializedName("patientType")
    var patientType: String? = null

    @SerializedName("patientName")
    var patientName: String? = null

    @SerializedName("patientAge")
    var patientAge: String? = null

    @SerializedName("patientGender")
    var patientGender: String? = null

    @SerializedName("previousIssue")
    var previousIssue: String? = null

    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("providerName")
    var providerName: String? = null

    @SerializedName("providerType")
    var providerType: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("providerStatus")
    var providerStatus: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("extraFee")
    var extraFee: String? = null

    @SerializedName("isVirtualBooking")
    var isVirtualBooking: Int? = null


}