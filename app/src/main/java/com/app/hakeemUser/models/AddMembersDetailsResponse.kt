package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddMembersDetailsResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: Int? = null
}