package com.app.hakeemUser.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BookingDetailResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<BookingData>? = null


}

class BookingData() : Serializable, Parcelable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("distance")
    var distance: String? = null

    @SerializedName("providerLatitude")
    var providerLatitude: String? = null

    @SerializedName("providerLongitude")
    var providerLongitude: String? = null


    @SerializedName("bookingId")
    var bookingId: Int? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("reachedDestination")
    var reachedDestination: String? = null

    @SerializedName("bookingDate")
    var bookingDate: String? = null

    @SerializedName("bookingTime")
    var bookingTime: String? = null

    @SerializedName("bookingPeriod")
    var bookingPeriod: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("bookingLatitude")
    var bookingLatitude: String? = null

    @SerializedName("bookingLongitude")
    var bookingLongitude: String? = null

    @SerializedName("isFeatureBooking")
    var isFeatureBooking: Boolean? = null

    @SerializedName("patientType")
    var patientType: String? = null

    @SerializedName("patientName")
    var patientName: String? = null

    @SerializedName("patientAge")
    var patientAge: String? = null

    @SerializedName("patientGender")
    var patientGender: String? = null

    @SerializedName("previousIssue")
    var previousIssue: String? = null


    @SerializedName("specialityName")
    var specialityName: String? = null

    @SerializedName("providerName")
    var providerName: String? = null

    @SerializedName("providerType")
    var providerType: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("countryCode")
    var countryCode: Int? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("providerStatus")
    var providerStatus: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("patientLatitude")
    var patientLatitude: String? = null

    @SerializedName("patientLongitude")
    var patientLongitude: String? = null

    @SerializedName("averageRating")
    var averageRating: String? = null

    @SerializedName("fee")
    var fee: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        distance = parcel.readString()
        providerLatitude = parcel.readString()
        providerLongitude = parcel.readString()
        bookingId = parcel.readValue(Int::class.java.classLoader) as? Int
        status = parcel.readString()
        bookingDate = parcel.readString()
        bookingTime = parcel.readString()
        bookingPeriod = parcel.readString()
        location = parcel.readString()
        bookingLatitude = parcel.readString()
        bookingLongitude = parcel.readString()
        isFeatureBooking = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        patientType = parcel.readString()
        patientName = parcel.readString()
        patientAge = parcel.readString()
        patientGender = parcel.readString()
        previousIssue = parcel.readString()
        specialityName = parcel.readString()
        providerName = parcel.readString()
        providerType = parcel.readString()
        education = parcel.readString()
        expierence = parcel.readString()
        countryCode = parcel.readValue(Int::class.java.classLoader) as? Int
        mobileNumber = parcel.readString()
        providerStatus = parcel.readString()
        profilePic = parcel.readString()
        patientLatitude = parcel.readString()
        patientLongitude = parcel.readString()
        averageRating = parcel.readString()
        fee = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(distance)
        parcel.writeString(providerLatitude)
        parcel.writeString(providerLongitude)
        parcel.writeValue(bookingId)
        parcel.writeString(status)
        parcel.writeString(bookingDate)
        parcel.writeString(bookingTime)
        parcel.writeString(bookingPeriod)
        parcel.writeString(location)
        parcel.writeString(bookingLatitude)
        parcel.writeString(bookingLongitude)
        parcel.writeValue(isFeatureBooking)
        parcel.writeString(patientType)
        parcel.writeString(patientName)
        parcel.writeString(patientAge)
        parcel.writeString(patientGender)
        parcel.writeString(previousIssue)
        parcel.writeString(specialityName)
        parcel.writeString(providerName)
        parcel.writeString(providerType)
        parcel.writeString(education)
        parcel.writeString(expierence)
        parcel.writeValue(countryCode)
        parcel.writeString(mobileNumber)
        parcel.writeString(providerStatus)
        parcel.writeString(profilePic)
        parcel.writeString(patientLatitude)
        parcel.writeString(patientLongitude)
        parcel.writeString(averageRating)
        parcel.writeString(fee)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BookingData> {
        override fun createFromParcel(parcel: Parcel): BookingData {
            return BookingData(parcel)
        }

        override fun newArray(size: Int): Array<BookingData?> {
            return arrayOfNulls(size)
        }
    }


}