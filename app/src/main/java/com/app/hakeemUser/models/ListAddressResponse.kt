package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class ListAddressResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    /* @SerializedName("data")
     var data: String? = null
 */
    /*  @SerializedName("data")
      var data: List<MemberData>? = null*/

    @SerializedName("data")
    var data: detailsContainer1? = null
}

class detailsContainer1 : Serializable {

    @SerializedName("results")
    var addressDetails: ArrayList<AddressDetails>? = null
}

class AddressDetails : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("patient_id")
    var pid: Int? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("latitude")
    var lat: Double? = null

    @SerializedName("longitude")
    var lng: Double? = null
}
