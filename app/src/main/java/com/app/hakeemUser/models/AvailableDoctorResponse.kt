package com.app.hakeemUser.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AvailableDoctorResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("data")
    var data: DoctorListDetails? = null


}

class DoctorListDetails : Serializable {

    @SerializedName("currentpage")
    var currentpage: Int? = 0

    @SerializedName("pageCount")
    var pageCount: Int? = 0

    @SerializedName("doctors")
    var doctors: List<DoctorData>? = null


}


class DoctorData() : Serializable, Parcelable {

    @SerializedName("providerId")
    var providerId: Int? = null

    @SerializedName("default_fees")
    var default_fees: String? = null

    @SerializedName("virtual_fees")
    var virtual_fees: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("hospital")
    var hospital: String? = null

    @SerializedName("designation")
    var designation: String? = null

    @SerializedName("location")
    var location: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("distance")
    var distance: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("averageRating")
    var averageRating: String? = null

    @SerializedName("review")
    var review: ArrayList<ReviewData>? = null

    @SerializedName("speciality")
    var speciality: List<DoctorSpecialityList>? = ArrayList()

    constructor(parcel: Parcel) : this() {
        providerId = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        profilePic = parcel.readString()
        education = parcel.readString()
        expierence = parcel.readString()
        status = parcel.readString()
        distance = parcel.readString()
        fee = parcel.readString()
        averageRating = parcel.readString()
        hospital = parcel.readString()
        designation = parcel.readString()
        location = parcel.readString()
        speciality = parcel.createTypedArrayList(DoctorSpecialityList)
    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(providerId)
        parcel.writeString(name)
        parcel.writeString(profilePic)
        parcel.writeString(education)
        parcel.writeString(expierence)
        parcel.writeString(status)
        parcel.writeString(distance)
        parcel.writeString(fee)
        parcel.writeString(averageRating)
        parcel.writeString(hospital)
        parcel.writeString(designation)
        parcel.writeString(location)
        parcel.writeTypedList(speciality)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DoctorData> {
        override fun createFromParcel(parcel: Parcel): DoctorData {
            return DoctorData(parcel)
        }

        override fun newArray(size: Int): Array<DoctorData?> {
            return arrayOfNulls(size)
        }
    }


}

class ReviewData() : Serializable, Parcelable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("review")
    var review: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        rating = parcel.readString()
        review = parcel.readString()
        name = parcel.readString()
        profilePic = parcel.readString()
        createdAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(rating)
        parcel.writeString(review)
        parcel.writeString(name)
        parcel.writeString(profilePic)
        parcel.writeString(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReviewData> {
        override fun createFromParcel(parcel: Parcel): ReviewData {
            return ReviewData(parcel)
        }

        override fun newArray(size: Int): Array<ReviewData?> {
            return arrayOfNulls(size)
        }
    }

}

class DoctorSpecialityList() : Parcelable, Serializable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("virtualFee")
    var virtualFee: String? = null

    @SerializedName("virtualStreamFee")
    var virtualStreamFee: String? = null

    @SerializedName("chatVirtualFee")
    var chatVirtualFee: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        image = parcel.readString()
        fee = parcel.readString()
        virtualFee = parcel.readString()
        virtualStreamFee = parcel.readString()
        chatVirtualFee = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(image)
        parcel.writeString(fee)
        parcel.writeString(virtualFee)
        parcel.writeString(virtualStreamFee)
        parcel.writeString(chatVirtualFee)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DoctorSpecialityList> {
        override fun createFromParcel(parcel: Parcel): DoctorSpecialityList {
            return DoctorSpecialityList(parcel)
        }

        override fun newArray(size: Int): Array<DoctorSpecialityList?> {
            return arrayOfNulls(size)
        }
    }


}

