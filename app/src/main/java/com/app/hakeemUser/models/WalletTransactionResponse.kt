package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WalletTransactionResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: TransactionData? = null

}

class TransactionData : Serializable {

    @SerializedName("availableCredits")
    var availableCredits: Double? = null

    @SerializedName("history")
    var history: ArrayList<TransactionHistory>? = null

}

class TransactionHistory : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("patientId")
    var patientId: Int? = null

    @SerializedName("transactionId")
    var transactionId: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

}