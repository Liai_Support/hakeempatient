package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NewVirutualFeeResponse {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: vData? = null


}

class vData : Serializable {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("c_id")
    var cid: String? = null

    @SerializedName("ins_amt")
    var iamt: String? = null

    @SerializedName("vir_amt")
    var vamt: String? = null

    @SerializedName("com_name")
    var cname: String? = null


}