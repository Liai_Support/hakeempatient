package com.app.hakeemUser.models

import java.io.Serializable

class PatientDetails : Serializable {

    var patientType = ""
    var name = ""
    var dob = ""
    var age = 0
    var bloodGroup = ""
    var insurance = ""
    var gender = ""
    var healthIssue = ""
    var isSelected = false
    var memberid = 0

}