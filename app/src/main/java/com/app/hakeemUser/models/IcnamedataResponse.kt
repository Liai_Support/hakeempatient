package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class IcnamedataResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: icnamedata? = null

}

class icnamedata : Serializable {

    @SerializedName("results")
    var companyDetails: ArrayList<icnameDetails>? = null
}


class icnameDetails : Serializable {


    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("name_ar")
    var name_ar: String? = null

}
