package com.app.hakeemUser.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import kotlin.math.ln

class LivedoctorResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("data")
    var data: LiveDoctorListDetails? = null


}

class LiveDoctorListDetails : Serializable {

    @SerializedName("currentpage")
    var currentpage: Int? = 0

    @SerializedName("pageCount")
    var pageCount: Int? = 0

    @SerializedName("doctors")
    var doctors: ArrayList<LiveDoctorData>? = null

    @SerializedName("em_fees")
    var em_fees: String? = null
}


class LiveDoctorData() : Serializable, Parcelable {

    @SerializedName("latitude")
    var lat: Double? = null

    @SerializedName("longitude")
    var lng: Double? = null


    @SerializedName("providerId")
    var providerId: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("education")
    var education: String? = null

    @SerializedName("expierence")
    var expierence: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("distance")
    var distance: String? = null

    @SerializedName("fee")
    var fee: String? = null

    @SerializedName("averageRating")
    var averageRating: String? = null

    @SerializedName("review")
    var review: ArrayList<ReviewData>? = null

    @SerializedName("speciality")
    var speciality: List<DoctorSpecialityList>? = null

    constructor(parcel: Parcel) : this() {
        providerId = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        lat = parcel.readDouble()
        lng = parcel.readDouble()
        profilePic = parcel.readString()
        education = parcel.readString()
        expierence = parcel.readString()
        status = parcel.readString()
        distance = parcel.readString()
        fee = parcel.readString()
        averageRating = parcel.readString()
        speciality = parcel.createTypedArrayList(DoctorSpecialityList)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat!!)
        parcel.writeDouble(lng!!)
        parcel.writeValue(providerId)
        parcel.writeString(name)
        parcel.writeString(profilePic)
        parcel.writeString(education)
        parcel.writeString(expierence)
        parcel.writeString(status)
        parcel.writeString(distance)
        parcel.writeString(fee)
        parcel.writeString(averageRating)
        parcel.writeTypedList(speciality)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DoctorData> {
        override fun createFromParcel(parcel: Parcel): DoctorData {
            return DoctorData(parcel)
        }

        override fun newArray(size: Int): Array<DoctorData?> {
            return arrayOfNulls(size)
        }
    }


}




