package com.app.hakeemUser.models

data class CurrentBookingListJson(
    val bookingId: Int,
    val bookingIds:String,
    val isVirtualBooking:Int,
    val profilePic:String,
    val providerName:String,
    val providerId:Int,
    val expierence:String,
    val previousIssue:String,
    val fee:String,
    val bookingDate:String,
    val bookingTime:String,
    val booking_type:String,
    val chat:String,
    val status:String,
    val specialityName:String,
    val is_ins:String,
    val stream:String,
    val isFeatureBooking:String,
    val bookingPeriod:String,


)

