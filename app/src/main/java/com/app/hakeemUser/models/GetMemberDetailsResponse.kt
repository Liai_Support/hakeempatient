package com.app.hakeemUser.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

class GetMemberDetailsResponse : Serializable {
    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

   /* @SerializedName("data")
    var data: String? = null
*/
  /*  @SerializedName("data")
    var data: List<MemberData>? = null*/

    @SerializedName("data")
    var data: ArrayList<MemberData>? = null
}

class MemberData() : Serializable, Parcelable {
    @SerializedName("id")
    var memberId: Int? = null

    @SerializedName("patientId")
    var patientid: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("gender")
    var gender: String? = null

    @SerializedName("DOB")
    var dob: String? = null

    @SerializedName("bloodGroup")
    var bloodgroup: String? = null

    @SerializedName("insurence_id")
    var insuranceid: String? = null

    @SerializedName("insurence_com")
    var insurancecompany: String? = null

    @SerializedName("insurence_class")
    var insuranceclass: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobile")
    var mobile: String? = null

    @SerializedName("Iqama_No")
    var sauthiId: String? = null

    @SerializedName("admin_approve")
    var adminapprove: String? = "true"

    @SerializedName("isselected")
    var isSelected: Boolean? = false

    @SerializedName("age")
    var age: String? = "0"

    constructor(parcel: Parcel) : this() {
        memberId = parcel.readValue(Int::class.java.classLoader) as? Int
        patientid = parcel.readString()
        type = parcel.readString()
        name = parcel.readString()
        gender = parcel.readString()
        email = parcel.readString()
        mobile = parcel.readString()
        dob = parcel.readString()
        sauthiId = parcel.readString()
        bloodgroup = parcel.readString()
        insuranceid = parcel.readString()
        insurancecompany = parcel.readValue(String::class.java.classLoader) as? String
        insuranceclass = parcel.readValue(String::class.java.classLoader) as? String
        adminapprove = parcel.readString()
        isSelected = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        age = parcel.readValue(String()::class.java.classLoader) as? String
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(memberId)
        parcel.writeString(patientid)
        parcel.writeString(type)
        parcel.writeString(name)
        parcel.writeString(gender)
        parcel.writeString(email)
        parcel.writeString(mobile)
        parcel.writeString(dob)
        parcel.writeString(sauthiId)
        parcel.writeString(bloodgroup)
        parcel.writeString(insuranceid)
        parcel.writeValue(insurancecompany)
        parcel.writeValue(insuranceclass)
        parcel.writeString(adminapprove)
        parcel.writeValue(isSelected)
        parcel.writeValue(age)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MemberData> {
        override fun createFromParcel(parcel: Parcel): MemberData {
            return MemberData(parcel)
        }

        override fun newArray(size: Int): Array<MemberData?> {
            return arrayOfNulls(size)
        }
    }
}
