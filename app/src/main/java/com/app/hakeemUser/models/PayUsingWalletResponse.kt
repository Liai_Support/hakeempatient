package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PayUsingWalletResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("message")
    var message: String? = ""

    @SerializedName("data")
    var data: PayUsingWalletData? = null


}

class PayUsingWalletData : Serializable {

    @SerializedName("patientId")
    var patientId: Int? = null

    @SerializedName("transactionId")
    var transactionId: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("type")
    var type: String? = null

}