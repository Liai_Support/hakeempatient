package com.app.hakeemUser.models

import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import com.app.hakeemUser.R
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HomeDashBoardResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: HomeBoardData? = null

}

class HomeBoardData : Serializable {

    @SerializedName("bannerImages")
    var bannerImages: ArrayList<BannerImageData>? = null
    @SerializedName("speciality")
    var speciality: ArrayList<SpecialityImageData>? = null

    @SerializedName("articles")
    var articles: ArrayList<ArticlesData>? = null

    @SerializedName("configurations")
    var configurations: ArrayList<Configurations>? = null

}

class BannerImageData : Serializable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
    @SerializedName("updatedAt")
    var updatedAt: String? = null

    var txt1: String? = "Coronaa"
    var img: Int = R.drawable.doct

}

class ArticlesData() : Serializable, Parcelable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("articleImage")
    var articleImage: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("createdAt")
    var createdAt: String? = null
    @SerializedName("updatedAt")
    var updatedAt: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        articleImage = parcel.readString()
        description = parcel.readString()
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(title)
        parcel.writeString(articleImage)
        parcel.writeString(description)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArticlesData> {
        override fun createFromParcel(parcel: Parcel): ArticlesData {
            return ArticlesData(parcel)
        }

        override fun newArray(size: Int): Array<ArticlesData?> {
            return arrayOfNulls(size)
        }
    }

}

class SpecialityImageData() : Serializable, Parcelable {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("main_string")
    var name: String? = null

    @SerializedName("name")
    var aname: String? = null
    @SerializedName("image")
    var image: String? = null
    @SerializedName("fee")
    var fee: String? = null
    @SerializedName("virtualFee")
    var virtualFee: String? = null
    @SerializedName("virtualStreamFee")
    var virtualStreamFee: String? = null

    @SerializedName("chatVirtualFee")
    var chatVirtualFee: String? = null

    var isSelected = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        aname = parcel.readString()
        image = parcel.readString()
        fee = parcel.readString()
        virtualFee = parcel.readString()
        virtualStreamFee = parcel.readString()
        chatVirtualFee = parcel.readString()
        isSelected = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(aname)
        parcel.writeString(image)
        parcel.writeString(fee)
        parcel.writeString(virtualFee)
        parcel.writeString(virtualStreamFee)

        parcel.writeString(chatVirtualFee)

        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SpecialityImageData> {
        override fun createFromParcel(parcel: Parcel): SpecialityImageData {
            return SpecialityImageData(parcel)
        }

        override fun newArray(size: Int): Array<SpecialityImageData?> {
            return arrayOfNulls(size)
        }
    }


}
