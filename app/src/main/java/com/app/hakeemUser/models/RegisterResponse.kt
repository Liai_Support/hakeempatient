package com.app.hakeemUser.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RegisterResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: RegisterData? = null


}


class RegisterData : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("token")
    var token: String? = null

    @SerializedName("profilePic")
    var profilePic: String? = null

    @SerializedName("insurence_id")
    var insurenceid: String? = null

    @SerializedName("insurence_class")
    var insurenceclass: String? = null

    @SerializedName("admin_approve")
    var istatus: String? = null


}